#pragma once
#include "MXCOMM.H"
#include "KY5_Commands_19200.h"
#include "KY5CustomCoffView.h"
#include "KY5FaultyView.h"
#include "KY5MeasurementView.h"
namespace IonizationCameraTool {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� KY5View
	/// </summary>
	public ref class KY5View : public System::Windows::Forms::Form
	{
	public:
		KY5View(void)
		{
			InitializeComponent();

            _KY5CustomCoffView   = gcnew  KY5CustomCoffView();
            _KY5FaultyView       = gcnew  KY5FaultyView();
            _KY5MeasurementView  = gcnew  KY5MeasurementView();

			//
			//TODO: �������� ��� ������������
			//
		}


        void  SetMxCommHandle(MxComm*  MXCOMM)
        {
             _MXCOMM = MXCOMM;
              KY5    = new KY5_Commands_19200(_MXCOMM,100,true);

        }





    void Start_X_Ray(void)
{

           int HV            = 0;
           int PreCurrent    = 0;
           int Current       = 0;
           int Time          = 0;
           int mAs           = 0;
          

    
            if(
              int::TryParse (mAS_Text->Text,mAs)               &&
              int::TryParse(Time_Text->Text,Time)              && 
              int::TryParse(  CurrentText->Text,Current)       && 
              int::TryParse(  Precurent_Text->Text,PreCurrent) &&
              int::TryParse( HV_Text->Text,HV)  )
        {
   
     if(_FastMode)
     {
      KY5->ExpositionTimeIsShort();
	 

     }
     else
     {
      KY5->ExpositionTimeIsLong();
	

     }
    KY5->SetMaxAnodeHV(HV);
    KY5->SetAnodeHV(HV);

	KY5->SetMAS(mAs);
	
    KY5->SetExpositionTime(Time);
	KY5->SetMaxAnodeCurrent(Current);
	if(_BigFocuse) 
	{
		 KY5->IsBigFocus();
		 
		 KY5->SetMaxAnodeBigFocusPreCurrent(PreCurrent);
		 KY5->SetAnodeBigFocusPreCurrent(PreCurrent);
		 KY5->SetMaxAnodeBigFocusCurrent(Current);
		 KY5->SetAnodeBigFocusCurrent(Current);
		 
		 
			

	}
	else
	{
		 KY5->IsSmallFocus();
		
		 KY5->SetMaxAnodeSmallFocusPreCurrent(PreCurrent);
		 KY5->SetAnodeSmallFocusPreCurrent(PreCurrent);
		 KY5->SetMaxAnodeSmallFocusCurrent(Current);
		 KY5->SetAnodeSmallFocusCurrent(Current);
		

	}

	

	 KY5->Snapshot_Preparing();
	 KY5->WaitForPreparingStart(3000, 300);
	 KY5->Snapshot_HV_On();
	 KY5->WaitForSnapshotFinish(8000, 300);
     KY5->Snapshot_HV_Off();


	}
}








	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~KY5View()
		{
             _MXCOMM = NULL;
              KY5    = NULL;

			if (components)
			{
				delete components;
			}
		}

	private:

    KY5CustomCoffView^  _KY5CustomCoffView;
    KY5FaultyView^      _KY5FaultyView;
    KY5MeasurementView^ _KY5MeasurementView;
    MxComm*             _MXCOMM;
    KY5_Commands_19200*  KY5;
    bool                _BigFocuse;
    bool                _FastMode;
    private: System::Windows::Forms::GroupBox^  groupBox1;

    private: System::Windows::Forms::MenuStrip^  menuStrip1;
    private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  �������������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  CoffFast;
    private: System::Windows::Forms::ToolStripMenuItem^  CoffSlow;


    private: System::Windows::Forms::ToolStripMenuItem^  ����������������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  BigFocuse;
    private: System::Windows::Forms::ToolStripMenuItem^  SmallFocuse;


    private: System::Windows::Forms::ToolStripMenuItem^  �����������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  PrographDeviceType;
    private: System::Windows::Forms::ToolStripMenuItem^  FlLATDeviceType;


    private: System::Windows::Forms::ToolStripMenuItem^  ��������������������������������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  FaultyInformation;
private: System::Windows::Forms::ToolStripMenuItem^  MeasurementsInfo;


    private: System::Windows::Forms::Button^  HV_Sart;
    private: System::Windows::Forms::Button^  HV_Stop;


    private: System::Windows::Forms::StatusStrip^  statusStrip1;
    private: System::Windows::Forms::Label^  label3;
    private: System::Windows::Forms::Label^  label2;
    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::Label^  label4;
    private: System::Windows::Forms::TextBox^  Time_Text;

    private: System::Windows::Forms::TextBox^  CurrentText;

    private: System::Windows::Forms::TextBox^  Precurent_Text;

    private: System::Windows::Forms::TextBox^  HV_Text;

    private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel1;
    private: System::Windows::Forms::TextBox^  mAS_Text;
    private: System::Windows::Forms::Label^  mAs;


             /// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            this->mAS_Text = (gcnew System::Windows::Forms::TextBox());
            this->mAs = (gcnew System::Windows::Forms::Label());
            this->Time_Text = (gcnew System::Windows::Forms::TextBox());
            this->CurrentText = (gcnew System::Windows::Forms::TextBox());
            this->Precurent_Text = (gcnew System::Windows::Forms::TextBox());
            this->HV_Text = (gcnew System::Windows::Forms::TextBox());
            this->label4 = (gcnew System::Windows::Forms::Label());
            this->label3 = (gcnew System::Windows::Forms::Label());
            this->label2 = (gcnew System::Windows::Forms::Label());
            this->label1 = (gcnew System::Windows::Forms::Label());
            this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
            this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->�������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->CoffFast = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->CoffSlow = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->����������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->BigFocuse = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->SmallFocuse = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->�����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->PrographDeviceType = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->FlLATDeviceType = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->��������������������������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->FaultyInformation = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->MeasurementsInfo = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->HV_Sart = (gcnew System::Windows::Forms::Button());
            this->HV_Stop = (gcnew System::Windows::Forms::Button());
            this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            this->toolStripStatusLabel1 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->groupBox1->SuspendLayout();
            this->menuStrip1->SuspendLayout();
            this->statusStrip1->SuspendLayout();
            this->SuspendLayout();
            // 
            // groupBox1
            // 
            this->groupBox1->Controls->Add(this->mAS_Text);
            this->groupBox1->Controls->Add(this->mAs);
            this->groupBox1->Controls->Add(this->Time_Text);
            this->groupBox1->Controls->Add(this->CurrentText);
            this->groupBox1->Controls->Add(this->Precurent_Text);
            this->groupBox1->Controls->Add(this->HV_Text);
            this->groupBox1->Controls->Add(this->label4);
            this->groupBox1->Controls->Add(this->label3);
            this->groupBox1->Controls->Add(this->label2);
            this->groupBox1->Controls->Add(this->label1);
            this->groupBox1->Location = System::Drawing::Point(12, 31);
            this->groupBox1->Name = L"groupBox1";
            this->groupBox1->Size = System::Drawing::Size(245, 176);
            this->groupBox1->TabIndex = 0;
            this->groupBox1->TabStop = false;
            this->groupBox1->Text = L"��������� ��������";
            // 
            // mAS_Text
            // 
            this->mAS_Text->Location = System::Drawing::Point(125, 139);
            this->mAS_Text->Name = L"mAS_Text";
            this->mAS_Text->Size = System::Drawing::Size(100, 20);
            this->mAS_Text->TabIndex = 9;
            // 
            // mAs
            // 
            this->mAs->AutoSize = true;
            this->mAs->Location = System::Drawing::Point(87, 139);
            this->mAs->Name = L"mAs";
            this->mAs->Size = System::Drawing::Size(27, 13);
            this->mAs->TabIndex = 8;
            this->mAs->Text = L"mAs";
            // 
            // Time_Text
            // 
            this->Time_Text->Location = System::Drawing::Point(125, 110);
            this->Time_Text->Name = L"Time_Text";
            this->Time_Text->Size = System::Drawing::Size(100, 20);
            this->Time_Text->TabIndex = 7;
            // 
            // CurrentText
            // 
            this->CurrentText->Location = System::Drawing::Point(125, 81);
            this->CurrentText->Name = L"CurrentText";
            this->CurrentText->Size = System::Drawing::Size(100, 20);
            this->CurrentText->TabIndex = 6;
            // 
            // Precurent_Text
            // 
            this->Precurent_Text->Location = System::Drawing::Point(125, 51);
            this->Precurent_Text->Name = L"Precurent_Text";
            this->Precurent_Text->Size = System::Drawing::Size(100, 20);
            this->Precurent_Text->TabIndex = 5;
            // 
            // HV_Text
            // 
            this->HV_Text->Location = System::Drawing::Point(125, 21);
            this->HV_Text->Name = L"HV_Text";
            this->HV_Text->Size = System::Drawing::Size(100, 20);
            this->HV_Text->TabIndex = 4;
            // 
            // label4
            // 
            this->label4->AutoSize = true;
            this->label4->Location = System::Drawing::Point(16, 113);
            this->label4->Name = L"label4";
            this->label4->Size = System::Drawing::Size(103, 13);
            this->label4->TabIndex = 3;
            this->label4->Text = L"����� ����������";
            // 
            // label3
            // 
            this->label3->AutoSize = true;
            this->label3->Location = System::Drawing::Point(16, 57);
            this->label3->Name = L"label3";
            this->label3->Size = System::Drawing::Size(89, 13);
            this->label3->TabIndex = 2;
            this->label3->Text = L"��� ����������";
            // 
            // label2
            // 
            this->label2->AutoSize = true;
            this->label2->Location = System::Drawing::Point(16, 84);
            this->label2->Name = L"label2";
            this->label2->Size = System::Drawing::Size(65, 13);
            this->label2->TabIndex = 1;
            this->label2->Text = L"��� ������";
            // 
            // label1
            // 
            this->label1->AutoSize = true;
            this->label1->Location = System::Drawing::Point(16, 28);
            this->label1->Name = L"label1";
            this->label1->Size = System::Drawing::Size(74, 13);
            this->label1->TabIndex = 0;
            this->label1->Text = L"���������� ";
            // 
            // menuStrip1
            // 
            this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->���������ToolStripMenuItem, 
                this->��������������������������������ToolStripMenuItem});
            this->menuStrip1->Location = System::Drawing::Point(0, 0);
            this->menuStrip1->Name = L"menuStrip1";
            this->menuStrip1->Size = System::Drawing::Size(269, 24);
            this->menuStrip1->TabIndex = 2;
            this->menuStrip1->Text = L"menuStrip1";
            // 
            // ���������ToolStripMenuItem
            // 
            this->���������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->�������������ToolStripMenuItem, 
                this->���������ToolStripMenuItem, this->�����������ToolStripMenuItem});
            this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
            this->���������ToolStripMenuItem->Size = System::Drawing::Size(83, 20);
            this->���������ToolStripMenuItem->Text = L"���������";
            // 
            // �������������ToolStripMenuItem
            // 
            this->�������������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->CoffFast, 
                this->CoffSlow, this->����������������ToolStripMenuItem});
            this->�������������ToolStripMenuItem->Name = L"�������������ToolStripMenuItem";
            this->�������������ToolStripMenuItem->Size = System::Drawing::Size(163, 22);
            this->�������������ToolStripMenuItem->Text = L"�������������";
            // 
            // CoffFast
            // 
            this->CoffFast->CheckOnClick = true;
            this->CoffFast->Name = L"CoffFast";
            this->CoffFast->Size = System::Drawing::Size(176, 22);
            this->CoffFast->Text = L"������";
            this->CoffFast->Click += gcnew System::EventHandler(this, &KY5View::CoffFast_Click);
            // 
            // CoffSlow
            // 
            this->CoffSlow->CheckOnClick = true;
            this->CoffSlow->Name = L"CoffSlow";
            this->CoffSlow->Size = System::Drawing::Size(176, 22);
            this->CoffSlow->Text = L"��������";
            this->CoffSlow->Click += gcnew System::EventHandler(this, &KY5View::CoffSlow_Click);
            // 
            // ����������������ToolStripMenuItem
            // 
            this->����������������ToolStripMenuItem->CheckOnClick = true;
            this->����������������ToolStripMenuItem->Name = L"����������������ToolStripMenuItem";
            this->����������������ToolStripMenuItem->Size = System::Drawing::Size(176, 22);
            this->����������������ToolStripMenuItem->Text = L"����������������";
            // 
            // ���������ToolStripMenuItem
            // 
            this->���������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->BigFocuse, 
                this->SmallFocuse});
            this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
            this->���������ToolStripMenuItem->Size = System::Drawing::Size(163, 22);
            this->���������ToolStripMenuItem->Text = L"��� ������";
            // 
            // BigFocuse
            // 
            this->BigFocuse->CheckOnClick = true;
            this->BigFocuse->Name = L"BigFocuse";
            this->BigFocuse->Size = System::Drawing::Size(126, 22);
            this->BigFocuse->Text = L"�������";
            this->BigFocuse->Click += gcnew System::EventHandler(this, &KY5View::BigFocuse_Click);
            // 
            // SmallFocuse
            // 
            this->SmallFocuse->CheckOnClick = true;
            this->SmallFocuse->Name = L"SmallFocuse";
            this->SmallFocuse->Size = System::Drawing::Size(126, 22);
            this->SmallFocuse->Text = L"�����";
            this->SmallFocuse->Click += gcnew System::EventHandler(this, &KY5View::SmallFocuse_Click);
            // 
            // �����������ToolStripMenuItem
            // 
            this->�����������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->PrographDeviceType, 
                this->FlLATDeviceType});
            this->�����������ToolStripMenuItem->Name = L"�����������ToolStripMenuItem";
            this->�����������ToolStripMenuItem->Size = System::Drawing::Size(163, 22);
            this->�����������ToolStripMenuItem->Text = L"��� ��������";
            // 
            // PrographDeviceType
            // 
            this->PrographDeviceType->Name = L"PrographDeviceType";
            this->PrographDeviceType->Size = System::Drawing::Size(160, 22);
            this->PrographDeviceType->Text = L"�������� 7000";
            this->PrographDeviceType->Click += gcnew System::EventHandler(this, &KY5View::PrographDeviceType_Click);
            // 
            // FlLATDeviceType
            // 
            this->FlLATDeviceType->Name = L"FlLATDeviceType";
            this->FlLATDeviceType->Size = System::Drawing::Size(160, 22);
            this->FlLATDeviceType->Text = L"��� ���/FLAT ";
            this->FlLATDeviceType->Click += gcnew System::EventHandler(this, &KY5View::FlLATDeviceType_Click);
            // 
            // ��������������������������������ToolStripMenuItem
            // 
            this->��������������������������������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->FaultyInformation, 
                this->MeasurementsInfo});
            this->��������������������������������ToolStripMenuItem->Name = L"��������������������������������ToolStripMenuItem";
            this->��������������������������������ToolStripMenuItem->Size = System::Drawing::Size(93, 20);
            this->��������������������������������ToolStripMenuItem->Text = L"����������";
            // 
            // FaultyInformation
            // 
            this->FaultyInformation->Name = L"FaultyInformation";
            this->FaultyInformation->Size = System::Drawing::Size(312, 22);
            this->FaultyInformation->Text = L"��������� ���������� � ��������������";
            this->FaultyInformation->Click += gcnew System::EventHandler(this, &KY5View::FaultyInformation_Click);
            // 
            // MeasurementsInfo
            // 
            this->MeasurementsInfo->Name = L"MeasurementsInfo";
            this->MeasurementsInfo->Size = System::Drawing::Size(312, 22);
            this->MeasurementsInfo->Text = L"��������� ������ ��������";
            this->MeasurementsInfo->Click += gcnew System::EventHandler(this, &KY5View::MeasurementsInfo_Click);
            // 
            // HV_Sart
            // 
            this->HV_Sart->Location = System::Drawing::Point(12, 213);
            this->HV_Sart->Name = L"HV_Sart";
            this->HV_Sart->Size = System::Drawing::Size(114, 23);
            this->HV_Sart->TabIndex = 3;
            this->HV_Sart->Text = L"������ �������";
            this->HV_Sart->UseVisualStyleBackColor = true;
            this->HV_Sart->Click += gcnew System::EventHandler(this, &KY5View::HV_Sart_Click);
            // 
            // HV_Stop
            // 
            this->HV_Stop->Location = System::Drawing::Point(132, 213);
            this->HV_Stop->Name = L"HV_Stop";
            this->HV_Stop->Size = System::Drawing::Size(111, 23);
            this->HV_Stop->TabIndex = 4;
            this->HV_Stop->Text = L" ������ ��������";
            this->HV_Stop->UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->toolStripStatusLabel1});
            this->statusStrip1->Location = System::Drawing::Point(0, 262);
            this->statusStrip1->Name = L"statusStrip1";
            this->statusStrip1->Size = System::Drawing::Size(269, 22);
            this->statusStrip1->TabIndex = 5;
            this->statusStrip1->Text = L"statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this->toolStripStatusLabel1->Name = L"toolStripStatusLabel1";
            this->toolStripStatusLabel1->Size = System::Drawing::Size(118, 17);
            this->toolStripStatusLabel1->Text = L"toolStripStatusLabel1";
            // 
            // KY5View
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
            this->ClientSize = System::Drawing::Size(269, 284);
            this->Controls->Add(this->statusStrip1);
            this->Controls->Add(this->HV_Stop);
            this->Controls->Add(this->HV_Sart);
            this->Controls->Add(this->groupBox1);
            this->Controls->Add(this->menuStrip1);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
            this->MainMenuStrip = this->menuStrip1;
            this->MaximizeBox = false;
            this->Name = L"KY5View";
            this->Text = L"����� ���������� KY5";
            this->groupBox1->ResumeLayout(false);
            this->groupBox1->PerformLayout();
            this->menuStrip1->ResumeLayout(false);
            this->menuStrip1->PerformLayout();
            this->statusStrip1->ResumeLayout(false);
            this->statusStrip1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    private: System::Void CoffFast_Click(System::Object^  sender, System::EventArgs^  e) {
                  if(!CoffFast->Checked)
                  {

                  CoffSlow->Checked = false;
                  _FastMode = true;

                  }
             
             }
private: System::Void CoffSlow_Click(System::Object^  sender, System::EventArgs^  e) {
                  if(!CoffSlow->Checked)
                  {

                  CoffFast->Checked = false;
                  _FastMode = false;

                  }
         }
private: System::Void BigFocuse_Click(System::Object^  sender, System::EventArgs^  e) {
            if(!BigFocuse->Checked)
            {
                 SmallFocuse->Checked = false;
                 _BigFocuse = true;
            }
         }
private: System::Void SmallFocuse_Click(System::Object^  sender, System::EventArgs^  e) {
          
          if(!SmallFocuse->Checked)
            {
                BigFocuse->Checked = false;
                _BigFocuse = false;
            }
         }

private: System::Void PrographDeviceType_Click(System::Object^  sender, System::EventArgs^  e) {
            
            if(!PrographDeviceType->Checked)
            {
                 FlLATDeviceType->Checked = false;
                 KY5->SetDeviceType(1); 
            }
            }

private: System::Void FlLATDeviceType_Click(System::Object^  sender, System::EventArgs^  e) {
          
          
           if(!FlLATDeviceType->Checked)
            {

            PrographDeviceType->Checked = false;
            KY5->SetDeviceType(0x40); 

           }
         }


private: System::Void HV_Sart_Click(System::Object^  sender, System::EventArgs^  e) {
         }

private: System::Void MeasurementsInfo_Click(System::Object^  sender, System::EventArgs^  e) {
         
         if (!_KY5MeasurementView->Visible)
		{
			
		if (_KY5MeasurementView->IsDisposed)
		{
			_KY5MeasurementView =  gcnew KY5MeasurementView();
		}

		  _KY5MeasurementView->Show();
		}

        }

private: System::Void FaultyInformation_Click(System::Object^  sender, System::EventArgs^  e) {
             
      if (!_KY5FaultyView->Visible)
		{
			
		if (_KY5FaultyView->IsDisposed)
		{
			_KY5FaultyView =  gcnew  KY5FaultyView();
		}

		  _KY5FaultyView->Show();
		}
          


         }
};
}
