#pragma once
#ifndef _MAIN_VIEV_
#define _MAIN_VIEV_
#include "FTDIView.h"
#include "IC_Module.h"
#include "KY5View.h"
#include "MXCOMM.H"
#include "OsciloramView.h"
#include "SetupModuleView.h"

namespace IonizationCameraTool {
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public
ref class MainView : public System::Windows::Forms::Form {

private:
  ~MainView();
#pragma region Windows Form Designer generated code
  void InitializeComponent(void);
#pragma endregion
  System::Void KY5_Open_Click(System::Object ^ sender, System::EventArgs ^ e);
  System::Void IC_Setup_Click(System::Object ^ sender, System::EventArgs ^ e);
  System::Void IC_Measurements_Click(System::Object ^ sender,
                                     System::EventArgs ^ e);
  System::Void FTDI_Setup_Click(System::Object ^ sender, System::EventArgs ^ e);
  System::Void FTDI_Connect_Click(System::Object ^ sender,
                                  System::EventArgs ^ e);
  System::Void FTDI_Disconect_Click(System::Object ^ sender,
                                    System::EventArgs ^ e);

  System::Windows::Forms::Label ^ ConectionIndicator;
  System::Windows::Forms::ToolTip ^ InfoToolTip;
  System::Windows::Forms::StatusStrip ^ statusStrip1;
  System::Windows::Forms::ToolStripStatusLabel ^ toolStripDCLevel;
  System::Windows::Forms::Timer ^ StatePolling;
  System::Windows::Forms::GroupBox ^ groupBox1;
  System::Windows::Forms::Label ^ SoftwareVersion;
  System::Windows::Forms::Label ^ HardwareVersion;
  System::Windows::Forms::Label ^ label2;
  System::Windows::Forms::Label ^ label1;
  System::Windows::Forms::MenuStrip ^ menuStrip1;
  System::Windows::Forms::ToolStripMenuItem ^ fTDIToolStripMenuItem;
  System::Windows::Forms::ToolStripMenuItem ^ FTDI_Connection;
  System::Windows::Forms::ToolStripMenuItem ^ FTDI_Connect;
  System::Windows::Forms::ToolStripMenuItem ^ FTDI_Disconect;
  System::Windows::Forms::ToolStripMenuItem ^ FTDI_Setup;
  System::Windows::Forms::ToolStripMenuItem ^ kY5ToolStripMenuItem;
  System::Windows::Forms::ToolStripMenuItem ^ KY5_Open;
  System::Windows::Forms::ToolStripMenuItem ^ IC_Setup;
  System::Windows::Forms::ToolStripMenuItem ^ IC_Measurements;
  System::ComponentModel::IContainer ^ components;
  UINT nSpeed;
  UINT nDataBits;
  UINT nParity;
  UINT nStopBits;

public:
  MainView(void);
  bool isConected;
  std::shared_ptr<IC_Module> *_IC_Module;
  std::shared_ptr<MxComm> *_MXCOMM;
  KY5View ^ _KY5;
  FTDIView ^ _FTDI;
  OsciloramView ^ _Oscilogram;
  SetupModuleView ^ _ModuleSetup;
  DWORD SelectedDevice;

  void SetParametrsFTDI(UINT Speed, UINT DataBits, UINT StopBitS);
  void GetParametrsFTDI(UINT Speed, UINT DataBits, UINT StopBitS);
  void InitFTDIDefault(void);
};
} // namespace IonizationCameraTool

#endif
