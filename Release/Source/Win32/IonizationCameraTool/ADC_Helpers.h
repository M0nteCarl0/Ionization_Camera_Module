#include <stdint.h>

uint8_t GetSampleRateValueRaw(int ID);
int GetIndexBySampleRate(uint8_t Rate);
uint8_t GetypeFilter(int ID);
int GetIndexFilter(uint8_t ID);
uint8_t GetSampleRate50HertzRejectiom(int ID);
int GetIndexBytSampleRate50HertzRejectiom(uint8_t ID);
