#pragma once

namespace IonizationCameraTool {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� KY5FaultyView
	/// </summary>
	public ref class KY5FaultyView : public System::Windows::Forms::Form
	{
	public:
		KY5FaultyView(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~KY5FaultyView()
		{
			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::GroupBox^  groupBox1;
    protected: 
    private: System::Windows::Forms::CheckBox^  checkBox4;
    private: System::Windows::Forms::CheckBox^  checkBox3;
    private: System::Windows::Forms::CheckBox^  checkBox2;
    private: System::Windows::Forms::CheckBox^  checkBox1;
    private: System::Windows::Forms::CheckBox^  checkBox7;
    private: System::Windows::Forms::CheckBox^  checkBox6;
    private: System::Windows::Forms::CheckBox^  checkBox5;
    private: System::Windows::Forms::CheckBox^  checkBox9;
    private: System::Windows::Forms::CheckBox^  checkBox8;

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
            this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
            this->checkBox3 = (gcnew System::Windows::Forms::CheckBox());
            this->checkBox4 = (gcnew System::Windows::Forms::CheckBox());
            this->checkBox5 = (gcnew System::Windows::Forms::CheckBox());
            this->checkBox6 = (gcnew System::Windows::Forms::CheckBox());
            this->checkBox7 = (gcnew System::Windows::Forms::CheckBox());
            this->checkBox8 = (gcnew System::Windows::Forms::CheckBox());
            this->checkBox9 = (gcnew System::Windows::Forms::CheckBox());
            this->groupBox1->SuspendLayout();
            this->SuspendLayout();
            // 
            // groupBox1
            // 
            this->groupBox1->Controls->Add(this->checkBox9);
            this->groupBox1->Controls->Add(this->checkBox8);
            this->groupBox1->Controls->Add(this->checkBox7);
            this->groupBox1->Controls->Add(this->checkBox6);
            this->groupBox1->Controls->Add(this->checkBox5);
            this->groupBox1->Controls->Add(this->checkBox4);
            this->groupBox1->Controls->Add(this->checkBox3);
            this->groupBox1->Controls->Add(this->checkBox2);
            this->groupBox1->Controls->Add(this->checkBox1);
            this->groupBox1->Location = System::Drawing::Point(12, 12);
            this->groupBox1->Name = L"groupBox1";
            this->groupBox1->Size = System::Drawing::Size(243, 279);
            this->groupBox1->TabIndex = 0;
            this->groupBox1->TabStop = false;
            this->groupBox1->Text = L"�������������";
            // 
            // checkBox1
            // 
            this->checkBox1->AutoSize = true;
            this->checkBox1->Location = System::Drawing::Point(6, 32);
            this->checkBox1->Name = L"checkBox1";
            this->checkBox1->Size = System::Drawing::Size(89, 17);
            this->checkBox1->TabIndex = 0;
            this->checkBox1->Text = L"��� �������";
            this->checkBox1->UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this->checkBox2->AutoSize = true;
            this->checkBox2->Location = System::Drawing::Point(6, 55);
            this->checkBox2->Name = L"checkBox2";
            this->checkBox2->Size = System::Drawing::Size(84, 17);
            this->checkBox2->TabIndex = 1;
            this->checkBox2->Text = L"��� ������";
            this->checkBox2->UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this->checkBox3->AutoSize = true;
            this->checkBox3->Location = System::Drawing::Point(6, 88);
            this->checkBox3->Name = L"checkBox3";
            this->checkBox3->Size = System::Drawing::Size(89, 17);
            this->checkBox3->TabIndex = 2;
            this->checkBox3->Text = L"������ ���";
            this->checkBox3->UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this->checkBox4->AutoSize = true;
            this->checkBox4->Location = System::Drawing::Point(6, 111);
            this->checkBox4->Name = L"checkBox4";
            this->checkBox4->Size = System::Drawing::Size(75, 17);
            this->checkBox4->TabIndex = 3;
            this->checkBox4->Text = L"��������";
            this->checkBox4->UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this->checkBox5->AutoSize = true;
            this->checkBox5->Location = System::Drawing::Point(6, 142);
            this->checkBox5->Name = L"checkBox5";
            this->checkBox5->Size = System::Drawing::Size(116, 17);
            this->checkBox5->TabIndex = 4;
            this->checkBox5->Text = L"���������� ���";
            this->checkBox5->UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this->checkBox6->AutoSize = true;
            this->checkBox6->Location = System::Drawing::Point(6, 165);
            this->checkBox6->Name = L"checkBox6";
            this->checkBox6->Size = System::Drawing::Size(139, 17);
            this->checkBox6->TabIndex = 5;
            this->checkBox6->Text = L"���������� �������";
            this->checkBox6->UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this->checkBox7->AutoSize = true;
            this->checkBox7->Location = System::Drawing::Point(6, 188);
            this->checkBox7->Name = L"checkBox7";
            this->checkBox7->Size = System::Drawing::Size(207, 17);
            this->checkBox7->TabIndex = 6;
            this->checkBox7->Text = L"���������� �������� ����������";
            this->checkBox7->UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this->checkBox8->AutoSize = true;
            this->checkBox8->Location = System::Drawing::Point(6, 211);
            this->checkBox8->Name = L"checkBox8";
            this->checkBox8->Size = System::Drawing::Size(181, 17);
            this->checkBox8->TabIndex = 7;
            this->checkBox8->Text = L"���������� ���� ����������";
            this->checkBox8->UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this->checkBox9->AutoSize = true;
            this->checkBox9->Location = System::Drawing::Point(6, 234);
            this->checkBox9->Name = L"checkBox9";
            this->checkBox9->Size = System::Drawing::Size(157, 17);
            this->checkBox9->TabIndex = 8;
            this->checkBox9->Text = L"���������� ���� ������";
            this->checkBox9->UseVisualStyleBackColor = true;
            // 
            // KY5FaultyView
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
            this->ClientSize = System::Drawing::Size(264, 295);
            this->Controls->Add(this->groupBox1);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
            this->Name = L"KY5FaultyView";
            this->Text = L"KY5FaultyView";
            this->groupBox1->ResumeLayout(false);
            this->groupBox1->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
	};
}
