//
//  autor: Vitaly Kalendarev, kalendarev@roentgenprom.ru
//
//  date: 21.10.2014
//
//#include "stdafx.h"

#include <string.h>

#include "FTDI_ActionInfo.h"

//========================================================================
//     SimpleActionInfo       SimpleActionInfo      SimpleActionInfo
//========================================================================

SimpleActionInfo::SimpleActionInfo()
 :IsSuccess(true),
  NumberOfWrittenBytes(0),
  NumberOfReadBytes(0),
  WrittenBytes(nullptr),
  ReadBytes(nullptr),
  ActionDuration(0)
{

}

SimpleActionInfo::SimpleActionInfo(
			const bool IncomingSuccess,
			const int IncomingNumberOfWrittenBytes,
			const int IncomingNumberOfReadBytes,
			const BYTE * const IncomingWrittenBytes,
			const BYTE * const IncomingReadBytes,
			const float IncomingActionDuration)
{
	IsSuccess = IncomingSuccess;
	NumberOfWrittenBytes = IncomingNumberOfWrittenBytes;
	NumberOfReadBytes = IncomingNumberOfReadBytes;

	if(IncomingNumberOfWrittenBytes > 0 && IncomingWrittenBytes != nullptr)
	{
		WrittenBytes = new BYTE[IncomingNumberOfWrittenBytes];
		memcpy(WrittenBytes,IncomingWrittenBytes,IncomingNumberOfWrittenBytes*sizeof(BYTE));
	}
	else
		WrittenBytes = nullptr;

	if(IncomingNumberOfReadBytes > 0 && IncomingReadBytes != nullptr)
	{
		ReadBytes = new BYTE[IncomingNumberOfReadBytes];
		memcpy(ReadBytes,IncomingReadBytes,IncomingNumberOfReadBytes*sizeof(BYTE));
	}
	else
		ReadBytes = nullptr;

	ActionDuration = IncomingActionDuration;
}

SimpleActionInfo::~SimpleActionInfo()
{
	Clear();
}

SimpleActionInfo::SimpleActionInfo(const SimpleActionInfo & rhs)
{
	Copy(rhs);
}

SimpleActionInfo SimpleActionInfo::operator=(const SimpleActionInfo & rhs)
{
	if(&rhs == this)
		return *this;

	Clear();
	Copy(rhs);

	return *this;
}

void SimpleActionInfo::Clear()
{
	delete [] WrittenBytes;
	WrittenBytes = nullptr;

	delete [] ReadBytes;
	ReadBytes = nullptr;
}

void SimpleActionInfo::Copy(const SimpleActionInfo & rhs)
{
	IsSuccess = rhs.IsSuccess;
	NumberOfWrittenBytes = rhs.NumberOfWrittenBytes;
	NumberOfReadBytes = rhs.NumberOfReadBytes;
	
	if(NumberOfWrittenBytes != 0 && rhs.WrittenBytes != nullptr)
	{
		WrittenBytes = new BYTE[NumberOfWrittenBytes];
		memcpy(WrittenBytes,rhs.WrittenBytes,NumberOfWrittenBytes*sizeof(BYTE));
	}
	else
		WrittenBytes = nullptr;

	if(NumberOfReadBytes != 0 && rhs.ReadBytes != nullptr)
	{
		ReadBytes = new BYTE[NumberOfReadBytes];
		memcpy(ReadBytes,rhs.ReadBytes,NumberOfReadBytes*sizeof(BYTE));
	}
	else
		ReadBytes = nullptr;

	ActionDuration = rhs.ActionDuration;
}

//========================================================================
//     FTDI_ActionInfo         FTDI_ActionInfo       FTDI_ActionInfo
//========================================================================

FTDI_ActionInfo::FTDI_ActionInfo()
{
	ByDefault();
}

FTDI_ActionInfo::FTDI_ActionInfo(
	const bool IncomingSuccess,
	const int IncomingNumberOfWrittenBytes,
	const int IncomingNumberOfReadBytes,
	const BYTE * const IncomingWrittenBytes,
	const BYTE * const IncomingReadBytes,
	const float IncomingActionDuration)
{
	IsSuccess = IncomingSuccess;
	NumberOfActions = 1;
	

     Actions = NULL;
	 Actions = new SimpleActionInfo[1];
   
	 Actions[0] = SimpleActionInfo(IncomingSuccess,
								  IncomingNumberOfWrittenBytes,
								  IncomingNumberOfReadBytes,
								  IncomingWrittenBytes,
								  IncomingReadBytes,
								  IncomingActionDuration);
}

FTDI_ActionInfo::FTDI_ActionInfo(const bool IncomingSuccess)
{
	ByDefault();
	IsSuccess = IncomingSuccess;
}

FTDI_ActionInfo::FTDI_ActionInfo(const FTDI_ActionInfo & rhs)
{
	Copy(rhs);
}

FTDI_ActionInfo::~FTDI_ActionInfo()
{
	Clear();
}

FTDI_ActionInfo FTDI_ActionInfo::operator=(const FTDI_ActionInfo & rhs)
{
	if(&rhs == this)
		return *this;

	Clear();
	Copy(rhs);

	return *this;
}

FTDI_ActionInfo FTDI_ActionInfo::operator+(const FTDI_ActionInfo & rhs) const
{
	FTDI_ActionInfo Result;

	Result.IsSuccess = IsSuccess && rhs.IsSuccess;
	Result.NumberOfActions = NumberOfActions + rhs.NumberOfActions;

	if(Result.NumberOfActions > 0)
		Result.Actions = new SimpleActionInfo[Result.NumberOfActions];
	else
		return Result;

	if(Actions != nullptr)
		for(int i=0;i<NumberOfActions;i++)
			Result.Actions[i] = Actions[i];
	
	if(rhs.Actions != nullptr)
		for(int i=0;i<rhs.NumberOfActions;i++)
			Result.Actions[NumberOfActions + i] = rhs.Actions[i];

	return Result;
}

FTDI_ActionInfo FTDI_ActionInfo::operator+(const bool & rhs) const
{
	return *this + FTDI_ActionInfo(rhs);
}

FTDI_ActionInfo FTDI_ActionInfo::operator+=(const FTDI_ActionInfo & rhs)
{
	return *this = *this + rhs;
}

void FTDI_ActionInfo::Copy(const FTDI_ActionInfo & rhs)
{
	IsSuccess = rhs.IsSuccess;
	NumberOfActions = rhs.NumberOfActions;

	if(rhs.Actions != nullptr && NumberOfActions > 0)
	{
		Actions = new SimpleActionInfo[NumberOfActions];

		for(int i=0;i<NumberOfActions;i++)
			Actions[i] = rhs.Actions[i];
	}
	else
		Actions = nullptr;
}

void FTDI_ActionInfo::Clear()
{
	
    delete [] Actions;

	ByDefault();
}

void FTDI_ActionInfo::ByDefault()
{
	IsSuccess = true;
	NumberOfActions = 0;
	Actions = nullptr;
}

bool operator==(const FTDI_ActionInfo & lhs,const FTDI_ActionInfo & rhs)
{
	return lhs.GetSuccess() == rhs.GetSuccess();
}

bool operator==(const bool & lhs,const FTDI_ActionInfo & rhs)
{
	return lhs == rhs.GetSuccess();
}

bool operator==(const FTDI_ActionInfo & lhs, const bool & rhs)
{
	return lhs.GetSuccess() == rhs;
}
