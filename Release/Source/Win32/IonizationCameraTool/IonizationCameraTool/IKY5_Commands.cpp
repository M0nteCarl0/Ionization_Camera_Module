//
//  autor: Vitaly Kalendarev, kalendarev@roentgenprom.ru
//
//  date: 21.10.2014
//
#include "stdafx.h"

#include "IKY5_Commands.h"
#include "BitOperations.h"

IKY5_Commands::IKY5_Commands(MxComm * const Comm,const int MultipleModeTimeoutToSet,const bool PrintFlag)
 :DeviceType(0x00),
  FocusMode(0x00),
  ExpositionTimeMode(KY5_EXPOSITION_TIME_IS_LONG),
  Blockings_1(0x00),
  Blockings_2(0x00),
  Blockings_3(0x00),
  Messages(0x00),
  Warnings(0x00),
  GivenAnodeHV(0),
  GivenAnodeCurrent(0),
  GivenAnodeSmallFocusPreCurrent(0),
  GivenAnodeSmallFocusCurrent(0),
  GivenAnodeBigFocusPreCurrent(0),
  GivenAnodeBigFocusCurrent(0),
  GivenMAS(0),
  GivenExpositionTime(0),
  GivenMaxAnodeCurrent(0),
  GivenMaxAnodeHV(0),
  GivenMaxAnodeSmallFocusPreCurrent(0),
  GivenMaxAnodeSmallFocusCurrent(0),
  GivenMaxAnodeBigFocusPreCurrent(0),
  GivenMaxAnodeBigFocusCurrent(0),
  MeasuredAnodeHV(0),
  MeasuredAnodePreCurrent(0),
  MeasuredPositiveAnodeCurrent(0),
  MeasuredNegativeAnodeCurrent(0),
  MeasuredAnodeCurrentDuringSnapshot(0),
  MeasuredMAS(0),
  MeasuredExpositionTime(0),
  ExpositionTimeCoefficient(KY5_EXPOSITION_TIME_COEFFICIENT),
  MeasuredAnodeCurrentCoefficient(1),
  MASCoefficient(KY5_MAS_COEFFICIENT),
  SingleModeStartTime(0),
  MultipleModeStartTime(0),
  SnapshotFinishTime(0),
  MinWhileBetweenSnapshots(MultipleModeTimeoutToSet)
{
	SetComm(Comm);
	m_FlgPrint = PrintFlag;
}

FTDI_ActionInfo IKY5_Commands::SetDeviceType(const BYTE DeviceTypeToSet) const
{
	return WriteReg(KY5_DEVICE_TYPE_ADDRESS,DeviceTypeToSet);
}

FTDI_ActionInfo IKY5_Commands::FocusIsSmall() const
{
	return WriteReg(KY5_FOCUS_ADDRESS,KY5_FOCUS_IS_SMALL);
}

FTDI_ActionInfo IKY5_Commands::FocusIsBig() const
{
	return WriteReg(KY5_FOCUS_ADDRESS,KY5_FOCUS_IS_BIG);
}

FTDI_ActionInfo IKY5_Commands::ExpositionTimeIsShort()
{
	MeasuredAnodeCurrentCoefficient = 1;
	ExpositionTimeCoefficient = KY5_EXPOSITION_TIME_COEFFICIENT;

	return WriteReg(KY5_HV_EXPOSITION_TIME_SCALE_ADDRESS,KY5_EXPOSITION_TIME_IS_SHORT);
}

FTDI_ActionInfo IKY5_Commands::ExpositionTimeIsLong()
{
	MeasuredAnodeCurrentCoefficient = KY5_MEASURED_ANODE_CURRENT_COEFFICIENT_FOR_LONG_EXPOSITION_TIME_MODE;
	ExpositionTimeCoefficient = 1;

	return WriteReg(KY5_HV_EXPOSITION_TIME_SCALE_ADDRESS,KY5_EXPOSITION_TIME_IS_LONG);
}

FTDI_ActionInfo IKY5_Commands::SetAnodeHV(const int AnodeHVToSet) const
{
	return WriteValue37(AnodeHVToSet * KY5_ANODE_HV_COEFFICIENT,KY5_GIVEN_ANODE_VOLTAGE_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_VOLTAGE_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo IKY5_Commands::SetAnodeCurrent(const int AnodeCurrentToSet) const
{
	return WriteValue37(AnodeCurrentToSet,KY5_GIVEN_ANODE_CURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_CURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo IKY5_Commands::SetAnodeSmallFocusPreCurrent(const int AnodeSmallFocusPreCurrentToSet) const
{
	return WriteValue37(AnodeSmallFocusPreCurrentToSet,KY5_GIVEN_ANODE_SMALL_FOCUS_PRECURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_SMALL_FOCUS_PRECURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo IKY5_Commands::SetAnodeSmallFocusCurrent(const int AnodeSmallFocusCurrentToSet) const
{
	return WriteValue37(AnodeSmallFocusCurrentToSet,KY5_GIVEN_ANODE_SMALL_FOCUS_CURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_SMALL_FOCUS_CURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo IKY5_Commands::SetAnodeBigFocusPreCurrent(const int AnodeBigFocusPreCurrentToSet) const
{
	return WriteValue37(AnodeBigFocusPreCurrentToSet,KY5_GIVEN_ANODE_BIG_FOCUS_PRECURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_BIG_FOCUS_PRECURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo IKY5_Commands::SetAnodeBigFocusCurrent(const int AnodeBigFocusCurrentToSet) const
{
	return WriteValue37(AnodeBigFocusCurrentToSet,KY5_GIVEN_ANODE_BIG_FOCUS_CURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_BIG_FOCUS_CURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo IKY5_Commands::SetMAS(const float MASToSet) const
{
	return WriteValue77((int)(MASToSet * MASCoefficient),KY5_GIVEN_MAS_7_LO_BITS_ADDRESS,KY5_GIVEN_MAS_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo IKY5_Commands::SetExpositionTime(const int ExpositionTimeToSet) const
{
	return WriteValue77((int)(ExpositionTimeToSet * ExpositionTimeCoefficient),KY5_GIVEN_EXPOSITION_TIME_7_LO_BITS_ADDRESS,KY5_GIVEN_EXPOSITION_TIME_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo IKY5_Commands::SetHWType(byte HWType)
{
	return  WriteReg(0x98, HWType);
}

FTDI_ActionInfo IKY5_Commands::SetMaxAnodeCurrent(const BYTE MaxAnodeCurrentToSet) const
{
	return WriteReg(KY5_MAX_ANODE_CURRENT_ADDRESS,MaxAnodeCurrentToSet);
}

FTDI_ActionInfo IKY5_Commands::SetMaxAnodeHV(const BYTE MaxAnodeHVToSet) const
{
	return WriteReg(KY5_MAX_ANODE_VOLTAGE_ADDRESS,MaxAnodeHVToSet);
}

FTDI_ActionInfo IKY5_Commands::SetMaxAnodeSmallFocusPreCurrent(const BYTE MaxAnodeSmallFocusPreCurrentToSet) const
{
	return WriteReg(KY5_MAX_ANODE_SMALL_FOCUS_PRECURRENT_ADDRESS,MaxAnodeSmallFocusPreCurrentToSet);
}

FTDI_ActionInfo IKY5_Commands::SetMaxAnodeSmallFocusCurrent(const BYTE MaxAnodeSmallFocusCurrentToSet) const
{
	return WriteReg(KY5_MAX_ANODE_SMALL_FOCUS_CURRENT_ADDRESS,MaxAnodeSmallFocusCurrentToSet);
}

FTDI_ActionInfo IKY5_Commands::SetMaxAnodeBigFocusPreCurrent(const BYTE MaxAnodeBigFocusPreCurrentToSet) const
{
	return WriteReg(KY5_MAX_ANODE_BIG_FOCUS_PRECURRENT_ADDRESS,MaxAnodeBigFocusPreCurrentToSet);
}

FTDI_ActionInfo IKY5_Commands::SetMaxAnodeBigFocusCurrent(const BYTE MaxAnodeBigFocusCurrentToSet) const
{
	return WriteReg(KY5_MAX_ANODE_BIG_FOCUS_CURRENT_ADDRESS,MaxAnodeBigFocusCurrentToSet);
}

FTDI_ActionInfo IKY5_Commands::Snapshot_Preparing() const
{
	return WriteReg(KY5_SNAPSHOT_ADDRESS,KY5_SNAPSHOT_PREPARING);
}

FTDI_ActionInfo IKY5_Commands::Snapshot_HV_On() const
{
	return WriteReg(KY5_SNAPSHOT_ADDRESS,KY5_SNAPSHOT_HV_ON);
}

FTDI_ActionInfo IKY5_Commands::Snapshot_HV_On_InMultipleMode() const
{
	return WriteReg(KY5_SNAPSHOT_ADDRESS,KY5_SNAPSHOT_HV_ON_IN_MULTIPLE_MODE);
}

FTDI_ActionInfo IKY5_Commands::Snapshot_HV_Off() const
{
	return WriteReg(KY5_SNAPSHOT_ADDRESS,KY5_SNAPSHOT_HV_OFF);
}

FTDI_ActionInfo IKY5_Commands::GetHWType(byte * HW)
{
	return ReadRegDat(0x98, HW);
}

 FTDI_ActionInfo IKY5_Commands::ReadRegisters()
{
	EnterCriticalSection(&CrSection);

		const FTDI_ActionInfo SendRequestInfo = WriteReg(KY5_READ_STATE_ADDRESS,KY5_READ_STATE_COMMAND);

		BYTE ReadBuffer[KY5_READ_DATA_SIZE];
		memset(ReadBuffer,'\0',KY5_READ_DATA_SIZE * sizeof(BYTE));

		const FTDI_ActionInfo ReceiveAnswerInfo = Receive(ReadBuffer,KY5_READ_DATA_SIZE);

	LeaveCriticalSection(&CrSection);

	DeviceType = ReadBuffer[9];
	
	FocusMode = ReadBuffer[7];
	
	Blockings_1 = ReadBuffer[59];
	Blockings_2 = ReadBuffer[61];
	Blockings_3 = ReadBuffer[63];
	Messages    = ReadBuffer[65];
	Warnings    = ReadBuffer[67];

	GivenAnodeHV                   = JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[17],ReadBuffer[19]) / KY5_ANODE_HV_COEFFICIENT;
	GivenAnodeCurrent              = JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[21],ReadBuffer[23]);
	GivenAnodeSmallFocusPreCurrent = JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[25],ReadBuffer[27]);
	GivenAnodeSmallFocusCurrent    = JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[29],ReadBuffer[31]);
	GivenAnodeBigFocusPreCurrent   = JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[33],ReadBuffer[35]);
	GivenAnodeBigFocusCurrent      = JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[37],ReadBuffer[39]);

	GivenMAS            = (float)JOIN_7_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[41],ReadBuffer[43]) / MASCoefficient;
	GivenExpositionTime = (int)(JOIN_7_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[11],ReadBuffer[13]) / ExpositionTimeCoefficient);

	GivenMaxAnodeCurrent              = GET_7_LO_ORDER_BITS(ReadBuffer[45]);
	GivenMaxAnodeHV                   = GET_7_LO_ORDER_BITS(ReadBuffer[47]);
	GivenMaxAnodeSmallFocusPreCurrent = GET_7_LO_ORDER_BITS(ReadBuffer[49]);
	GivenMaxAnodeSmallFocusCurrent    = GET_7_LO_ORDER_BITS(ReadBuffer[51]);
	GivenMaxAnodeBigFocusPreCurrent   = GET_7_LO_ORDER_BITS(ReadBuffer[53]);
	GivenMaxAnodeBigFocusCurrent      = GET_7_LO_ORDER_BITS(ReadBuffer[55]);

	MeasuredAnodeHV                    = JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[85],ReadBuffer[87]) / KY5_ANODE_HV_COEFFICIENT;
	MeasuredPositiveAnodeCurrent       = (int)(JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[89],ReadBuffer[91]) / KY5_MEASURED_ANODE_CURRENT_HIDDEN_COEFFICIENT * MeasuredAnodeCurrentCoefficient);
	MeasuredNegativeAnodeCurrent       = (int)(JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[93],ReadBuffer[95]) / KY5_MEASURED_ANODE_CURRENT_HIDDEN_COEFFICIENT * MeasuredAnodeCurrentCoefficient);
	MeasuredAnodePreCurrent            = JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[101],ReadBuffer[103]);
	MeasuredAnodeCurrentDuringSnapshot = JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[105],ReadBuffer[107]);
	
	MeasuredMAS            = (float)JOIN_7_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[97],ReadBuffer[99]) / MASCoefficient;
	MeasuredExpositionTime = (int)(JOIN_7_LO_ORDER_AND_7_HI_ORDER_BITS(ReadBuffer[69],ReadBuffer[71]) / ExpositionTimeCoefficient);

	// check received bytes


	return SendRequestInfo + ReceiveAnswerInfo;
}

bool IKY5_Commands::WaitNeededSnapshotState(const BYTE NeededSnapshotState,const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime)
{
	bool Success = false;
    ForceInterpt = false;
	const clock_t T0 = clock();
	clock_t T1 = T0;

	do
	{
        if(ForceInterpt)
        {
            break;
        }
        

		if(WaitingIsProhibitedFlag == TRUE)
			break;

		const FTDI_ActionInfo ReadRegistersInfo = ReadRegisters();

		T1 = clock();

		if(ReadRegistersInfo == false)
			break;

		if(Warnings == NeededSnapshotState)
		{
			Success = true;
			break;
		}

		if(DelayBetweenRequests > 0)
			Sleep(DelayBetweenRequests);
	}
	while(T1 - T0 < MaxWaitingTime);

	if(WaitingTime != nullptr)
		*WaitingTime = T1 - T0;

	return Success;
}

bool IKY5_Commands::WaitForPreparingStart(const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime)
{
	return WaitNeededSnapshotState(KY5_WARNINGS_PREPARING_START,MaxWaitingTime,DelayBetweenRequests,WaitingTime);
}

bool IKY5_Commands::WaitForSnapshotStart(const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime)
{
	const bool Success = WaitNeededSnapshotState(KY5_WARNINGS_SNAPSHOT_START,MaxWaitingTime,DelayBetweenRequests,WaitingTime);

	SingleModeStartTime = clock();

	return Success;
}

bool IKY5_Commands::WaitForMultipleModeSnapshotStart(const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime)
{
	const bool Success = WaitNeededSnapshotState(KY5_WARNINGS_MULTIPLE_MODE_SNAPSHOT_START,MaxWaitingTime,DelayBetweenRequests,WaitingTime);

	MultipleModeStartTime = clock();

	return Success;
}

bool IKY5_Commands::WaitForSnapshotFinish(const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime)
{
	const bool Success = WaitNeededSnapshotState(KY5_WARNINGS_SNAPSHOT_FINISH,MaxWaitingTime,DelayBetweenRequests,WaitingTime);

	SnapshotFinishTime = clock();

	return Success;
}

 void  IKY5_Commands:: InteruptWaitHV(void)
 {

 ForceInterpt = true;

 }

 bool   IKY5_Commands::GetInteruptWaitHV(void)
 {
 return  ForceInterpt;
 }