//
//  autor: Vitaly Kalendarev, kalendarev@roentgenprom.ru
//
//  date: 21.10.2014
//
//#include "stdafx.h"

#ifndef KY5_COMMANDS_19200_H
#define KY5_COMMANDS_19200_H

#include "IKY5_Commands.h"

	// class KY5_Commands_19200
class KY5_Commands_19200 : public IKY5_Commands
{
public:
	KY5_Commands_19200(MxComm * const Comm = nullptr,const int MultipleModeTimeoutToSet = KY5_SNAPSHOT_MULTIPLE_MODE_TIMEOUT_BY_DEFAULT,const bool PrintFlag = false);

public:
	bool AsyncWaitPrepareStart(void);
	bool AsyncWaitPrepareFinish(void);
	bool WaitAsyncAction(int Time);
	bool HVActionAsync(void);
	bool HVActionAsyncEx(void);
	FTDI_ActionInfo GetDeviceType(BYTE * const DeviceType) const;
	FTDI_ActionInfo GetURPVersion(BYTE * const URPVersion) const;
		
	FTDI_ActionInfo GetFocusState(BYTE * const State) const;
		
	FTDI_ActionInfo GetAnodeHV(int * const AnodeHV) const;
	FTDI_ActionInfo GetAnodeCurrent(int * const AnodeCurrent) const;
	FTDI_ActionInfo GetAnodeSmallFocusPreCurrent(int * const AnodeSmallFocusPreCurrent) const;
	FTDI_ActionInfo GetAnodeSmallFocusCurrent(int * const AnodeSmallFocusCurrent) const;
	FTDI_ActionInfo GetAnodeBigFocusPreCurrent(int * const AnodeBigFocusPreCurrent) const;
	FTDI_ActionInfo GetAnodeBigFocusCurrent(int * const AnodeBigFocusCurrent) const;
		
	FTDI_ActionInfo GetMAS(float * const MAS) const;
	FTDI_ActionInfo GetExpositionTime(int * const ExpositionTime) const;

	FTDI_ActionInfo GetMaxAnodeCurrent(BYTE * const MaxAnodeCurrent) const;
	FTDI_ActionInfo GetMaxAnodeHV(BYTE * const MaxAnodeHV) const;
	FTDI_ActionInfo GetMaxAnodeSmallFocusPreCurrent(BYTE * const MaxAnodeSmallFocusPreCurrent) const;
	FTDI_ActionInfo GetMaxAnodeSmallFocusCurrent(BYTE * const MaxAnodeSmallFocusCurrent) const;
	FTDI_ActionInfo GetMaxAnodeBigFocusPreCurrent(BYTE * const MaxAnodeBigFocusPreCurrent) const;
	FTDI_ActionInfo GetMaxAnodeBigFocusCurrent(BYTE * const MaxAnodeBigFocusCurrent) const;

	FTDI_ActionInfo GetMeasuredAnodeHV(int * const MeasuredAnodeHV) const;
	FTDI_ActionInfo GetMeasuredAnodePreCurrent(int * const MeasuredAnodePreCurrent) const;
	FTDI_ActionInfo GetMeasuredPositiveAnodeCurrent(int * const MeasuredPositiveAnodeCurrent) const;
	FTDI_ActionInfo GetMeasuredNegativeAnodeCurrent(int * const MeasuredNegativeAnodeCurrent) const;
	FTDI_ActionInfo GetMeasuredAnodeCurrentDuringSnapshot(int * const MeasuredAnodeCurrentDuringSnapshot) const;
	FTDI_ActionInfo GetMeasuredMAS(float * const MeasuredMAS) const;
	FTDI_ActionInfo GetMeasuredExpositionTime(int * const MeasuredExpositionTime) const;

	FTDI_ActionInfo GetBlockings1(BYTE * const Blockings1) const;
	FTDI_ActionInfo GetBlockings2(BYTE * const Blockings2) const;
	FTDI_ActionInfo GetBlockings3(BYTE * const Blockings3) const;
	FTDI_ActionInfo GetMessages(BYTE * const Messages) const;
	FTDI_ActionInfo GetWarnings(BYTE * const Warnings) const;

	virtual FTDI_ActionInfo ReadRegisters() override;

	virtual bool WaitNeededSnapshotState(const BYTE NeededSnapshotState,const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime = nullptr) override;
};

#endif