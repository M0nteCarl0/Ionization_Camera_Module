#pragma once

#include "IC_Module.h"
#include <stdio.h>
#include <msclr\marshal_cppstd.h>
#include <string.h>
namespace IonizationCameraTool {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
    using namespace msclr;

	/// <summary>
	/// ������ ��� OsciloramView
	/// </summary>
	public ref class OsciloramView : public System::Windows::Forms::Form
	{
	public:
		OsciloramView(void)
		{
			InitializeComponent();
            _IC_Module = NULL;
			//
			//TODO: �������� ��� ������������
			//
		}

        void  SetICModuleHandle(IC_Module* IC_Module)
       {
         _IC_Module = IC_Module;
       }
       

       void UptdateMeasurements(void)
       {

      OpearionIndicator->BackColor = Color::Red;
       uint8_t     _Control = 0;
       uint8_t     _State = 0;
       uint32_t    _ThersholdTrigger = 0;
       uint16_t    _ValueHighVoltage = 0;

      
       uint64_t    _GlobalDosa = 0;
       uint64_t    _LocalDosa = 0;
       uint8_t     _CounterEventsTrigger   =0;
       uint16_t    _DurabilityExposition   =0;
       uint32_t    _AvergageAmplitudeMeasure=0;
       uint16_t    _QuanityMeasuredAmplitudes=0;
       
       uint32_t   _ArrayOfAmplitudes[1000];
      TableAmplitudes->Rows->Clear();
	  AmplitudeGraph->Series["AmplitidesPoints"]->Points->Clear(); 
     

    
         //memset(_ArrayOfAmplitudes,0,sizeof(uint32_t) * _QuanityMeasuredAmplitudes);
     
    
 
        _IC_Module->ReadParams(  &_Control, & _State,  &_ThersholdTrigger,
                                  &_GlobalDosa,  &_LocalDosa,  &_CounterEventsTrigger,
                                  &_DurabilityExposition, &_AvergageAmplitudeMeasure,  
                                  &_QuanityMeasuredAmplitudes,&_ValueHighVoltage);


      
                            
  
    ReadAmlitudesFromModule(_ArrayOfAmplitudes,_QuanityMeasuredAmplitudes);
  
    TimeExposition->Text    = _DurabilityExposition.ToString();
    AvergageAmplitude->Text = _AvergageAmplitudeMeasure.ToString();
    CounterTrigger->Text    = _CounterEventsTrigger.ToString();
    GlobalDosa->Text        = _GlobalDosa.ToString();
    LocalDosa->Text         = _LocalDosa.ToString();
    CountAmplitudes->Text   = _QuanityMeasuredAmplitudes.ToString();

   

        
       if(_QuanityMeasuredAmplitudes > 1000)
       {
       
       _QuanityMeasuredAmplitudes  = 1000;
       
       }  


	   AmplitudeGraph->ChartAreas["ChartArea1"]->AxisY->Minimum = MinAmlitudeInArray(_ArrayOfAmplitudes, _QuanityMeasuredAmplitudes);
       AmplitudeGraph->ChartAreas["ChartArea1"]->AxisY->Maximum = MaxAmlitudeInArray(_ArrayOfAmplitudes, _QuanityMeasuredAmplitudes);

      for( uint16_t i = 0; i <_QuanityMeasuredAmplitudes;i++)
      {


          
             TableAmplitudes->Rows->Add();
             TableAmplitudes->Rows[i]->Cells[0]->Value = (0.5 * i).ToString();
             TableAmplitudes->Rows[i]->Cells[1]->Value =  _ArrayOfAmplitudes[i].ToString();
			 AmplitudeGraph->Series["AmplitidesPoints"]->Points->AddXY((0.5 * i), _ArrayOfAmplitudes[i]);
       }
        
        OpearionIndicator->BackColor = Color::Green;
            
        // memset(_ArrayOfAmplitudes,0,sizeof(uint32_t) * _QuanityMeasuredAmplitudes);
       

       }
    private: System::Windows::Forms::SaveFileDialog^  DialogSaveAmplitudes;
    private: System::Windows::Forms::Button^  Upfate_Aqusition;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column1;
    private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column2;
    private: System::Windows::Forms::Label^  OpearionIndicator;
    public: 


    public: 

    private:
    IC_Module* _IC_Module;
   // FILE* ProtocolMeasure;
	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~OsciloramView()
		{

             _IC_Module = NULL;
             //delete _IC_Module;
            // TableAmplitudes->Rows->Clear();
           
			if (components)
			{
				delete components;
			}
		}
private: System::Windows::Forms::DataVisualization::Charting::Chart^  AmplitudeGraph;
protected: 

    protected: 

    private: System::Windows::Forms::Button^  SaveMeauremnts;
    private: System::Windows::Forms::Label^  label3;
    private: System::Windows::Forms::Label^  CountAmplitudes;
private: System::Windows::Forms::DataGridView^  TableAmplitudes;


    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::GroupBox^  groupBox3;
    private: System::Windows::Forms::Label^  TimeExposition;

	private: System::Windows::Forms::Label^  label10;
    private: System::Windows::Forms::Label^  AvergageAmplitude;


	private: System::Windows::Forms::Label^  label8;
    private: System::Windows::Forms::Label^  CounterTrigger;

	private: System::Windows::Forms::Label^  label5;
    private: System::Windows::Forms::Label^  GlobalDosa;

    private: System::Windows::Forms::Label^  LocalDosa;


	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
            System::Windows::Forms::DataVisualization::Charting::Legend^  legend1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
            System::Windows::Forms::DataVisualization::Charting::Series^  series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
            this->AmplitudeGraph = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
            this->TableAmplitudes = (gcnew System::Windows::Forms::DataGridView());
            this->Column1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->Column2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
            this->label1 = (gcnew System::Windows::Forms::Label());
            this->label2 = (gcnew System::Windows::Forms::Label());
            this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
            this->OpearionIndicator = (gcnew System::Windows::Forms::Label());
            this->SaveMeauremnts = (gcnew System::Windows::Forms::Button());
            this->Upfate_Aqusition = (gcnew System::Windows::Forms::Button());
            this->CountAmplitudes = (gcnew System::Windows::Forms::Label());
            this->label3 = (gcnew System::Windows::Forms::Label());
            this->TimeExposition = (gcnew System::Windows::Forms::Label());
            this->label10 = (gcnew System::Windows::Forms::Label());
            this->AvergageAmplitude = (gcnew System::Windows::Forms::Label());
            this->label8 = (gcnew System::Windows::Forms::Label());
            this->CounterTrigger = (gcnew System::Windows::Forms::Label());
            this->label5 = (gcnew System::Windows::Forms::Label());
            this->GlobalDosa = (gcnew System::Windows::Forms::Label());
            this->LocalDosa = (gcnew System::Windows::Forms::Label());
            this->label11 = (gcnew System::Windows::Forms::Label());
            this->label12 = (gcnew System::Windows::Forms::Label());
            this->DialogSaveAmplitudes = (gcnew System::Windows::Forms::SaveFileDialog());
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->AmplitudeGraph))->BeginInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->TableAmplitudes))->BeginInit();
            this->groupBox3->SuspendLayout();
            this->SuspendLayout();
            // 
            // AmplitudeGraph
            // 
            chartArea1->Name = L"ChartArea1";
            this->AmplitudeGraph->ChartAreas->Add(chartArea1);
            legend1->Name = L"Legend1";
            this->AmplitudeGraph->Legends->Add(legend1);
            this->AmplitudeGraph->Location = System::Drawing::Point(12, 45);
            this->AmplitudeGraph->Name = L"AmplitudeGraph";
            series1->ChartArea = L"ChartArea1";
            series1->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Spline;
            series1->Legend = L"Legend1";
            series1->Name = L"AmplitidesPoints";
            this->AmplitudeGraph->Series->Add(series1);
            this->AmplitudeGraph->Size = System::Drawing::Size(903, 300);
            this->AmplitudeGraph->TabIndex = 0;
            this->AmplitudeGraph->Text = L"chart1";
            // 
            // TableAmplitudes
            // 
            this->TableAmplitudes->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
            this->TableAmplitudes->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(2) {this->Column1, 
                this->Column2});
            this->TableAmplitudes->Location = System::Drawing::Point(12, 351);
            this->TableAmplitudes->Name = L"TableAmplitudes";
            this->TableAmplitudes->Size = System::Drawing::Size(243, 300);
            this->TableAmplitudes->TabIndex = 1;
            // 
            // Column1
            // 
            this->Column1->HeaderText = L"�����";
            this->Column1->Name = L"Column1";
            this->Column1->ReadOnly = true;
            // 
            // Column2
            // 
            this->Column2->HeaderText = L"���������";
            this->Column2->Name = L"Column2";
            this->Column2->ReadOnly = true;
            // 
            // label1
            // 
            this->label1->AutoSize = true;
            this->label1->Location = System::Drawing::Point(94, 335);
            this->label1->Name = L"label1";
            this->label1->Size = System::Drawing::Size(106, 13);
            this->label1->TabIndex = 2;
            this->label1->Text = L"�������� ��������";
            // 
            // label2
            // 
            this->label2->AutoSize = true;
            this->label2->Location = System::Drawing::Point(429, 19);
            this->label2->Name = L"label2";
            this->label2->Size = System::Drawing::Size(96, 13);
            this->label2->TabIndex = 3;
            this->label2->Text = L"������ ��������";
            // 
            // groupBox3
            // 
            this->groupBox3->Controls->Add(this->OpearionIndicator);
            this->groupBox3->Controls->Add(this->SaveMeauremnts);
            this->groupBox3->Controls->Add(this->Upfate_Aqusition);
            this->groupBox3->Controls->Add(this->CountAmplitudes);
            this->groupBox3->Controls->Add(this->label3);
            this->groupBox3->Controls->Add(this->TimeExposition);
            this->groupBox3->Controls->Add(this->label10);
            this->groupBox3->Controls->Add(this->AvergageAmplitude);
            this->groupBox3->Controls->Add(this->label8);
            this->groupBox3->Controls->Add(this->CounterTrigger);
            this->groupBox3->Controls->Add(this->label5);
            this->groupBox3->Controls->Add(this->GlobalDosa);
            this->groupBox3->Controls->Add(this->LocalDosa);
            this->groupBox3->Controls->Add(this->label11);
            this->groupBox3->Controls->Add(this->label12);
            this->groupBox3->Location = System::Drawing::Point(261, 351);
            this->groupBox3->Name = L"groupBox3";
            this->groupBox3->Size = System::Drawing::Size(264, 300);
            this->groupBox3->TabIndex = 5;
            this->groupBox3->TabStop = false;
            this->groupBox3->Text = L"���������";
            // 
            // OpearionIndicator
            // 
            this->OpearionIndicator->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->OpearionIndicator->Location = System::Drawing::Point(226, 15);
            this->OpearionIndicator->Name = L"OpearionIndicator";
            this->OpearionIndicator->Size = System::Drawing::Size(13, 14);
            this->OpearionIndicator->TabIndex = 12;
            // 
            // SaveMeauremnts
            // 
            this->SaveMeauremnts->Location = System::Drawing::Point(10, 217);
            this->SaveMeauremnts->Name = L"SaveMeauremnts";
            this->SaveMeauremnts->Size = System::Drawing::Size(144, 23);
            this->SaveMeauremnts->TabIndex = 6;
            this->SaveMeauremnts->Text = L"��������� ���������";
            this->SaveMeauremnts->UseVisualStyleBackColor = true;
            this->SaveMeauremnts->Click += gcnew System::EventHandler(this, &OsciloramView::SaveMeauremnts_Click);
            // 
            // Upfate_Aqusition
            // 
            this->Upfate_Aqusition->Location = System::Drawing::Point(9, 179);
            this->Upfate_Aqusition->Name = L"Upfate_Aqusition";
            this->Upfate_Aqusition->Size = System::Drawing::Size(143, 23);
            this->Upfate_Aqusition->TabIndex = 7;
            this->Upfate_Aqusition->Text = L"�������� ������";
            this->Upfate_Aqusition->UseVisualStyleBackColor = true;
            this->Upfate_Aqusition->Click += gcnew System::EventHandler(this, &OsciloramView::Upfate_Aqusition_Click);
            // 
            // CountAmplitudes
            // 
            this->CountAmplitudes->AutoSize = true;
            this->CountAmplitudes->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->CountAmplitudes->Location = System::Drawing::Point(163, 140);
            this->CountAmplitudes->Name = L"CountAmplitudes";
            this->CountAmplitudes->Size = System::Drawing::Size(15, 15);
            this->CountAmplitudes->TabIndex = 11;
            this->CountAmplitudes->Text = L"0";
            // 
            // label3
            // 
            this->label3->AutoSize = true;
            this->label3->Location = System::Drawing::Point(7, 140);
            this->label3->Name = L"label3";
            this->label3->Size = System::Drawing::Size(117, 13);
            this->label3->TabIndex = 10;
            this->label3->Text = L"���������� ��������";
            // 
            // TimeExposition
            // 
            this->TimeExposition->AutoSize = true;
            this->TimeExposition->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->TimeExposition->Location = System::Drawing::Point(163, 116);
            this->TimeExposition->Name = L"TimeExposition";
            this->TimeExposition->Size = System::Drawing::Size(15, 15);
            this->TimeExposition->TabIndex = 9;
            this->TimeExposition->Text = L"0";
            // 
            // label10
            // 
            this->label10->AutoSize = true;
            this->label10->Location = System::Drawing::Point(6, 118);
            this->label10->Name = L"label10";
            this->label10->Size = System::Drawing::Size(143, 13);
            this->label10->TabIndex = 8;
            this->label10->Text = L"������������ ����������";
            // 
            // AvergageAmplitude
            // 
            this->AvergageAmplitude->AutoSize = true;
            this->AvergageAmplitude->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->AvergageAmplitude->Location = System::Drawing::Point(163, 93);
            this->AvergageAmplitude->Name = L"AvergageAmplitude";
            this->AvergageAmplitude->Size = System::Drawing::Size(15, 15);
            this->AvergageAmplitude->TabIndex = 7;
            this->AvergageAmplitude->Text = L"0";
            // 
            // label8
            // 
            this->label8->AutoSize = true;
            this->label8->Location = System::Drawing::Point(7, 93);
            this->label8->Name = L"label8";
            this->label8->Size = System::Drawing::Size(107, 13);
            this->label8->TabIndex = 6;
            this->label8->Text = L"������� ���������";
            // 
            // CounterTrigger
            // 
            this->CounterTrigger->AutoSize = true;
            this->CounterTrigger->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->CounterTrigger->Location = System::Drawing::Point(164, 66);
            this->CounterTrigger->Name = L"CounterTrigger";
            this->CounterTrigger->Size = System::Drawing::Size(15, 15);
            this->CounterTrigger->TabIndex = 5;
            this->CounterTrigger->Text = L"0";
            // 
            // label5
            // 
            this->label5->AutoSize = true;
            this->label5->Location = System::Drawing::Point(7, 65);
            this->label5->Name = L"label5";
            this->label5->Size = System::Drawing::Size(131, 13);
            this->label5->TabIndex = 4;
            this->label5->Text = L"��������� �����������";
            // 
            // GlobalDosa
            // 
            this->GlobalDosa->AutoSize = true;
            this->GlobalDosa->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->GlobalDosa->Location = System::Drawing::Point(164, 40);
            this->GlobalDosa->Name = L"GlobalDosa";
            this->GlobalDosa->Size = System::Drawing::Size(15, 15);
            this->GlobalDosa->TabIndex = 3;
            this->GlobalDosa->Text = L"0";
            // 
            // LocalDosa
            // 
            this->LocalDosa->AutoSize = true;
            this->LocalDosa->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->LocalDosa->Location = System::Drawing::Point(164, 16);
            this->LocalDosa->Name = L"LocalDosa";
            this->LocalDosa->Size = System::Drawing::Size(15, 15);
            this->LocalDosa->TabIndex = 2;
            this->LocalDosa->Text = L"0";
            // 
            // label11
            // 
            this->label11->AutoSize = true;
            this->label11->Location = System::Drawing::Point(7, 43);
            this->label11->Name = L"label11";
            this->label11->Size = System::Drawing::Size(94, 13);
            this->label11->TabIndex = 1;
            this->label11->Text = L"���������� ����";
            // 
            // label12
            // 
            this->label12->AutoSize = true;
            this->label12->Location = System::Drawing::Point(7, 19);
            this->label12->Name = L"label12";
            this->label12->Size = System::Drawing::Size(90, 13);
            this->label12->TabIndex = 0;
            this->label12->Text = L"��������� ����";
            // 
            // DialogSaveAmplitudes
            // 
            this->DialogSaveAmplitudes->DefaultExt = L"LOG";
            this->DialogSaveAmplitudes->Filter = L"\"�������� ��������(.LOG)\" |* .LOG ";
            this->DialogSaveAmplitudes->RestoreDirectory = true;
            this->DialogSaveAmplitudes->Title = L"���������� ��������� ���������";
            // 
            // OsciloramView
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
            this->ClientSize = System::Drawing::Size(927, 668);
            this->Controls->Add(this->groupBox3);
            this->Controls->Add(this->label2);
            this->Controls->Add(this->label1);
            this->Controls->Add(this->TableAmplitudes);
            this->Controls->Add(this->AmplitudeGraph);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
            this->MaximizeBox = false;
            this->Name = L"OsciloramView";
            this->Text = L"OsciloramView";
            this->Load += gcnew System::EventHandler(this, &OsciloramView::OsciloramView_Load);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->AmplitudeGraph))->EndInit();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->TableAmplitudes))->EndInit();
            this->groupBox3->ResumeLayout(false);
            this->groupBox3->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
/**********************************************************************************************************/
    private: System::Void SaveMeauremnts_Click(System::Object^  sender, System::EventArgs^  e) {

        OpearionIndicator->BackColor = Color::Red;        
       uint8_t     _Control;
       uint8_t     _State;
       uint32_t    _ThersholdTrigger;
       uint16_t    _ValueHighVoltage;

      
       uint64_t    _GlobalDosa;
       uint64_t    _LocalDosa;
       uint8_t     _CounterEventsTrigger;
       uint16_t    _DurabilityExposition;
       uint32_t    _AvergageAmplitudeMeasure;
       uint16_t    _QuanityMeasuredAmplitudes;
       uint32_t   _ArrayOfAmplitudes[1000];
        memset(_ArrayOfAmplitudes,0,sizeof(uint32_t)*1000);
        _IC_Module->ReadParams(  &_Control, & _State,  &_ThersholdTrigger,
                                  &_GlobalDosa,  &_LocalDosa,  &_CounterEventsTrigger,
                                  &_DurabilityExposition, &_AvergageAmplitudeMeasure,  
                                  &_QuanityMeasuredAmplitudes,&_ValueHighVoltage);
                               
                                  
                                  
                                 ReadAmlitudesFromModule(_ArrayOfAmplitudes,_QuanityMeasuredAmplitudes);
                                 if(DialogSaveAmplitudes->ShowDialog() == System::Windows::Forms::DialogResult::OK)
                                 {
									 

                                 SaveProtocolFile(
                               DialogSaveAmplitudes->FileName,
                               _Control,
                               _State,
                               _ThersholdTrigger,
                               _ValueHighVoltage,
                               _ArrayOfAmplitudes,
                               _GlobalDosa,
                               _LocalDosa,
                               _CounterEventsTrigger,
                               _DurabilityExposition,
                               _AvergageAmplitudeMeasure,
                               _QuanityMeasuredAmplitudes);

      OpearionIndicator->BackColor = Color::Green;


                               }  
             
             }
/**********************************************************************************************************/
void ReadAmlitudesFromModule(uint32_t* ArrayOfAmplitudes, uint32_t AmplitudeCount)
{
     uint8_t OffsetCounter = 0;
     if(AmplitudeCount >= 1000)
     {
        AmplitudeCount = 1000;
     }
     for(uint32_t i = 0;i<AmplitudeCount;i+=40)
     {
    
        _IC_Module->Readmlitudes(&OffsetCounter,
                                   &ArrayOfAmplitudes[OffsetCounter*40]);
                                   OffsetCounter++;
     }


}
/**********************************************************************************************************/
        void SaveProtocolFile(
        
        String^     _FileName,
        uint8_t     _Control,
        uint8_t     _State,
        uint32_t    _ThersholdTrigger,
        uint16_t    _ValueHighVoltage,
        
        uint32_t*   _ArrayOfAmplitudes,
        uint64_t    _GlobalDosa,
        uint64_t    _LocalDosa,
        uint8_t     _CounterEventsTrigger,
        uint16_t    _DurabilityExposition,

        uint32_t    _AvergageAmplitudeMeasure,
        uint16_t    _QuanityMeasuredAmplitudes)

        {    
             std::string FileName;
             char AmlitudesBuffer[32];
             FileName = interop::marshal_as<std::string>(_FileName);
             FILE* Protocol = fopen(FileName.c_str(),"w+");
             
             fprintf(Protocol,"Control Word: 0x%x\n",_Control);
             fprintf(Protocol,"State Word: 0x%x\n",_State);
             fprintf(Protocol,"Thershold Trigger: %i LSB\n",_ThersholdTrigger);

             fprintf(Protocol,"Local  Dosa: %i LSB\n",_LocalDosa);
             fprintf(Protocol,"Global Dosa: %i LSB\n",_GlobalDosa);
             fprintf(Protocol,"Avergage Amplitude meaurements :%i\n",_AvergageAmplitudeMeasure);
             fprintf(Protocol,"Quanity Measure Amplitudes: %i\n",_QuanityMeasuredAmplitudes);
             
             fprintf(Protocol,"Counter Events Trigger: %i\n",_CounterEventsTrigger);
             fprintf(Protocol,"Time Exposition: %i mS\n",_DurabilityExposition);
             fprintf(Protocol,"---------------------------------------------------------------------------\n");
             fprintf(Protocol,"RAW Amplitudes data\n");
             fprintf(Protocol,"---------------------------------------------------------------------------\n");
             
			 fprintf(Protocol,"Time(mS)\t Value(LSB)\n");


             if (_QuanityMeasuredAmplitudes>1000)
			{
			 _QuanityMeasuredAmplitudes = 1000;

			}


             for(uint16_t i = 0;i<_QuanityMeasuredAmplitudes;i++)
             {
              sprintf(AmlitudesBuffer,"%.2f\t\t  %i\n",(i+1)*0.5, _ArrayOfAmplitudes[i]);
			  ComToDota(AmlitudesBuffer);
			  fprintf(Protocol, "%s", AmlitudesBuffer);

             }
           
             fclose(Protocol);
        }

		void ComToDota(char* Source)
		{

			for (size_t i = 0; i < strlen(Source); i++)
			{
				if (Source[i]  == '.')
				{
					Source[i] = ',';

				}

			}

		}


		int  MaxAmlitudeInArray(uint32_t* ArrayOfAmplitudes, uint32_t AmplitudeCount)
		{
			

			int Max = ArrayOfAmplitudes[0];
			for (uint32_t i = 0; i<AmplitudeCount; i += 40)
			{
				if (ArrayOfAmplitudes[i] > Max)
				{
					Max = ArrayOfAmplitudes[i];

				}


			}
			return Max;


		}



		int  MinAmlitudeInArray(uint32_t* ArrayOfAmplitudes, uint32_t AmplitudeCount)
		{
			int Min = ArrayOfAmplitudes[0];
			for (uint32_t i = 0; i<AmplitudeCount; i += 40)
			{
				if (ArrayOfAmplitudes[i] < Min)
				{
					Min = ArrayOfAmplitudes[i];

				}

				
			}
			return Min;

		}





private: System::Void Upfate_Aqusition_Click(System::Object^  sender, System::EventArgs^  e) {
    UptdateMeasurements();
         }
private: System::Void OsciloramView_Load(System::Object^  sender, System::EventArgs^  e) {
 //  UptdateMeasurements();
         }
};
}
