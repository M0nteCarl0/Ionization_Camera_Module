#pragma once
#include "IC_Module.h"
#include <stdint.h>
namespace IonizationCameraTool {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� SetupModuleView
	/// </summary>
	public ref class SetupModuleView : public System::Windows::Forms::Form
	{
	public:
		SetupModuleView(void)
		{
			InitializeComponent();
            _TimeHV = 0;
			//
			//TODO: �������� ��� ������������
			//
		}

       void  SetICModuleHandle(IC_Module* IC_Module)
       {

         _IC_Module = IC_Module;
       }


       void WriteParamsInModule(void)
       {


       ControlWord _ControlW;
       uint8_t     _Control;
       uint32_t    _ThersholdTrigger;
       uint16_t    _ValueHighVoltage;



        uint16_t::TryParse(TimeForceTrigger->Text,_TimeHV);
        uint16_t::TryParse(ValueOfHV->Text,_ValueHighVoltage);
        uint32_t::TryParse(Thershold->Text,_ThersholdTrigger);


      if(ExternalTrigger->Checked)
      {

        _ControlW.Bits.TypeTrigger  = TypeTrigger_External;
      }
      else
      {
         _ControlW.Bits.TypeTrigger  = TypeTrigger_Thershold;

      }


      if(GainFactor_10x->Checked)
      {
        _ControlW.Bits.GainFactor = GainFactor_10X;
      }
      else
      {
        _ControlW.Bits.GainFactor = GainFactor_1X;
      }


      if(HVSourceOn->Checked)
      {
        _ControlW.Bits.StateHV =StateHV_On;
      }
      else
      {
       _ControlW.Bits.StateHV =StateHV_Off;
      }

       _Control =  _ControlW.Raw;
       _IC_Module->WriteParams(&_Control,&_ThersholdTrigger,&_ValueHighVoltage);

       }



        void ReadParamsFromModule(void)
       {

       
       uint8_t     _Control;
       uint8_t     _State;
       uint32_t    _ThersholdTrigger;
       uint16_t    _ValueHighVoltage;

       uint32_t*   _ArrayOfAmplitudes;
       uint64_t    _GlobalDosa;
       uint64_t    _LocalDosa;
       uint8_t     _CounterEventsTrigger;
       uint16_t    _DurabilityExposition;
       uint32_t    _AvergageAmplitudeMeasure;
       uint16_t    _QuanityMeasuredAmplitudes;

       ControlWord _ControlR;


        _IC_Module->ReadParams(  &_Control, & _State,  &_ThersholdTrigger,
                                  &_GlobalDosa,  &_LocalDosa,  &_CounterEventsTrigger,
                                  &_DurabilityExposition, &_AvergageAmplitudeMeasure,  
                                  &_QuanityMeasuredAmplitudes,&_ValueHighVoltage);


        _ControlR.Raw =  _Control;
      
      if(_ControlR.Bits.TypeTrigger  == TypeTrigger_External )
      {
        ExternalTrigger->Checked = true;
      }
      else
      {
        ThersholdTrigger->Checked = true;

      }


      if(_ControlR.Bits.GainFactor == GainFactor_10X)
      {
        GainFactor_10x->Checked = true;
      }
      else
      {
       GainFactor_1x->Checked = true;
      }


      if(_ControlR.Bits.StateHV == StateHV_Off)
      {
       
        HVSourceOff->Checked = true; 
      }
      else
      {
        HVSourceOn->Checked = true;

      }

       ValueOfHV->Text        =  _ValueHighVoltage.ToString();
       Thershold->Text        =  _ThersholdTrigger.ToString();
       TimeForceTrigger->Text = _DurabilityExposition.ToString();
       
       }


      uint16_t GetTimeHV(void)
      {

      return _TimeHV;

      }


    private:
    IC_Module* _IC_Module;
    uint16_t _TimeHV;
    uint16_t _ValueV;
    private: System::Windows::Forms::GroupBox^  Group_GainFactor;

    private: System::Windows::Forms::RadioButton^  GainFactor_10x;
    private: System::Windows::Forms::RadioButton^  GainFactor_1x;
    private: System::Windows::Forms::TextBox^  TimeForceTrigger;
private: System::Windows::Forms::GroupBox^  GroupHVSourceState;

private: System::Windows::Forms::RadioButton^  HVSourceOff;

private: System::Windows::Forms::RadioButton^  HVSourceOn;

         uint32_t _Thershold;


	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~SetupModuleView()
		{
             _IC_Module = NULL;
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
    private: System::Windows::Forms::RadioButton^  ThersholdTrigger;
    protected: 

    private: System::Windows::Forms::RadioButton^  ExternalTrigger;

	private: System::Windows::Forms::GroupBox^  groupBox4;
    private: System::Windows::Forms::TextBox^  ValueOfHV;

    private: System::Windows::Forms::TextBox^  Thershold;

	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::GroupBox^  groupBox5;


	private: System::Windows::Forms::Label^  label13;
    private: System::Windows::Forms::Button^  WriteParams;

    private: System::Windows::Forms::Button^  ReadParams;



	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            this->ThersholdTrigger = (gcnew System::Windows::Forms::RadioButton());
            this->ExternalTrigger = (gcnew System::Windows::Forms::RadioButton());
            this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
            this->ValueOfHV = (gcnew System::Windows::Forms::TextBox());
            this->Thershold = (gcnew System::Windows::Forms::TextBox());
            this->label12 = (gcnew System::Windows::Forms::Label());
            this->label11 = (gcnew System::Windows::Forms::Label());
            this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
            this->TimeForceTrigger = (gcnew System::Windows::Forms::TextBox());
            this->label13 = (gcnew System::Windows::Forms::Label());
            this->WriteParams = (gcnew System::Windows::Forms::Button());
            this->ReadParams = (gcnew System::Windows::Forms::Button());
            this->Group_GainFactor = (gcnew System::Windows::Forms::GroupBox());
            this->GainFactor_10x = (gcnew System::Windows::Forms::RadioButton());
            this->GainFactor_1x = (gcnew System::Windows::Forms::RadioButton());
            this->GroupHVSourceState = (gcnew System::Windows::Forms::GroupBox());
            this->HVSourceOff = (gcnew System::Windows::Forms::RadioButton());
            this->HVSourceOn = (gcnew System::Windows::Forms::RadioButton());
            this->groupBox1->SuspendLayout();
            this->groupBox4->SuspendLayout();
            this->groupBox5->SuspendLayout();
            this->Group_GainFactor->SuspendLayout();
            this->GroupHVSourceState->SuspendLayout();
            this->SuspendLayout();
            // 
            // groupBox1
            // 
            this->groupBox1->Controls->Add(this->ThersholdTrigger);
            this->groupBox1->Controls->Add(this->ExternalTrigger);
            this->groupBox1->Location = System::Drawing::Point(10, 5);
            this->groupBox1->Name = L"groupBox1";
            this->groupBox1->Size = System::Drawing::Size(127, 95);
            this->groupBox1->TabIndex = 1;
            this->groupBox1->TabStop = false;
            this->groupBox1->Text = L"��� ��������";
            // 
            // ThersholdTrigger
            // 
            this->ThersholdTrigger->AutoSize = true;
            this->ThersholdTrigger->Location = System::Drawing::Point(10, 42);
            this->ThersholdTrigger->Name = L"ThersholdTrigger";
            this->ThersholdTrigger->Size = System::Drawing::Size(113, 17);
            this->ThersholdTrigger->TabIndex = 2;
            this->ThersholdTrigger->TabStop = true;
            this->ThersholdTrigger->Text = L"������ �� ������";
            this->ThersholdTrigger->UseVisualStyleBackColor = true;
            // 
            // ExternalTrigger
            // 
            this->ExternalTrigger->AutoSize = true;
            this->ExternalTrigger->Location = System::Drawing::Point(10, 19);
            this->ExternalTrigger->Name = L"ExternalTrigger";
            this->ExternalTrigger->Size = System::Drawing::Size(108, 17);
            this->ExternalTrigger->TabIndex = 0;
            this->ExternalTrigger->TabStop = true;
            this->ExternalTrigger->Text = L"������� ������";
            this->ExternalTrigger->UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this->groupBox4->Controls->Add(this->ValueOfHV);
            this->groupBox4->Controls->Add(this->Thershold);
            this->groupBox4->Controls->Add(this->label12);
            this->groupBox4->Controls->Add(this->label11);
            this->groupBox4->Location = System::Drawing::Point(392, 5);
            this->groupBox4->Name = L"groupBox4";
            this->groupBox4->Size = System::Drawing::Size(200, 95);
            this->groupBox4->TabIndex = 6;
            this->groupBox4->TabStop = false;
            this->groupBox4->Text = L"���������";
            // 
            // ValueOfHV
            // 
            this->ValueOfHV->Location = System::Drawing::Point(146, 44);
            this->ValueOfHV->Name = L"ValueOfHV";
            this->ValueOfHV->Size = System::Drawing::Size(48, 20);
            this->ValueOfHV->TabIndex = 3;
            this->ValueOfHV->Text = L"0";
            // 
            // Thershold
            // 
            this->Thershold->Location = System::Drawing::Point(146, 17);
            this->Thershold->Name = L"Thershold";
            this->Thershold->Size = System::Drawing::Size(48, 20);
            this->Thershold->TabIndex = 2;
            this->Thershold->Text = L"0";
            // 
            // label12
            // 
            this->label12->AutoSize = true;
            this->label12->Location = System::Drawing::Point(7, 44);
            this->label12->Name = L"label12";
            this->label12->Size = System::Drawing::Size(127, 13);
            this->label12->TabIndex = 1;
            this->label12->Text = L"���������� �� ������";
            // 
            // label11
            // 
            this->label11->AutoSize = true;
            this->label11->Location = System::Drawing::Point(10, 20);
            this->label11->Name = L"label11";
            this->label11->Size = System::Drawing::Size(86, 13);
            this->label11->TabIndex = 0;
            this->label11->Text = L"����� ��������";
            // 
            // groupBox5
            // 
            this->groupBox5->Controls->Add(this->TimeForceTrigger);
            this->groupBox5->Controls->Add(this->label13);
            this->groupBox5->Location = System::Drawing::Point(598, 13);
            this->groupBox5->Name = L"groupBox5";
            this->groupBox5->Size = System::Drawing::Size(173, 87);
            this->groupBox5->TabIndex = 7;
            this->groupBox5->TabStop = false;
            this->groupBox5->Text = L"��������� ������� �������";
            // 
            // TimeForceTrigger
            // 
            this->TimeForceTrigger->Location = System::Drawing::Point(98, 19);
            this->TimeForceTrigger->Name = L"TimeForceTrigger";
            this->TimeForceTrigger->Size = System::Drawing::Size(48, 20);
            this->TimeForceTrigger->TabIndex = 4;
            this->TimeForceTrigger->Text = L"5000";
            // 
            // label13
            // 
            this->label13->AutoSize = true;
            this->label13->Location = System::Drawing::Point(6, 24);
            this->label13->Name = L"label13";
            this->label13->Size = System::Drawing::Size(40, 13);
            this->label13->TabIndex = 3;
            this->label13->Text = L"�����";
            // 
            // WriteParams
            // 
            this->WriteParams->Location = System::Drawing::Point(12, 106);
            this->WriteParams->Name = L"WriteParams";
            this->WriteParams->Size = System::Drawing::Size(140, 23);
            this->WriteParams->TabIndex = 8;
            this->WriteParams->Text = L"�������� ���������";
            this->WriteParams->UseVisualStyleBackColor = true;
            this->WriteParams->Click += gcnew System::EventHandler(this, &SetupModuleView::WriteParams_Click);
            // 
            // ReadParams
            // 
            this->ReadParams->Location = System::Drawing::Point(158, 106);
            this->ReadParams->Name = L"ReadParams";
            this->ReadParams->Size = System::Drawing::Size(140, 23);
            this->ReadParams->TabIndex = 9;
            this->ReadParams->Text = L"��������� ���������";
            this->ReadParams->UseVisualStyleBackColor = true;
            this->ReadParams->Click += gcnew System::EventHandler(this, &SetupModuleView::ReadParams_Click);
            // 
            // Group_GainFactor
            // 
            this->Group_GainFactor->Controls->Add(this->GainFactor_10x);
            this->Group_GainFactor->Controls->Add(this->GainFactor_1x);
            this->Group_GainFactor->Location = System::Drawing::Point(143, 5);
            this->Group_GainFactor->Name = L"Group_GainFactor";
            this->Group_GainFactor->Size = System::Drawing::Size(119, 95);
            this->Group_GainFactor->TabIndex = 10;
            this->Group_GainFactor->TabStop = false;
            this->Group_GainFactor->Text = L"������� ��������";
            // 
            // GainFactor_10x
            // 
            this->GainFactor_10x->AutoSize = true;
            this->GainFactor_10x->Location = System::Drawing::Point(10, 42);
            this->GainFactor_10x->Name = L"GainFactor_10x";
            this->GainFactor_10x->Size = System::Drawing::Size(42, 17);
            this->GainFactor_10x->TabIndex = 2;
            this->GainFactor_10x->TabStop = true;
            this->GainFactor_10x->Text = L"10x";
            this->GainFactor_10x->UseVisualStyleBackColor = true;
            // 
            // GainFactor_1x
            // 
            this->GainFactor_1x->AutoSize = true;
            this->GainFactor_1x->Location = System::Drawing::Point(10, 19);
            this->GainFactor_1x->Name = L"GainFactor_1x";
            this->GainFactor_1x->Size = System::Drawing::Size(36, 17);
            this->GainFactor_1x->TabIndex = 0;
            this->GainFactor_1x->TabStop = true;
            this->GainFactor_1x->Text = L"1x";
            this->GainFactor_1x->UseVisualStyleBackColor = true;
            // 
            // GroupHVSourceState
            // 
            this->GroupHVSourceState->Controls->Add(this->HVSourceOff);
            this->GroupHVSourceState->Controls->Add(this->HVSourceOn);
            this->GroupHVSourceState->Location = System::Drawing::Point(277, 4);
            this->GroupHVSourceState->Name = L"GroupHVSourceState";
            this->GroupHVSourceState->Size = System::Drawing::Size(119, 96);
            this->GroupHVSourceState->TabIndex = 11;
            this->GroupHVSourceState->TabStop = false;
            this->GroupHVSourceState->Text = L"�������� �������� ����������";
            // 
            // HVSourceOff
            // 
            this->HVSourceOff->AutoSize = true;
            this->HVSourceOff->Location = System::Drawing::Point(10, 66);
            this->HVSourceOff->Name = L"HVSourceOff";
            this->HVSourceOff->Size = System::Drawing::Size(52, 17);
            this->HVSourceOff->TabIndex = 2;
            this->HVSourceOff->TabStop = true;
            this->HVSourceOff->Text = L"����";
            this->HVSourceOff->UseVisualStyleBackColor = true;
            // 
            // HVSourceOn
            // 
            this->HVSourceOn->AutoSize = true;
            this->HVSourceOn->Location = System::Drawing::Point(10, 43);
            this->HVSourceOn->Name = L"HVSourceOn";
            this->HVSourceOn->Size = System::Drawing::Size(44, 17);
            this->HVSourceOn->TabIndex = 0;
            this->HVSourceOn->TabStop = true;
            this->HVSourceOn->Text = L"���";
            this->HVSourceOn->UseVisualStyleBackColor = true;
            // 
            // SetupModuleView
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(784, 135);
            this->Controls->Add(this->GroupHVSourceState);
            this->Controls->Add(this->Group_GainFactor);
            this->Controls->Add(this->ReadParams);
            this->Controls->Add(this->WriteParams);
            this->Controls->Add(this->groupBox5);
            this->Controls->Add(this->groupBox4);
            this->Controls->Add(this->groupBox1);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
            this->MaximizeBox = false;
            this->Name = L"SetupModuleView";
            this->Text = L"��������� ������";
            this->groupBox1->ResumeLayout(false);
            this->groupBox1->PerformLayout();
            this->groupBox4->ResumeLayout(false);
            this->groupBox4->PerformLayout();
            this->groupBox5->ResumeLayout(false);
            this->groupBox5->PerformLayout();
            this->Group_GainFactor->ResumeLayout(false);
            this->Group_GainFactor->PerformLayout();
            this->GroupHVSourceState->ResumeLayout(false);
            this->GroupHVSourceState->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
/***********************************************************************************************************/
    private: System::Void WriteParams_Click(System::Object^  sender, System::EventArgs^  e) {
            WriteParamsInModule();
             }
/***********************************************************************************************************/
private: System::Void ReadParams_Click(System::Object^  sender, System::EventArgs^  e) {
             ReadParamsFromModule();
         }
/***********************************************************************************************************/
};
}
