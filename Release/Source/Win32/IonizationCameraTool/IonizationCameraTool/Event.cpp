#include "StdAfx.h"
#include "Event.h"

CRITICAL_SECTION _Sec;
Event::Event(void)
{
	InitializeCriticalSection(&_Sec);
	_Event = CreateEvent(NULL,true,false,NULL);
}

/***********************************************************/
Event::~Event(void)
{
	CloseHandle(_Event);
	_Event = NULL;
	
}
/***********************************************************/
Event::Event(const char* SymbolicName)
{
	InitializeCriticalSection(&_Sec);
	_Event = CreateEvent(NULL,true,false,SymbolicName);

};
/***********************************************************/
DWORD Event::Wait(void)
{
	
	return WaitForSingleObject(_Event,INFINITE);
}
/***********************************************************/
DWORD Event::Wait(const int Timeout)
{

	return WaitForSingleObject(_Event,Timeout);
}
/***********************************************************/
bool Event::Set(void)
{
	bool Res = false;

		if(_Event!=NULL)
		{
			EnterCriticalSection(&_Sec);
			Res =	SetEvent(_Event);
			_Emited =true;
			LeaveCriticalSection(&_Sec);
		}
		else
		{
			Res = false;
		}

		return  Res;
	}
/***********************************************************/
bool Event::Reset(void)
	{

			bool Res = false;

		if(_Event!=NULL)
		{

			EnterCriticalSection(&_Sec);
			Res =	ResetEvent(_Event);
			_Emited = false;
			LeaveCriticalSection(&_Sec);
		}
		else
		{
			Res = false;
		}

		return Res;
	}
/***********************************************************/
bool Event::IsEmited(void){
	return  _Emited;
};