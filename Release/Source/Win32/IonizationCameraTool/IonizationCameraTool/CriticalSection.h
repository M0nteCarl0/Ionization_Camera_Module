#pragma once
#include <Windows.h>
#include <string>
class CriticalSection
{
public:
	CriticalSection();
	CriticalSection(const char* Decription);
	const char* GetDescription(void);
	~CriticalSection();
void	Enter(void);
void	Leave(void);
private:
	CRITICAL_SECTION _Sec;
	void Init(void);
	std::string _Description;
	
};

