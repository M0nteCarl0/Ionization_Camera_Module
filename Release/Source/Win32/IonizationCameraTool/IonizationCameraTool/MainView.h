#pragma once
#include  "MXCOMM.H"
#include  "KY5View.h"
#include  "OsciloramView.h"
#include  "SetupModuleView.h"
#include  "FTDIView.h"
#include  "IC_Module.h"
namespace IonizationCameraTool {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class MainView : public System::Windows::Forms::Form
	{
	public:
       MxComm*          _MXCOMM;
       KY5View^         _KY5;
       FTDIView^        _FTDI;
       OsciloramView^   _Oscilogram; 
       SetupModuleView^ _ModuleSetup;
       DWORD SelectedDevice;
    private: System::Windows::Forms::CheckBox^  Timeout_Emited;
    public: 

    public: 
        IC_Module*        _IC_Module;
    private: System::Windows::Forms::Timer^  StatePolling;
    private: System::Windows::Forms::Label^  ConectionIndicator;
    private: System::Windows::Forms::ToolTip^  InfoToolTip;
    public: 
        bool isConected;
		MainView(void)
		{
			InitializeComponent();

            _MXCOMM       = new MxComm();
            _IC_Module    = new IC_Module();
            _KY5          = gcnew KY5View();
            _FTDI         = gcnew FTDIView();
            _Oscilogram   = gcnew OsciloramView();
            _ModuleSetup  = gcnew  SetupModuleView();
           
            InitFTDIDefault();
           
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MainView()
		{
           
            delete _MXCOMM;
			if (components)
			{
				delete components;
			}
		}

    protected: 
    private: System::Windows::Forms::Button^  ResetAllMeasuremnets;
    private: System::Windows::Forms::GroupBox^  groupBox1;
    private: System::Windows::Forms::Label^  SoftwareVersion;

    private: System::Windows::Forms::Label^  HardwareVersion;

    private: System::Windows::Forms::Label^  label2;
    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::CheckBox^  MeasureState;



    private: System::Windows::Forms::GroupBox^  groupBox2;
    private: System::Windows::Forms::RadioButton^  Thershold_Trigger;
    private: System::Windows::Forms::RadioButton^  Forced_Trigger;


    private: System::Windows::Forms::RadioButton^  Extrenal_Trigger;

    private: System::Windows::Forms::Button^  Forced_Start;


	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fTDIToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  FTDI_Connection;

    private: System::Windows::Forms::ToolStripMenuItem^  FTDI_Connect;
    private: System::Windows::Forms::ToolStripMenuItem^  FTDI_Disconect;
    private: System::Windows::Forms::ToolStripMenuItem^  FTDI_Setup;



	private: System::Windows::Forms::ToolStripMenuItem^  kY5ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  KY5_Open;
    private: System::Windows::Forms::ToolStripMenuItem^  IC_Setup;
    private: System::Windows::Forms::ToolStripMenuItem^  IC_Measurements;
	private:

             UINT nSpeed ;   
             UINT nDataBits; 
             UINT nParity;   
             UINT nStopBits;
    private: System::ComponentModel::IContainer^  components;
             /// <summary>
		/// ��������� ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->MeasureState = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->Timeout_Emited = (gcnew System::Windows::Forms::CheckBox());
			this->Thershold_Trigger = (gcnew System::Windows::Forms::RadioButton());
			this->Forced_Trigger = (gcnew System::Windows::Forms::RadioButton());
			this->Extrenal_Trigger = (gcnew System::Windows::Forms::RadioButton());
			this->Forced_Start = (gcnew System::Windows::Forms::Button());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fTDIToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->FTDI_Connection = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->FTDI_Connect = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->FTDI_Disconect = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->FTDI_Setup = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->kY5ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->KY5_Open = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->IC_Setup = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->IC_Measurements = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->ConectionIndicator = (gcnew System::Windows::Forms::Label());
			this->SoftwareVersion = (gcnew System::Windows::Forms::Label());
			this->HardwareVersion = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->ResetAllMeasuremnets = (gcnew System::Windows::Forms::Button());
			this->StatePolling = (gcnew System::Windows::Forms::Timer(this->components));
			this->InfoToolTip = (gcnew System::Windows::Forms::ToolTip(this->components));
			this->groupBox2->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// MeasureState
			// 
			this->MeasureState->AutoCheck = false;
			this->MeasureState->AutoSize = true;
			this->MeasureState->Location = System::Drawing::Point(7, 20);
			this->MeasureState->Name = L"MeasureState";
			this->MeasureState->Size = System::Drawing::Size(110, 17);
			this->MeasureState->TabIndex = 0;
			this->MeasureState->Text = L"���� ���������";
			this->MeasureState->UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->Timeout_Emited);
			this->groupBox2->Controls->Add(this->Thershold_Trigger);
			this->groupBox2->Controls->Add(this->Forced_Trigger);
			this->groupBox2->Controls->Add(this->Extrenal_Trigger);
			this->groupBox2->Controls->Add(this->MeasureState);
			this->groupBox2->Location = System::Drawing::Point(12, 27);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(291, 114);
			this->groupBox2->TabIndex = 1;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"��������� �����";
			// 
			// Timeout_Emited
			// 
			this->Timeout_Emited->AutoSize = true;
			this->Timeout_Emited->Location = System::Drawing::Point(6, 44);
			this->Timeout_Emited->Name = L"Timeout_Emited";
			this->Timeout_Emited->Size = System::Drawing::Size(124, 17);
			this->Timeout_Emited->TabIndex = 6;
			this->Timeout_Emited->Text = L"��������� �������";
			this->Timeout_Emited->UseVisualStyleBackColor = true;
			// 
			// Thershold_Trigger
			// 
			this->Thershold_Trigger->AutoCheck = false;
			this->Thershold_Trigger->AutoSize = true;
			this->Thershold_Trigger->Location = System::Drawing::Point(136, 42);
			this->Thershold_Trigger->Name = L"Thershold_Trigger";
			this->Thershold_Trigger->Size = System::Drawing::Size(113, 17);
			this->Thershold_Trigger->TabIndex = 5;
			this->Thershold_Trigger->TabStop = true;
			this->Thershold_Trigger->Text = L"������ �� ������";
			this->InfoToolTip->SetToolTip(this->Thershold_Trigger, L"���������� ��� ���������� ������ �� ��������� �������� ��������.");
			this->Thershold_Trigger->UseVisualStyleBackColor = true;
			// 
			// Forced_Trigger
			// 
			this->Forced_Trigger->AutoCheck = false;
			this->Forced_Trigger->AutoSize = true;
			this->Forced_Trigger->Location = System::Drawing::Point(136, 65);
			this->Forced_Trigger->Name = L"Forced_Trigger";
			this->Forced_Trigger->Size = System::Drawing::Size(63, 17);
			this->Forced_Trigger->TabIndex = 4;
			this->Forced_Trigger->TabStop = true;
			this->Forced_Trigger->Text = L"������ ";
			this->InfoToolTip->SetToolTip(this->Forced_Trigger, L"��������� �� �� ��� ��������� ����������� � ������������ ������");
			this->Forced_Trigger->UseVisualStyleBackColor = true;
			// 
			// Extrenal_Trigger
			// 
			this->Extrenal_Trigger->AutoCheck = false;
			this->Extrenal_Trigger->AutoSize = true;
			this->Extrenal_Trigger->Location = System::Drawing::Point(136, 19);
			this->Extrenal_Trigger->Name = L"Extrenal_Trigger";
			this->Extrenal_Trigger->Size = System::Drawing::Size(108, 17);
			this->Extrenal_Trigger->TabIndex = 3;
			this->Extrenal_Trigger->TabStop = true;
			this->Extrenal_Trigger->Text = L"������� ������";
			this->InfoToolTip->SetToolTip(this->Extrenal_Trigger, L"���������� ��� ���������� ������ �� ��������������.");
			this->Extrenal_Trigger->UseVisualStyleBackColor = true;
			// 
			// Forced_Start
			// 
			this->Forced_Start->Location = System::Drawing::Point(12, 157);
			this->Forced_Start->Name = L"Forced_Start";
			this->Forced_Start->Size = System::Drawing::Size(109, 23);
			this->Forced_Start->TabIndex = 7;
			this->Forced_Start->Text = L"������ ������";
			this->Forced_Start->UseVisualStyleBackColor = true;
			this->Forced_Start->Click += gcnew System::EventHandler(this, &MainView::Forced_Start_Click);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->fTDIToolStripMenuItem, 
				this->kY5ToolStripMenuItem, this->IC_Setup, this->IC_Measurements});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(480, 24);
			this->menuStrip1->TabIndex = 8;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fTDIToolStripMenuItem
			// 
			this->fTDIToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->FTDI_Connection, 
				this->FTDI_Setup});
			this->fTDIToolStripMenuItem->Name = L"fTDIToolStripMenuItem";
			this->fTDIToolStripMenuItem->Size = System::Drawing::Size(43, 20);
			this->fTDIToolStripMenuItem->Text = L"FTDI";
			// 
			// FTDI_Connection
			// 
			this->FTDI_Connection->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->FTDI_Connect, 
				this->FTDI_Disconect});
			this->FTDI_Connection->Name = L"FTDI_Connection";
			this->FTDI_Connection->Size = System::Drawing::Size(152, 22);
			this->FTDI_Connection->Text = L"�����������";
			// 
			// FTDI_Connect
			// 
			this->FTDI_Connect->Name = L"FTDI_Connect";
			this->FTDI_Connect->Size = System::Drawing::Size(156, 22);
			this->FTDI_Connect->Text = L"������������";
			this->FTDI_Connect->Click += gcnew System::EventHandler(this, &MainView::FTDI_Connect_Click);
			// 
			// FTDI_Disconect
			// 
			this->FTDI_Disconect->Name = L"FTDI_Disconect";
			this->FTDI_Disconect->Size = System::Drawing::Size(156, 22);
			this->FTDI_Disconect->Text = L"�����������";
			this->FTDI_Disconect->Click += gcnew System::EventHandler(this, &MainView::FTDI_Disconect_Click);
			// 
			// FTDI_Setup
			// 
			this->FTDI_Setup->Name = L"FTDI_Setup";
			this->FTDI_Setup->Size = System::Drawing::Size(152, 22);
			this->FTDI_Setup->Text = L"���������";
			this->FTDI_Setup->Click += gcnew System::EventHandler(this, &MainView::FTDI_Setup_Click);
			// 
			// kY5ToolStripMenuItem
			// 
			this->kY5ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->KY5_Open});
			this->kY5ToolStripMenuItem->Name = L"kY5ToolStripMenuItem";
			this->kY5ToolStripMenuItem->Size = System::Drawing::Size(39, 20);
			this->kY5ToolStripMenuItem->Text = L"KY5";
			this->kY5ToolStripMenuItem->Visible = false;
			// 
			// KY5_Open
			// 
			this->KY5_Open->Name = L"KY5_Open";
			this->KY5_Open->Size = System::Drawing::Size(254, 22);
			this->KY5_Open->Text = L"������� ������ ���������� KY5";
			this->KY5_Open->Click += gcnew System::EventHandler(this, &MainView::KY5_Open_Click);
			// 
			// IC_Setup
			// 
			this->IC_Setup->Name = L"IC_Setup";
			this->IC_Setup->Size = System::Drawing::Size(130, 20);
			this->IC_Setup->Text = L"��������� ������ ";
			this->IC_Setup->Click += gcnew System::EventHandler(this, &MainView::IC_Setup_Click);
			// 
			// IC_Measurements
			// 
			this->IC_Measurements->Name = L"IC_Measurements";
			this->IC_Measurements->Size = System::Drawing::Size(81, 20);
			this->IC_Measurements->Text = L"���������";
			this->IC_Measurements->Click += gcnew System::EventHandler(this, &MainView::IC_Measurements_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->ConectionIndicator);
			this->groupBox1->Controls->Add(this->SoftwareVersion);
			this->groupBox1->Controls->Add(this->HardwareVersion);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(309, 27);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(163, 114);
			this->groupBox1->TabIndex = 9;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"������";
			// 
			// ConectionIndicator
			// 
			this->ConectionIndicator->BackColor = System::Drawing::Color::Red;
			this->ConectionIndicator->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ConectionIndicator->Location = System::Drawing::Point(142, 19);
			this->ConectionIndicator->Name = L"ConectionIndicator";
			this->ConectionIndicator->Size = System::Drawing::Size(13, 14);
			this->ConectionIndicator->TabIndex = 4;
			this->InfoToolTip->SetToolTip(this->ConectionIndicator, L"��������� �����������");
			// 
			// SoftwareVersion
			// 
			this->SoftwareVersion->AutoSize = true;
			this->SoftwareVersion->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->SoftwareVersion->Location = System::Drawing::Point(112, 46);
			this->SoftwareVersion->Name = L"SoftwareVersion";
			this->SoftwareVersion->Size = System::Drawing::Size(24, 15);
			this->SoftwareVersion->TabIndex = 3;
			this->SoftwareVersion->Text = L"0,0";
			// 
			// HardwareVersion
			// 
			this->HardwareVersion->AutoSize = true;
			this->HardwareVersion->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->HardwareVersion->Location = System::Drawing::Point(112, 21);
			this->HardwareVersion->Name = L"HardwareVersion";
			this->HardwareVersion->Size = System::Drawing::Size(24, 15);
			this->HardwareVersion->TabIndex = 2;
			this->HardwareVersion->Text = L"0,0";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(7, 46);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(97, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"������ ��������";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(7, 20);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(85, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"������ ������";
			// 
			// ResetAllMeasuremnets
			// 
			this->ResetAllMeasuremnets->Location = System::Drawing::Point(134, 157);
			this->ResetAllMeasuremnets->Name = L"ResetAllMeasuremnets";
			this->ResetAllMeasuremnets->Size = System::Drawing::Size(198, 23);
			this->ResetAllMeasuremnets->TabIndex = 10;
			this->ResetAllMeasuremnets->Text = L"�������� ���������� ���������";
			this->ResetAllMeasuremnets->UseVisualStyleBackColor = true;
			this->ResetAllMeasuremnets->Click += gcnew System::EventHandler(this, &MainView::ResetAllMeasuremnets_Click);
			// 
			// StatePolling
			// 
			this->StatePolling->Interval = 200;
			this->StatePolling->Tick += gcnew System::EventHandler(this, &MainView::StatePolling_Tick);
			// 
			// InfoToolTip
			// 
			this->InfoToolTip->IsBalloon = true;
			this->InfoToolTip->ToolTipIcon = System::Windows::Forms::ToolTipIcon::Info;
			// 
			// MainView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->ClientSize = System::Drawing::Size(480, 183);
			this->Controls->Add(this->ResetAllMeasuremnets);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->Forced_Start);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->menuStrip1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->MainMenuStrip = this->menuStrip1;
			this->MaximizeBox = false;
			this->Name = L"MainView";
			this->Text = L"Ionization Camera Control 1.0                                                    " 
				L"                                                                         ";
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
/***********************************************************************************************/
    private: System::Void KY5_Open_Click(System::Object^  sender, System::EventArgs^  e) {
    if(isConected)
    {
        if (!_KY5->Visible)
		{
			
		    if (_KY5->IsDisposed)
		    {
		    	_KY5 =  gcnew KY5View();
		    }

          _KY5->SetMxCommHandle(_MXCOMM);
		  _KY5->Show();
		}
        }
     }
/***********************************************************************************************/
private: System::Void IC_Setup_Click(System::Object^  sender, System::EventArgs^  e) {

        if(isConected)
        {
                if (!_ModuleSetup->Visible)
		        {
		        	
		            if (_ModuleSetup->IsDisposed)
		            {
		            	_ModuleSetup =  gcnew SetupModuleView();
		            }

                   _ModuleSetup->SetICModuleHandle(_IC_Module);
                   _ModuleSetup->ReadParamsFromModule();
		           _ModuleSetup->Show();
		        }

        }
      } 
/***********************************************************************************************/
private: System::Void IC_Measurements_Click(System::Object^  sender, System::EventArgs^  e) {
  
        
        if(isConected)
        {
        if (!_Oscilogram->Visible)
		{
			
		    if (_Oscilogram->IsDisposed)
		    {
		    	_Oscilogram =  gcnew OsciloramView();
		    }

           _Oscilogram->SetICModuleHandle(_IC_Module);
           
		    _Oscilogram->UptdateMeasurements();
           _Oscilogram->Show();
         
		}

        }
      }
/***********************************************************************************************/
private: System::Void FTDI_Setup_Click(System::Object^  sender, System::EventArgs^  e) {
    
        if (!_FTDI->Visible)
		{	
		    if (_FTDI->IsDisposed)
		    {
		    	_FTDI =  gcnew FTDIView();
               _FTDI->SetParametrsFTDI(nSpeed,   
		                                nDataBits,
                                        nStopBits);
               _FTDI-> SetMXComm(_MXCOMM);  
             }
             else
             {

              _FTDI->SetParametrsFTDI(nSpeed,   
		                                nDataBits,
                                        nStopBits);
                  _FTDI-> SetMXComm(_MXCOMM);  

             }

		         if( _FTDI->ShowDialog() == System::Windows::Forms::DialogResult::OK)
                 {
                    _FTDI->GetParametrsFTDI(nSpeed,  
                                            nDataBits,
                                            nStopBits);


                 SelectedDevice =  _FTDI->GetSelectedDeviceID();

                 }
		}
     }
/***********************************************************************************************/
private: System::Void Forced_Start_Click(System::Object^  sender, System::EventArgs^  e) {

     
     if(isConected)
     {
      if (_ModuleSetup->Visible)
		{
     
          uint16_t Time =   _ModuleSetup->GetTimeHV();


           _IC_Module->StartForcedMeaure(&Time);

         }

         }

         }
/***********************************************************************************************/
private: System::Void FTDI_Connect_Click(System::Object^  sender, System::EventArgs^  e) {

           if(_MXCOMM->ConfigureByID(SelectedDevice,nSpeed, nDataBits,nParity,nStopBits))
           {
             _MXCOMM->SetTimeout(120);
             _IC_Module->Init(_MXCOMM);
              BYTE Name[4];
			  BYTE SW[4];
			  BYTE HW[4];
			  ConectionIndicator->BackColor = Color::Green;
               _IC_Module-> ReadVersion(Name,HW,SW);


               if(!strcmp((const char *)Name,"IC"))
               {

                isConected = true;
                SoftwareVersion->Text = SW[0] + "." + SW[1];
                HardwareVersion->Text =  HW[0] + "." + HW[1];
                StatePolling->Enabled = true;
               }
           }


         }
/***********************************************************************************************/
private: System::Void FTDI_Disconect_Click(System::Object^  sender, System::EventArgs^  e) {
               
          StatePolling->Enabled = false;
          _MXCOMM->Close();
          isConected = false;
          SoftwareVersion->Text = 0 + "." + 0;
          HardwareVersion->Text = 0 + "." + 0;
		  ConectionIndicator->BackColor = Color::Red;
         }
/***********************************************************************************************/
public:  void SetParametrsFTDI(UINT Speed,   
                               UINT  DataBits,
                               UINT  StopBitS) 
{
    nSpeed    = Speed;   
    nDataBits = DataBits; 
    nStopBits = StopBitS;
}

/***********************************************************************************************/
public: void GetParametrsFTDI(UINT Speed,   
                               UINT  DataBits,
                               UINT  StopBitS)

{
      Speed    =  nSpeed ;   
      DataBits =  nDataBits; 
      StopBitS = nStopBits;
}
/***********************************************************************************************/
void InitFTDIDefault(void)
{

    nSpeed    = 19200; 
    nDataBits = 8; 
    nParity   = 1; 
    nStopBits = 2;
}
/***********************************************************************************************/
private: System::Void ResetAllMeasuremnets_Click(System::Object^  sender, System::EventArgs^  e) {
         if(isConected)
        {
            _IC_Module->CleanAllMeasurement();
        }
         }
/***********************************************************************************************/
private: System::Void StatePolling_Tick(System::Object^  sender, System::EventArgs^  e) {
            if(isConected)
            {



            uint8_t     _Control;
            uint8_t     _State;
            uint32_t    _ThersholdTrigger;
            uint16_t    _ValueHighVoltage;

    
            uint64_t    _GlobalDosa;
            uint64_t    _LocalDosa;
            uint8_t     _CounterEventsTrigger;
            uint16_t    _DurabilityExposition;
            uint32_t    _AvergageAmplitudeMeasure;
            uint16_t    _QuanityMeasuredAmplitudes;
            StateWord   StateW;
      
         _IC_Module->ReadParams(  &_Control, & _State,  &_ThersholdTrigger,
                                  &_GlobalDosa,  &_LocalDosa,  &_CounterEventsTrigger,
                                  &_DurabilityExposition, &_AvergageAmplitudeMeasure,  
                                  &_QuanityMeasuredAmplitudes,&_ValueHighVoltage);



           StateW.Raw = _State;

          if(StateW.Bits.AqusitionState == AqusitionState_Complete)
          {


                MeasureState->Checked = false;


          }



          if(StateW.Bits.AqusitionState == AqusitionState_InProgress)
          {


             MeasureState->Checked = true;


          }



          if(StateW.Bits.SourceStart == SourceStart_Forced)
          {

          Forced_Trigger->Checked = true;
          Extrenal_Trigger->Checked = false;
          Thershold_Trigger->Checked = false;

          }


           if(StateW.Bits.SourceStart == SourceStart_External)
          {
             Forced_Trigger->Checked = false;
             Extrenal_Trigger->Checked = true;
             Thershold_Trigger->Checked = false;

          }




           if(StateW.Bits.SourceStart == SourceStart_Thershold)
          {

          Extrenal_Trigger->Checked = false;
          Thershold_Trigger->Checked = true;
           Forced_Trigger->Checked = false;

          }



           if(StateW.Bits.TimeoutEventIndicator == TimeoutEventIndicator_Measure_Normal)
          {

             Timeout_Emited->Checked = false;
          }
          

          if(StateW.Bits.TimeoutEventIndicator ==  TimeoutEventIndicator_Measure_Triggered)
          {

            Timeout_Emited->Checked = true;

          }


            }

         }

};
}

