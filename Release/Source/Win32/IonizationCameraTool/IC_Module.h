#pragma once
#include "MXCOMM.H"
#include <stdint.h>
#include <vector>
#define NCOM 1024
#define mID 0x96
#define mADDR 0x08

enum CommadID {
  CommadID_WRITE_DETECTOR_SETUP = 0x1,
  CommadID_DETECTOR_FORCE_START = 0x2,
  CommadID_DETECTOR_RESET_ALL = 0x3,
  CommadID_WRITE_ADC_SETUP = 0x4,
  CommadID_READ_HW_SW = 0x40,
  CommadID_READ_DETECTOR_SETUP = 0x41,
  CommadID_READ_DETECTOR_AMPLITUDES = 0x42,
  CommadID_READ_ADC_SETUP = 0x44,
  CommadID_READ_HV_LEVEL = 0x45,

};

typedef struct ControlWordS {
  uint8_t TypeTrigger : 2;
  uint8_t GainFactor : 1;
  uint8_t StateHV : 1;
  uint8_t MovingAvergageFilter : 1;

} ControlWordS;

typedef union ControlWord {
  ControlWordS Bits;
  uint8_t Raw;
} ControlWord;

enum TypeTrigger {
  TypeTrigger_InfiniteIntegration = 0x0,
  TypeTrigger_External = 0x1,
  TypeTrigger_Thershold = 0x2,
};

enum GainFactor {
  GainFactor_10X = 0x0,
  GainFactor_1X = 0x1,
};

enum StateHV {
  StateHV_Off = 0x0,
  StateHV_On = 0x1,
};

typedef struct StateWordS
{
  uint8_t AqusitionState : 1;
  uint8_t SourceStart : 3;
  uint8_t WaitMeasure : 1;
  uint8_t EndMeasure : 1;
  uint8_t TimeoutEventIndicator : 1;
}StateWordS;


typedef union StateWord {
  StateWordS Bits;
  uint8_t Raw;
} StateWord;

enum AqusitionState {
  AqusitionState_InProgress = 0x1,
  AqusitionState_Complete = 0x0,
};

enum SourceStart {
  SourceStart_InfiniteIntegration = 0x0,
  SourceStart_Forced = 0x3,
  SourceStart_External = 0x1,
  SourceStart_Thershold = 0x2,
};

typedef enum TimeoutEventIndicator {
  TimeoutEventIndicator_Measure_Normal = 0x0,
  TimeoutEventIndicator_Measure_Triggered = 0x1,
};

class IC_Module {
public:
  IC_Module(void);
  ~IC_Module(void);
  void Init(std::shared_ptr<MxComm> *Comm);
  int ReadVersion(uint8_t *Name, uint8_t *Hard, uint8_t *Soft);
  int ReadParams(uint8_t *Control, uint8_t *State, uint32_t *ThersholdTrigger,
                 int64_t *GlobalDosa, int64_t *LocalDosa,
                 uint8_t *CounterEventsTrigger, uint32_t *DurabilityExposition,
                 int32_t *AvergageAmplitudeMeasure,
                 uint32_t *QuanityMeasuredAmplitudes,
                 uint16_t *ValueHighVoltage, uint32_t *ValueDC);

  int Readmlitudes(uint8_t *Offset, int32_t *PartArrayAmplitues);

  int WriteParams(uint8_t *Control, uint32_t *Thershold,
                  uint16_t *ValueofSourceHV, uint32_t *ValueDC);

  int StartForcedMeaure(uint16_t *TimeAqusiotion);
  int CleanAllMeasurement(void);
  int WriteADCParametrs(uint8_t *SampleRate, uint8_t *TypeFilter,
                        uint8_t *Filter50Hertz,
                        uint8_t *SampleRateFilter50Hertz);
  int ReadADCParametrs(uint8_t *SampleRate, uint8_t *TypeFilter,
                       uint8_t *Filter50Hertz,
                       uint8_t *SampleRateFilter50Hertz);

  int ReadHVLevel(double *AVG, double *RMS);

  int CheckErrorWR(BYTE *DataS, int NS, BYTE *DataR, int NR);
  int ReadCommand(BYTE *DataS, int NS, BYTE *DataR, int NR);
  int WriteCommand(BYTE *DataS, int NS);
  void FlushBuffers(void);
  bool CheckCRC(BYTE *Data, int N);
  void AddCRC(BYTE *Data, int N);
  BYTE MakeCRC(BYTE *Data, int N);
  void CleanRxBuffer(void);

private:
  std::shared_ptr<MxComm> *_FTDI;
  BYTE m_DataS[NCOM];
  BYTE m_DataR[NCOM];
  bool m_FlgDebugS;
  bool m_FlgDebugR;
  bool m_FlgCheckCRCwrite;
  bool m_FlgCheckCRCread;
  CRITICAL_SECTION _Section;
};
typedef IC_Module ICM;
