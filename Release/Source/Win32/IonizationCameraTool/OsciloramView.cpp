
#include "OsciloramView.h"
using namespace IonizationCameraTool;

#pragma region Windows Form Designer generated code
void OsciloramView::InitializeComponent(void) {
  this->components = (gcnew System::ComponentModel::Container());
  System::Windows::Forms::DataVisualization::Charting::ChartArea ^ chartArea1 =
      (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
  System::Windows::Forms::DataVisualization::Charting::Legend ^ legend1 =
      (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
  System::Windows::Forms::DataVisualization::Charting::Series ^ series1 =
      (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
  this->ResetAllMeasuremnets = (gcnew System::Windows::Forms::Button());
  this->AmplitudeGraph =
      (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
  this->TableAmplitudes = (gcnew System::Windows::Forms::DataGridView());
  this->Column1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
  this->Column2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
  this->label1 = (gcnew System::Windows::Forms::Label());
  this->label2 = (gcnew System::Windows::Forms::Label());
  this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
  this->ReadOsc = (gcnew System::Windows::Forms::Button());
  this->Forced_Start = (gcnew System::Windows::Forms::Button());
  this->OpearionIndicator = (gcnew System::Windows::Forms::Label());
  this->SaveMeauremnts = (gcnew System::Windows::Forms::Button());
  this->Upfate_Aqusition = (gcnew System::Windows::Forms::Button());
  this->CountAmplitudes = (gcnew System::Windows::Forms::Label());
  this->label3 = (gcnew System::Windows::Forms::Label());
  this->TimeExposition = (gcnew System::Windows::Forms::Label());
  this->label10 = (gcnew System::Windows::Forms::Label());
  this->AvergageAmplitude = (gcnew System::Windows::Forms::Label());
  this->label8 = (gcnew System::Windows::Forms::Label());
  this->CounterTrigger = (gcnew System::Windows::Forms::Label());
  this->label5 = (gcnew System::Windows::Forms::Label());
  this->GlobalDosa = (gcnew System::Windows::Forms::Label());
  this->LocalDosa = (gcnew System::Windows::Forms::Label());
  this->label11 = (gcnew System::Windows::Forms::Label());
  this->label12 = (gcnew System::Windows::Forms::Label());
  this->DialogSaveAmplitudes = (gcnew System::Windows::Forms::SaveFileDialog());
  this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
  this->Infinite_Integration = (gcnew System::Windows::Forms::RadioButton());
  this->Timeout_Emited = (gcnew System::Windows::Forms::CheckBox());
  this->Thershold_Trigger = (gcnew System::Windows::Forms::RadioButton());
  this->Forced_Trigger = (gcnew System::Windows::Forms::RadioButton());
  this->Extrenal_Trigger = (gcnew System::Windows::Forms::RadioButton());
  this->MeasureState = (gcnew System::Windows::Forms::CheckBox());
  this->StatePolling = (gcnew System::Windows::Forms::Timer(this->components));
  this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
  this->StripStatusLoadAmplitudes =
      (gcnew System::Windows::Forms::ToolStripStatusLabel());
  this->toolStripProgressBar1 =
      (gcnew System::Windows::Forms::ToolStripProgressBar());
  this->ModuleState = (gcnew System::Windows::Forms::ToolStripStatusLabel());
  this->AmplitudeLoader = (gcnew System::ComponentModel::BackgroundWorker());
  this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
  this->���������������ToolStripMenuItem =
      (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->������������ToolStripMenuItem =
      (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->toolStripPolingTime =
      (gcnew System::Windows::Forms::ToolStripTextBox());
  this->EndMeasure_checkBox = (gcnew System::Windows::Forms::CheckBox());
  this->WautMeasure_checkBox = (gcnew System::Windows::Forms::CheckBox());
  (cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(
       this->AmplitudeGraph))
      ->BeginInit();
  (cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(
       this->TableAmplitudes))
      ->BeginInit();
  this->groupBox3->SuspendLayout();
  this->groupBox2->SuspendLayout();
  this->statusStrip1->SuspendLayout();
  this->menuStrip1->SuspendLayout();
  this->SuspendLayout();
  //
  // ResetAllMeasuremnets
  //
  this->ResetAllMeasuremnets->Location = System::Drawing::Point(5, 254);
  this->ResetAllMeasuremnets->Name = L"ResetAllMeasuremnets";
  this->ResetAllMeasuremnets->Size = System::Drawing::Size(198, 23);
  this->ResetAllMeasuremnets->TabIndex = 13;
  this->ResetAllMeasuremnets->Text = L"�������� ���������� ���������";
  this->ResetAllMeasuremnets->UseVisualStyleBackColor = true;
  this->ResetAllMeasuremnets->Click += gcnew System::EventHandler(
      this, &OsciloramView::ResetAllMeasuremnets_Click);
  //
  // AmplitudeGraph
  //
  chartArea1->AxisX->IntervalAutoMode = System::Windows::Forms::
      DataVisualization::Charting::IntervalAutoMode::VariableCount;
  chartArea1->CursorX->IsUserEnabled = true;
  chartArea1->CursorX->IsUserSelectionEnabled = true;
  chartArea1->CursorY->IsUserEnabled = true;
  chartArea1->CursorY->IsUserSelectionEnabled = true;
  chartArea1->Name = L"ChartArea1";
  this->AmplitudeGraph->ChartAreas->Add(chartArea1);
  legend1->Name = L"Legend1";
  this->AmplitudeGraph->Legends->Add(legend1);
  this->AmplitudeGraph->Location = System::Drawing::Point(12, 45);
  this->AmplitudeGraph->Name = L"AmplitudeGraph";
  series1->ChartArea = L"ChartArea1";
  series1->ChartType = System::Windows::Forms::DataVisualization::Charting::
      SeriesChartType::Spline;
  series1->Legend = L"Legend1";
  series1->Name = L"AmplitidesPoints";
  this->AmplitudeGraph->Series->Add(series1);
  this->AmplitudeGraph->Size = System::Drawing::Size(903, 300);
  this->AmplitudeGraph->TabIndex = 0;
  this->AmplitudeGraph->Text = L"chart1";
  //
  // TableAmplitudes
  //
  this->TableAmplitudes->ColumnHeadersHeightSizeMode =
      System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
  this->TableAmplitudes->Columns->AddRange(
      gcnew cli::array<System::Windows::Forms::DataGridViewColumn ^>(2){
          this->Column1, this->Column2});
  this->TableAmplitudes->Location = System::Drawing::Point(12, 351);
  this->TableAmplitudes->Name = L"TableAmplitudes";
  this->TableAmplitudes->Size = System::Drawing::Size(243, 300);
  this->TableAmplitudes->TabIndex = 1;
  //
  // Column1
  //
  this->Column1->HeaderText = L"�����";
  this->Column1->Name = L"Column1";
  this->Column1->ReadOnly = true;
  //
  // Column2
  //
  this->Column2->HeaderText = L"���������";
  this->Column2->Name = L"Column2";
  this->Column2->ReadOnly = true;
  //
  // label1
  //
  this->label1->AutoSize = true;
  this->label1->Location = System::Drawing::Point(94, 335);
  this->label1->Name = L"label1";
  this->label1->Size = System::Drawing::Size(106, 13);
  this->label1->TabIndex = 2;
  this->label1->Text = L"�������� ��������";
  //
  // label2
  //
  this->label2->AutoSize = true;
  this->label2->Location = System::Drawing::Point(422, 24);
  this->label2->Name = L"label2";
  this->label2->Size = System::Drawing::Size(96, 13);
  this->label2->TabIndex = 3;
  this->label2->Text = L"������ ��������";
  //
  // groupBox3
  //
  this->groupBox3->Controls->Add(this->ReadOsc);
  this->groupBox3->Controls->Add(this->ResetAllMeasuremnets);
  this->groupBox3->Controls->Add(this->Forced_Start);
  this->groupBox3->Controls->Add(this->OpearionIndicator);
  this->groupBox3->Controls->Add(this->SaveMeauremnts);
  this->groupBox3->Controls->Add(this->Upfate_Aqusition);
  this->groupBox3->Controls->Add(this->CountAmplitudes);
  this->groupBox3->Controls->Add(this->label3);
  this->groupBox3->Controls->Add(this->TimeExposition);
  this->groupBox3->Controls->Add(this->label10);
  this->groupBox3->Controls->Add(this->AvergageAmplitude);
  this->groupBox3->Controls->Add(this->label8);
  this->groupBox3->Controls->Add(this->CounterTrigger);
  this->groupBox3->Controls->Add(this->label5);
  this->groupBox3->Controls->Add(this->GlobalDosa);
  this->groupBox3->Controls->Add(this->LocalDosa);
  this->groupBox3->Controls->Add(this->label11);
  this->groupBox3->Controls->Add(this->label12);
  this->groupBox3->Location = System::Drawing::Point(261, 351);
  this->groupBox3->Name = L"groupBox3";
  this->groupBox3->Size = System::Drawing::Size(347, 300);
  this->groupBox3->TabIndex = 5;
  this->groupBox3->TabStop = false;
  this->groupBox3->Text = L"���������";
  //
  // ReadOsc
  //
  this->ReadOsc->Location = System::Drawing::Point(155, 196);
  this->ReadOsc->Name = L"ReadOsc";
  this->ReadOsc->Size = System::Drawing::Size(189, 23);
  this->ReadOsc->TabIndex = 14;
  this->ReadOsc->Text = L"������ �����������";
  this->ReadOsc->UseVisualStyleBackColor = true;
  this->ReadOsc->Click +=
      gcnew System::EventHandler(this, &OsciloramView::ReadOsc_Click);
  //
  // Forced_Start
  //
  this->Forced_Start->Location = System::Drawing::Point(155, 225);
  this->Forced_Start->Name = L"Forced_Start";
  this->Forced_Start->Size = System::Drawing::Size(189, 23);
  this->Forced_Start->TabIndex = 12;
  this->Forced_Start->Text = L"������ ������";
  this->Forced_Start->UseVisualStyleBackColor = true;
  this->Forced_Start->Click +=
      gcnew System::EventHandler(this, &OsciloramView::Forced_Start_Click);
  //
  // OpearionIndicator
  //
  this->OpearionIndicator->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->OpearionIndicator->Location = System::Drawing::Point(10, 170);
  this->OpearionIndicator->Name = L"OpearionIndicator";
  this->OpearionIndicator->Size = System::Drawing::Size(13, 14);
  this->OpearionIndicator->TabIndex = 12;
  //
  // SaveMeauremnts
  //
  this->SaveMeauremnts->Location = System::Drawing::Point(5, 225);
  this->SaveMeauremnts->Name = L"SaveMeauremnts";
  this->SaveMeauremnts->Size = System::Drawing::Size(144, 23);
  this->SaveMeauremnts->TabIndex = 6;
  this->SaveMeauremnts->Text = L"��������� ���������";
  this->SaveMeauremnts->UseVisualStyleBackColor = true;
  this->SaveMeauremnts->Click +=
      gcnew System::EventHandler(this, &OsciloramView::SaveMeauremnts_Click);
  //
  // Upfate_Aqusition
  //
  this->Upfate_Aqusition->Location = System::Drawing::Point(5, 196);
  this->Upfate_Aqusition->Name = L"Upfate_Aqusition";
  this->Upfate_Aqusition->Size = System::Drawing::Size(143, 23);
  this->Upfate_Aqusition->TabIndex = 7;
  this->Upfate_Aqusition->Text = L"�������� ������";
  this->Upfate_Aqusition->UseVisualStyleBackColor = true;
  this->Upfate_Aqusition->Click +=
      gcnew System::EventHandler(this, &OsciloramView::Upfate_Aqusition_Click);
  //
  // CountAmplitudes
  //
  this->CountAmplitudes->AutoSize = true;
  this->CountAmplitudes->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->CountAmplitudes->Location = System::Drawing::Point(163, 140);
  this->CountAmplitudes->Name = L"CountAmplitudes";
  this->CountAmplitudes->Size = System::Drawing::Size(15, 15);
  this->CountAmplitudes->TabIndex = 11;
  this->CountAmplitudes->Text = L"0";
  //
  // label3
  //
  this->label3->AutoSize = true;
  this->label3->Location = System::Drawing::Point(7, 140);
  this->label3->Name = L"label3";
  this->label3->Size = System::Drawing::Size(117, 13);
  this->label3->TabIndex = 10;
  this->label3->Text = L"���������� ��������";
  //
  // TimeExposition
  //
  this->TimeExposition->AutoSize = true;
  this->TimeExposition->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->TimeExposition->Location = System::Drawing::Point(163, 116);
  this->TimeExposition->Name = L"TimeExposition";
  this->TimeExposition->Size = System::Drawing::Size(15, 15);
  this->TimeExposition->TabIndex = 9;
  this->TimeExposition->Text = L"0";
  //
  // label10
  //
  this->label10->AutoSize = true;
  this->label10->Location = System::Drawing::Point(6, 118);
  this->label10->Name = L"label10";
  this->label10->Size = System::Drawing::Size(143, 13);
  this->label10->TabIndex = 8;
  this->label10->Text = L"������������ ����������";
  //
  // AvergageAmplitude
  //
  this->AvergageAmplitude->AutoSize = true;
  this->AvergageAmplitude->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->AvergageAmplitude->Location = System::Drawing::Point(163, 93);
  this->AvergageAmplitude->Name = L"AvergageAmplitude";
  this->AvergageAmplitude->Size = System::Drawing::Size(15, 15);
  this->AvergageAmplitude->TabIndex = 7;
  this->AvergageAmplitude->Text = L"0";
  //
  // label8
  //
  this->label8->AutoSize = true;
  this->label8->Location = System::Drawing::Point(7, 93);
  this->label8->Name = L"label8";
  this->label8->Size = System::Drawing::Size(107, 13);
  this->label8->TabIndex = 6;
  this->label8->Text = L"������� ���������";
  //
  // CounterTrigger
  //
  this->CounterTrigger->AutoSize = true;
  this->CounterTrigger->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->CounterTrigger->Location = System::Drawing::Point(164, 66);
  this->CounterTrigger->Name = L"CounterTrigger";
  this->CounterTrigger->Size = System::Drawing::Size(15, 15);
  this->CounterTrigger->TabIndex = 5;
  this->CounterTrigger->Text = L"0";
  //
  // label5
  //
  this->label5->AutoSize = true;
  this->label5->Location = System::Drawing::Point(7, 65);
  this->label5->Name = L"label5";
  this->label5->Size = System::Drawing::Size(136, 13);
  this->label5->TabIndex = 4;
  this->label5->Text = L"���������� �����������";
  //
  // GlobalDosa
  //
  this->GlobalDosa->AutoSize = true;
  this->GlobalDosa->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
  this->GlobalDosa->Location = System::Drawing::Point(164, 40);
  this->GlobalDosa->Name = L"GlobalDosa";
  this->GlobalDosa->Size = System::Drawing::Size(15, 15);
  this->GlobalDosa->TabIndex = 3;
  this->GlobalDosa->Text = L"0";
  //
  // LocalDosa
  //
  this->LocalDosa->AutoSize = true;
  this->LocalDosa->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
  this->LocalDosa->Location = System::Drawing::Point(164, 16);
  this->LocalDosa->Name = L"LocalDosa";
  this->LocalDosa->Size = System::Drawing::Size(15, 15);
  this->LocalDosa->TabIndex = 2;
  this->LocalDosa->Text = L"0";
  //
  // label11
  //
  this->label11->AutoSize = true;
  this->label11->Location = System::Drawing::Point(7, 43);
  this->label11->Name = L"label11";
  this->label11->Size = System::Drawing::Size(94, 13);
  this->label11->TabIndex = 1;
  this->label11->Text = L"���������� ����";
  //
  // label12
  //
  this->label12->AutoSize = true;
  this->label12->Location = System::Drawing::Point(7, 19);
  this->label12->Name = L"label12";
  this->label12->Size = System::Drawing::Size(90, 13);
  this->label12->TabIndex = 0;
  this->label12->Text = L"��������� ����";
  //
  // DialogSaveAmplitudes
  //
  this->DialogSaveAmplitudes->DefaultExt = L"LOG";
  this->DialogSaveAmplitudes->Filter = L"\"�������� ��������(.LOG)\" |* .LOG ";
  this->DialogSaveAmplitudes->Title = L"���������� ��������� ���������";
  //
  // groupBox2
  //
  this->groupBox2->Controls->Add(this->EndMeasure_checkBox);
  this->groupBox2->Controls->Add(this->WautMeasure_checkBox);
  this->groupBox2->Controls->Add(this->Infinite_Integration);
  this->groupBox2->Controls->Add(this->Timeout_Emited);
  this->groupBox2->Controls->Add(this->Thershold_Trigger);
  this->groupBox2->Controls->Add(this->Forced_Trigger);
  this->groupBox2->Controls->Add(this->Extrenal_Trigger);
  this->groupBox2->Controls->Add(this->MeasureState);
  this->groupBox2->Location = System::Drawing::Point(614, 351);
  this->groupBox2->Name = L"groupBox2";
  this->groupBox2->Size = System::Drawing::Size(346, 119);
  this->groupBox2->TabIndex = 11;
  this->groupBox2->TabStop = false;
  this->groupBox2->Text = L"��������� �����";
  //
  // Infinite_Integration
  //
  this->Infinite_Integration->AutoSize = true;
  this->Infinite_Integration->Location = System::Drawing::Point(146, 89);
  this->Infinite_Integration->Name = L"Infinite_Integration";
  this->Infinite_Integration->Size = System::Drawing::Size(180, 17);
  this->Infinite_Integration->TabIndex = 7;
  this->Infinite_Integration->TabStop = true;
  this->Infinite_Integration->Text = L"����������� ��������������";
  this->Infinite_Integration->UseVisualStyleBackColor = true;
  //
  // Timeout_Emited
  //
  this->Timeout_Emited->AutoCheck = false;
  this->Timeout_Emited->AutoSize = true;
  this->Timeout_Emited->Location = System::Drawing::Point(6, 44);
  this->Timeout_Emited->Name = L"Timeout_Emited";
  this->Timeout_Emited->Size = System::Drawing::Size(124, 17);
  this->Timeout_Emited->TabIndex = 6;
  this->Timeout_Emited->Text = L"��������� �������";
  this->Timeout_Emited->UseVisualStyleBackColor = true;
  //
  // Thershold_Trigger
  //
  this->Thershold_Trigger->AutoCheck = false;
  this->Thershold_Trigger->AutoSize = true;
  this->Thershold_Trigger->Location = System::Drawing::Point(146, 42);
  this->Thershold_Trigger->Name = L"Thershold_Trigger";
  this->Thershold_Trigger->Size = System::Drawing::Size(113, 17);
  this->Thershold_Trigger->TabIndex = 5;
  this->Thershold_Trigger->TabStop = true;
  this->Thershold_Trigger->Text = L"������ �� ������";
  this->Thershold_Trigger->UseVisualStyleBackColor = true;
  //
  // Forced_Trigger
  //
  this->Forced_Trigger->AutoCheck = false;
  this->Forced_Trigger->AutoSize = true;
  this->Forced_Trigger->Location = System::Drawing::Point(146, 65);
  this->Forced_Trigger->Name = L"Forced_Trigger";
  this->Forced_Trigger->Size = System::Drawing::Size(63, 17);
  this->Forced_Trigger->TabIndex = 4;
  this->Forced_Trigger->TabStop = true;
  this->Forced_Trigger->Text = L"������ ";
  this->Forced_Trigger->UseVisualStyleBackColor = true;
  //
  // Extrenal_Trigger
  //
  this->Extrenal_Trigger->AutoCheck = false;
  this->Extrenal_Trigger->AutoSize = true;
  this->Extrenal_Trigger->Location = System::Drawing::Point(146, 19);
  this->Extrenal_Trigger->Name = L"Extrenal_Trigger";
  this->Extrenal_Trigger->Size = System::Drawing::Size(108, 17);
  this->Extrenal_Trigger->TabIndex = 3;
  this->Extrenal_Trigger->TabStop = true;
  this->Extrenal_Trigger->Text = L"������� ������";
  this->Extrenal_Trigger->UseVisualStyleBackColor = true;
  //
  // MeasureState
  //
  this->MeasureState->AutoCheck = false;
  this->MeasureState->AutoSize = true;
  this->MeasureState->Location = System::Drawing::Point(7, 20);
  this->MeasureState->Name = L"MeasureState";
  this->MeasureState->Size = System::Drawing::Size(110, 17);
  this->MeasureState->TabIndex = 0;
  this->MeasureState->Text = L"���� ���������";
  this->MeasureState->UseVisualStyleBackColor = true;
  //
  // StatePolling
  //
  this->StatePolling->Interval = 1500;
  this->StatePolling->Tick +=
      gcnew System::EventHandler(this, &OsciloramView::StatePolling_Tick);
  //
  // statusStrip1
  //
  this->statusStrip1->Items->AddRange(
      gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(3){
          this->StripStatusLoadAmplitudes, this->toolStripProgressBar1,
          this->ModuleState});
  this->statusStrip1->Location = System::Drawing::Point(0, 654);
  this->statusStrip1->Name = L"statusStrip1";
  this->statusStrip1->Size = System::Drawing::Size(1072, 22);
  this->statusStrip1->TabIndex = 12;
  this->statusStrip1->Text = L"statusStrip1";
  //
  // StripStatusLoadAmplitudes
  //
  this->StripStatusLoadAmplitudes->Name = L"StripStatusLoadAmplitudes";
  this->StripStatusLoadAmplitudes->Size = System::Drawing::Size(120, 17);
  this->StripStatusLoadAmplitudes->Text = L"StateLoadAmplitudes";
  //
  // toolStripProgressBar1
  //
  this->toolStripProgressBar1->Name = L"toolStripProgressBar1";
  this->toolStripProgressBar1->Size = System::Drawing::Size(100, 16);
  this->toolStripProgressBar1->Style =
      System::Windows::Forms::ProgressBarStyle::Continuous;
  //
  // ModuleState
  //
  this->ModuleState->Name = L"ModuleState";
  this->ModuleState->Size = System::Drawing::Size(118, 17);
  this->ModuleState->Text = L"toolStripStatusLabel1";
  //
  // AmplitudeLoader
  //
  this->AmplitudeLoader->WorkerReportsProgress = true;
  this->AmplitudeLoader->DoWork +=
      gcnew System::ComponentModel::DoWorkEventHandler(
          this, &OsciloramView::AmplitudeLoader_DoWork);
  this->AmplitudeLoader->ProgressChanged +=
      gcnew System::ComponentModel::ProgressChangedEventHandler(
          this, &OsciloramView::AmplitudeLoader_ProgressChanged);
  this->AmplitudeLoader->RunWorkerCompleted +=
      gcnew System::ComponentModel::RunWorkerCompletedEventHandler(
          this, &OsciloramView::AmplitudeLoader_RunWorkerCompleted);
  //
  // menuStrip1
  //
  this->menuStrip1->Items->AddRange(
      gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(1){
          this->���������������ToolStripMenuItem});
  this->menuStrip1->Location = System::Drawing::Point(0, 0);
  this->menuStrip1->Name = L"menuStrip1";
  this->menuStrip1->Size = System::Drawing::Size(1072, 24);
  this->menuStrip1->TabIndex = 13;
  this->menuStrip1->Text = L"menuStrip1";
  //
  // ���������������ToolStripMenuItem
  //
  this->���������������ToolStripMenuItem->DropDownItems->AddRange(
      gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(1){
          this->������������ToolStripMenuItem});
  this->���������������ToolStripMenuItem->Name =
      L"���������������ToolStripMenuItem";
  this->���������������ToolStripMenuItem->Size = System::Drawing::Size(121, 20);
  this->���������������ToolStripMenuItem->Text = L"��������� ������";
  //
  // ������������ToolStripMenuItem
  //
  this->������������ToolStripMenuItem->DropDownItems->AddRange(
      gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(1){
          this->toolStripPolingTime});
  this->������������ToolStripMenuItem->Name = L"������������ToolStripMenuItem";
  this->������������ToolStripMenuItem->Size = System::Drawing::Size(159, 22);
  this->������������ToolStripMenuItem->Text = L"������ ������";
  //
  // toolStripPolingTime
  //
  this->toolStripPolingTime->Name = L"toolStripPolingTime";
  this->toolStripPolingTime->Size = System::Drawing::Size(152, 23);
  this->toolStripPolingTime->Text = L"200";
  this->toolStripPolingTime->TextChanged += gcnew System::EventHandler(
      this, &OsciloramView::toolStripPolingTime_TextChanged);
  //
  // EndMeasure_checkBox
  //
  this->EndMeasure_checkBox->AutoCheck = false;
  this->EndMeasure_checkBox->AutoSize = true;
  this->EndMeasure_checkBox->Location = System::Drawing::Point(6, 91);
  this->EndMeasure_checkBox->Name = L"EndMeasure_checkBox";
  this->EndMeasure_checkBox->Size = System::Drawing::Size(116, 17);
  this->EndMeasure_checkBox->TabIndex = 9;
  this->EndMeasure_checkBox->Text = L"����� ���������";
  this->EndMeasure_checkBox->UseVisualStyleBackColor = true;
  //
  // WautMeasure_checkBox
  //
  this->WautMeasure_checkBox->AutoCheck = false;
  this->WautMeasure_checkBox->AutoSize = true;
  this->WautMeasure_checkBox->Location = System::Drawing::Point(7, 67);
  this->WautMeasure_checkBox->Name = L"WautMeasure_checkBox";
  this->WautMeasure_checkBox->Size = System::Drawing::Size(140, 17);
  this->WautMeasure_checkBox->TabIndex = 8;
  this->WautMeasure_checkBox->Text = L" �������� ���������";
  this->WautMeasure_checkBox->UseVisualStyleBackColor = true;
  //
  // OsciloramView
  //
  this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
  this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
  this->ClientSize = System::Drawing::Size(1072, 676);
  this->Controls->Add(this->statusStrip1);
  this->Controls->Add(this->menuStrip1);
  this->Controls->Add(this->groupBox3);
  this->Controls->Add(this->groupBox2);
  this->Controls->Add(this->label2);
  this->Controls->Add(this->label1);
  this->Controls->Add(this->TableAmplitudes);
  this->Controls->Add(this->AmplitudeGraph);
  this->FormBorderStyle =
      System::Windows::Forms::FormBorderStyle::FixedToolWindow;
  this->MainMenuStrip = this->menuStrip1;
  this->MaximizeBox = false;
  this->Name = L"OsciloramView";
  this->Text = L"OscilogramView";
  this->Load +=
      gcnew System::EventHandler(this, &OsciloramView::OsciloramView_Load);
  (cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(
       this->AmplitudeGraph))
      ->EndInit();
  (cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(
       this->TableAmplitudes))
      ->EndInit();
  this->groupBox3->ResumeLayout(false);
  this->groupBox3->PerformLayout();
  this->groupBox2->ResumeLayout(false);
  this->groupBox2->PerformLayout();
  this->statusStrip1->ResumeLayout(false);
  this->statusStrip1->PerformLayout();
  this->menuStrip1->ResumeLayout(false);
  this->menuStrip1->PerformLayout();
  this->ResumeLayout(false);
  this->PerformLayout();
}
#pragma endregion
/**********************************************************************************************************/
OsciloramView::OsciloramView(void) {
  InitializeComponent();
  _IC_Module = NULL;
  _AmplBuffer = new vector<int32_t>(2000);
  toolStripPolingTime->Text = StatePolling->Interval.ToString();
  _Statistica = new std::shared_ptr<Statistica>(new Statistica());
  _AmplitudesStoreFile =
      new std::shared_ptr<AmplitudesStoreFile>(new AmplitudesStoreFile());
  DialogSaveAmplitudes->Filter = "Log ����� (*.log)|*.log";
}
/**********************************************************************************************************/
OsciloramView::~OsciloramView() {

  CloseHandle(_Thread);
  if (AmplitudeLoader->IsBusy) {
    AmplitudeLoader->CancelAsync();
  }

  if (components) {
    delete components;
  }
}
/**********************************************************************************************************/
void OsciloramView::UptdateMeasurements(void) {

  OpearionIndicator->BackColor = Color::Red;
  uint8_t _Control = 0;
  uint8_t _State = 0;
  uint32_t _ThersholdTrigger = 0;
  uint16_t _ValueHighVoltage = 0;
  _TimeScaleFactor = 0;
  int64_t _GlobalDosa = 0;
  int64_t _LocalDosa = 0;
  uint8_t _CounterEventsTrigger = 0;
  uint32_t _DurabilityExposition = 0;
  int32_t _AvergageAmplitudeMeasure = 0;
  uint32_t QuanityMeasuredAmplitudes = 0;
  uint32_t ValueDC = 0;
  uint32_t _ArrayOfAmplitudes[2000];

  _IC_Module->get()->ReadParams(
      &_Control, &_State, &_ThersholdTrigger, &_GlobalDosa, &_LocalDosa,
      &_CounterEventsTrigger, &_DurabilityExposition,
      &_AvergageAmplitudeMeasure, &QuanityMeasuredAmplitudes,
      &_ValueHighVoltage, &ValueDC);

  _QuanityMeasuredAmplitudes = QuanityMeasuredAmplitudes;
  ReadAmlitudesFromModule(_AmplBuffer, _QuanityMeasuredAmplitudes);

  WriteToUI(_GlobalDosa, _LocalDosa, _CounterEventsTrigger,
            _DurabilityExposition, _AvergageAmplitudeMeasure,
            QuanityMeasuredAmplitudes);

  if (_DurabilityExposition > 0 && _QuanityMeasuredAmplitudes > 0) {
    _TimeScaleFactor =
        (float)_DurabilityExposition / (float)_QuanityMeasuredAmplitudes;
  }
  if (_QuanityMeasuredAmplitudes > 2000) {

    _QuanityMeasuredAmplitudes = 2000;
  }

  DrawPlot();
  DrawTable();
  OpearionIndicator->BackColor = Color::Green;
};
/**********************************************************************************************************/
void OsciloramView::DrawPlot(void) {
  AmplitudeGraph->Series["AmplitidesPoints"]->Points->Clear();
  for (uint16_t i = 0; i < TotalAmplitudeCount; i++) {
    AmplitudeGraph->Series["AmplitidesPoints"]->Points->AddXY(
        (_TimeScaleFactor * (i + 1)), _AmplBuffer->data()[i]);
  }
}
/**********************************************************************************************************/
void OsciloramView::DrawTable(void) {
  TableAmplitudes->Rows->Clear();
  for (uint16_t i = 0; i < TotalAmplitudeCount; i++) {
    TableAmplitudes->Rows->Add();
    TableAmplitudes->Rows[i]->Cells[0]->Value =
        String::Format("{0:0.00}", (_TimeScaleFactor * (i + 1)));
    TableAmplitudes->Rows[i]->Cells[1]->Value =
        _AmplBuffer->data()[i].ToString();
  }
}
/**********************************************************************************************************/
void OsciloramView::ResetIsConected(void) {
  _isConected = false;
  StatePolling->Enabled = false;
};

/**********************************************************************************************************/
void OsciloramView::SetIsConected(void) {

  _isConected = true;
  StatePolling->Enabled = true;
}

/**********************************************************************************************************/
void OsciloramView::SetSetupView(SetupModuleView ^ Setup) {
  SetupView = Setup;
  SetupView->SetTimeerHandle(StatePolling);
};
/**********************************************************************************************************/
System::Windows::Forms::Timer ^ OsciloramView::GetTimerHandle(void) {
  return StatePolling;
}

/**********************************************************************************************************/
void OsciloramView::WndProc(Message % m) { Form::WndProc(m); }
/**********************************************************************************************************/
void OsciloramView::StopTimer(void) { StatePolling->Enabled = false; }
/**********************************************************************************************************/
void OsciloramView::StartTimer(void) { StatePolling->Enabled = true; }
/**********************************************************************************************************/
void OsciloramView::SetICModuleHandle(std::shared_ptr<ICM> *IC_Module) {
  _IC_Module = IC_Module;
}
/**********************************************************************************************************/
System::Void OsciloramView::AmplitudeLoader_RunWorkerCompleted(
    System::Object ^ sender,
    System::ComponentModel::RunWorkerCompletedEventArgs ^ e) {

  if (this != nullptr) {

    StatePolling->Enabled = true;
    SetupView->WriteParams->Enabled = true;
    SetupView->ReadParams->Enabled = true;
    OpearionIndicator->BackColor = Color::Green;
    StripStatusLoadAmplitudes->Text = "�������� �������� ���������";
    toolStripProgressBar1->Value = TotalAmplitudeCount;
    DrawPlot();
    DrawTable();
    ReadOsc->Enabled = true;
    Forced_Start->Enabled = true;
    Upfate_Aqusition->Enabled = true;
    SaveMeauremnts->Enabled = true;
    ResetAllMeasuremnets->Enabled = true;
  }
}
/**********************************************************************************************************/
bool OsciloramView::CheckIsModuleConected(void) {

  bool Res = false;
  BYTE Name[4];
  BYTE SW[4];
  BYTE HW[4];
  _IC_Module->get()->ReadVersion(Name, HW, SW);
  if (!strcmp((const char *)Name, "IC")) {
    Res = true;
  } else {
    _IC_Module->get()->FlushBuffers();
  }
  return Res;
}
/**********************************************************************************************************/
System::Void OsciloramView::AmplitudeLoader_ProgressChanged(
    System::Object ^ sender,
    System::ComponentModel::ProgressChangedEventArgs ^ e) {
  if (this != nullptr) {
    toolStripProgressBar1->Value = e->ProgressPercentage;
    StripStatusLoadAmplitudes->Text = "��������� " + e->ProgressPercentage +
                                      " ��������  �� " +
                                      TotalAmplitudeCount.ToString();
  }
}
/**********************************************************************************************************/
System::Void
OsciloramView::toolStripPolingTime_TextChanged(System::Object ^ sender,
                                               System::EventArgs ^ e) {
  if (this != nullptr) {
    int TimePoling = 0;
    int ::TryParse(toolStripPolingTime->Text, TimePoling);
    if (TimePoling > 0) {
      StatePolling->Interval = TimePoling;
    }
  }
}

/**********************************************************************************************************/
System::Void OsciloramView::AmplitudeLoader_DoWork(
    System::Object ^ sender, System::ComponentModel::DoWorkEventArgs ^ e) {
  if (this != nullptr) {
    BackgroundWorker ^ Worker = (BackgroundWorker ^) sender;
    _Thread = GetCurrentThread();
    uint8_t OffsetCounter = 0;
    uint32_t AmplitudeCount;
    if (TotalAmplitudeCount >= 1000) {
      TotalAmplitudeCount = 1000;
      _AmplBuffer->resize(2016);
    }
    for (uint32_t i = 0; i < TotalAmplitudeCount; i += 24) {

      _IC_Module->get()->Readmlitudes(&OffsetCounter,
                                      &_AmplBuffer->data()[OffsetCounter * 24]);
      OffsetCounter++;
      Worker->ReportProgress(i);
    }
  }
}
/**********************************************************************************************************/
System::Void OsciloramView::ReadOsc_Click(System::Object ^ sender,
                                          System::EventArgs ^ e) {

  if (this != nullptr) {
    if (CheckModuleConectionState()) {
      if (!AmplitudeLoader->IsBusy) {
        if (AqusitionIsInProgress()) {
          MessageBox::Show(
              "������ �������� ��������� �� ����� �������� ���������!");
          return;
        }

        SetupView->WriteParams->Enabled = false;
        SetupView->ReadParams->Enabled = false;
        StatePolling->Enabled = false;
        ReadOsc->Enabled = false;
        Forced_Start->Enabled = false;
        Upfate_Aqusition->Enabled = false;
        SaveMeauremnts->Enabled = false;
        ResetAllMeasuremnets->Enabled = false;
        UpdateMainData();
        AmplitudeLoader->RunWorkerAsync();

      } else {
        MessageBox::Show("��������� �������� ������!");
      }

    } else {
      MessageBox::Show("��� �������� ������������ � ������!");
    }
  }
}

/**********************************************************************************************************/
System::Void OsciloramView::StatePolling_Tick(System::Object ^ sender,
                                              System::EventArgs ^ e) {
  if (!CheckModuleConectionState()) {
    return;
  }
  ReadStatePoll();
}
/**********************************************************************************************************/
System::Void OsciloramView::ResetAllMeasuremnets_Click(System::Object ^ sender,
                                                       System::EventArgs ^ e) {
  if (this != nullptr) {
    if (CheckModuleConectionState()) {
      if (!AmplitudeLoader->IsBusy) {
        if (AqusitionIsInProgress()) {
          MessageBox::Show(
              "����� ������ � ��������� ������ �� ����� ��������� ��������!");
          return;
        }

        _IC_Module->get()->CleanAllMeasurement();
        TableAmplitudes->Rows->Clear();
        AmplitudeGraph->Series["AmplitidesPoints"]->Points->Clear();
      } else {
        MessageBox::Show("���� �������� ��������! ��������� �������� ������ � "
                         "��������� �������!");
      }

    } else {
      MessageBox::Show("��� �������� ������������ � ������!");
    }
  }
}

/**********************************************************************************************************/
bool OsciloramView::AqusitionIsInProgress(void) {

  bool flag = false;

  uint8_t _Control;
  uint8_t _State;
  uint32_t _ThersholdTrigger;
  uint16_t _ValueHighVoltage;
  int64_t _GlobalDosa;
  int64_t _LocalDosa;
  uint8_t _CounterEventsTrigger;
  uint32_t _DurabilityExposition;
  int32_t _AvergageAmplitudeMeasure;
  uint32_t _QuanityMeasuredAmplitudes;
  uint32_t _DCLevel;
  StateWord StateW;

  _IC_Module->get()->ReadParams(
      &_Control, &_State, &_ThersholdTrigger, &_GlobalDosa, &_LocalDosa,
      &_CounterEventsTrigger, &_DurabilityExposition,
      &_AvergageAmplitudeMeasure, &_QuanityMeasuredAmplitudes,
      &_ValueHighVoltage, &_DCLevel);

  StateW.Raw = _State;

  if (StateW.Bits.AqusitionState == AqusitionState_InProgress) {
    flag = true;
  }

  return flag;
}

/**********************************************************************************************************/
System::Void OsciloramView::SaveMeauremnts_Click(System::Object ^ sender,
                                                 System::EventArgs ^ e) {

  if (this != nullptr) {

    if (AqusitionIsInProgress()) {
      MessageBox::Show(
          "��������� �������� �������� �� ����� �������� ���������!��������� "
          "����� ����������� ������� ���������!");
      return;
    }
    if (!CheckModuleConectionState()) {
      MessageBox::Show("��� �������� ������������ � ������!");
      return;
    }

    if (!AmplitudeLoader->IsBusy) {
      DialogSaveAmplitudes->FileName = "";
      if (StoreAnplitudesEvent()) {
        MessageBox::Show("��������� �������� �� ���� ���������!");
      } else {
        MessageBox::Show("��������� �������� �� ���� �������� �������������!");
      }

    } else {
      MessageBox::Show("���� �������� ��������! ��������� �������� ������ � "
                       "��������� �������!");
    }
  }
}
/**********************************************************************************************************/

bool OsciloramView::StoreAnplitudesEvent(void) {

  bool Flag = false;
  OpearionIndicator->BackColor = Color::Red;

  uint8_t _Control;
  uint8_t _State;
  uint32_t _ThersholdTrigger;
  uint16_t _ValueHighVoltage;
  int64_t _GlobalDosa;
  int64_t _LocalDosa;
  uint8_t _CounterEventsTrigger;
  uint32_t _DurabilityExposition;
  int32_t _AvergageAmplitudeMeasure;
  uint32_t _QuanityMeasuredAmplitudes;
  uint32_t ValueDC;

  _IC_Module->get()->ReadParams(
      &_Control, &_State, &_ThersholdTrigger, &_GlobalDosa, &_LocalDosa,
      &_CounterEventsTrigger, &_DurabilityExposition,
      &_AvergageAmplitudeMeasure, &_QuanityMeasuredAmplitudes,
      &_ValueHighVoltage, &ValueDC);

  if (_QuanityMeasuredAmplitudes < 24 || _QuanityMeasuredAmplitudes >= 1000) {
    _AmplBuffer->resize(2016);
  }

  ReadAmlitudesFromModule(_AmplBuffer, _QuanityMeasuredAmplitudes);
  System::Windows::Forms::DialogResult Result =
      DialogSaveAmplitudes->ShowDialog();

  if (Result == System::Windows::Forms::DialogResult::OK) {
    Flag = true;
    std::string FileName =
        interop::marshal_as<std::string>(DialogSaveAmplitudes->FileName);

    _AmplitudesStoreFile->get()->SaveProtocolFile(
        FileName.c_str(), _Control, _State, _ThersholdTrigger,
        _ValueHighVoltage, _AmplBuffer, _GlobalDosa, _LocalDosa,
        _CounterEventsTrigger, _DurabilityExposition, _AvergageAmplitudeMeasure,
        _QuanityMeasuredAmplitudes, ValueDC);

    OpearionIndicator->BackColor = Color::Green;
  }

  return Flag;
}
/**********************************************************************************************************/
void OsciloramView::ReadAmlitudesFromModule(vector<int32_t> *ArrayOfAmplitudes,
                                            uint32_t AmplitudeCount) {
  uint8_t OffsetCounter = 0;
  int32_t TemppBUff[2016];

  if (AmplitudeCount >= 1000) {
    AmplitudeCount = 1000;
  }

  for (uint32_t i = 0; i < AmplitudeCount; i += 30) {
    short PositionInBuffer = OffsetCounter * 30;
    _IC_Module->get()->Readmlitudes(&OffsetCounter,
                                    &TemppBUff[PositionInBuffer]);
    OffsetCounter++;
  }

  for (uint32_t i = 0; i < AmplitudeCount; i++) {
    ArrayOfAmplitudes->data()[i] = TemppBUff[i];
  }
}
/**********************************************************************************************************/
System::Void OsciloramView::Upfate_Aqusition_Click(System::Object ^ sender,
                                                   System::EventArgs ^ e) {

  if (!CheckModuleConectionState()) {
    MessageBox::Show("��� �������� ������������ � ������!");
    return;
  }
  UpdateMainData();
}
/**********************************************************************************************************/
void OsciloramView::UpdateMainData(void) {

  if (this != nullptr) {

    uint8_t _Control;
    uint8_t _State;
    uint32_t _ThersholdTrigger;
    uint16_t _ValueHighVoltage;
    int64_t _GlobalDosa;
    int64_t _LocalDosa;
    uint8_t _CounterEventsTrigger;
    uint32_t _DurabilityExposition;
    int32_t _AvergageAmplitudeMeasure;
    uint32_t _QuanityMeasuredAmplitudes;
    uint32_t _DCLevel;
    StateWord StateW;

    _IC_Module->get()->ReadParams(
        &_Control, &_State, &_ThersholdTrigger, &_GlobalDosa, &_LocalDosa,
        &_CounterEventsTrigger, &_DurabilityExposition,
        &_AvergageAmplitudeMeasure, &_QuanityMeasuredAmplitudes,
        &_ValueHighVoltage, &_DCLevel);

    WriteToUI(_GlobalDosa, _LocalDosa, _CounterEventsTrigger,
              _DurabilityExposition, _AvergageAmplitudeMeasure,
              _QuanityMeasuredAmplitudes);

    toolStripProgressBar1->Value = 0;
    StripStatusLoadAmplitudes->Text = "������ ��������� ���������!";
    TotalAmplitudeCount = _QuanityMeasuredAmplitudes;
    if (_QuanityMeasuredAmplitudes >= 1000) {
      TotalAmplitudeCount = 1000;
    }

    toolStripProgressBar1->Maximum = TotalAmplitudeCount;
    _TimeScaleFactor =
        (float)_DurabilityExposition / (float)_QuanityMeasuredAmplitudes;
  }
}

/**********************************************************************************************************/
System::Void OsciloramView::OsciloramView_Load(System::Object ^ sender,
                                               System::EventArgs ^ e) {}
/**********************************************************************************************************/

void OsciloramView::ReadStatePoll(void) {

  if (this != nullptr) {

    uint8_t _Control;
    uint8_t _State;
    uint32_t _ThersholdTrigger;
    uint16_t _ValueHighVoltage;
    int64_t _GlobalDosa;
    int64_t _LocalDosa;
    uint8_t _CounterEventsTrigger;
    uint32_t _DurabilityExposition;
    int32_t _AvergageAmplitudeMeasure;
    uint32_t _QuanityMeasuredAmplitudes;
    uint32_t _DCLevel;
    StateWord StateW;

    if (!AmplitudeLoader->IsBusy) {
      _IC_Module->get()->ReadParams(
          &_Control, &_State, &_ThersholdTrigger, &_GlobalDosa, &_LocalDosa,
          &_CounterEventsTrigger, &_DurabilityExposition,
          &_AvergageAmplitudeMeasure, &_QuanityMeasuredAmplitudes,
          &_ValueHighVoltage, &_DCLevel);
      StateW.Raw = _State;
      if (CheckAQComplete(StateW, _QuanityMeasuredAmplitudes,
                          _DurabilityExposition)) {
        WriteToUI(_GlobalDosa, _LocalDosa, _CounterEventsTrigger,
                  _DurabilityExposition, _AvergageAmplitudeMeasure,
                  _QuanityMeasuredAmplitudes);
      }
      CheckAQInProgess(StateW);
    }
  }
}

/**********************************************************************************************************/
void OsciloramView::WriteToUI(int64_t &_GlobalDosa, int64_t &_LocalDosa,
                              uint8_t &_CounterEventsTrigger,
                              uint32_t &_DurabilityExposition,
                              int32_t &_AvergageAmplitudeMeasure,
                              uint32_t &_QuanityMeasuredAmplitudes)

{

  ImpulseCounter = _CounterEventsTrigger;
  TimeExposition->Text = _DurabilityExposition.ToString();
  AvergageAmplitude->Text = _AvergageAmplitudeMeasure.ToString();
  CounterTrigger->Text = _CounterEventsTrigger.ToString();
  GlobalDosa->Text = _GlobalDosa.ToString();
  LocalDosa->Text = _LocalDosa.ToString();
  CountAmplitudes->Text = _QuanityMeasuredAmplitudes.ToString();

  if (ImpulseCounter < _CounterEventsTrigger) {
    StripStatusLoadAmplitudes->Text =
        "�������� ����� ������ ���������!����� ������ ������������!";
  }
}
/**********************************************************************************************************/
bool OsciloramView::CheckModuleConectionState(void) {

  bool Flag = false;
  if (CheckIsModuleConected()) {
    ModuleState->ForeColor = Color::Green;
    ModuleState->Text = "������ ���������!";
    Flag = true;
  } else {
    ModuleState->ForeColor = Color::Red;
    ModuleState->Text = "������ �� ���������!";
  }
  return Flag;
}

/**********************************************************************************************************/
bool OsciloramView::CheckAQComplete(StateWord &StateW,
                                    uint32_t &_QuanityMeasuredAmplitudes,
                                    uint32_t &_DurabilityExposition) {

  bool Flag = false;
  if (StateW.Bits.AqusitionState == AqusitionState_Complete) {
    if (!AmplitudeLoader->IsBusy) {

      StripStatusLoadAmplitudes->Text = "������ �������� ������� ���������!";
      Flag = true;
      MeasureState->Checked = false;
      if (StateW.Bits.SourceStart != SourceStart_InfiniteIntegration) {
        if (!LastStateMeasure) {
          OpearionIndicator->BackColor = Color::Red;
          _TimeScaleFactor =
              (float)_DurabilityExposition / (float)_QuanityMeasuredAmplitudes;
          TotalAmplitudeCount = _QuanityMeasuredAmplitudes;

          if (_QuanityMeasuredAmplitudes >= 1000) {
            TotalAmplitudeCount = 1000;
          }
        }

        LastStateMeasure = true;
      }
    }
  }
  return Flag;
}
/**********************************************************************************************************/
void OsciloramView::CheckAQInProgess(StateWord &StateW) {

  WautMeasure_checkBox->Checked = StateW.Bits.WaitMeasure;
  EndMeasure_checkBox->Checked = StateW.Bits.EndMeasure;
  if (StateW.Bits.AqusitionState == AqusitionState_InProgress) {
    MeasureState->Checked = true;
    StripStatusLoadAmplitudes->Text = "������ ���������� ���������!";
    if (StateW.Bits.SourceStart != SourceStart_InfiniteIntegration) {
      LastStateMeasure = false;
    }

    CheckSourceStartIndicator(StateW);
    CheckTimeOEventIndicator(StateW);
  }
}
/**********************************************************************************************************/
void OsciloramView::CheckSourceStartIndicator(StateWord &StateW) {

  switch (StateW.Bits.SourceStart) {

  case SourceStart_Forced: {
    Forced_Trigger->Checked = true;
    Extrenal_Trigger->Checked = false;
    Thershold_Trigger->Checked = false;
    Infinite_Integration->Checked = false;
    break;
  }

  case SourceStart_External: {
    Forced_Trigger->Checked = false;
    Extrenal_Trigger->Checked = true;
    Thershold_Trigger->Checked = false;
    Infinite_Integration->Checked = false;
    break;
  }

  case SourceStart_Thershold: {

    Extrenal_Trigger->Checked = false;
    Thershold_Trigger->Checked = true;
    Forced_Trigger->Checked = false;
    Infinite_Integration->Checked = false;
    break;
  }

  case SourceStart_InfiniteIntegration: {
    Extrenal_Trigger->Checked = false;
    Thershold_Trigger->Checked = false;
    Forced_Trigger->Checked = false;
    Infinite_Integration->Checked = true;
    break;
  }
  }
}
/**********************************************************************************************************/
void OsciloramView::CheckTimeOEventIndicator(StateWord &StateW) {

  if (StateW.Bits.TimeoutEventIndicator ==
      TimeoutEventIndicator_Measure_Normal) {

    Timeout_Emited->Checked = false;
  }

  if (StateW.Bits.TimeoutEventIndicator ==
      TimeoutEventIndicator_Measure_Triggered) {

    Timeout_Emited->Checked = true;
  }
}
/**********************************************************************************************************/
System::Void OsciloramView::Forced_Start_Click(System::Object ^ sender,
                                               System::EventArgs ^ e) {

  if (this != nullptr) {
    uint16_t TimeAqusiotion = SetupView->GetTimeHV();
    if (CheckModuleConectionState()) {
      if (!AmplitudeLoader->IsBusy) {
        if (AqusitionIsInProgress()) {
          MessageBox::Show(
              "������ ������ �������� �� ����� �������� ���������!");
          return;
        }

        _IC_Module->get()->StartForcedMeaure(&TimeAqusiotion);
      } else {
        MessageBox::Show("���� �������� ��������! ��������� �������� ������ � "
                         "��������� �������!");
      }

    } else {
      MessageBox::Show("��� �������� ������������ � ������!");
    }
  }
}
/**********************************************************************************************************/
