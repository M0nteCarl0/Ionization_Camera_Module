#pragma once
class Statistica {
public:
  Statistica(void);
  ~Statistica(void);

  template <typename T> T Min1D(T *SourceBuffer, size_t CountElements) {
    T Min = ArrayOfAmplitudes[0];
    for (size_t i = 0; i < AmplitudeCount; i += 24) {
      if (ArrayOfAmplitudes[i] < Min) {
        Min = ArrayOfAmplitudes[i];
      }
    }
    return Min;
  };

  template <typename T> T Max1D(T *SourceBuffer, size_t CountElements) {
    T Max = ArrayOfAmplitudes[0];
    for (size_t i = 0; i < AmplitudeCount; i += 24) {
      if (ArrayOfAmplitudes[i] > Max) {
        Max = ArrayOfAmplitudes[i];
      }
    }
    return Max;
  };
};
