//
//  autor: Vitaly Kalendarev, kalendarev@roentgenprom.ru
//
//  date: 21.10.2014
//
#include "stdafx.h"
#include "Event.h"
#include "KY5_Commands_19200.h"
Event WaitComplte("Waiteble");


DWORD WaitebleThreadStart(void* ptr)
{
	KY5_Commands_19200 * Recipient = new KY5_Commands_19200();
	WaitComplte.Reset();
	Recipient = (KY5_Commands_19200*)ptr;
	if (Recipient->WaitForPreparingStart(3000, 300))
	{
		WaitComplte.Set();

	}
	return 0x92;
}

DWORD WaitebleThreadHV(void* ptr)
{
	KY5_Commands_19200 * Recipient = new KY5_Commands_19200();
	WaitComplte.Reset();
	Recipient = (KY5_Commands_19200*)ptr;
	Recipient->Snapshot_Preparing();
	if (Recipient->WaitForPreparingStart(3000, 300))
	{
		Recipient->Snapshot_HV_On();
		Recipient->WaitForSnapshotFinish(8000, 300);
		Recipient->Snapshot_HV_Off();

	
	}
	WaitComplte.Set();
	return 0x92;
}



	DWORD WaitebleThreadFinish(void* ptr)
	{
		KY5_Commands_19200 * Recipient = new KY5_Commands_19200();
		WaitComplte.Reset();
		Recipient = (KY5_Commands_19200*)ptr;
	    
		if(Recipient->WaitForSnapshotFinish(8000, 300))
		{
			WaitComplte.Set();

		}
		return 0x94;

}

bool KY5_Commands_19200:: HVActionAsyncEx(void)
{
 bool Res = false;
 WaitComplte.Reset();
 HANDLE thWS = CreateThread(NULL, 1024, (LPTHREAD_START_ROUTINE)WaitebleThreadHV, (LPVOID(this)),NULL, NULL);
 if(WaitAsyncAction(11000))
 {
    ReadRegisters();
	Res = true;
 };
 return Res;
}


bool KY5_Commands_19200::HVActionAsync(void)
{
 bool Res = false;
 Snapshot_Preparing();
 AsyncWaitPrepareStart();
 if(WaitAsyncAction(3000))
 {
	 Snapshot_HV_On();
	 if(AsyncWaitPrepareFinish())
	 {
		 if(WaitAsyncAction(6000))
		 {
			Snapshot_HV_Off();
			ReadRegisters();
			Res=true;
		 }
	 }

 }
 return Res;

}


KY5_Commands_19200::KY5_Commands_19200(MxComm * const Comm,const int MultipleModeTimeoutToSet,const bool PrintFlag)
  :IKY5_Commands(Comm,MultipleModeTimeoutToSet,PrintFlag)
{

}

bool KY5_Commands_19200:: WaitAsyncAction(int Time)
{
	bool Res = false;
	WaitComplte.Wait(Time);
	if(WaitComplte.IsEmited())
	{
	Res = true;
	}
	return Res;

}

bool KY5_Commands_19200::AsyncWaitPrepareStart(void)
{

	bool Res = false;
	WaitComplte.Reset();
	HANDLE thWS = CreateThread(NULL, 1024, (LPTHREAD_START_ROUTINE)WaitebleThreadStart, (LPVOID(this)),NULL, NULL);
	return Res;
}

bool KY5_Commands_19200::AsyncWaitPrepareFinish(void)
{


	bool Res = false;
	WaitComplte.Reset();
	HANDLE thWS = CreateThread(NULL, 1024, (LPTHREAD_START_ROUTINE)WaitebleThreadFinish, (LPVOID(this)),NULL,NULL);
	


	return Res;

	
}

FTDI_ActionInfo KY5_Commands_19200::GetDeviceType(BYTE * const DeviceType) const
{
	return ReadRegDat(KY5_DEVICE_TYPE_ADDRESS,DeviceType);
}

FTDI_ActionInfo KY5_Commands_19200::GetURPVersion(BYTE * const URPVersion) const
{
	return ReadRegDat(KY5_DEVICE_NUMBER_7_HO_BITS_ADDRESS,URPVersion);
}

FTDI_ActionInfo KY5_Commands_19200::GetFocusState(BYTE * const State) const
{
	return ReadRegDat(KY5_FOCUS_ADDRESS,State);
}

FTDI_ActionInfo KY5_Commands_19200::GetAnodeHV(int * const AnodeHV) const
{
	const FTDI_ActionInfo Info = ReadValue37(AnodeHV,KY5_GIVEN_ANODE_VOLTAGE_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_VOLTAGE_7_HO_BITS_ADDRESS);
	
	*AnodeHV /= KY5_ANODE_HV_COEFFICIENT;

	return Info;
}

FTDI_ActionInfo KY5_Commands_19200::GetAnodeCurrent(int * const AnodeCurrent) const
{
	return ReadValue37(AnodeCurrent,KY5_GIVEN_ANODE_CURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_CURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo KY5_Commands_19200::GetAnodeSmallFocusPreCurrent(int * const AnodeSmallFocusPreCurrent) const
{
	return ReadValue37(AnodeSmallFocusPreCurrent,KY5_GIVEN_ANODE_SMALL_FOCUS_PRECURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_SMALL_FOCUS_PRECURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo KY5_Commands_19200::GetAnodeSmallFocusCurrent(int * const AnodeSmallFocusCurrent) const
{
	return ReadValue37(AnodeSmallFocusCurrent,KY5_GIVEN_ANODE_SMALL_FOCUS_CURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_SMALL_FOCUS_CURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo KY5_Commands_19200::GetAnodeBigFocusPreCurrent(int * const AnodeBigFocusPreCurrent) const
{
	return ReadValue37(AnodeBigFocusPreCurrent,KY5_GIVEN_ANODE_BIG_FOCUS_PRECURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_BIG_FOCUS_PRECURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo KY5_Commands_19200::GetAnodeBigFocusCurrent(int * const AnodeBigFocusCurrent) const
{
	return ReadValue37(AnodeBigFocusCurrent,KY5_GIVEN_ANODE_BIG_FOCUS_CURRENT_3_LO_BITS_ADDRESS,KY5_GIVEN_ANODE_BIG_FOCUS_CURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo KY5_Commands_19200::GetMAS(float * const MAS) const
{
	int mAs = 0;

	FTDI_ActionInfo Info = ReadValue77(&mAs,KY5_GIVEN_MAS_7_LO_BITS_ADDRESS,KY5_GIVEN_MAS_7_HO_BITS_ADDRESS);

	*MAS = (float)mAs / MASCoefficient;

	return Info;
}

FTDI_ActionInfo KY5_Commands_19200::GetExpositionTime(int * const ExpositionTime) const
{
	FTDI_ActionInfo Info = ReadValue77(ExpositionTime,KY5_GIVEN_EXPOSITION_TIME_7_LO_BITS_ADDRESS,KY5_GIVEN_EXPOSITION_TIME_7_HO_BITS_ADDRESS);

	*ExpositionTime = (int)(*ExpositionTime / ExpositionTimeCoefficient);

	return Info;
}

FTDI_ActionInfo KY5_Commands_19200::GetMaxAnodeCurrent(BYTE * const MaxAnodeCurrent) const
{
	return ReadRegDat(KY5_MAX_ANODE_CURRENT_ADDRESS,MaxAnodeCurrent);
}

FTDI_ActionInfo KY5_Commands_19200::GetMaxAnodeHV(BYTE * const MaxAnodeHV) const
{
	return ReadRegDat(KY5_MAX_ANODE_VOLTAGE_ADDRESS,MaxAnodeHV);
}

FTDI_ActionInfo KY5_Commands_19200::GetMaxAnodeSmallFocusPreCurrent(BYTE * const MaxAnodeSmallFocusPreCurrent) const
{
	return ReadRegDat(KY5_MAX_ANODE_SMALL_FOCUS_PRECURRENT_ADDRESS,MaxAnodeSmallFocusPreCurrent);
}

FTDI_ActionInfo KY5_Commands_19200::GetMaxAnodeSmallFocusCurrent(BYTE * const MaxAnodeSmallFocusCurrent) const
{
	return ReadRegDat(KY5_MAX_ANODE_SMALL_FOCUS_CURRENT_ADDRESS,MaxAnodeSmallFocusCurrent);
}

FTDI_ActionInfo KY5_Commands_19200::GetMaxAnodeBigFocusPreCurrent(BYTE * const MaxAnodeBigFocusPreCurrent) const
{
	return ReadRegDat(KY5_MAX_ANODE_BIG_FOCUS_PRECURRENT_ADDRESS,MaxAnodeBigFocusPreCurrent);
}

FTDI_ActionInfo KY5_Commands_19200::GetMaxAnodeBigFocusCurrent(BYTE * const MaxAnodeBigFocusCurrent) const
{
	return ReadRegDat(KY5_MAX_ANODE_BIG_FOCUS_CURRENT_ADDRESS,MaxAnodeBigFocusCurrent);
}

FTDI_ActionInfo KY5_Commands_19200::GetMeasuredAnodeHV(int * const MeasuredAnodeHV) const
{
	const FTDI_ActionInfo Info = ReadValue37(MeasuredAnodeHV,KY5_MEASURED_POSITIVE_ANODE_VOLTAGE_3_LO_BITS_ADDRESS,KY5_MEASURED_POSITIVE_ANODE_VOLTAGE_7_HO_BITS_ADDRESS);

	*MeasuredAnodeHV /= KY5_ANODE_HV_COEFFICIENT;

	return Info;
}

FTDI_ActionInfo KY5_Commands_19200::GetMeasuredAnodePreCurrent(int * const MeasuredAnodePreCurrent) const
{
	return ReadValue37(MeasuredAnodePreCurrent,KY5_MEASURED_PRECURRENT_3_LO_BITS_ADDRESS,KY5_MEASURED_PRECURRENT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo KY5_Commands_19200::GetMeasuredPositiveAnodeCurrent(int * const MeasuredPositiveAnodeCurrent) const
{
	FTDI_ActionInfo Info = ReadValue37(MeasuredPositiveAnodeCurrent,KY5_MEASURED_POSITIVE_ANODE_CURRENT_3_LO_BITS_ADDRESS,KY5_MEASURED_POSITIVE_ANODE_CURRENT_7_HO_BITS_ADDRESS);

	*MeasuredPositiveAnodeCurrent = (int)(*MeasuredPositiveAnodeCurrent / KY5_MEASURED_ANODE_CURRENT_HIDDEN_COEFFICIENT * MeasuredAnodeCurrentCoefficient);

	return Info;
}

FTDI_ActionInfo KY5_Commands_19200::GetMeasuredNegativeAnodeCurrent(int * const MeasuredNegativeAnodeCurrent) const
{
	FTDI_ActionInfo Info = ReadValue37(MeasuredNegativeAnodeCurrent,KY5_MEASURED_NEGATIVE_ANODE_CURRENT_3_LO_BITS_ADDRESS,KY5_MEASURED_NEGATIVE_ANODE_CURRENT_7_HO_BITS_ADDRESS);
	
	*MeasuredNegativeAnodeCurrent = (int)(*MeasuredNegativeAnodeCurrent / KY5_MEASURED_ANODE_CURRENT_HIDDEN_COEFFICIENT * MeasuredAnodeCurrentCoefficient);
	
	return Info;
}

FTDI_ActionInfo KY5_Commands_19200::GetMeasuredAnodeCurrentDuringSnapshot(int * const MeasuredAnodeCurrentDuringSnapshot) const
{
	return ReadValue37(MeasuredAnodeCurrentDuringSnapshot,KY5_MEASURED_CURRENT_DURING_SNAPSHOT_3_LO_BITS_ADDRESS,KY5_MEASURED_CURRENT_DURING_SNAPSHOT_7_HO_BITS_ADDRESS);
}

FTDI_ActionInfo KY5_Commands_19200::GetMeasuredMAS(float * const MeasuredMAS) const
{
	int mAs = 0;

	FTDI_ActionInfo Info = ReadValue77(&mAs,KY5_MEASURED_MAS_7_LO_BITS_ADDRESS,KY5_MEASURED_MAS_7_HO_BITS_ADDRESS);

	*MeasuredMAS = (float)mAs / MASCoefficient;

	return Info;
}

FTDI_ActionInfo KY5_Commands_19200::GetMeasuredExpositionTime(int * const MeasuredExpositionTime) const
{
	FTDI_ActionInfo Info = ReadValue77(MeasuredExpositionTime,KY5_MEASURED_EXPOSITION_TIME_7_LO_BITS_ADDRESS,KY5_MEASURED_EXPOSITION_TIME_7_HO_BITS_ADDRESS);
	
	*MeasuredExpositionTime = (int)(*MeasuredExpositionTime / ExpositionTimeCoefficient);
	
	return Info;
}

FTDI_ActionInfo KY5_Commands_19200::GetBlockings1(BYTE * const Blockings1) const
{
	return ReadRegDat(KY5_BLOCKINGS_1_ADDRESS,Blockings1);
}

FTDI_ActionInfo KY5_Commands_19200::GetBlockings2(BYTE * const Blockings2) const
{
	return ReadRegDat(KY5_BLOCKINGS_2_ADDRESS,Blockings2);
}

FTDI_ActionInfo KY5_Commands_19200::GetBlockings3(BYTE * const Blockings3) const
{
	return ReadRegDat(KY5_BLOCKINGS_3_ADDRESS,Blockings3);
}

FTDI_ActionInfo KY5_Commands_19200::GetMessages(BYTE * const Messages) const
{
	return ReadRegDat(KY5_MESSAGES_ADDRESS,Messages);
}

FTDI_ActionInfo KY5_Commands_19200::GetWarnings(BYTE * const Warnings) const
{
	return ReadRegDat(KY5_WARNINGS_ADDRESS,Warnings);
}

FTDI_ActionInfo KY5_Commands_19200::ReadRegisters()
{
	const FTDI_ActionInfo GetDeviceTypeInfo = GetDeviceType(&DeviceType);
	
	const FTDI_ActionInfo GetFocusStateInfo = GetFocusState(&FocusMode);
	
	const FTDI_ActionInfo GetBlockings1Info = GetBlockings1(&Blockings_1);
	const FTDI_ActionInfo GetBlockings2Info = GetBlockings2(&Blockings_2);
	const FTDI_ActionInfo GetBlockings3Info = GetBlockings3(&Blockings_3);
	const FTDI_ActionInfo GetMessagesInfo   = GetMessages(&Messages);
	const FTDI_ActionInfo GetWarningsInfo   = GetWarnings(&Warnings);

	const FTDI_ActionInfo GetAnodeHVInfo                   = GetAnodeHV(&GivenAnodeHV);
	const FTDI_ActionInfo GetAnodeCurrentInfo              = GetAnodeCurrent(&GivenAnodeCurrent);
	const FTDI_ActionInfo GetAnodeSmallFocusPreCurrentInfo = GetAnodeSmallFocusPreCurrent(&GivenAnodeSmallFocusPreCurrent);
	const FTDI_ActionInfo GetAnodeSmallFocusCurrentInfo    = GetAnodeSmallFocusCurrent(&GivenAnodeSmallFocusCurrent);
	const FTDI_ActionInfo GetAnodeBigFocusPreCurrentInfo   = GetAnodeBigFocusPreCurrent(&GivenAnodeBigFocusPreCurrent);
	const FTDI_ActionInfo GetAnodeBigFocusCurrentInfo      = GetAnodeBigFocusCurrent(&GivenAnodeBigFocusCurrent);

	const FTDI_ActionInfo GetMASInfo = GetMAS(&GivenMAS);
	const FTDI_ActionInfo GetExpositionTimeInfo = GetExpositionTime(&GivenExpositionTime);

	const FTDI_ActionInfo GetMaxAnodeCurrentInfo              = GetMaxAnodeCurrent(&GivenMaxAnodeCurrent);
	const FTDI_ActionInfo GetMaxAnodeHVInfo                   = GetMaxAnodeHV(&GivenMaxAnodeHV);
	const FTDI_ActionInfo GetMaxAnodeSmallFocusPreCurrentInfo = GetMaxAnodeSmallFocusPreCurrent(&GivenMaxAnodeSmallFocusPreCurrent);
	const FTDI_ActionInfo GetMaxAnodeSmallFocusCurrentInfo    = GetMaxAnodeSmallFocusCurrent(&GivenMaxAnodeSmallFocusCurrent);
	const FTDI_ActionInfo GetMaxAnodeBigFocusPreCurrentInfo   = GetMaxAnodeBigFocusPreCurrent(&GivenMaxAnodeBigFocusPreCurrent);
	const FTDI_ActionInfo GetMaxAnodeBigFocusCurrentInfo      = GetMaxAnodeBigFocusCurrent(&GivenMaxAnodeBigFocusCurrent);

	const FTDI_ActionInfo GetMeasuredAnodeHVInfo = GetMeasuredAnodeHV(&MeasuredAnodeHV);
	const FTDI_ActionInfo GetMeasuredPositiveAnodeCurrentInfo = GetMeasuredPositiveAnodeCurrent(&MeasuredPositiveAnodeCurrent);
	const FTDI_ActionInfo GetMeasuredNegativeAnodeCurrentInfo = GetMeasuredNegativeAnodeCurrent(&MeasuredNegativeAnodeCurrent);
	const FTDI_ActionInfo GetMeasuredAnodePreCurrentInfo = GetMeasuredAnodePreCurrent(&MeasuredAnodePreCurrent);
	const FTDI_ActionInfo GetMeasuredAnodeCurrentDuringSnapshotInfo = GetMeasuredAnodeCurrentDuringSnapshot(&MeasuredAnodeCurrentDuringSnapshot);
	
	const FTDI_ActionInfo GetMeasuredMASInfo = GetMeasuredMAS(&MeasuredMAS);
	const FTDI_ActionInfo GetMeasuredExpositionTimeInfo = GetMeasuredExpositionTime(&MeasuredExpositionTime);

	return //true;
		GetDeviceTypeInfo +
		GetFocusStateInfo +
		GetBlockings1Info +
		GetBlockings2Info +
		GetBlockings3Info +
		GetMessagesInfo +
		GetWarningsInfo +
		GetAnodeHVInfo +
		GetAnodeCurrentInfo +
		GetAnodeSmallFocusPreCurrentInfo +
		GetAnodeSmallFocusCurrentInfo +
		GetAnodeBigFocusPreCurrentInfo +
		GetAnodeBigFocusCurrentInfo +
		GetMASInfo +
		GetExpositionTimeInfo +
		GetMaxAnodeCurrentInfo +
		GetMaxAnodeHVInfo +
		GetMaxAnodeSmallFocusPreCurrentInfo +
		GetMaxAnodeSmallFocusCurrentInfo +
		GetMaxAnodeBigFocusPreCurrentInfo +
		GetMaxAnodeBigFocusCurrentInfo +
		GetMeasuredAnodeHVInfo +
		GetMeasuredPositiveAnodeCurrentInfo +
		GetMeasuredNegativeAnodeCurrentInfo +
		GetMeasuredAnodePreCurrentInfo +
		GetMeasuredAnodeCurrentDuringSnapshotInfo +
		GetMeasuredMASInfo +
		GetMeasuredExpositionTimeInfo;
}

bool KY5_Commands_19200::WaitNeededSnapshotState(const BYTE NeededSnapshotState,const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime)
{
	bool Success = false;

	const clock_t T0 = clock();
	clock_t T1 = T0;

	do
	{
		if(WaitingIsProhibitedFlag == TRUE)
			break;

		const FTDI_ActionInfo GetWarningsInfo = GetWarnings(&Warnings);

		T1 = clock();

		if(GetWarningsInfo == false)
			break;

		if(Warnings == NeededSnapshotState)
		{
			Success = true;
			break;
		}

		if(DelayBetweenRequests > 0)
			Sleep(DelayBetweenRequests);
	}
	while(T1 - T0 < MaxWaitingTime);

	if(WaitingTime != nullptr)
		*WaitingTime = T1 - T0;

	return Success;
}