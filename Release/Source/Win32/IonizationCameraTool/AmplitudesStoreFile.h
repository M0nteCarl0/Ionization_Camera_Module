#pragma once
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <vector>
class AmplitudesStoreFile {
public:
  AmplitudesStoreFile(void);
  ~AmplitudesStoreFile(void);
  void SaveProtocolFile(const char *_Filename, uint8_t _Control, uint8_t _State,
                        uint32_t _ThersholdTrigger, uint16_t _ValueHighVoltage,
                        std::vector<int32_t> *_ArrayOfAmplitudes,
                        int64_t _GlobalDosa, int64_t _LocalDosa,
                        uint8_t _CounterEventsTrigger,
                        uint16_t _DurabilityExposition,
                        uint32_t _AvergageAmplitudeMeasure,
                        uint16_t _QuanityMeasuredAmplitudes, uint32_t _LevelDC);

  void ComToDota(char *Source);

private:
  FILE *_Protocol;
};
