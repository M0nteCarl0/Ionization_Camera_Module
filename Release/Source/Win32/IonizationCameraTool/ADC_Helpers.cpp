#include "ADC_Helpers.h"
/***********************************************************************/
uint8_t GetSampleRateValueRaw(int ID) {
  uint8_t SampleRate[] = {0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE};
  return SampleRate[ID];
}
/***********************************************************************/
int GetIndexBySampleRate(uint8_t Rate) {
  int Index = 0;
  uint8_t SampleRate[] = {
      0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE,
  };
  for (size_t i = 0; i < 8; i++) {
    if (SampleRate[i] == Rate) {
      Index = i;
      break;
    }
  }
  return Index;
}

/***********************************************************************/
uint8_t GetypeFilter(int ID) {
  uint8_t FilterType[] = {0x0, 0x3};
  return FilterType[ID];
}
/***********************************************************************/
int GetIndexFilter(uint8_t ID) {
  uint8_t FilterType[] = {0x0, 0x3};
  int Index = 0;
  for (size_t i = 0; i < 2; i++) {
    if (FilterType[i] == ID) {
      Index = i;
      break;
    }
  }
  return Index;
}
/***********************************************************************/
uint8_t GetSampleRate50HertzRejectiom(int ID) {
  uint8_t SampleRate[] = {0x2, 0x3, 0x5, 0x6};
  return SampleRate[ID];
}
/***********************************************************************/
int GetIndexBytSampleRate50HertzRejectiom(uint8_t ID) {
  int Index = 0;
  uint8_t SampleRate[] = {0x2, 0x3, 0x5, 0x6};
  for (size_t i = 0; i < 4; i++) {
    if (SampleRate[i] == ID) {
      Index = i;
      break;
    }
  }

  return Index;
}
/***********************************************************************/
