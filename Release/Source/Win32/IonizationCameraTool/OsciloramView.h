#pragma once
#ifndef _OSC_VIEV_
#define _OSC_VIEV_
#include "AmplitudesStoreFile.h"
#include "IC_Module.h"
#include "SetupModuleView.h"
#include "Statistica.h"
#include <msclr\marshal_cppstd.h>
#include <stdio.h>
#include <string.h>
#include <vector>
#
using namespace std;
namespace IonizationCameraTool {

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace msclr;
public
ref class OsciloramView : public System::Windows::Forms::Form {

private:
  ~OsciloramView();
  System::Void AmplitudeLoader_ProgressChanged(
      System::Object ^ sender,
      System::ComponentModel::ProgressChangedEventArgs ^ e);
  System::Void Forced_Start_Click(System::Object ^ sender,
                                  System::EventArgs ^ e);
  System::Void ResetAllMeasuremnets_Click(System::Object ^ sender,
                                          System::EventArgs ^ e);
  System::Void StatePolling_Tick(System::Object ^ sender,
                                 System::EventArgs ^ e);
  System::Void ReadOsc_Click(System::Object ^ sender, System::EventArgs ^ e);
  System::Void AmplitudeLoader_DoWork(System::Object ^ sender,
                                      System::ComponentModel::DoWorkEventArgs ^
                                          e);
  System::Void toolStripPolingTime_TextChanged(System::Object ^ sender,
                                               System::EventArgs ^ e);
  System::Void AmplitudeLoader_RunWorkerCompleted(
      System::Object ^ sender,
      System::ComponentModel::RunWorkerCompletedEventArgs ^ e);
  System::Void OsciloramView_Load(System::Object ^ sender,
                                  System::EventArgs ^ e);
  System::Void Upfate_Aqusition_Click(System::Object ^ sender,
                                      System::EventArgs ^ e);
  System::Void SaveMeauremnts_Click(System::Object ^ sender,
                                    System::EventArgs ^ e);
#pragma region Windows Form Designer generated code
  void InitializeComponent(void);
#pragma endregion
  bool CheckModuleConectionState(void);
  bool CheckAQComplete(StateWord &StateW, uint32_t &_QuanityMeasuredAmplitudes,
                       uint32_t &_DurabilityExposition);
  bool AqusitionIsInProgress(void);
  bool CheckIsModuleConected(void);

  bool StoreAnplitudesEvent(void);
  void CheckSourceStartIndicator(StateWord &StateW);
  void CheckTimeOEventIndicator(StateWord &StateW);
  void CheckAQInProgess(StateWord &StateW);
  void WriteToUI(int64_t &_GlobalDosa, int64_t &_LocalDosa,
                 uint8_t &_CounterEventsTrigger,
                 uint32_t &_DurabilityExposition,
                 int32_t &_AvergageAmplitudeMeasure,
                 uint32_t &_QuanityMeasuredAmplitudes);
  void ReadAmlitudesFromModule(vector<int32_t> *ArrayOfAmplitudes,
                               uint32_t AmplitudeCount);
  void UpdateMainData(void);
  void ReadStatePoll(void);
  void DrawPlot(void);
  void DrawTable(void);
  bool LastStateMeasure;

  System::Windows::Forms::SaveFileDialog ^ DialogSaveAmplitudes;
  System::Windows::Forms::Button ^ Upfate_Aqusition;
  System::Windows::Forms::DataGridViewTextBoxColumn ^ Column1;
  System::Windows::Forms::DataGridViewTextBoxColumn ^ Column2;
  System::Windows::Forms::Label ^ OpearionIndicator;
  System::Windows::Forms::Button ^ Forced_Start;
  System::Windows::Forms::GroupBox ^ groupBox2;
  System::Windows::Forms::CheckBox ^ Timeout_Emited;
  System::Windows::Forms::RadioButton ^ Thershold_Trigger;
  System::Windows::Forms::RadioButton ^ Forced_Trigger;
  System::Windows::Forms::RadioButton ^ Extrenal_Trigger;
  System::Windows::Forms::CheckBox ^ MeasureState;
  System::Windows::Forms::Timer ^ StatePolling;
  System::Windows::Forms::Button ^ SaveMeauremnts;
  System::Windows::Forms::Label ^ label3;
  System::Windows::Forms::Label ^ CountAmplitudes;
  System::Windows::Forms::DataGridView ^ TableAmplitudes;
  System::Windows::Forms::Label ^ label1;
  System::Windows::Forms::Label ^ label2;
  System::Windows::Forms::GroupBox ^ groupBox3;
  System::Windows::Forms::Label ^ TimeExposition;
  System::Windows::Forms::Label ^ label10;
  System::Windows::Forms::Label ^ AvergageAmplitude;
  System::Windows::Forms::Label ^ label8;
  System::Windows::Forms::Label ^ CounterTrigger;
  System::Windows::Forms::Label ^ label5;
  System::Windows::Forms::Label ^ GlobalDosa;
  System::Windows::Forms::Label ^ LocalDosa;
  System::Windows::Forms::Label ^ label11;
  System::Windows::Forms::Label ^ label12;
  System::ComponentModel::IContainer ^ components;
  System::Windows::Forms::DataVisualization::Charting::Chart ^ AmplitudeGraph;
  System::Windows::Forms::Button ^ ResetAllMeasuremnets;
  System::Windows::Forms::ToolStripStatusLabel ^ ModuleState;
  System::Windows::Forms::RadioButton ^ Infinite_Integration;
  System::Windows::Forms::Button ^ ReadOsc;
  System::Windows::Forms::StatusStrip ^ statusStrip1;
  System::Windows::Forms::ToolStripStatusLabel ^ StripStatusLoadAmplitudes;
  System::Windows::Forms::ToolStripProgressBar ^ toolStripProgressBar1;
  System::ComponentModel::BackgroundWorker ^ AmplitudeLoader;
  System::Windows::Forms::MenuStrip ^ menuStrip1;
  System::Windows::Forms::ToolStripMenuItem ^                   ���������������ToolStripMenuItem;
  System::Windows::Forms::ToolStripMenuItem ^                   ������������ToolStripMenuItem;
  System::Windows::Forms::ToolStripTextBox ^ toolStripPolingTime;
  SetupModuleView ^ SetupView;
  std::shared_ptr<ICM> *_IC_Module;
  vector<int32_t> *_AmplBuffer;

  int ImpulseCounter;
  int TotalAmplitudeCount;
  float _TimeScaleFactor;
  bool _isConected;
private: System::Windows::Forms::CheckBox^  EndMeasure_checkBox;
private: System::Windows::Forms::CheckBox^  WautMeasure_checkBox;
		 uint32_t _QuanityMeasuredAmplitudes;

public:
  OsciloramView(void);
  System::Windows::Forms::Timer ^ GetTimerHandle(void);
  virtual void WndProc(Message % m) override;
  void StopTimer(void);
  void StartTimer(void);
  void SetICModuleHandle(std::shared_ptr<ICM> *IC_Module);
  void SetSetupView(SetupModuleView ^ Setup);
  void SetIsConected(void);
  void ResetIsConected(void);
  void UptdateMeasurements(void);

  std::shared_ptr<Statistica> *_Statistica;
  std::shared_ptr<AmplitudesStoreFile> *_AmplitudesStoreFile;
  int32_t MaxAplitudel;
  int32_t MinAplitude;
  HANDLE _Thread;
};
} // namespace IonizationCameraTool
#endif
