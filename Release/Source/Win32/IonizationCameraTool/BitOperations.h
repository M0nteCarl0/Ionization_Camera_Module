//
//  autor: Vitaly Kalendarev, kalendarev@roentgenprom.ru
//
//  date: 21.10.2014
//
//#include "stdafx.h"

#ifndef BIT_OPERATIONS_H
#define BIT_OPERATIONS_H

#define GET_1_LO_ORDER_BITS(Number) (Number & 0x01)
#define GET_4_LO_ORDER_BITS(Number) (Number & 0x0F)
#define GET_5_LO_ORDER_BITS(Number) (Number & 0x1F)
#define GET_1_HI_ORDER_BITS(Number) ((Number & 0x80) >> 7)
#define GET_5_HI_ORDER_BITS(Number) ((Number & 0xF80) >> 7)

#define GET_3_LO_ORDER_BITS(Number) (Number & 0x07)
#define GET_7_LO_ORDER_BITS(Number)  (Number & 0x7F)
#define GET_7_HI_ORDER_BITS(Number)  ((Number & 0x3F80) >> 7)
#define GET_7_HI_ORDER2_BITS(Number) ((Number & 0x1FC00) >> 14)
#define GET_7_HI_ORDER3_BITS(Number) ((Number & 0xFE00000) >> 21)

#define GET_8_LO_ORDER_BITS(Number) (Number & 0xFF)

#define JOIN_3_LO_ORDER_AND_7_HI_ORDER_BITS(LoOrder,HiOrder) ((int)(GET_7_LO_ORDER_BITS(HiOrder) << 3) | (int)GET_3_LO_ORDER_BITS(LoOrder))
#define JOIN_7_LO_ORDER_AND_1_HI_ORDER_BITS(LoOrder,HiOrder) ((int)(GET_1_LO_ORDER_BITS(HiOrder) << 7) | (int)GET_7_LO_ORDER_BITS(LoOrder))
#define JOIN_7_LO_ORDER_AND_5_HI_ORDER_BITS(LoOrder,HiOrder) ((int)(GET_5_LO_ORDER_BITS(HiOrder) << 7) | (int)GET_7_LO_ORDER_BITS(LoOrder))
#define JOIN_7_LO_ORDER_AND_7_HI_ORDER_BITS(LoOrder,HiOrder) ((int)(GET_7_LO_ORDER_BITS(HiOrder) << 7) | (int)GET_7_LO_ORDER_BITS(LoOrder))
#define JOIN_8_LO_ORDER_AND_8_HI_ORDER_BITS(LoOrder,HiOrder) ((int)(GET_8_LO_ORDER_BITS(HiOrder) << 8) | (int)GET_8_LO_ORDER_BITS(LoOrder))

#define JOIN_7x4_BITS(LoOrder,HiOrder1,HiOrder2,HiOrder3) ((int)((GET_7_LO_ORDER_BITS(HiOrder3) << 21) | (GET_7_LO_ORDER_BITS(HiOrder2) << 14) | (GET_7_LO_ORDER_BITS(HiOrder1) << 7) | GET_7_LO_ORDER_BITS(LoOrder)))

#define JOIN_7_LO_ORDER_AND_7_HI_ORDER_BITS_OF_2s_COMPLEMENT(LoOrder,HiOrder) ((int)(GET_7_LO_ORDER_BITS(HiOrder) << 7) | (int)GET_7_LO_ORDER_BITS(LoOrder) - ((HiOrder & 0x40) == 0x40  ? 0x4000 : 0))

// presentation of BYTE in 7-bit format
//  --description------------------------
//
//       bytes (n x 7bit)          byte
//       |   |   |   |     |   |   |7|6|5|4|3|2|1|0|
//       | 1 | 2 | 3 | ... | n |
//       |   |   |   |     |   |        1 2 3 ... n
//       (7 lo-order bits)         (1 hi-order bit)
//
#define GET_BIT_FROM_BYTE(Byte,BitNumber) (GET_1_LO_ORDER_BITS((Byte >> BitNumber)))

#endif