#include "StdAfx.h"
#include "IC_Module.h"

/***********************************************************************/
    IC_Module::IC_Module(void)
    {
                m_FlgDebugS = false;
                m_FlgDebugR = false;
                m_FlgCheckCRCwrite = true;
                m_FlgCheckCRCread  = true;
    }
    
 /***********************************************************************/   
    IC_Module::~IC_Module(void)
    {
    }
/***********************************************************************/
    void IC_Module:: Init(MxComm* Comm)
    {
          _FTDI = Comm;
    }
/***********************************************************************/
    int IC_Module:: ReadVersion(uint8_t *Name, uint8_t *Hard, uint8_t *Soft)
    {


   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS[2] = CommadID_READ_HW_SW;                 // Commanda
                  // Commanda
               // Commanda

   int NR = 14;                         // Amount of Bytes  RECEIVE 5+8+1=14

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   BYTE SB = 0x01;
   Name [2] = Name [3] = 0;
   Name [0] = m_DataR[5];
   if (m_DataR[8] & SB) Name[0] |= 0x80;
   SB = SB << 1;

   Name [1] = m_DataR[6];
   if (m_DataR[8] & SB) Name[1] |= 0x80;
   SB = SB << 1;

   Name[0] = m_DataR[5];
   Name[1] = m_DataR[6];
   Name[2] = m_DataR[7];
   Name[3] = m_DataR[8];

   Hard[0] = m_DataR[9];
   Hard[1] = m_DataR[10];
   Soft[0] = m_DataR[11];
   Soft[1] = m_DataR[12];

   return Er;

    }
/***********************************************************************/
    int  IC_Module::ReadParams(   uint8_t*     Control,
                                  uint8_t*     State,
                                  uint32_t*    ThersholdTrigger,
                                  uint64_t*    GlobalDosa,
                                  uint64_t*    LocalDosa,
                                  uint8_t*     CounterEventsTrigger,
                                  uint16_t*    DurabilityExposition,
                                  uint32_t*    AvergageAmplitudeMeasure,
                                  uint16_t*    QuanityMeasuredAmplitudes,
                                  uint16_t*    ValueHighVoltage)
    {

   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS[2] = CommadID_READ_DETECTOR_SETUP;                 // Commanda
   int NR = 41;                         // Amount of Bytes  RECEIVE 5+8+1=14
   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);


    *Control                     = m_DataR[5];
    *State                       = m_DataR[6];
    *ThersholdTrigger            = m_DataR[7] | m_DataR[8] << 7 | m_DataR[9] << 14;
    *GlobalDosa                  = m_DataR[10] | m_DataR[11] << 7 | m_DataR[12] << 14 | m_DataR[13] << 21 | m_DataR[14] << 28 | m_DataR[15] << 35 |  m_DataR[16] << 42 |  m_DataR[17] << 49 | m_DataR[18] << 53 | m_DataR[19] << 60;
    *LocalDosa                   = m_DataR[20] | m_DataR[21] << 7 | m_DataR[22] << 14 | m_DataR[23] << 21 | m_DataR[24] << 28 | m_DataR[25] << 35 |  m_DataR[26] << 42 |  m_DataR[27] << 49 | m_DataR[28] << 53 | m_DataR[29] << 60;
    *CounterEventsTrigger        = m_DataR[30];
    *DurabilityExposition        = m_DataR[31] | m_DataR[32] << 7; 
    *AvergageAmplitudeMeasure    = m_DataR[33] | m_DataR[34] << 7| m_DataR[35] << 14; 
    *QuanityMeasuredAmplitudes   = m_DataR[36] | m_DataR[37] << 7; 
    *ValueHighVoltage            = m_DataR[38] | m_DataR[39] << 7; 

    return Er;

    }
/***********************************************************************/
    int IC_Module::Readmlitudes(uint8_t* Offset,
                                   uint32_t* PartArrayAmplitues)
    {
   int NS = 6;                         // Amount of Bytes  SEND
   m_DataS[2]  = CommadID_READ_DETECTOR_AMPLITUDES;
   m_DataS[5]  = *Offset;                 // Commanda
   int NR = 127;                         // Amount of Bytes  RECEIVE 5+8+1=14
   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);
   uint8_t DestinationCounter = 0;
   for(uint8_t i = 5;i<123;i+=3)
   {
     PartArrayAmplitues[DestinationCounter] = m_DataR[i] | m_DataR[i+1] << 7 | m_DataR[i+2] << 14;
     DestinationCounter++;
   };
   return Er ;
    }
/***********************************************************************/

    int IC_Module:: WriteParams(uint8_t* Control
                                ,uint32_t* Thershold,
                                 uint16_t* ValueofSourceHV)
   {

   int NS = 12;                         // Amount of Bytes  SEND
   m_DataS[2] = CommadID_WRITE_DETECTOR_SETUP;
   m_DataS[5] = *Control;

   m_DataS[6] = *Thershold &0x7f;
   m_DataS[7] = (*Thershold >>7)  & 0x7f;
   m_DataS[8] = (*Thershold >>14)  & 0x7f; 
   
   m_DataS[9] = *ValueofSourceHV & 0x7f; 
   m_DataS[10] = (*ValueofSourceHV >>7) & 0x7f; 
   
   int Er = WriteCommand (m_DataS,NS);
   return Er;
   }

/***********************************************************************/
   int  IC_Module:: StartForcedMeaure(uint16_t* TimeAqusiotion)
   {

    int NS = 8;                         // Amount of Bytes  SEND
    m_DataS[2] =   CommadID_DETECTOR_FORCE_START;
    m_DataS[5] =   *TimeAqusiotion & 0x7f;
    m_DataS[6] =   (*TimeAqusiotion >> 7) & 0x7f; 
    int Er = WriteCommand (m_DataS,NS);
    return Er;
   }
/***********************************************************************/
   int  IC_Module:: CleanAllMeasurement(void)
   {

   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS[2] = CommadID_DETECTOR_RESET_ALL;
   int Er = WriteCommand (m_DataS,NS);
   return Er;
   }

/***********************************************************************/
int IC_Module::CheckErrorWR (BYTE *DataS, int NS, BYTE *DataR, int NR)
{
   if (NR < 5)                return 0x10;
//   if (NS != NR)              return 0x20;
   if (DataR[3] != NR)        return 0x30;
   return 0;

}
/***********************************************************************/
int  IC_Module::ReadCommand (BYTE *DataS, int NS, BYTE *DataR, int NR)
{


  

   DataS [0] = mID;
   DataS [1] = mADDR;

   DataS [3] = NS;                   // Amount of bytes
  // int  Er;
   if (m_FlgCheckCRCwrite)
      AddCRC (DataS, NS);
   bool Ret; 
   DWORD NumRec;
  
  if(_FTDI!=NULL)
  {

    Ret =   _FTDI->SimpleWrite (DataS, NS);
    if(Ret)
    {
	     Ret = _FTDI->SimpleRead(DataR, NR,&NumRec);
    }
   }
   if (!Ret)                     return 0x01;
   if (NR != NumRec)             return 0x02;
   if (m_FlgCheckCRCread && !CheckCRC (m_DataR, NR))  return 0x03;

   int Er = CheckErrorWR (DataS, NS, DataR, NR);
   return Er;


}
/***********************************************************************/
int IC_Module::WriteCommand (BYTE *DataS, int NS)
{

   DataS [0] = mID;
   DataS [1] = mADDR;
   DataS [3] = NS;                   // Amount of bytes

   if (m_FlgCheckCRCwrite)
      AddCRC (DataS, NS);

   int NR = NS;
   DWORD NumRec;
  
   int Er;

   if(_FTDI!=NULL)
   {
    Er = _FTDI->SimpleWrite(DataS, NS);
   }
   return Er;

}
/***********************************************************************/
bool IC_Module::CheckCRC (BYTE *Data, int N)
{


 if (N < 5) return false;

   BYTE CRC = MakeCRC (Data, 4);
   if (CRC != Data[4]) 
      return false;

   if (N > 6){
      CRC = MakeCRC (&Data[5], N-6);
      if (CRC != Data[N-1]) 
         return false;
   }
   return true;

}
/***********************************************************************/
void IC_Module::AddCRC (BYTE *Data, int N)
{

  if (N < 5) return;

   Data [4] = MakeCRC (Data, 4);

   if (N > 6){
      Data [N-1] = MakeCRC (&Data[5], N-6);
   }

}
/***********************************************************************/
 BYTE  IC_Module::MakeCRC (BYTE *Data, int N)
 {

   int Summ = 0;
   for (int i = 0; i < N; i++){
      Summ += Data[i];
   }
   int SummH = (Summ >> 7) & 0x7F;

   BYTE mCRC = (BYTE) (SummH + (Summ & 0x7F));

   mCRC &= 0x7F;
   return mCRC;

 }
 /***********************************************************************/


