//----------------------------------------------------------------------------
//    Create by Novikov
//    data 29.06.97
//    last data 25.07.97
//----------------------------------------------------------------------------
//    Modified: Kalendarev V
//    date 06.12.2012
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//    Modified: Moloatliev A
//    date 9.11.2015
//---------------------------

#ifndef __MXCOMM_H
#define __MXCOMM_H

//#include "stdafx.h"

#include "FTD2XX.H"
#include "FTDI_ActionInfo.h"

#define COMM_BUFF 64
//----------------------------------------------------------------------------
class MxComm
{
public:
	MxComm();	      // GENERIC_READ | GENERIC_WRITE
	~MxComm();

protected:
	static CRITICAL_SECTION CrSection;

protected:
	char m_Buff[COMM_BUFF];

	FT_HANDLE m_FTDI;

private:
	static int nobjects;// number of objects of this class

public:
    FT_DEVICE_LIST_INFO_NODE* EnumerateFTDIDevices(size_t* CountConectedDevices);
    bool AutoConnectForFreeModule(const UINT nSpeed, const UINT nDataBits, const UINT nParity, const UINT nStopBits);
	bool Configure(const UINT nSpeed, const UINT nDataBits, const UINT nParity, const UINT nStopBits);
    bool ConfigureByID(const UINT ID  ,const UINT nSpeed, const UINT nDataBits, const UINT nParity, const UINT nStopBits);
	bool Close();

	FTDI_ActionInfo Read(BYTE * const Buffer, const DWORD BytesToRead, DWORD * const BytesReturned) const;
	FTDI_ActionInfo Write(BYTE * const Buffer, const DWORD BytesToWrite) const;
    int SimpleWrite(BYTE * const Buffer, const DWORD BytesToWrite);
    int SimpleRead(BYTE * const Buffer, const DWORD BytesToRead, DWORD * const BytesReturned);
	static void Delay(const int Del);

	void PurgeTX() const;
	void PurgeRX() const;
	bool IsFTDIBoxAvalible(void);
	void SetTimeout(const DWORD Constant);
	
};
//----------------------------------------------------------------------------

#endif
