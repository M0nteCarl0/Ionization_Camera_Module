#include "CriticalSection.h"
#include "stdafx.h"

CriticalSection::CriticalSection() { Init(); }

CriticalSection::CriticalSection(const char *Decription) {
  Init();
  _Description = Decription;
}

const char *CriticalSection::GetDescription(void) {
  return _Description.data();
}

CriticalSection::~CriticalSection() {
  // CloseHandle(&_Sec);
}

void CriticalSection::Enter(void) { EnterCriticalSection(&_Sec); }

void CriticalSection::Leave(void) { LeaveCriticalSection(&_Sec); }

void CriticalSection::Init(void) { InitializeCriticalSection(&_Sec); }
