#include "AmplitudesStoreFile.h"

AmplitudesStoreFile::AmplitudesStoreFile(void) { _Protocol = NULL; }

void AmplitudesStoreFile::SaveProtocolFile(
    const char *_Filename, uint8_t _Control, uint8_t _State,
    uint32_t _ThersholdTrigger, uint16_t _ValueHighVoltage,
    std::vector<int32_t> *_ArrayOfAmplitudes, int64_t _GlobalDosa,
    int64_t _LocalDosa, uint8_t _CounterEventsTrigger,
    uint16_t _DurabilityExposition, uint32_t _AvergageAmplitudeMeasure,
    uint16_t _QuanityMeasuredAmplitudes, uint32_t _LevelDC) {

  float TimeScaleFactor = 0;
  char AmlitudesBuffer[32];
  _Protocol = fopen(_Filename, "w+");

  fprintf(_Protocol, "Control Word: 0x%x\n", _Control);
  fprintf(_Protocol, "State Word: 0x%x\n", _State);
  fprintf(_Protocol, "Thershold Trigger: %i LSB\n", _ThersholdTrigger);
  fprintf(_Protocol, "Local  Dosa: %i LSB\n", _LocalDosa);
  fprintf(_Protocol, "Global Dosa: %i LSB\n", _GlobalDosa);
  fprintf(_Protocol, "Level DC: %i LSB\n", _LevelDC);
  fprintf(_Protocol, "Avergage Amplitude meaurements :%i\n",
          _AvergageAmplitudeMeasure);
  fprintf(_Protocol, "Quanity Measure Amplitudes: %i\n",
          _QuanityMeasuredAmplitudes);
  fprintf(_Protocol, "Counter Events Trigger: %i\n", _CounterEventsTrigger);
  fprintf(_Protocol, "Time Exposition: %i mS\n", _DurabilityExposition);
  fprintf(_Protocol, "---------------------------------------------------------"
                     "------------------\n");
  fprintf(_Protocol, "RAW Amplitudes data\n");
  fprintf(_Protocol, "---------------------------------------------------------"
                     "------------------\n");

  fprintf(_Protocol, "Time(mS)\t Value(LSB)\n");

  if (_DurabilityExposition > 0 && _QuanityMeasuredAmplitudes > 0) {
    TimeScaleFactor =
        (float)_DurabilityExposition / (float)_QuanityMeasuredAmplitudes;
  }

  if (_QuanityMeasuredAmplitudes > 1000) {
    _QuanityMeasuredAmplitudes = 1000;
  }

  for (uint16_t i = 0; i < _QuanityMeasuredAmplitudes; i++) {
    sprintf(AmlitudesBuffer, "%.2f\t\t  %i\n", (i + 1) * TimeScaleFactor,
            _ArrayOfAmplitudes->data()[i]);
    ComToDota(AmlitudesBuffer);
    fprintf(_Protocol, "%s", AmlitudesBuffer);
  }

  fclose(_Protocol);
}

void AmplitudesStoreFile::ComToDota(char *Source) {
  for (size_t i = 0; i < strlen(Source); i++) {
    if (Source[i] == '.') {
      Source[i] = ',';
    }
  }
}

AmplitudesStoreFile::~AmplitudesStoreFile(void) {
  if (_Protocol) {
    fclose(_Protocol);
  }
}
