#pragma once
#include "MXCOMM.H"
#include <stdint.h>

#define  NCOM   1024
#define  mID    0x96
#define  mADDR  0x08



enum CommadID
{
   CommadID_WRITE_DETECTOR_SETUP	 = 0x1,
   CommadID_DETECTOR_FORCE_START     = 0x2,
   CommadID_DETECTOR_RESET_ALL       = 0x3,
   CommadID_READ_HW_SW				 = 0x40,
   CommadID_READ_DETECTOR_SETUP	     = 0x41,
   CommadID_READ_DETECTOR_AMPLITUDES = 0x42,
   
};

 typedef struct ControlWordS
{
  uint8_t TypeTrigger:2;
  uint8_t GainFactor:1;
  uint8_t StateHV:1;
}ControlWordS;
  
typedef  union ControlWord
{
  ControlWordS Bits;
  uint8_t Raw;
}ControlWord;

enum   TypeTrigger
{
  TypeTrigger_External     = 0x1,
  TypeTrigger_Thershold    = 0x2,
}; 
  
enum  GainFactor  
{  
  GainFactor_10X           = 0x0,
  GainFactor_1X            = 0x1,
}; 

enum  StateHV        
{        
  StateHV_Off               = 0x0,
  StateHV_On                = 0x1,
};

 typedef struct StateWordS
{
  uint8_t AqusitionState:1;
  uint8_t SourceStart:3;
  uint8_t Reserved:3;
  uint8_t TimeoutEventIndicator:1;
}StateWordS;
  
typedef  union StateWord
{
 StateWordS Bits;
 uint8_t Raw;
}StateWord;
      
enum  AqusitionState
{
  AqusitionState_Complete   = 0x0,
  AqusitionState_InProgress = 0x1,  
};

enum SourceStart
{
  SourceStart_Forced       = 0x3,
  SourceStart_External     = 0x1,
  SourceStart_Thershold    = 0x2,
};
  
  typedef  enum  TimeoutEventIndicator
{
  TimeoutEventIndicator_Measure_Normal = 0x0,
  TimeoutEventIndicator_Measure_Triggered = 0x1,
};


class IC_Module
{
public:
    IC_Module(void);
    ~IC_Module(void);
    void Init(MxComm* Comm);
    int  ReadVersion(uint8_t *Name, uint8_t *Hard, uint8_t *Soft);
    int  ReadParams(              
                                  uint8_t*      Control,
                                  uint8_t*      State,
                                  uint32_t*     ThersholdTrigger,
                                  uint64_t*     GlobalDosa,
                                  uint64_t*     LocalDosa,
                                  uint8_t*     CounterEventsTrigger,
                                  uint16_t*     DurabilityExposition,
                                  uint32_t*     AvergageAmplitudeMeasure,
                                  uint16_t*     QuanityMeasuredAmplitudes,
                                  uint16_t*    ValueHighVoltage);

    int   Readmlitudes(uint8_t* Offset,
                                   uint32_t* PartArrayAmplitues);

    int  WriteParams(uint8_t* Control
                                ,uint32_t* Thershold,
                                 uint16_t* ValueofSourceHV);


   int   StartForcedMeaure(uint16_t* TimeAqusiotion);
   int  CleanAllMeasurement(void);


   int CheckErrorWR (BYTE *DataS, int NS, BYTE *DataR, int NR);
   int ReadCommand (BYTE *DataS, int NS, BYTE *DataR, int NR);
   int WriteCommand (BYTE *DataS, int NS);

   bool CheckCRC (BYTE *Data, int N);
   void AddCRC (BYTE *Data, int N);
   BYTE MakeCRC (BYTE *Data, int N);

    private:
    MxComm* _FTDI;
    BYTE     m_DataS[NCOM];
	BYTE     m_DataR[NCOM];
	bool m_FlgDebugS;
	bool m_FlgDebugR;
	bool m_FlgCheckCRCwrite;
	bool m_FlgCheckCRCread;


};

