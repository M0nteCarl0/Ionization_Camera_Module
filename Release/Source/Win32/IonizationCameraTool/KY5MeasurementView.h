#pragma once

namespace IonizationCameraTool {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� KY5MeasurementView
	/// </summary>
	public ref class KY5MeasurementView : public System::Windows::Forms::Form
	{
	public:

     void  UpdateFormData(int Time,int HV,int mAs,int Curent)
    {

    CurrentText->Text        = Curent.ToString();
    HighVoltageText->Text    = HV.ToString();
    TimeExpositionText->Text = Time.ToString();
    mAsText->Text            = mAs.ToString();

    }





		KY5MeasurementView(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~KY5MeasurementView()
		{
			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::GroupBox^  groupBox1;
    private: System::Windows::Forms::Label^  CurrentText;


    private: System::Windows::Forms::Label^  label3;

    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::Label^  mAsText;


    private: System::Windows::Forms::Label^  label7;
    private: System::Windows::Forms::Label^  HighVoltageText;
    private: System::Windows::Forms::Label^  TimeExpositionText;



    private: System::Windows::Forms::Label^  label5;
    protected: 

	private:
    

   




		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            this->mAsText = (gcnew System::Windows::Forms::Label());
            this->label7 = (gcnew System::Windows::Forms::Label());
            this->HighVoltageText = (gcnew System::Windows::Forms::Label());
            this->TimeExpositionText = (gcnew System::Windows::Forms::Label());
            this->label5 = (gcnew System::Windows::Forms::Label());
            this->CurrentText = (gcnew System::Windows::Forms::Label());
            this->label3 = (gcnew System::Windows::Forms::Label());
            this->label1 = (gcnew System::Windows::Forms::Label());
            this->groupBox1->SuspendLayout();
            this->SuspendLayout();
            // 
            // groupBox1
            // 
            this->groupBox1->Controls->Add(this->mAsText);
            this->groupBox1->Controls->Add(this->label7);
            this->groupBox1->Controls->Add(this->HighVoltageText);
            this->groupBox1->Controls->Add(this->TimeExpositionText);
            this->groupBox1->Controls->Add(this->label5);
            this->groupBox1->Controls->Add(this->CurrentText);
            this->groupBox1->Controls->Add(this->label3);
            this->groupBox1->Controls->Add(this->label1);
            this->groupBox1->Location = System::Drawing::Point(1, 12);
            this->groupBox1->Name = L"groupBox1";
            this->groupBox1->Size = System::Drawing::Size(165, 129);
            this->groupBox1->TabIndex = 0;
            this->groupBox1->TabStop = false;
            this->groupBox1->Text = L"���������";
            // 
            // mAsText
            // 
            this->mAsText->AutoSize = true;
            this->mAsText->Location = System::Drawing::Point(129, 94);
            this->mAsText->Name = L"mAsText";
            this->mAsText->Size = System::Drawing::Size(13, 13);
            this->mAsText->TabIndex = 8;
            this->mAsText->Text = L"0";
            // 
            // label7
            // 
            this->label7->AutoSize = true;
            this->label7->Location = System::Drawing::Point(17, 94);
            this->label7->Name = L"label7";
            this->label7->Size = System::Drawing::Size(28, 13);
            this->label7->TabIndex = 7;
            this->label7->Text = L"���";
            // 
            // HighVoltageText
            // 
            this->HighVoltageText->AutoSize = true;
            this->HighVoltageText->Location = System::Drawing::Point(129, 16);
            this->HighVoltageText->Name = L"HighVoltageText";
            this->HighVoltageText->Size = System::Drawing::Size(13, 13);
            this->HighVoltageText->TabIndex = 6;
            this->HighVoltageText->Text = L"0";
            // 
            // TimeExpositionText
            // 
            this->TimeExpositionText->AutoSize = true;
            this->TimeExpositionText->Location = System::Drawing::Point(129, 71);
            this->TimeExpositionText->Name = L"TimeExpositionText";
            this->TimeExpositionText->Size = System::Drawing::Size(13, 13);
            this->TimeExpositionText->TabIndex = 5;
            this->TimeExpositionText->Text = L"0";
            // 
            // label5
            // 
            this->label5->AutoSize = true;
            this->label5->Location = System::Drawing::Point(17, 71);
            this->label5->Name = L"label5";
            this->label5->Size = System::Drawing::Size(106, 13);
            this->label5->TabIndex = 4;
            this->label5->Text = L"����� ���������� ";
            // 
            // CurrentText
            // 
            this->CurrentText->AutoSize = true;
            this->CurrentText->Location = System::Drawing::Point(129, 44);
            this->CurrentText->Name = L"CurrentText";
            this->CurrentText->Size = System::Drawing::Size(13, 13);
            this->CurrentText->TabIndex = 3;
            this->CurrentText->Text = L"0";
            // 
            // label3
            // 
            this->label3->AutoSize = true;
            this->label3->Location = System::Drawing::Point(17, 44);
            this->label3->Name = L"label3";
            this->label3->Size = System::Drawing::Size(26, 13);
            this->label3->TabIndex = 2;
            this->label3->Text = L"���";
            // 
            // label1
            // 
            this->label1->AutoSize = true;
            this->label1->Location = System::Drawing::Point(17, 20);
            this->label1->Name = L"label1";
            this->label1->Size = System::Drawing::Size(69, 13);
            this->label1->TabIndex = 0;
            this->label1->Text = L"����������";
            // 
            // KY5MeasurementView
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
            this->ClientSize = System::Drawing::Size(177, 153);
            this->Controls->Add(this->groupBox1);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
            this->Name = L"KY5MeasurementView";
            this->Text = L"KY5MeasurementView";
            this->groupBox1->ResumeLayout(false);
            this->groupBox1->PerformLayout();
            this->ResumeLayout(false);

        }
#pragma endregion
	};
}
