#include "MainView.h"
using namespace IonizationCameraTool;
#pragma region Windows Form Designer generated code
/***********************************************************************************************/
MainView::MainView(void) {
  InitializeComponent();
  _MXCOMM = new shared_ptr<MxComm>(new MxComm());
  _IC_Module = new shared_ptr<ICM>(new IC_Module());
  _KY5 = gcnew KY5View();
  _FTDI = gcnew FTDIView();
  _Oscilogram = gcnew OsciloramView();
  _ModuleSetup = gcnew SetupModuleView();
  InitFTDIDefault();
  if (_MXCOMM->get()->AutoConnectForFreeModule(nSpeed, nDataBits, nParity,
                                               nStopBits)) {
    _MXCOMM->get()->SetTimeout(120);
    _IC_Module->get()->Init(_MXCOMM);
    BYTE Name[4];
    BYTE SW[4];
    BYTE HW[4];
    _IC_Module->get()->ReadVersion(Name, HW, SW);
    if (!strcmp((const char *)Name, "IC")) {
      ConectionIndicator->BackColor = Color::Green;
      isConected = true;
      SoftwareVersion->Text = SW[0] + "." + SW[1];
      HardwareVersion->Text = HW[0] + "." + HW[1];
      StatePolling->Enabled = true;
      toolStripDCLevel->Text = "���������������� FTDI ������ �������";
    } else {
      toolStripDCLevel->Text = "���������������� FTDI �� ���������,�������� "
                               "������� FTDI � ������������";
    }
  }
}
/***********************************************************************************************/
MainView::~MainView() {
  if (components) {
    delete components;
  }
}
/***********************************************************************************************/
void MainView::InitializeComponent(void) {
  this->components = (gcnew System::ComponentModel::Container());
  this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
  this->fTDIToolStripMenuItem =
      (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->FTDI_Connection = (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->FTDI_Connect = (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->FTDI_Disconect = (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->FTDI_Setup = (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->kY5ToolStripMenuItem =
      (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->KY5_Open = (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->IC_Setup = (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->IC_Measurements = (gcnew System::Windows::Forms::ToolStripMenuItem());
  this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
  this->ConectionIndicator = (gcnew System::Windows::Forms::Label());
  this->SoftwareVersion = (gcnew System::Windows::Forms::Label());
  this->HardwareVersion = (gcnew System::Windows::Forms::Label());
  this->label2 = (gcnew System::Windows::Forms::Label());
  this->label1 = (gcnew System::Windows::Forms::Label());
  this->InfoToolTip = (gcnew System::Windows::Forms::ToolTip(this->components));
  this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
  this->toolStripDCLevel =
      (gcnew System::Windows::Forms::ToolStripStatusLabel());
  this->StatePolling = (gcnew System::Windows::Forms::Timer(this->components));
  this->menuStrip1->SuspendLayout();
  this->groupBox1->SuspendLayout();
  this->statusStrip1->SuspendLayout();
  this->SuspendLayout();
  //
  // menuStrip1
  //
  this->menuStrip1->Items->AddRange(
      gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(4){
          this->fTDIToolStripMenuItem, this->kY5ToolStripMenuItem,
          this->IC_Setup, this->IC_Measurements});
  this->menuStrip1->Location = System::Drawing::Point(0, 0);
  this->menuStrip1->Name = L"menuStrip1";
  this->menuStrip1->Size = System::Drawing::Size(327, 24);
  this->menuStrip1->TabIndex = 8;
  this->menuStrip1->Text = L"menuStrip1";
  //
  // fTDIToolStripMenuItem
  //
  this->fTDIToolStripMenuItem->DropDownItems->AddRange(
      gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(2){
          this->FTDI_Connection, this->FTDI_Setup});
  this->fTDIToolStripMenuItem->Name = L"fTDIToolStripMenuItem";
  this->fTDIToolStripMenuItem->Size = System::Drawing::Size(43, 20);
  this->fTDIToolStripMenuItem->Text = L"FTDI";
  //
  // FTDI_Connection
  //
  this->FTDI_Connection->DropDownItems->AddRange(
      gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(2){
          this->FTDI_Connect, this->FTDI_Disconect});
  this->FTDI_Connection->Name = L"FTDI_Connection";
  this->FTDI_Connection->Size = System::Drawing::Size(152, 22);
  this->FTDI_Connection->Text = L"�����������";
  //
  // FTDI_Connect
  //
  this->FTDI_Connect->Name = L"FTDI_Connect";
  this->FTDI_Connect->Size = System::Drawing::Size(156, 22);
  this->FTDI_Connect->Text = L"������������";
  this->FTDI_Connect->Click +=
      gcnew System::EventHandler(this, &MainView::FTDI_Connect_Click);
  //
  // FTDI_Disconect
  //
  this->FTDI_Disconect->Name = L"FTDI_Disconect";
  this->FTDI_Disconect->Size = System::Drawing::Size(156, 22);
  this->FTDI_Disconect->Text = L"�����������";
  this->FTDI_Disconect->Click +=
      gcnew System::EventHandler(this, &MainView::FTDI_Disconect_Click);
  //
  // FTDI_Setup
  //
  this->FTDI_Setup->Name = L"FTDI_Setup";
  this->FTDI_Setup->Size = System::Drawing::Size(152, 22);
  this->FTDI_Setup->Text = L"���������";
  this->FTDI_Setup->Click +=
      gcnew System::EventHandler(this, &MainView::FTDI_Setup_Click);
  //
  // kY5ToolStripMenuItem
  //
  this->kY5ToolStripMenuItem->DropDownItems->AddRange(
      gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(1){
          this->KY5_Open});
  this->kY5ToolStripMenuItem->Name = L"kY5ToolStripMenuItem";
  this->kY5ToolStripMenuItem->Size = System::Drawing::Size(39, 20);
  this->kY5ToolStripMenuItem->Text = L"KY5";
  this->kY5ToolStripMenuItem->Visible = false;
  //
  // KY5_Open
  //
  this->KY5_Open->Name = L"KY5_Open";
  this->KY5_Open->Size = System::Drawing::Size(254, 22);
  this->KY5_Open->Text = L"������� ������ ���������� KY5";
  this->KY5_Open->Click +=
      gcnew System::EventHandler(this, &MainView::KY5_Open_Click);
  //
  // IC_Setup
  //
  this->IC_Setup->Name = L"IC_Setup";
  this->IC_Setup->Size = System::Drawing::Size(130, 20);
  this->IC_Setup->Text = L"��������� ������ ";
  this->IC_Setup->Click +=
      gcnew System::EventHandler(this, &MainView::IC_Setup_Click);
  //
  // IC_Measurements
  //
  this->IC_Measurements->Name = L"IC_Measurements";
  this->IC_Measurements->Size = System::Drawing::Size(81, 20);
  this->IC_Measurements->Text = L"���������";
  this->IC_Measurements->Click +=
      gcnew System::EventHandler(this, &MainView::IC_Measurements_Click);
  //
  // groupBox1
  //
  this->groupBox1->Controls->Add(this->ConectionIndicator);
  this->groupBox1->Controls->Add(this->SoftwareVersion);
  this->groupBox1->Controls->Add(this->HardwareVersion);
  this->groupBox1->Controls->Add(this->label2);
  this->groupBox1->Controls->Add(this->label1);
  this->groupBox1->Location = System::Drawing::Point(0, 27);
  this->groupBox1->Name = L"groupBox1";
  this->groupBox1->Size = System::Drawing::Size(315, 62);
  this->groupBox1->TabIndex = 9;
  this->groupBox1->TabStop = false;
  this->groupBox1->Text = L"������";
  //
  // ConectionIndicator
  //
  this->ConectionIndicator->BackColor = System::Drawing::Color::Red;
  this->ConectionIndicator->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->ConectionIndicator->Location = System::Drawing::Point(286, 24);
  this->ConectionIndicator->Name = L"ConectionIndicator";
  this->ConectionIndicator->Size = System::Drawing::Size(13, 14);
  this->ConectionIndicator->TabIndex = 4;
  this->InfoToolTip->SetToolTip(this->ConectionIndicator,
                                L"��������� �����������");
  //
  // SoftwareVersion
  //
  this->SoftwareVersion->AutoSize = true;
  this->SoftwareVersion->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->SoftwareVersion->Location = System::Drawing::Point(247, 23);
  this->SoftwareVersion->Name = L"SoftwareVersion";
  this->SoftwareVersion->Size = System::Drawing::Size(24, 15);
  this->SoftwareVersion->TabIndex = 3;
  this->SoftwareVersion->Text = L"0,0";
  //
  // HardwareVersion
  //
  this->HardwareVersion->AutoSize = true;
  this->HardwareVersion->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->HardwareVersion->Location = System::Drawing::Point(112, 21);
  this->HardwareVersion->Name = L"HardwareVersion";
  this->HardwareVersion->Size = System::Drawing::Size(24, 15);
  this->HardwareVersion->TabIndex = 2;
  this->HardwareVersion->Text = L"0,0";
  //
  // label2
  //
  this->label2->AutoSize = true;
  this->label2->Location = System::Drawing::Point(142, 23);
  this->label2->Name = L"label2";
  this->label2->Size = System::Drawing::Size(97, 13);
  this->label2->TabIndex = 1;
  this->label2->Text = L"������ ��������";
  //
  // label1
  //
  this->label1->AutoSize = true;
  this->label1->Location = System::Drawing::Point(7, 20);
  this->label1->Name = L"label1";
  this->label1->Size = System::Drawing::Size(85, 13);
  this->label1->TabIndex = 0;
  this->label1->Text = L"������ ������";
  //
  // InfoToolTip
  //
  this->InfoToolTip->IsBalloon = true;
  this->InfoToolTip->ToolTipIcon = System::Windows::Forms::ToolTipIcon::Info;
  //
  // statusStrip1
  //
  this->statusStrip1->Items->AddRange(
      gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(1){
          this->toolStripDCLevel});
  this->statusStrip1->Location = System::Drawing::Point(0, 95);
  this->statusStrip1->Name = L"statusStrip1";
  this->statusStrip1->Size = System::Drawing::Size(327, 22);
  this->statusStrip1->TabIndex = 12;
  this->statusStrip1->Text = L"statusStrip1";
  //
  // toolStripDCLevel
  //
  this->toolStripDCLevel->Name = L"toolStripDCLevel";
  this->toolStripDCLevel->Size = System::Drawing::Size(211, 17);
  this->toolStripDCLevel->Text = L"������� ��������� ����: 0 LSB(0 mV)";
  //
  // MainView
  //
  this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
  this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
  this->ClientSize = System::Drawing::Size(327, 117);
  this->Controls->Add(this->statusStrip1);
  this->Controls->Add(this->groupBox1);
  this->Controls->Add(this->menuStrip1);
  this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
  this->MainMenuStrip = this->menuStrip1;
  this->MaximizeBox = false;
  this->Name = L"MainView";
  this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
  this->Text = L"Ionization Camera Control 1.1                                 "
               L"                   "
               L"                                                              "
               L"          ";
  this->menuStrip1->ResumeLayout(false);
  this->menuStrip1->PerformLayout();
  this->groupBox1->ResumeLayout(false);
  this->groupBox1->PerformLayout();
  this->statusStrip1->ResumeLayout(false);
  this->statusStrip1->PerformLayout();
  this->ResumeLayout(false);
  this->PerformLayout();
}
#pragma endregion
/***********************************************************************************************/
System::Void MainView::KY5_Open_Click(System::Object ^ sender,
                                      System::EventArgs ^ e) {
  if (isConected && !_KY5->Visible && _KY5->IsDisposed) {
    _KY5 = gcnew KY5View();
  }
}
/***********************************************************************************************/
System::Void MainView::IC_Setup_Click(System::Object ^ sender,
                                      System::EventArgs ^ e) {
  if (isConected && !_ModuleSetup->Visible) {
    if (_ModuleSetup->IsDisposed) {
      _ModuleSetup = gcnew SetupModuleView();
    }

    _ModuleSetup->SetICModuleHandle(_IC_Module);
    _Oscilogram->SetICModuleHandle(_IC_Module);
    _Oscilogram->SetSetupView(_ModuleSetup);
    _ModuleSetup->ReadParamsFromModuleExtended();
    _ModuleSetup->Show();
  }
}
/***********************************************************************************************/
System::Void MainView::IC_Measurements_Click(System::Object ^ sender,
                                             System::EventArgs ^ e) {
  if (isConected && !_Oscilogram->Visible) {
    if (_Oscilogram->IsDisposed) {
      _Oscilogram = gcnew OsciloramView();
    }
    _Oscilogram->SetICModuleHandle(_IC_Module);
    _Oscilogram->SetSetupView(_ModuleSetup);

    if (_ModuleSetup->Visible) {
      _Oscilogram->SetIsConected();
      _Oscilogram->Show();
    } else {
      MessageBox::Show("������� �������� ���������");
    }
  }
}
/***********************************************************************************************/
System::Void MainView::FTDI_Setup_Click(System::Object ^ sender,
                                        System::EventArgs ^ e) {
  if (!_FTDI->Visible) {
    if (_FTDI->IsDisposed) {
      _FTDI = gcnew FTDIView();
      _FTDI->SetParametrsFTDI(nSpeed, nDataBits, nStopBits);
      _FTDI->SetMXComm(_MXCOMM);
    } else {
      _FTDI->SetParametrsFTDI(nSpeed, nDataBits, nStopBits);
      _FTDI->SetMXComm(_MXCOMM);
    }
    if (_FTDI->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
      _FTDI->GetParametrsFTDI(nSpeed, nDataBits, nStopBits);
      SelectedDevice = _FTDI->GetSelectedDeviceID();
    }
  }
}
/***********************************************************************************************/
System::Void MainView::FTDI_Connect_Click(System::Object ^ sender,
                                          System::EventArgs ^ e) {
  if (_MXCOMM->get()->ConfigureByID(SelectedDevice, nSpeed, nDataBits, nParity,
                                    nStopBits)) {
    _MXCOMM->get()->SetTimeout(3000);
    _IC_Module->get()->Init(_MXCOMM);
    BYTE Name[4];
    BYTE SW[4];
    BYTE HW[4];
    int Er = _IC_Module->get()->ReadVersion(Name, HW, SW);
    if (!strcmp((const char *)Name, "IC")) {
      ConectionIndicator->BackColor = Color::Green;
      isConected = true;
      SoftwareVersion->Text = SW[0] + "." + SW[1];
      HardwareVersion->Text = HW[0] + "." + HW[1];
      StatePolling->Enabled = true;

      if (_Oscilogram->Visible) {
        _Oscilogram->SetICModuleHandle(_IC_Module);
        _Oscilogram->SetIsConected();
      }

      if (_ModuleSetup->Visible) {
        _ModuleSetup->SetICModuleHandle(_IC_Module);
      }
    }
  }
}
/***********************************************************************************************/
System::Void MainView::FTDI_Disconect_Click(System::Object ^ sender,
                                            System::EventArgs ^ e) {

  StatePolling->Enabled = false;
  isConected = false;
  SoftwareVersion->Text = 0 + "." + 0;
  HardwareVersion->Text = 0 + "." + 0;
  ConectionIndicator->BackColor = Color::Red;
  _MXCOMM->get()->Close();
}
/***********************************************************************************************/
void MainView::SetParametrsFTDI(UINT Speed, UINT DataBits, UINT StopBitS) {
  nSpeed = Speed;
  nDataBits = DataBits;
  nStopBits = StopBitS;
}
/***********************************************************************************************/
void MainView::GetParametrsFTDI(UINT Speed, UINT DataBits, UINT StopBitS)

{
  Speed = nSpeed;
  DataBits = nDataBits;
  StopBitS = nStopBits;
}
/***********************************************************************************************/
void MainView::InitFTDIDefault(void) {

  nSpeed = 19200;
  nDataBits = 8;
  nParity = 1;
  nStopBits = 2;
}
/***********************************************************************************************/
