#include "IC_Module.h"
#include "ByteConversion.hpp"
#include "StdAfx.h"
using namespace Conversion;
/***********************************************************************/
IC_Module::IC_Module(void) {
  m_FlgDebugS = false;
  m_FlgDebugR = false;
  m_FlgCheckCRCwrite = true;
  m_FlgCheckCRCread = true;
  InitializeCriticalSection(&_Section);
}

/***********************************************************************/
IC_Module::~IC_Module(void) { CloseHandle(&_Section); }
/***********************************************************************/
void IC_Module::Init(std::shared_ptr<MxComm> *Comm) { _FTDI = Comm; }
/***********************************************************************/
int IC_Module::ReadVersion(uint8_t *Name, uint8_t *Hard, uint8_t *Soft) {

  EnterCriticalSection(&_Section);
  int NS = 5;                       // Amount of Bytes  SEND
  m_DataS[2] = CommadID_READ_HW_SW; // Commanda

  int NR = 14; // Amount of Bytes  RECEIVE 5+8+1=14
  int Er = ReadCommand(m_DataS, NS, m_DataR, NR);

  BYTE SB = 0x01;
  Name[2] = Name[3] = 0;
  Name[0] = m_DataR[5];
  if (m_DataR[8] & SB)
    Name[0] |= 0x80;
  SB = SB << 1;
  Name[1] = m_DataR[6];
  if (m_DataR[8] & SB)
    Name[1] |= 0x80;
  SB = SB << 1;
  Name[0] = m_DataR[5];
  Name[1] = m_DataR[6];
  Name[2] = m_DataR[7];
  Name[3] = m_DataR[8];
  Hard[0] = m_DataR[9];
  Hard[1] = m_DataR[10];
  Soft[0] = m_DataR[11];
  Soft[1] = m_DataR[12];

  LeaveCriticalSection(&_Section);
  return Er;
}
/***********************************************************************/
int IC_Module::ReadParams(uint8_t *Control, uint8_t *State,
                          uint32_t *ThersholdTrigger, int64_t *GlobalDosa,
                          int64_t *LocalDosa, uint8_t *CounterEventsTrigger,
                          uint32_t *DurabilityExposition,
                          int32_t *AvergageAmplitudeMeasure,
                          uint32_t *QuanityMeasuredAmplitudes,
                          uint16_t *ValueHighVoltage, uint32_t *ValueDC

)

{

  EnterCriticalSection(&_Section);
  int NS = 5; // Amount of Bytes  SEND
              // Commanda
  int NR = 48;
  m_DataS[2] = CommadID_READ_DETECTOR_SETUP;
  int Er = ReadCommand(m_DataS, NS, m_DataR, NR);

  *Control = m_DataR[5];

  *State = m_DataR[6];

  *ThersholdTrigger = m_DataR[7] | m_DataR[8] << 7 | m_DataR[9] << 14;

  Unsigned7bitFormaTotSigned64Bit_v2(*GlobalDosa, m_DataR[10], m_DataR[11],
                                     m_DataR[12], m_DataR[13], m_DataR[14],
                                     m_DataR[15], m_DataR[16], m_DataR[17],
                                     m_DataR[18], m_DataR[19]);

  Unsigned7bitFormaTotSigned64Bit_v2(*LocalDosa, m_DataR[20], m_DataR[21],
                                     m_DataR[22], m_DataR[23], m_DataR[24],
                                     m_DataR[25], m_DataR[26], m_DataR[27],
                                     m_DataR[28], m_DataR[29]);

  *CounterEventsTrigger = m_DataR[30];

  *DurabilityExposition =
      m_DataR[31] | m_DataR[32] << 7 | m_DataR[33] << 14 | m_DataR[34] << 21;

  Unsigned7bitFormaTotSigned32Bit_v2(*AvergageAmplitudeMeasure, m_DataR[35],
                                     m_DataR[36], m_DataR[37], m_DataR[38]);

  *QuanityMeasuredAmplitudes =
      m_DataR[39] | m_DataR[40] << 7 | m_DataR[41] << 14;

  *ValueHighVoltage = m_DataR[42] | m_DataR[43] << 7;

  *ValueDC = m_DataR[44] | m_DataR[45] << 7 | m_DataR[46] << 14;

  LeaveCriticalSection(&_Section);
  return Er;
}
/***********************************************************************/
int IC_Module::Readmlitudes(uint8_t *Offset, int32_t *PartArrayAmplitues) {

  EnterCriticalSection(&_Section);
  int NS = 7; // Amount of Bytes  SEND
  m_DataS[2] = CommadID_READ_DETECTOR_AMPLITUDES;

  m_DataS[5] = *Offset; // Commanda

  int NR = 126; // Amount of Bytes  RECEIVE 5+8+1=14
  int Er = ReadCommand(m_DataS, NS, m_DataR, NR);
  uint8_t DestinationCounter = 0;
  for (uint8_t i = 5; i < 122; i += 4) {
    Unsigned7bitFormaTotSigned32Bit_v2(PartArrayAmplitues[DestinationCounter],
                                       m_DataR[i], m_DataR[i + 1],
                                       m_DataR[i + 2], m_DataR[i + 3]);
    DestinationCounter++;
  };
  LeaveCriticalSection(&_Section);
  return Er;
}
/***********************************************************************/

int IC_Module::WriteParams(uint8_t *Control, uint32_t *Thershold,
                           uint16_t *ValueofSourceHV, uint32_t *ValueDC) {

  EnterCriticalSection(&_Section);
  int NS = 15; // Amount of Bytes  SEND
  m_DataS[2] = CommadID_WRITE_DETECTOR_SETUP;
  m_DataS[5] = *Control;

  m_DataS[6] = *Thershold & 0x7f;
  m_DataS[7] = (*Thershold >> 7) & 0x7f;
  m_DataS[8] = (*Thershold >> 14) & 0x7f;

  m_DataS[9] = *ValueofSourceHV & 0x7f;
  m_DataS[10] = (*ValueofSourceHV >> 7) & 0x7f;

  m_DataS[11] = *ValueDC & 0x7f;
  m_DataS[12] = (*ValueDC >> 7) & 0x7f;
  m_DataS[13] = (*ValueDC >> 14) & 0x7f;

  int Er = ReadCommand(m_DataS, NS, m_DataR, NS);
  LeaveCriticalSection(&_Section);
  return Er;
}

/***********************************************************************/
int IC_Module::StartForcedMeaure(uint16_t *TimeAqusiotion) {

  EnterCriticalSection(&_Section);
  int NS = 8; // Amount of Bytes  SEND

  m_DataS[2] = CommadID_DETECTOR_FORCE_START;

  m_DataS[5] = *TimeAqusiotion & 0x7f;

  m_DataS[6] = (*TimeAqusiotion >> 7) & 0x7f;

  int Er = ReadCommand(m_DataS, NS, m_DataR, NS);
  LeaveCriticalSection(&_Section);
  return Er;
}
/***********************************************************************/
int IC_Module::CleanAllMeasurement(void) {
  EnterCriticalSection(&_Section);

  int NS = 5;
  m_DataS[2] = CommadID_DETECTOR_RESET_ALL;
  int Er = ReadCommand(m_DataS, NS, m_DataR, NS);

  LeaveCriticalSection(&_Section);
  return Er;
}

/***********************************************************************/
int IC_Module::WriteADCParametrs(uint8_t *SampleRate, uint8_t *TypeFilter,
                                 uint8_t *Filter50Hertz,
                                 uint8_t *SampleRateFilter50Hertz) {
  EnterCriticalSection(&_Section);
  int NS = 10; // Amount of Bytes  SEND
  m_DataS[2] = CommadID_WRITE_ADC_SETUP;

  m_DataS[5] = *SampleRate;

  m_DataS[6] = *TypeFilter;

  m_DataS[7] = *Filter50Hertz;

  m_DataS[8] = *SampleRateFilter50Hertz;

  int Er = ReadCommand(m_DataS, NS, m_DataR, NS);
  LeaveCriticalSection(&_Section);
  return Er;
}
/***********************************************************************/
int IC_Module::ReadADCParametrs(uint8_t *SampleRate, uint8_t *TypeFilter,
                                uint8_t *Filter50Hertz,
                                uint8_t *SampleRateFilter50Hertz) {

  EnterCriticalSection(&_Section);
  int NS = 5;

  m_DataS[2] = CommadID_READ_ADC_SETUP;

  int NR = 10; // Amount of Bytes  RECEIVE 5+8+1=14

  int Er = ReadCommand(m_DataS, NS, m_DataR, NR);

  *SampleRate = m_DataR[5];

  *TypeFilter = m_DataR[6];

  *Filter50Hertz = m_DataR[7];

  *SampleRateFilter50Hertz = m_DataR[8];

  LeaveCriticalSection(&_Section);
  return Er;
}

/***********************************************************************/
int IC_Module::ReadHVLevel(double *AVG, double *RMS) {

  EnterCriticalSection(&_Section);

  int NS = 5;

  m_DataS[2] = CommadID_READ_HV_LEVEL;

  int NR = 5 + 6 + 1; // Amount of Bytes  RECEIVE 5+8+1=14

  int Er = ReadCommand(m_DataS, NS, m_DataR, NR);

  *AVG = (double)(m_DataR[5] | m_DataR[6] << 7 | m_DataR[7] << 14) / 100;

  *RMS = (double)(m_DataR[8] | m_DataR[9] << 7 | m_DataR[10] << 14) / 100;

  LeaveCriticalSection(&_Section);
  return Er;
}
/***********************************************************************/
int IC_Module::CheckErrorWR(BYTE *DataS, int NS, BYTE *DataR, int NR) {
  if (NR < 5)
    return 0x10;
  //   if (NS != NR)              return 0x20;
  if (DataR[3] != NR)
    return 0x30;
  return 0;
}
/***********************************************************************/
int IC_Module::ReadCommand(BYTE *DataS, int NS, BYTE *DataR, int NR) {

  DataS[0] = mID;
  DataS[1] = mADDR;
  DataS[3] = NS; // Amount of bytes
  // int  Er;
  if (m_FlgCheckCRCwrite)
    AddCRC(DataS, NS);
  bool Ret;
  DWORD NumRec;

  if (_FTDI != NULL) {
    Ret = _FTDI->get()->SimpleWrite(DataS, NS);
    if (Ret) {
      Ret = _FTDI->get()->SimpleRead(DataR, NR, &NumRec);
    }
  }
  if (!Ret)
    return 0x01;
  if (NR != NumRec)
    return 0x02;
  if (m_FlgCheckCRCread && !CheckCRC(m_DataR, NR))
    return 0x03;

  int Er = CheckErrorWR(DataS, NS, DataR, NR);
  return Er;
}
/***********************************************************************/
int IC_Module::WriteCommand(BYTE *DataS, int NS) {
  DataS[0] = mID;
  DataS[1] = mADDR;
  DataS[3] = NS; // Amount of bytes

  if (m_FlgCheckCRCwrite)
    AddCRC(DataS, NS);

  int NR = NS;
  DWORD NumRec;

  int Er;

  if (_FTDI != NULL) {
    Er = _FTDI->get()->SimpleWrite(DataS, NS);
  }
  return Er;
}
/***********************************************************************/
void IC_Module::FlushBuffers(void) {
  _FTDI->get()->PurgeRX();
  _FTDI->get()->PurgeTX();
}
/***********************************************************************/
bool IC_Module::CheckCRC(BYTE *Data, int N) {
  if (N < 5)
    return false;

  BYTE CRC = MakeCRC(Data, 4);
  if (CRC != Data[4])
    return false;

  if (N > 6) {
    CRC = MakeCRC(&Data[5], N - 6);
    if (CRC != Data[N - 1])
      return false;
  }
  return true;
}
/***********************************************************************/
void IC_Module::AddCRC(BYTE *Data, int N) {
  if (N < 5)
    return;
  Data[4] = MakeCRC(Data, 4);
  if (N > 6) {
    Data[N - 1] = MakeCRC(&Data[5], N - 6);
  }
}
/***********************************************************************/
BYTE IC_Module::MakeCRC(BYTE *Data, int N) {
  int Summ = 0;
  for (int i = 0; i < N; i++) {
    Summ += Data[i];
  }
  int SummH = (Summ >> 7) & 0x7F;

  BYTE mCRC = (BYTE)(SummH + (Summ & 0x7F));

  mCRC &= 0x7F;
  return mCRC;
}
/***********************************************************************/
void IC_Module::CleanRxBuffer(void) { memset(m_DataR, 0, sizeof(byte) * NCOM); }
/***********************************************************************/
