#include "SetupModuleView.h"
#include "ADC_Helpers.h"
#include "StdAfx.h"
using namespace IonizationCameraTool;
/***********************************************************************************************************/
SetupModuleView::SetupModuleView(void) {
  InitializeComponent();
  _TimeHV = 1000;
}
/***********************************************************************************************************/
SetupModuleView::~SetupModuleView() {
  if (components) {
    delete components;
  }
}
/***********************************************************************************************************/
void SetupModuleView::WriteParamsInModule(void) {
  bool Suucces = true;
  WriteParams->Enabled = false;

  ControlWord _ControlW;
  ControlWord _ControlSR;
  StateWord StateAqusiion;
  uint8_t _Control;
  uint32_t _ThersholdTrigger;
  uint16_t _ValueHighVoltage;
  uint32_t _ValueDC;

  uint8_t _ControlR;
  uint32_t _ThersholdTriggerR;
  uint16_t _ValueHighVoltageR;
  uint32_t _ValueDCR;

  uint8_t ADCSampleRateR;
  uint8_t ADCFilterR;
  uint8_t ADC50HertzR;
  uint8_t ADC50HertzFilterRateR;

  uint8_t _State;
  int64_t _GlobalDosa;
  int64_t _LocalDosa;
  uint8_t _CounterEventsTrigger;
  uint32_t _DurabilityExposition;
  int32_t _AvergageAmplitudeMeasure;
  uint32_t _LevelDC;
  uint32_t _QuanityMeasuredAmplitudes;

  uint16_t::TryParse(TimeForceTrigger->Text, _TimeHV);
  uint16_t::TryParse(ValueOfHV->Text, _ValueHighVoltage);
  uint32_t::TryParse(Thershold->Text, _ThersholdTrigger);
  uint32_t::TryParse(DarkCurrent->Text, _ValueDC);

  _ControlW.Raw = 0;

  _ControlW.Bits.TypeTrigger = TypeTriggerList->SelectedIndex;
  _ControlW.Bits.GainFactor = GainFactorList->SelectedIndex;
  _ControlW.Bits.StateHV = HVSourceState->SelectedIndex;
  _ControlW.Bits.MovingAvergageFilter = FilterMVA->Checked;

  _Control = _ControlW.Raw;
  uint8_t ADCSampleRate = GetSampleRateValueRaw(ADC_SampleRate->SelectedIndex);
  uint8_t ADCFilter = GetypeFilter(ADC_TypeFilter->SelectedIndex);
  uint8_t ADC50Hertz = 0;
  uint8_t ADC50HertzFilterRate =
      GetSampleRate50HertzRejectiom(SampleRate50HertzFilter->SelectedIndex);
  ADC50Hertz = Filter50Hertz->Checked;

  _IC_Module->get()->WriteADCParametrs(&ADCSampleRate, &ADCFilter, &ADC50Hertz,
                                       &ADC50HertzFilterRate);
  _IC_Module->get()->WriteParams(&_Control, &_ThersholdTrigger,
                                 &_ValueHighVoltage, &_ValueDC);

  _IC_Module->get()->ReadADCParametrs(&ADCSampleRateR, &ADCFilterR,
                                      &ADC50HertzR, &ADC50HertzFilterRateR);
  _IC_Module->get()->ReadParams(
      &_ControlR, &_State, &_ThersholdTriggerR, &_GlobalDosa, &_LocalDosa,
      &_CounterEventsTrigger, &_DurabilityExposition,
      &_AvergageAmplitudeMeasure, &_QuanityMeasuredAmplitudes,
      &_ValueHighVoltageR, &_LevelDC);

  _ControlSR.Raw = _ControlR;
  StateAqusiion.Raw = _State;

  if (StateAqusiion.Bits.AqusitionState == AqusitionState_InProgress) {
    OperationInfo->ForeColor = Color::Red;
    OperationInfo->Text =
        "�������y�� ��������� ��� �� ����������� ��� ��� ���� ���������!";
  }

  if (ADCSampleRate != ADCSampleRateR || ADCFilterR != ADCFilter ||
      ADC50Hertz != ADC50HertzR ||
      ADC50HertzFilterRate != ADC50HertzFilterRateR) {

    OperationInfo->ForeColor = Color::Red;
    OperationInfo->Text = "������ ������ ���������� ���";
    Suucces = false;
  }

  if (_Control != _ControlR || _ThersholdTrigger != _ThersholdTriggerR ||
      _ValueHighVoltage != _ValueHighVoltageR ||
      _ValueDC != _LevelDC && !Suucces) {
    OperationInfo->ForeColor = Color::Red;
    OperationInfo->Text = "������ ������  �������� ����������  ������";
    Suucces = false;
  }

  if (Suucces) {
    OperationInfo->ForeColor = Color::Green;
    OperationInfo->Text = "��������� ������� �������� � ������!";
  }
  WriteParams->Enabled = true;
}

/***********************************************************************************************************/
void SetupModuleView::ReadParamsFromModule(void) {

  if (_StatePolling != nullptr) {
    _StatePolling->Enabled = false;
  }
  ReadParams->Enabled = false;
  ControlWord _ControlR;
  uint8_t ADCSampleRate = 0;
  uint8_t ADCTypeFilter = 0;
  uint8_t ADC50HerzFilterState = 0;
  uint8_t ADC50HerzFilterRate = 0;
  uint8_t _Control = 0;
  uint8_t _State = 0;
  uint8_t _CounterEventsTrigger = 0;
  uint16_t _ValueHighVoltage = 0;
  uint16_t _HVLevel = 0;
  uint32_t _ThersholdTrigger = 0;
  uint32_t _DurabilityExposition = 0;
  uint32_t _LevelDC = 0;
  uint32_t _QuanityMeasuredAmplitudes = 0;
  uint32_t *_ArrayOfAmplitudes = 0;
  int32_t _AvergageAmplitudeMeasure = 0;
  int64_t _GlobalDosa = 0;
  int64_t _LocalDosa = 0;
  double _ValueHighVoltageAVG = 0;
  double _ValueHighVoltageRMS = 0;
  _ValueHighVoltageAVG = 0;
  _ValueHighVoltageRMS = 0;

  _IC_Module->get()->ReadHVLevel(&_ValueHighVoltageAVG, &_ValueHighVoltageRMS);
  RMSVoltage->Text = String::Format("���:{0:0.00} ADC", _ValueHighVoltageRMS);
  AverageAMplitude->Text =
      String::Format("����������:{0:0.00} ADC", _ValueHighVoltageAVG);

  _IC_Module->get()->ReadADCParametrs(&ADCSampleRate, &ADCTypeFilter,
                                      &ADC50HerzFilterState,
                                      &ADC50HerzFilterRate);

  _IC_Module->get()->ReadParams(
      &_Control, &_State, &_ThersholdTrigger, &_GlobalDosa, &_LocalDosa,
      &_CounterEventsTrigger, &_DurabilityExposition,
      &_AvergageAmplitudeMeasure, &_QuanityMeasuredAmplitudes,
      &_ValueHighVoltage, &_LevelDC);

  ADC_SampleRate->SelectedIndex = GetIndexBySampleRate(ADCSampleRate);
  ADC_TypeFilter->SelectedIndex = GetIndexFilter(ADCTypeFilter);
  SampleRate50HertzFilter->SelectedIndex =
      GetIndexBytSampleRate50HertzRejectiom(ADC50HerzFilterRate);
  Filter50Hertz->Checked = (bool)ADC50HerzFilterState;
  _ControlR.Raw = _Control;
  TypeTriggerList->SelectedIndex = _ControlR.Bits.TypeTrigger;
  GainFactorList->SelectedIndex = _ControlR.Bits.GainFactor;
  HVSourceState->SelectedIndex = _ControlR.Bits.StateHV;
  FilterMVA->Checked = (bool)_ControlR.Bits.MovingAvergageFilter;
  ValueOfHV->Text = _ValueHighVoltage.ToString();
  Thershold->Text = _ThersholdTrigger.ToString();
  TimeForceTrigger->Text = _TimeHV.ToString();
  DarkCurrent->Text = _LevelDC.ToString();
  ReadParams->Enabled = true;

  if (_StatePolling != nullptr) {
    _StatePolling->Enabled = true;
  }
}
/***********************************************************************************************************/
System::Void SetupModuleView::WriteParams_Click(System::Object ^ sender,
                                                System::EventArgs ^ e) {
  if (CheckIsModuleConected()) {
    WriteParamsInModule();
  } else {
    OperationInfo->ForeColor = Color::Red;
    OperationInfo->Text = "������ �� ��������!";
  }
}
/***********************************************************************************************************/
System::Void SetupModuleView::ReadParams_Click(System::Object ^ sender,
                                               System::EventArgs ^ e) {
  ReadParamsFromModuleExtended();
}
/***********************************************************************************************************/
void SetupModuleView::ReadParamsFromModuleExtended(void) {

  if (CheckIsModuleConected()) {
    ReadParamsFromModule();
    OperationInfo->ForeColor = Color::Green;
    OperationInfo->Text = "��������� ������� ��������� �� ������!";
  } else {
    OperationInfo->ForeColor = Color::Red;
    OperationInfo->Text = "������ �� ��������!";
  }
}

/***********************************************************************************************************/
System::Void SetupModuleView::ADC_SampleRate_SelectionChangeCommitted(
    System::Object ^ sender, System::EventArgs ^ e) {
  _ADCSampleRate = GetSampleRateValueRaw(ADC_SampleRate->SelectedIndex);
}
/***********************************************************************************************************/
System::Void SetupModuleView::ADC_TypeFilter_SelectionChangeCommitted(
    System::Object ^ sender, System::EventArgs ^ e) {
  _ADCTypeFilter = GetypeFilter(ADC_TypeFilter->SelectedIndex);
}
/***********************************************************************************************************/
System::Void SetupModuleView::SampleRate50HertzFilter_SelectionChangeCommitted(
    System::Object ^ sender, System::EventArgs ^ e) {
  _ADC50HerzFilterRate =
      GetSampleRate50HertzRejectiom(SampleRate50HertzFilter->SelectedIndex);
}
/***********************************************************************************************************/
System::Void
SetupModuleView::Filter50Hertz_CheckedChanged(System::Object ^ sender,
                                              System::EventArgs ^ e) {
  _ADC50HerzFilterState = Filter50Hertz->Checked;
}
/***********************************************************************************************************/
System::Void SetupModuleView::SetupModuleView_Activated(System::Object ^ sender,
                                                        System::EventArgs ^ e) {

}
/***********************************************************************************************************/
System::Void
SetupModuleView::SetupModuleView_Deactivate(System::Object ^ sender,
                                            System::EventArgs ^ e) {}
/***********************************************************************************************************/
System::Void
SetupModuleView::ExternalTrigger_CheckedChanged(System::Object ^ sender,
                                                System::EventArgs ^ e) {}
/***********************************************************************************************************/
System::Void SetupModuleView::Thershold_TextChanged(System::Object ^ sender,
                                                    System::EventArgs ^ e) {}
/***********************************************************************************************************/
System::Void SetupModuleView::ExternalTrigger_MouseClick(
    System::Object ^ sender, System::Windows::Forms::MouseEventArgs ^ e) {}
/***********************************************************************************************************/
System::Void SetupModuleView::ExternalTrigger_Leave(System::Object ^ sender,
                                                    System::EventArgs ^ e) {}
/***********************************************************************************************************/
System::Void SetupModuleView::SetupModuleView_Leave(System::Object ^ sender,
                                                    System::EventArgs ^ e) {}
/***********************************************************************************************************/
bool SetupModuleView::CheckIsModuleConected(void) {
  bool Res = false;
  BYTE Name[4];
  BYTE SW[4];
  BYTE HW[4];
  _IC_Module->get()->ReadVersion(Name, HW, SW);
  if (!strcmp((const char *)Name, "IC")) {
    Res = true;
  } else {
    _IC_Module->get()->FlushBuffers();
  }

  return Res;
}
/***********************************************************************************************************/
uint16_t SetupModuleView::GetTimeHV(void) { return _TimeHV; }
/***********************************************************************************************************/
void SetupModuleView::SetICModuleHandle(std::shared_ptr<ICM> *IC_Module) {
  _IC_Module = IC_Module;
}
/***********************************************************************************************************/
void SetupModuleView::SetTimeerHandle(System::Windows::Forms::Timer ^
                                      StatePolling) {
  _StatePolling = StatePolling;
}
/***********************************************************************************************************/
#pragma region Windows Form Designer generated code
void SetupModuleView::InitializeComponent(void) {
  this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
  this->AverageAMplitude = (gcnew System::Windows::Forms::Label());
  this->label7 = (gcnew System::Windows::Forms::Label());
  this->RMSVoltage = (gcnew System::Windows::Forms::Label());
  this->label6 = (gcnew System::Windows::Forms::Label());
  this->label5 = (gcnew System::Windows::Forms::Label());
  this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
  this->TimeForceTrigger = (gcnew System::Windows::Forms::TextBox());
  this->label13 = (gcnew System::Windows::Forms::Label());
  this->HVSourceState = (gcnew System::Windows::Forms::ComboBox());
  this->GainFactorList = (gcnew System::Windows::Forms::ComboBox());
  this->TypeTriggerList = (gcnew System::Windows::Forms::ComboBox());
  this->DarkCurrent = (gcnew System::Windows::Forms::TextBox());
  this->label4 = (gcnew System::Windows::Forms::Label());
  this->FilterMVA = (gcnew System::Windows::Forms::CheckBox());
  this->ValueOfHV = (gcnew System::Windows::Forms::TextBox());
  this->Thershold = (gcnew System::Windows::Forms::TextBox());
  this->label12 = (gcnew System::Windows::Forms::Label());
  this->label11 = (gcnew System::Windows::Forms::Label());
  this->WriteParams = (gcnew System::Windows::Forms::Button());
  this->ReadParams = (gcnew System::Windows::Forms::Button());
  this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
  this->SampleRate50HertzFilter = (gcnew System::Windows::Forms::ComboBox());
  this->label3 = (gcnew System::Windows::Forms::Label());
  this->Filter50Hertz = (gcnew System::Windows::Forms::CheckBox());
  this->ADC_TypeFilter = (gcnew System::Windows::Forms::ComboBox());
  this->label2 = (gcnew System::Windows::Forms::Label());
  this->ADC_SampleRate = (gcnew System::Windows::Forms::ComboBox());
  this->label1 = (gcnew System::Windows::Forms::Label());
  this->OperationInfo = (gcnew System::Windows::Forms::Label());
  this->groupBox4->SuspendLayout();
  this->groupBox5->SuspendLayout();
  this->groupBox2->SuspendLayout();
  this->SuspendLayout();
  //
  // groupBox4
  //
  this->groupBox4->Controls->Add(this->AverageAMplitude);
  this->groupBox4->Controls->Add(this->label7);
  this->groupBox4->Controls->Add(this->RMSVoltage);
  this->groupBox4->Controls->Add(this->label6);
  this->groupBox4->Controls->Add(this->label5);
  this->groupBox4->Controls->Add(this->groupBox5);
  this->groupBox4->Controls->Add(this->HVSourceState);
  this->groupBox4->Controls->Add(this->GainFactorList);
  this->groupBox4->Controls->Add(this->TypeTriggerList);
  this->groupBox4->Controls->Add(this->DarkCurrent);
  this->groupBox4->Controls->Add(this->label4);
  this->groupBox4->Controls->Add(this->FilterMVA);
  this->groupBox4->Controls->Add(this->ValueOfHV);
  this->groupBox4->Controls->Add(this->Thershold);
  this->groupBox4->Controls->Add(this->label12);
  this->groupBox4->Controls->Add(this->label11);
  this->groupBox4->Location = System::Drawing::Point(12, 12);
  this->groupBox4->Name = L"groupBox4";
  this->groupBox4->Size = System::Drawing::Size(520, 177);
  this->groupBox4->TabIndex = 6;
  this->groupBox4->TabStop = false;
  this->groupBox4->Text = L"���������";
  //
  // AverageAMplitude
  //
  this->AverageAMplitude->AutoSize = true;
  this->AverageAMplitude->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->AverageAMplitude->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
  this->AverageAMplitude->Location = System::Drawing::Point(234, 67);
  this->AverageAMplitude->Name = L"AverageAMplitude";
  this->AverageAMplitude->Size = System::Drawing::Size(37, 15);
  this->AverageAMplitude->TabIndex = 14;
  this->AverageAMplitude->Text = L"label8";
  //
  // label7
  //
  this->label7->AutoSize = true;
  this->label7->Location = System::Drawing::Point(10, 145);
  this->label7->Name = L"label7";
  this->label7->Size = System::Drawing::Size(144, 13);
  this->label7->TabIndex = 12;
  this->label7->Text = L"�������������� ��������";
  //
  // RMSVoltage
  //
  this->RMSVoltage->AutoSize = true;
  this->RMSVoltage->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
  this->RMSVoltage->Location = System::Drawing::Point(396, 67);
  this->RMSVoltage->Name = L"RMSVoltage";
  this->RMSVoltage->Size = System::Drawing::Size(37, 15);
  this->RMSVoltage->TabIndex = 15;
  this->RMSVoltage->Text = L"label8";
  //
  // label6
  //
  this->label6->AutoSize = true;
  this->label6->Location = System::Drawing::Point(10, 118);
  this->label6->Name = L"label6";
  this->label6->Size = System::Drawing::Size(105, 13);
  this->label6->TabIndex = 11;
  this->label6->Text = L"������� ���������";
  //
  // label5
  //
  this->label5->AutoSize = true;
  this->label5->Location = System::Drawing::Point(10, 91);
  this->label5->Name = L"label5";
  this->label5->Size = System::Drawing::Size(74, 13);
  this->label5->TabIndex = 10;
  this->label5->Text = L"��� ��������";
  //
  // groupBox5
  //
  this->groupBox5->Controls->Add(this->TimeForceTrigger);
  this->groupBox5->Controls->Add(this->label13);
  this->groupBox5->Location = System::Drawing::Point(333, 91);
  this->groupBox5->Name = L"groupBox5";
  this->groupBox5->Size = System::Drawing::Size(135, 81);
  this->groupBox5->TabIndex = 7;
  this->groupBox5->TabStop = false;
  this->groupBox5->Text =
      L"��������� ������� ������� � ��������� ��������� ����";
  //
  // TimeForceTrigger
  //
  this->TimeForceTrigger->Location = System::Drawing::Point(52, 46);
  this->TimeForceTrigger->Name = L"TimeForceTrigger";
  this->TimeForceTrigger->Size = System::Drawing::Size(48, 20);
  this->TimeForceTrigger->TabIndex = 4;
  this->TimeForceTrigger->Text = L"5000";
  this->TimeForceTrigger->TextChanged +=
      gcnew System::EventHandler(this, &SetupModuleView::Thershold_TextChanged);
  this->TimeForceTrigger->Leave +=
      gcnew System::EventHandler(this, &SetupModuleView::ExternalTrigger_Leave);
  //
  // label13
  //
  this->label13->AutoSize = true;
  this->label13->Location = System::Drawing::Point(6, 48);
  this->label13->Name = L"label13";
  this->label13->Size = System::Drawing::Size(40, 13);
  this->label13->TabIndex = 3;
  this->label13->Text = L"�����";
  //
  // HVSourceState
  //
  this->HVSourceState->DropDownStyle =
      System::Windows::Forms::ComboBoxStyle::DropDownList;
  this->HVSourceState->FormattingEnabled = true;
  this->HVSourceState->Items->AddRange(
      gcnew cli::array<System::Object ^>(2){L"��������", L"�������"});
  this->HVSourceState->Location = System::Drawing::Point(157, 142);
  this->HVSourceState->Name = L"HVSourceState";
  this->HVSourceState->Size = System::Drawing::Size(121, 21);
  this->HVSourceState->TabIndex = 9;
  this->HVSourceState->SelectedIndexChanged += gcnew System::EventHandler(
      this, &SetupModuleView::SampleRate50HertzFilter_SelectionChangeCommitted);
  //
  // GainFactorList
  //
  this->GainFactorList->DropDownStyle =
      System::Windows::Forms::ComboBoxStyle::DropDownList;
  this->GainFactorList->FormattingEnabled = true;
  this->GainFactorList->Items->AddRange(
      gcnew cli::array<System::Object ^>(2){L"10X", L"1X"});
  this->GainFactorList->Location = System::Drawing::Point(157, 116);
  this->GainFactorList->Name = L"GainFactorList";
  this->GainFactorList->Size = System::Drawing::Size(121, 21);
  this->GainFactorList->TabIndex = 8;
  this->GainFactorList->SelectedIndexChanged += gcnew System::EventHandler(
      this, &SetupModuleView::ADC_TypeFilter_SelectionChangeCommitted);
  //
  // TypeTriggerList
  //
  this->TypeTriggerList->DropDownStyle =
      System::Windows::Forms::ComboBoxStyle::DropDownList;
  this->TypeTriggerList->FormattingEnabled = true;
  this->TypeTriggerList->Items->AddRange(gcnew cli::array<System::Object ^>(3){
      L"����������� ��������������", L"������� ������", L"������ �� ������"});
  this->TypeTriggerList->Location = System::Drawing::Point(157, 91);
  this->TypeTriggerList->Name = L"TypeTriggerList";
  this->TypeTriggerList->Size = System::Drawing::Size(121, 21);
  this->TypeTriggerList->TabIndex = 7;
  this->TypeTriggerList->SelectedIndexChanged += gcnew System::EventHandler(
      this, &SetupModuleView::SampleRate50HertzFilter_SelectionChangeCommitted);
  //
  // DarkCurrent
  //
  this->DarkCurrent->Location = System::Drawing::Point(180, 37);
  this->DarkCurrent->Name = L"DarkCurrent";
  this->DarkCurrent->Size = System::Drawing::Size(48, 20);
  this->DarkCurrent->TabIndex = 6;
  this->DarkCurrent->Text = L"0";
  this->DarkCurrent->TextChanged +=
      gcnew System::EventHandler(this, &SetupModuleView::Thershold_TextChanged);
  this->DarkCurrent->Leave +=
      gcnew System::EventHandler(this, &SetupModuleView::ExternalTrigger_Leave);
  //
  // label4
  //
  this->label4->AutoSize = true;
  this->label4->Location = System::Drawing::Point(10, 40);
  this->label4->Name = L"label4";
  this->label4->Size = System::Drawing::Size(102, 13);
  this->label4->TabIndex = 5;
  this->label4->Text = L"�������� ��� (DC)";
  //
  // FilterMVA
  //
  this->FilterMVA->AutoSize = true;
  this->FilterMVA->Location = System::Drawing::Point(333, 16);
  this->FilterMVA->Name = L"FilterMVA";
  this->FilterMVA->Size = System::Drawing::Size(92, 17);
  this->FilterMVA->TabIndex = 4;
  this->FilterMVA->Text = L"������ SMA";
  this->FilterMVA->UseVisualStyleBackColor = true;
  this->FilterMVA->CheckedChanged += gcnew System::EventHandler(
      this, &SetupModuleView::ExternalTrigger_CheckedChanged);
  this->FilterMVA->Leave +=
      gcnew System::EventHandler(this, &SetupModuleView::ExternalTrigger_Leave);
  //
  // ValueOfHV
  //
  this->ValueOfHV->Location = System::Drawing::Point(180, 62);
  this->ValueOfHV->Name = L"ValueOfHV";
  this->ValueOfHV->Size = System::Drawing::Size(48, 20);
  this->ValueOfHV->TabIndex = 3;
  this->ValueOfHV->Text = L"0";
  this->ValueOfHV->TextChanged +=
      gcnew System::EventHandler(this, &SetupModuleView::Thershold_TextChanged);
  this->ValueOfHV->Leave +=
      gcnew System::EventHandler(this, &SetupModuleView::ExternalTrigger_Leave);
  //
  // Thershold
  //
  this->Thershold->Location = System::Drawing::Point(180, 17);
  this->Thershold->Name = L"Thershold";
  this->Thershold->Size = System::Drawing::Size(48, 20);
  this->Thershold->TabIndex = 2;
  this->Thershold->Text = L"0";
  this->Thershold->TextChanged +=
      gcnew System::EventHandler(this, &SetupModuleView::Thershold_TextChanged);
  this->Thershold->Leave +=
      gcnew System::EventHandler(this, &SetupModuleView::ExternalTrigger_Leave);
  //
  // label12
  //
  this->label12->AutoSize = true;
  this->label12->Location = System::Drawing::Point(10, 62);
  this->label12->Name = L"label12";
  this->label12->Size = System::Drawing::Size(127, 13);
  this->label12->TabIndex = 1;
  this->label12->Text = L"���������� �� ������";
  //
  // label11
  //
  this->label11->AutoSize = true;
  this->label11->Location = System::Drawing::Point(10, 20);
  this->label11->Name = L"label11";
  this->label11->Size = System::Drawing::Size(164, 13);
  this->label11->TabIndex = 0;
  this->label11->Text = L"����� ��������(�������� DC)";
  //
  // WriteParams
  //
  this->WriteParams->Location = System::Drawing::Point(296, 236);
  this->WriteParams->Name = L"WriteParams";
  this->WriteParams->Size = System::Drawing::Size(140, 23);
  this->WriteParams->TabIndex = 8;
  this->WriteParams->Text = L"�������� ���������";
  this->WriteParams->UseVisualStyleBackColor = true;
  this->WriteParams->Click +=
      gcnew System::EventHandler(this, &SetupModuleView::WriteParams_Click);
  //
  // ReadParams
  //
  this->ReadParams->Location = System::Drawing::Point(297, 205);
  this->ReadParams->Name = L"ReadParams";
  this->ReadParams->Size = System::Drawing::Size(140, 23);
  this->ReadParams->TabIndex = 9;
  this->ReadParams->Text = L"��������� ���������";
  this->ReadParams->UseVisualStyleBackColor = true;
  this->ReadParams->Click +=
      gcnew System::EventHandler(this, &SetupModuleView::ReadParams_Click);
  //
  // groupBox2
  //
  this->groupBox2->Controls->Add(this->SampleRate50HertzFilter);
  this->groupBox2->Controls->Add(this->label3);
  this->groupBox2->Controls->Add(this->Filter50Hertz);
  this->groupBox2->Controls->Add(this->ADC_TypeFilter);
  this->groupBox2->Controls->Add(this->label2);
  this->groupBox2->Controls->Add(this->ADC_SampleRate);
  this->groupBox2->Controls->Add(this->label1);
  this->groupBox2->Location = System::Drawing::Point(12, 195);
  this->groupBox2->Name = L"groupBox2";
  this->groupBox2->Size = System::Drawing::Size(278, 117);
  this->groupBox2->TabIndex = 12;
  this->groupBox2->TabStop = false;
  this->groupBox2->Text = L"��������� ���";
  //
  // SampleRate50HertzFilter
  //
  this->SampleRate50HertzFilter->DropDownStyle =
      System::Windows::Forms::ComboBoxStyle::DropDownList;
  this->SampleRate50HertzFilter->FormattingEnabled = true;
  this->SampleRate50HertzFilter->Items->AddRange(
      gcnew cli::array<System::Object ^>(4){L"27SPS ", L"25SPS ", L"20SPS ",
                                            L"16SPS "});
  this->SampleRate50HertzFilter->Location = System::Drawing::Point(151, 66);
  this->SampleRate50HertzFilter->Name = L"SampleRate50HertzFilter";
  this->SampleRate50HertzFilter->Size = System::Drawing::Size(121, 21);
  this->SampleRate50HertzFilter->TabIndex = 9;
  this->SampleRate50HertzFilter->SelectionChangeCommitted +=
      gcnew System::EventHandler(
          this,
          &SetupModuleView::SampleRate50HertzFilter_SelectionChangeCommitted);
  this->SampleRate50HertzFilter->Leave +=
      gcnew System::EventHandler(this, &SetupModuleView::ExternalTrigger_Leave);
  //
  // label3
  //
  this->label3->AutoSize = true;
  this->label3->Location = System::Drawing::Point(6, 74);
  this->label3->Name = L"label3";
  this->label3->Size = System::Drawing::Size(98, 13);
  this->label3->TabIndex = 8;
  this->label3->Text = L"��� ������� 50hz";
  //
  // Filter50Hertz
  //
  this->Filter50Hertz->AutoSize = true;
  this->Filter50Hertz->Location = System::Drawing::Point(9, 90);
  this->Filter50Hertz->Name = L"Filter50Hertz";
  this->Filter50Hertz->Size = System::Drawing::Size(136, 17);
  this->Filter50Hertz->TabIndex = 7;
  this->Filter50Hertz->Text = L"���������� 50/60Hz ";
  this->Filter50Hertz->UseVisualStyleBackColor = true;
  this->Filter50Hertz->CheckedChanged += gcnew System::EventHandler(
      this, &SetupModuleView::Filter50Hertz_CheckedChanged);
  this->Filter50Hertz->Leave +=
      gcnew System::EventHandler(this, &SetupModuleView::ExternalTrigger_Leave);
  //
  // ADC_TypeFilter
  //
  this->ADC_TypeFilter->DropDownStyle =
      System::Windows::Forms::ComboBoxStyle::DropDownList;
  this->ADC_TypeFilter->FormattingEnabled = true;
  this->ADC_TypeFilter->Items->AddRange(
      gcnew cli::array<System::Object ^>(2){L" Sinc5+Sinc1", L" Sinc3       "});
  this->ADC_TypeFilter->Location = System::Drawing::Point(151, 39);
  this->ADC_TypeFilter->Name = L"ADC_TypeFilter";
  this->ADC_TypeFilter->Size = System::Drawing::Size(121, 21);
  this->ADC_TypeFilter->TabIndex = 6;
  this->ADC_TypeFilter->SelectionChangeCommitted += gcnew System::EventHandler(
      this, &SetupModuleView::ADC_TypeFilter_SelectionChangeCommitted);
  this->ADC_TypeFilter->Leave +=
      gcnew System::EventHandler(this, &SetupModuleView::ExternalTrigger_Leave);
  //
  // label2
  //
  this->label2->AutoSize = true;
  this->label2->Location = System::Drawing::Point(6, 51);
  this->label2->Name = L"label2";
  this->label2->Size = System::Drawing::Size(72, 13);
  this->label2->TabIndex = 5;
  this->label2->Text = L"��� �������";
  //
  // ADC_SampleRate
  //
  this->ADC_SampleRate->DropDownStyle =
      System::Windows::Forms::ComboBoxStyle::DropDownList;
  this->ADC_SampleRate->FormattingEnabled = true;
  this->ADC_SampleRate->Items->AddRange(gcnew cli::array<System::Object ^>(8){
      L"    10KPS                                                     ",
      L"    5KPS                                                     ",
      L"    2.5KPS                                                    ",
      L"    1KPS                                                     ",
      L"    500PS                                                     ",
      L"    397PS                                                    ",
      L"    200PS                                                     ",
      L"    100PS                                                              "
      L"          "
      L"     "});
  this->ADC_SampleRate->Location = System::Drawing::Point(151, 12);
  this->ADC_SampleRate->Name = L"ADC_SampleRate";
  this->ADC_SampleRate->Size = System::Drawing::Size(121, 21);
  this->ADC_SampleRate->TabIndex = 4;
  this->ADC_SampleRate->SelectionChangeCommitted += gcnew System::EventHandler(
      this, &SetupModuleView::ADC_SampleRate_SelectionChangeCommitted);
  this->ADC_SampleRate->Leave +=
      gcnew System::EventHandler(this, &SetupModuleView::ExternalTrigger_Leave);
  //
  // label1
  //
  this->label1->AutoSize = true;
  this->label1->Location = System::Drawing::Point(6, 24);
  this->label1->Name = L"label1";
  this->label1->Size = System::Drawing::Size(102, 13);
  this->label1->TabIndex = 3;
  this->label1->Text = L"�������� �������";
  //
  // OperationInfo
  //
  this->OperationInfo->AutoSize = true;
  this->OperationInfo->BorderStyle =
      System::Windows::Forms::BorderStyle::Fixed3D;
  this->OperationInfo->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
  this->OperationInfo->Location = System::Drawing::Point(296, 294);
  this->OperationInfo->Name = L"OperationInfo";
  this->OperationInfo->Size = System::Drawing::Size(124, 15);
  this->OperationInfo->TabIndex = 13;
  this->OperationInfo->Text = L"���������� � ������";
  //
  // SetupModuleView
  //
  this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
  this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
  this->ClientSize = System::Drawing::Size(541, 320);
  this->Controls->Add(this->OperationInfo);
  this->Controls->Add(this->groupBox2);
  this->Controls->Add(this->ReadParams);
  this->Controls->Add(this->WriteParams);
  this->Controls->Add(this->groupBox4);
  this->FormBorderStyle =
      System::Windows::Forms::FormBorderStyle::FixedToolWindow;
  this->MaximizeBox = false;
  this->Name = L"SetupModuleView";
  this->Text = L"��������� ������";
  this->groupBox4->ResumeLayout(false);
  this->groupBox4->PerformLayout();
  this->groupBox5->ResumeLayout(false);
  this->groupBox5->PerformLayout();
  this->groupBox2->ResumeLayout(false);
  this->groupBox2->PerformLayout();
  this->ResumeLayout(false);
  this->PerformLayout();
}
#pragma endregion
