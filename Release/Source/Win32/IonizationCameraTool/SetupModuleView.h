#pragma once
#ifndef _SETUP_VIEV_
#define _SETUP_VIEV_
#include "IC_Module.h"
#include <stdint.h>

namespace IonizationCameraTool {

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace Microsoft::Win32;

public
ref class SetupModuleView : public System::Windows::Forms::Form {

private:
  ~SetupModuleView();
#pragma region Windows Form Designer generated code
  void InitializeComponent(void);
#pragma endregion
  System::Void WriteParams_Click(System::Object ^ sender,
                                 System::EventArgs ^ e);
  System::Void ReadParams_Click(System::Object ^ sender, System::EventArgs ^ e);
  System::Void ADC_SampleRate_SelectionChangeCommitted(System::Object ^ sender,
                                                       System::EventArgs ^ e);
  System::Void ADC_TypeFilter_SelectionChangeCommitted(System::Object ^ sender,
                                                       System::EventArgs ^ e);
  System::Void
      SampleRate50HertzFilter_SelectionChangeCommitted(System::Object ^ sender,
                                                       System::EventArgs ^ e);
  System::Void Filter50Hertz_CheckedChanged(System::Object ^ sender,
                                            System::EventArgs ^ e);
  System::Void SetupModuleView_Activated(System::Object ^ sender,
                                         System::EventArgs ^ e);
  System::Void SetupModuleView_Deactivate(System::Object ^ sender,
                                          System::EventArgs ^ e);
  System::Void ExternalTrigger_CheckedChanged(System::Object ^ sender,
                                              System::EventArgs ^ e);
  System::Void Thershold_TextChanged(System::Object ^ sender,
                                     System::EventArgs ^ e);
  System::Void
      ExternalTrigger_MouseClick(System::Object ^ sender,
                                 System::Windows::Forms::MouseEventArgs ^ e);
  System::Void ExternalTrigger_Leave(System::Object ^ sender,
                                     System::EventArgs ^ e);
  System::Void SetupModuleView_Leave(System::Object ^ sender,
                                     System::EventArgs ^ e);

  System::ComponentModel::Container ^ components;
  System::Windows::Forms::GroupBox ^ groupBox4;
  System::Windows::Forms::TextBox ^ ValueOfHV;
  System::Windows::Forms::TextBox ^ Thershold;
  System::Windows::Forms::Label ^ label12;
  System::Windows::Forms::Label ^ label11;
  System::Windows::Forms::GroupBox ^ groupBox5;
  System::Windows::Forms::Label ^ label13;
  System::Windows::Forms::Label ^ label7;
  System::Windows::Forms::Label ^ label6;
  System::Windows::Forms::Label ^ label5;
  System::Windows::Forms::ComboBox ^ HVSourceState;
  System::Windows::Forms::ComboBox ^ GainFactorList;
  System::Windows::Forms::ComboBox ^ TypeTriggerList;
  System::Windows::Forms::Label ^ OperationInfo;
  System::Windows::Forms::Label ^ RMSVoltage;
  System::Windows::Forms::Label ^ AverageAMplitude;
  System::Windows::Forms::Timer ^ _StatePolling;

  System::Windows::Forms::TextBox ^ TimeForceTrigger;
  System::Windows::Forms::GroupBox ^ groupBox2;
  System::Windows::Forms::ComboBox ^ SampleRate50HertzFilter;
  System::Windows::Forms::Label ^ label3;
  System::Windows::Forms::CheckBox ^ Filter50Hertz;
  System::Windows::Forms::ComboBox ^ ADC_TypeFilter;
  System::Windows::Forms::Label ^ label2;
  System::Windows::Forms::ComboBox ^ ADC_SampleRate;
  System::Windows::Forms::Label ^ label1;
  System::Windows::Forms::CheckBox ^ FilterMVA;
  System::Windows::Forms::TextBox ^ DarkCurrent;
  System::Windows::Forms::Label ^ label4;

  std::shared_ptr<IC_Module> *_IC_Module;
  uint16_t _TimeHV;
  uint16_t _ValueV;
  uint8_t _ADCSampleRate;
  uint8_t _ADCTypeFilter;
  uint8_t _ADC50HerzFilterState;
  uint8_t _ADC50HerzFilterRate;
  uint32_t _Thershold;

public:
  SetupModuleView(void);
  void WriteParamsInModule(void);
  void ReadParamsFromModule(void);
  void ReadParamsFromModuleExtended(void);
  bool CheckIsModuleConected(void);
  void SetICModuleHandle(std::shared_ptr<ICM> *IC_Module);
  void SetTimeerHandle(System::Windows::Forms::Timer ^ StatePolling);
  uint16_t GetTimeHV(void);
  System::Windows::Forms::Button ^ WriteParams;
  System::Windows::Forms::Button ^ ReadParams;
};
} // namespace IonizationCameraTool
#endif
