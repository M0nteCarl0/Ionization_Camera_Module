#include "IC_Core.hpp"
#include "MAV_Filter.hpp"
#include "DAC_HV.h"
#include "Amplifer_control.h"
#include "LED.h"
#include "WindowsFilter.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ByteConversion.hpp"
#define NUM_AMPLITUDES 2000
using namespace Conversion;

   static ControlWord Control;
   static StateWord   State;
   uint8_t     ComputeResultComplete;
   uint32_t    ThersholdTrigger;
   int64_t     GlobalDosa = 0;
   int64_t     LocalDosa  = 0 ;
  
   uint8_t     CounterEventsTrigger;
   uint32_t    DurabilityExposition;
   int32_t    AvergageAmplitudeMeasure;
   uint32_t    QuanityMeasuredAmplitudes;
   uint16_t    ValueHighVoltage;
   uint16_t    CurrentStateExternal;
   uint16_t    LastStateExternal;
   
   int32_t    Amplitudes[NUM_AMPLITUDES];
 
   uint32_t    TimeotThershold = 8000;//8,1Sec
   uint64_t    TimeAQ = 0;
   uint64_t    TimeAQPolling = 0;
   uint64_t    NumberSamplesFilter = 0;
   uint64_t    DarkCurrentLevel = 0;
   uint64_t    TriggerLevel;
   uint64_t    FilterSampleCounter = 0;
   int64_t     LocalDosaStorage = 0;   
   MyMemmo WindowFilterKy;
   MVA_Filter MovingAvergage;
   int Freqe = 0;
   int ScaleFactorDosa = 0;
   int BlockMeasure;
   int SourceStartC;
 /*****************************************************************************/  
   void  Core_manual_Trigger_Init(void)
{
   RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8,ENABLE);
   TIM_TimeBaseInitTypeDef _TimerSetup;
   TIM_TimeBaseStructInit(&_TimerSetup);
  _TimerSetup.TIM_ClockDivision     = TIM_CKD_DIV1;
  _TimerSetup.TIM_CounterMode       = TIM_CounterMode_Up;
  _TimerSetup.TIM_Period            = 0;
  _TimerSetup.TIM_Prescaler         = Core_manual_Trigger_DIV; 
  _TimerSetup.TIM_RepetitionCounter = 0;
   TIM_TimeBaseInit(TIM8,&_TimerSetup);
   TIM_ITConfig(TIM8,TIM_IT_Update,DISABLE);
   TIM_ClearITPendingBit(TIM8,TIM_IT_Update);
   TIM_Cmd(TIM8,DISABLE);
   
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel                    = TIM8_UP_TIM13_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 2;   //1;
    NVIC_InitStructure.NVIC_IRQChannelCmd                 = DISABLE;
    NVIC_Init (&NVIC_InitStructure);
  
}
/*****************************************************************************/
void Core_Init_Polling_Timer(void)
{
  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
  TIM_TimeBaseInitTypeDef _TimerSetup;;
  _TimerSetup.TIM_ClockDivision = TIM_CKD_DIV1;
  _TimerSetup.TIM_CounterMode  = TIM_CounterMode_Up;
  _TimerSetup.TIM_Prescaler = Polling_Timer_DIV;
  _TimerSetup.TIM_Period = TimeAQPolling-1;
  TIM_TimeBaseInit(TIM3,&_TimerSetup);
  TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
  TIM_Cmd(TIM3,ENABLE);
  TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
  
   NVIC_InitTypeDef _SetupIRQ;
  _SetupIRQ.NVIC_IRQChannel =TIM3_IRQn;
  _SetupIRQ.NVIC_IRQChannelPreemptionPriority = 0;
  _SetupIRQ.NVIC_IRQChannelSubPriority = 1;
  _SetupIRQ.NVIC_IRQChannelCmd = ENABLE;
   NVIC_Init(&_SetupIRQ);
}
/*****************************************************************************/
void  Core_manual_Trigger_Stop(void)
{
    TIM_Cmd(TIM8,DISABLE);
    TIM_ITConfig(TIM8,TIM_IT_Update,DISABLE);
    NVIC_DisableIRQ(TIM8_UP_TIM13_IRQn);
    TIM_SetCounter(TIM8,0);
}
/*****************************************************************************/
void  Core_manual_Trigger_Start(uint16_t Duration)
{
     BlockMeasure = false;
     if(Duration == 1 || Duration == 0 )
     {
        Duration = 2;
     }
      Core_CleanAmplitudeBuffer();
      TIM_SetAutoreload(TIM8,Duration-1);
      TIM_SetCounter(TIM8,0);
      NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);
      TIM_ITConfig(TIM8,TIM_IT_Update,ENABLE);
      TIM_Cmd(TIM8,ENABLE);
      
      SourceStartC =  Control.Bits.TypeTrigger;
      Control.Bits.TypeTrigger  = SourceStart_External;
      
      State.Bits.AqusitionState  = AqusitionState_InProgress;
      State.Bits.SourceStart     = SourceStart_Forced;
      State.Bits.WaitMeasure = 0;
      State.Bits.EndMeasure = 0;
      
      ComputeResultComplete     = 0;
      QuanityMeasuredAmplitudes = 0;
      LocalDosa                 = 0;
      //CounterEventsTrigger++;
      RedLedOn();
      Core_ExpositionTimeCounterStart();
  
}
/*****************************************************************************/
void  TIM8_UP_TIM13_IRQHandler(void)
{
  
   if(TIM_GetITStatus(TIM8,TIM_IT_Update)!=RESET)
  {     
     TIM_ClearITPendingBit(TIM8,TIM_IT_Update);
    
     if(State.Bits.AqusitionState == AqusitionState_InProgress && State.Bits.SourceStart == SourceStart_Forced && !BlockMeasure )
     {
        Core_ExpositionTimeCounterStop();
        RedLedOff();
        State.Bits.AqusitionState = AqusitionState_Complete;
        State.Bits.TimeoutEventIndicator =  TimeoutEventIndicator_Measure_Normal;
        State.Bits.WaitMeasure = 0;
        State.Bits.EndMeasure = 1;
        Control.Bits.TypeTrigger  = SourceStartC;
        BlockMeasure = true;
     }
  
  }
}
/*****************************************************************************/
inline  void Core_ExternalTriggerEvent(void)
{
  
  
  if(Control.Bits.TypeTrigger == SourceStart_External)
 {
  
  WindowFilterKy.Put(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_0));     
  int  SumS0 = WindowFilterKy.GetAdd(0,3);
  int  SumS1 = WindowFilterKy.GetAdd(4,7);
  if (SumS0 >= 3 && SumS1 <= 1)
  {
        if(State.Bits.AqusitionState == AqusitionState_Complete && !BlockMeasure)
        {  
              Core_ExpositionTimeCounterStart();
              Core_CleanAmplitudeBuffer();
              State.Bits.AqusitionState = AqusitionState_InProgress;
              State.Bits.SourceStart    = SourceStart_External;
              State.Bits.WaitMeasure = 0;
              State.Bits.EndMeasure = 0;
              QuanityMeasuredAmplitudes = 0;
              ComputeResultComplete     = 0;
              LocalDosa                 = 0;
              CounterEventsTrigger++;
              RedLedOn();
            
        } 
   }
                                          
   if (SumS0 <= 1 && SumS1 >= 3)
   {
     
     if(State.Bits.AqusitionState == AqusitionState_InProgress && !BlockMeasure)
     {
           BlockMeasure = true;
           Core_ExpositionTimeCounterStop();
           State.Bits.WaitMeasure = 0;
           State.Bits.EndMeasure = 1;
           State.Bits.AqusitionState = AqusitionState_Complete;
           State.Bits.TimeoutEventIndicator =  TimeoutEventIndicator_Measure_Normal;
           RedLedOff();
          
     }
     
   }
  
  }
}
/*****************************************************************************/
void  TIM3_IRQHandler(void)
{
  
    if(TIM_GetITStatus(TIM3,TIM_IT_Update)!=RESET)
  {  
     TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
     Core_ExternalTriggerEvent();
     RSTATUS_REGISTER Status = ReadStatusRegister(); 
     int32_t CurrentSample;
     if(!Status.BITS.RDY)
     {
      CurrentSample = ReadADCData() >> 3 ; 
      if(Control.Bits.MovingAvergageFilter)
      {
         MovingAvergage.PutSample(CurrentSample);
         CurrentSample = MovingAvergage.GetResulst();
      }
      Core_MeasureData(CurrentSample, Status);
   }
}
}
/*****************************************************************************/
void Core_External_Trigget_Init(void)
{
   GPIO_InitTypeDef _HV_Trigge;
   RCC_APB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);    
  _HV_Trigge.GPIO_Mode                        = GPIO_Mode_IN;
  _HV_Trigge.GPIO_Pin                         = GPIO_Pin_0;
  _HV_Trigge.GPIO_PuPd                        = GPIO_PuPd_NOPULL;
  _HV_Trigge.GPIO_Speed                       = GPIO_Speed_2MHz;
   GPIO_Init(GPIOB,&_HV_Trigge);
   WindowFilterKy.Create(16);
   WindowFilterKy.Put(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_0));
}
/************************************************************************/
uint8_t   Core_External_Trigger_ReadInput(void)
{
  return  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_8);
}
/************************************************************************/
bool  Core_External_Trigger_RissingEdgeDeteced(void)
{
  return Core_External_Trigger_ReadInput()!=0;
}
/************************************************************************/
void Core_Init(void)
{
  
  Control.Raw                                 = 0;
  Control.Bits.GainFactor                     = GainFactor_1X;
  Control.Bits.StateHV                        = StateHV_On;
  Control.Bits.TypeTrigger                    = TypeTrigger_External;            
  State.Raw                                   = 0;
  State.Bits.AqusitionState                   = AqusitionState_Complete;
  State.Bits.SourceStart                      = SourceStart_External;
  State.Bits.TimeoutEventIndicator            = TimeoutEventIndicator_Measure_Normal;
  State.Bits.WaitMeasure                      = 0;
  State.Bits.EndMeasure                       = 0;
  
  ValueHighVoltage                            = 20;             
  ThersholdTrigger                            = 100;
  DarkCurrentLevel                            = 0;
  TriggerLevel                                = ThersholdTrigger + DarkCurrentLevel;
  MovingAvergage                              = MVA_Filter();
  Core_Init_SetSampleRate(ODR_2_5KPS);
  SingleChannelSetup(ODR_2_5KPS);
  Core_ResetAllMeasurements();
  Core_Init_ExpositionTimeCounter();
  Core_External_Trigget_Init();     
  Core_Init_Polling_Timer();
  Core_manual_Trigger_Init();
  LastStateExternal                           = GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_0);
  Core_SetControl(Control.Raw);
  Core_SetValueHighVoltage(ValueHighVoltage);
  Core_ResetAllMeasurements();
  State.Bits.WaitMeasure                      = 0;
   
};
/************************************************************************/
uint32_t    Core_GetQuanityMeasuredAmplitudes(void)
{
  return QuanityMeasuredAmplitudes;
}
/************************************************************************/
int32_t*   Core_GetAmplitudes(void)
{
  return Amplitudes;
}
/************************************************************************/
ControlWord  Core_GetControl(void)
{
  return Control;
}
/************************************************************************/
StateWord    Core_GetState(void)
{
  return State;
}
/************************************************************************/
uint32_t     Core_GetThersholdTrigger(void)
{
  return ThersholdTrigger;
}
/************************************************************************/
int64_t     Core_GetGlobalDosa(void)
{
  return GlobalDosa;
}
/************************************************************************/
int64_t     Core_GetLocalDosa(void)
{
  return LocalDosaStorage;
}
/************************************************************************/
uint8_t      Core_GetCounterEventsTrigger(void)
{
  return CounterEventsTrigger;
}
/************************************************************************/
uint32_t     Core_GetDurabilityExposition(void)
{
  return DurabilityExposition;
}
/************************************************************************/
int32_t     Core_GetAvergageAmplitudeMeasure(void)
{
  return AvergageAmplitudeMeasure;
}
/************************************************************************/
uint16_t     Core_GetValueHighVoltage(void)
{
  return ValueHighVoltage;
}
/************************************************************************/
void        Core_SetControl(uint8_t Value)
{
  
    Control.Raw = Value;
    
    switch (Control.Bits.GainFactor)
    {
      
      case GainFactor_10X:
      {
        
        AmpliferControl_SetGain10x();
        break;
      }
         
      case GainFactor_1X:
      {
        AmpliferControl_SetGain1x();
        break;
      }
   
      default:
      {
        break;
      }
    
  
    }
    
   
     switch (Control.Bits.StateHV)
    {
      
      case StateHV_On:
      {
        DAC_HV_Enable();
        break;  
      }
      
      case StateHV_Off:
      {
        DAC_HV_Disable();     
        break; 
      }
  
      default:
      {
        break;
      }
      
    }
    
    
    BlockMeasure = false;
    State.Bits.AqusitionState =   AqusitionState_Complete;
    
    State.Bits.WaitMeasure = 1;
    State.Bits.EndMeasure = 0;
    
    if(Control.Bits.TypeTrigger ==   TypeTrigger_InfiniteIntegration)
    {
       State.Bits.SourceStart               = SourceStart_InfiniteIntegration;
       State.Bits.AqusitionState            = AqusitionState_InProgress;
       RedLedOn();
       Core_ResetAllMeasurements();
       Core_ExpositionTimeCounterStart();
    }
     if(Control.Bits.TypeTrigger !=  TypeTrigger_InfiniteIntegration)
    {
      if(State.Bits.AqusitionState == AqusitionState_InProgress)
      {
        State.Bits.AqusitionState            = AqusitionState_Complete;
        Core_ExpositionTimeCounterStop();
        RedLedOff();
      }
    }
    
    
    
    
}
/************************************************************************/
void        Core_SetValueHighVoltage(uint16_t Value)
{  
    ValueHighVoltage = Value;
    uint16_t ConvertedVoltagesToMSB =  (ValueHighVoltage * 0.96)/0.129296875;
    DAC_HV_SetValue(ConvertedVoltagesToMSB);    
}
 /************************************************************************/   
void        Core_SetThersholdTrigger(uint32_t Value)
{
    TIM_Cmd(TIM3,DISABLE);
    ThersholdTrigger  =  Value;
    TriggerLevel = DarkCurrentLevel + ThersholdTrigger;
    TIM_Cmd(TIM3,ENABLE);
}
/************************************************************************/
void         Core_ComputeResult(void)
{
  if((!ComputeResultComplete && State.Bits.AqusitionState == AqusitionState_Complete) || Control.Bits.TypeTrigger ==   TypeTrigger_InfiniteIntegration)
  {
    AvergageAmplitudeMeasure  = 0;
    if( Control.Bits.TypeTrigger !=TypeTrigger_InfiniteIntegration)
    {
      AvergageAmplitudeMeasure  = LocalDosa / QuanityMeasuredAmplitudes;
      LocalDosa/=ScaleFactorDosa;
      if( State.Bits.SourceStart != SourceStart_Forced){
      GlobalDosa+=LocalDosa;
      }
      LocalDosaStorage = LocalDosa;
      
    }
    DurabilityExposition = Core_ExpositionTimeCounterGetTime();
    ComputeResultComplete = 1;
  }
}
/************************************************************************/
void         Core_ResetAllMeasurements(void)
{
  
    TIM_Cmd(TIM3,DISABLE);
    GlobalDosa                                  = 0;
    LocalDosa                                   = 0;
    CounterEventsTrigger                        = 0;
    DurabilityExposition                        = 0;
    AvergageAmplitudeMeasure                    = 0;
    QuanityMeasuredAmplitudes                   = 0;
    LocalDosaStorage                            = 0; 
    TIM_SetCounter(TIM2,0);    
    Core_CleanAmplitudeBuffer();
    TIM_Cmd(TIM3,ENABLE);
  
}
/************************************************************************/
bool         Core_CopyValuesOfArrayToBuffer(uint8_t* DestinationBuffer,uint8_t Offset )
{
  
  bool Flag  = false;
  uint16_t BeginPosition = 30 * Offset;
  if(BeginPosition  > NUM_AMPLITUDES - 30){ return Flag;}
  else
  {
    Flag = true;
  }
  uint16_t EndPostion    = 30 * (Offset+1);
  uint16_t PositionInDestinationBuffer = 0;
  for(uint16_t i = BeginPosition;i<EndPostion;i++)
  {
    
   Signed32BitToUnsigned7bitFormat_v2(  Amplitudes[i],
                                     DestinationBuffer[PositionInDestinationBuffer],
                                     DestinationBuffer[PositionInDestinationBuffer+1] ,
                                     DestinationBuffer[PositionInDestinationBuffer+2],
                                     DestinationBuffer[PositionInDestinationBuffer+3] 
                                     ); 
    PositionInDestinationBuffer+=4;
  }
  return Flag;
};
/************************************************************************/
inline  void         Core_CleanAmplitudeBuffer(void)
{
  for(uint16_t i = 0;i<NUM_AMPLITUDES;i++)
    {
       Amplitudes[i]                            = 0;
    }
}
/************************************************************************/
void Core_ComputePolling( int Frequncy)
{
   if(Frequncy <=0) { return;}
 
   TimeAQ = 1000000 / Frequncy;
   Freqe = Frequncy;
   ScaleFactorDosa = Freqe/10;
   Core_SetupMVAFilter(TimeAQ);
   TimeAQPolling  = TimeAQ/2;
  
}
/************************************************************************/
void Core_Init_SetSampleRate(ODR SampleRate)
{

  switch (SampleRate)
  {
   
  case  ODR_250KPS:
    {
      Core_ComputePolling(250000);
      break;  
    }
    
  case   ODR_125KPS:
    {
        Core_ComputePolling(125000);
        break;  
      
    }
    
  case   ODR_62_5KPS:
    {
         Core_ComputePolling(62500);
         break;  
    }
    
  case   ODR_50KPS:
    {
         Core_ComputePolling(50000);    
         break;  
    }
    
  case   ODR_31_25KPS:
    {
         Core_ComputePolling(31250);    
         break;  
      
    }
  case   ODR_25KPS:
    {
         Core_ComputePolling(25000);    
         break;  
    }
  case   ODR_15_628KPS:
    {
         Core_ComputePolling(15628);   
         break;  
      
    }
  case   ODR_10KPS:
    {
          Core_ComputePolling(10000);  
          break;  
    }
  case   ODR_5KPS:
    {
          Core_ComputePolling(5000); 
          break;  
    }
    
  case   ODR_2_5KPS:
    {
          Core_ComputePolling(2500); 
          break;  
    }
    
  case   ODR_1KPS:
    {
         Core_ComputePolling(1000); 
         break;  
    }
    
  case   ODR_500PS:
    {
         Core_ComputePolling(500); 
         break;  
    }
  case   ODR_397PS:
    {
        Core_ComputePolling(397); 
        break;  
    }
  case   ODR_200PS:
    {
        Core_ComputePolling(200); 
        break;  
    }
    
  case   ODR_100PS:
    {
         Core_ComputePolling(100);
         break;  
    }
    
  case   ODR_59PS:
    {
         Core_ComputePolling(59);
         break;  
    }
    
  case   ODR_49PS:
    {
         Core_ComputePolling(49);
         break;  
    }
    
  case   ODR_20PS:
    {
         Core_ComputePolling(20);
         break;   
    }
     
  case   ODR_16PS:
    {
        Core_ComputePolling(16);
        break;   
    }
    
  case   ODR_10KS:
    {
        Core_ComputePolling(10);
        break;   
    }
    
  case   ODR_5PS:
    {
        Core_ComputePolling(5);
        break;     
    }
    
  default:
    {
        break;
    }
}
}
/************************************************************************/
void Core_WriteSetupADC(ODR SampleRate,ORDER FilterType,uint8_t Rejection50Hertz,ENHFILT RejectionFilterRate)
{
  if(State.Bits.AqusitionState == AqusitionState_Complete)
  {
    
    if(SampleRate >= ODR_10KPS  && SampleRate <= ODR_100PS )
    {
        TIM_Cmd(TIM3,DISABLE);
        RFILTER_CONFIGURATION_REGISTER Filter;
        Filter.FILTER_CONFIGURATION_REGISTER_BITS.ODR       = SampleRate;
        Filter.FILTER_CONFIGURATION_REGISTER_BITS.ORDER     = FilterType; //ORDER_Sinc3;//ORDER_Sinc5_Sinc1;
        Filter.FILTER_CONFIGURATION_REGISTER_BITS.ENHFILTEN = Rejection50Hertz; 
        Filter.FILTER_CONFIGURATION_REGISTER_BITS.SINC3_MAP = 0;
        Filter.FILTER_CONFIGURATION_REGISTER_BITS.ENHFILT   = RejectionFilterRate;
        WriteFilterConfigurationRegister(Filter,0);
        Core_Init_SetSampleRate(SampleRate);
        TIM_SetAutoreload(TIM3,TimeAQPolling);
        TIM_Cmd(TIM3,ENABLE);
    }
 
  }
}
/************************************************************************/
void Core_ReadSetupADC(ODR& SampleRate,ORDER& FilterType,uint8_t& Rejection50Hertz,ENHFILT& RejectionFilterRate)
{
 
    RFILTER_CONFIGURATION_REGISTER FilterR;
    FilterR =ReadFilterConfigurationRegister(0);
    SampleRate         =(ODR) FilterR.FILTER_CONFIGURATION_REGISTER_BITS.ODR;
    FilterType         =(ORDER) FilterR.FILTER_CONFIGURATION_REGISTER_BITS.ORDER;
    Rejection50Hertz   = FilterR.FILTER_CONFIGURATION_REGISTER_BITS.ENHFILTEN;
    RejectionFilterRate =(ENHFILT) FilterR.FILTER_CONFIGURATION_REGISTER_BITS.ENHFILT;
}
/************************************************************************/
void  Core_Init_ExpositionTimeCounter(void)
{
  
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
   TIM_TimeBaseInitTypeDef _TimerSetup;
   TIM_TimeBaseStructInit(&_TimerSetup);
  _TimerSetup.TIM_ClockDivision     = TIM_CKD_DIV1;
  _TimerSetup.TIM_CounterMode       = TIM_CounterMode_Up;
  _TimerSetup.TIM_Period            = 0xFFFFFFFF;
  _TimerSetup.TIM_Prescaler         = Core_manual_Trigger_DIV; //42000;
  _TimerSetup.TIM_RepetitionCounter = 0;
   TIM_TimeBaseInit(TIM2,&_TimerSetup);
   TIM_Cmd(TIM2,DISABLE);
  
}
/************************************************************************/
void  Core_ExpositionTimeCounterStart(void)
{
   TIM_SetCounter(TIM2,0);
   TIM_Cmd(TIM2,ENABLE);  
  
}
/************************************************************************/
void  Core_ExpositionTimeCounterStop(void)
{
   TIM_Cmd(TIM2,DISABLE);  
}
/************************************************************************/
uint32_t  Core_ExpositionTimeCounterGetTime(void)
{
  return TIM_GetCounter(TIM2);
}
/************************************************************************/
void         Core_SetupMVAFilter(uint64_t TimePerSample_us)
{
   NumberSamplesFilter = 20000/TimePerSample_us;
   MovingAvergage.SetWindowSize(NumberSamplesFilter);
}
/************************************************************************/
uint32_t     Core_GetDarkCurrent(void)
{
  return DarkCurrentLevel;
}

/************************************************************************/
void        Core_SetDarkCurrent(uint32_t Value)
{
  TIM_Cmd(TIM3,DISABLE);
  DarkCurrentLevel = Value;
  TriggerLevel = DarkCurrentLevel + ThersholdTrigger;
  TIM_Cmd(TIM3,ENABLE);
  
}
/************************************************************************/
void  Core_MeasureData(int32_t CurrentSample, RSTATUS_REGISTER Status)
{
  
       if(Control.Bits.TypeTrigger == TypeTrigger_Thershold)
     {
       if(CurrentSample > TriggerLevel 
          && State.Bits.AqusitionState == AqusitionState_Complete && !BlockMeasure)
       {
          Core_ExpositionTimeCounterStart();
          Core_CleanAmplitudeBuffer();
          State.Bits.AqusitionState            = AqusitionState_InProgress;
          State.Bits.SourceStart               = SourceStart_Thershold;
          State.Bits.WaitMeasure = 0;
          State.Bits.EndMeasure = 0;
          QuanityMeasuredAmplitudes            = 0;
          ComputeResultComplete                = 0;
          LocalDosa                            = 0;
          CounterEventsTrigger++;
          RedLedOn();
          
       }
       
       if(CurrentSample <= TriggerLevel && 
          State.Bits.AqusitionState == AqusitionState_InProgress &&  !BlockMeasure)
       {
          Core_ExpositionTimeCounterStop();
          State.Bits.AqusitionState            = AqusitionState_Complete;
          State.Bits.TimeoutEventIndicator     =  TimeoutEventIndicator_Measure_Normal;
          State.Bits.WaitMeasure = 0;
          State.Bits.EndMeasure = 1;
          RedLedOff();
          BlockMeasure = true;
          
       }
     }
     
     if(State.Bits.AqusitionState ==  AqusitionState_InProgress)
     {
       if(!Status.BITS.RDY)
       {
          if(QuanityMeasuredAmplitudes < NUM_AMPLITUDES)
          {
              Amplitudes[QuanityMeasuredAmplitudes]= CurrentSample - DarkCurrentLevel;
          }
          
            LocalDosa+=(CurrentSample - DarkCurrentLevel);
            QuanityMeasuredAmplitudes++;
            
            if(LocalDosa == 9223372036854775807 || 
               LocalDosa == -9223372036854775807 || Core_ExpositionTimeCounterGetTime()  ==   0xFFFFFFFF-1)
               { 
                 Core_ExpositionTimeCounterStop();
                 State.Bits.AqusitionState        =  AqusitionState_Complete;
                 State.Bits.TimeoutEventIndicator =  TimeoutEventIndicator_Measure_Normal;
                 State.Bits.WaitMeasure = 0;
                 State.Bits.EndMeasure = 1;
                 RedLedOff();
                 BlockMeasure = true;
               }
       }
     
      if(State.Bits.SourceStart != SourceStart_InfiniteIntegration)
      {
          if(Core_ExpositionTimeCounterGetTime() >= TimeotThershold )
        {
          Core_ExpositionTimeCounterStop();
          State.Bits.AqusitionState = AqusitionState_Complete;
          State.Bits.TimeoutEventIndicator =  TimeoutEventIndicator_Measure_Triggered;
          State.Bits.WaitMeasure = 0;
          State.Bits.EndMeasure = 1;
          RedLedOff();
          BlockMeasure = true;
        }
      }
     } 
}
/************************************************************************/
 