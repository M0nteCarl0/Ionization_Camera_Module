#include <stdint.h>

  typedef struct DiagnostcStateWordS
{
   uint8_t SensorWarning:1;
   uint8_t EngineWarning:1;
}DiagnostcStateWordS;
/*****************************************************************************/
typedef union DiagnostcStateWord
{
  DiagnostcStateWordS Bits;
  uint8_t Raw;
}DiagnostcStateWord;;


#ifdef __cplusplus
 extern "C" {
#endif 
void HardFault_Handler(void);
void MemManage_Handler (void);
void BusFault_Handler(void);
void UsageFault_Handler (void);
void  NMI_Handler(void);

void SetSensorWarning(void);
void ResetSensorWarning(void);
void SetEngineWarning(void);
void ResetEngineWarning(void);
uint8_t GetWarningWord(void);
void CheckEngine(void);
#ifdef __cplusplus
 }
#endif 