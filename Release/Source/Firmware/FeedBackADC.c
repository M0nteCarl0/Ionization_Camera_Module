#include "FeedBackADC.h"
#include <math.h>
uint16_t AmplitudeBuffer[1000];
double RMS_IN;
double AVG_IN;

/*****************************************************************************/
void  FeedBackADC_Init(void)
{
  FeedBackADC_GPIOInit(); 
  FeedBackADC_ClockInit();
  
  
}
/*****************************************************************************/
void FeedBackADC_ClockInit(void)
{
 /*
   APB2 CLK             = 21 Mhz
   ADC_Prescaler        = 2
   ADC_CLK              = 10.5 Mhz
   T_ADC_CLK            = 0.095 uSec
   Channel_SampleTime   = 144(144*0.095us ->14 uSec)
   F_Channel_Conversiom = 67 Khz 
   */
  
  ADC_InitTypeDef _ADCSetup;
  ADC_CommonInitTypeDef _ComonInitADC;
  ::ADC_CommonStructInit(&_ComonInitADC);
  _ComonInitADC.ADC_Prescaler = ADC_Prescaler_Div2;// 21Mhz(APB2 CLK) /2 = 10.5 Mhz(100us)
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
  ADC_StructInit(&_ADCSetup);
  _ADCSetup.ADC_ContinuousConvMode =  ENABLE;
  _ADCSetup.ADC_DataAlign = ADC_DataAlign_Right;
  _ADCSetup.ADC_ExternalTrigConvEdge =  ADC_ExternalTrigConvEdge_None;
  _ADCSetup.ADC_Resolution  = ADC_Resolution_12b;
  _ADCSetup.ADC_ScanConvMode = ENABLE;
  _ADCSetup.ADC_NbrOfConversion = 1;
  ADC_RegularChannelConfig(ADC1,ADC_Channel_0,1,ADC_SampleTime_144Cycles);//144*0.1ms = 14.4 ms  - 70 KHz;
  ADC_Init(ADC1,&_ADCSetup);
  ADC_Cmd(ADC1,ENABLE);
  ADC_SoftwareStartConv(ADC1);
  
 
}
/*****************************************************************************/
void FeedBackADC_GPIOInit(void)
{
  RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOA,ENABLE); 
  GPIO_InitTypeDef GPIO_InitStructure;
  
  GPIO_InitStructure.GPIO_Pin          = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode         = GPIO_Mode_AN;          
  GPIO_Init (GPIOA, &GPIO_InitStructure); 
 
}
/*****************************************************************************/
uint16_t GetMeasuredVoltageLevel(void)
{
  uint16_t Value = ADC_GetConversionValue(ADC1);
  return  Value;
}
/*****************************************************************************/
void FeedBackADC_GetMeasuredVoltageParams(uint32_t* RMS_HV,uint32_t* AVG_HV)
{
  
  AVG_IN  = 0;
  RMS_IN  = 0;
  double Delta = 0;
  for(uint16_t i = 0;i<100;i++)
  {
    AmplitudeBuffer[i] = 0;
    
  }
   for(uint16_t i  = 0;i<100;i++)
  {
    do
    {
      
    }
    while(ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC)!=SET);
    AmplitudeBuffer[i] =  ADC_GetConversionValue(ADC1);
  
  }
  
  for( uint16_t i  = 0;i<100;i++)
  {
      AVG_IN+=(double)AmplitudeBuffer[i];
  }
    AVG_IN/=(double)100;
    for(uint16_t  i = 0;i<100;i++)
  {
    Delta =   abs((double)AmplitudeBuffer[i] -  AVG_IN);
  //  printf(" %i  -  %.4f   = Delta %f\n",AmplitudeBuffer[i] ,AVG_IN,Delta);
    RMS_IN+=Delta;
  }
  RMS_IN/=100;
  RMS_IN = sqrt(RMS_IN);
  //printf("RMS: %f\n",RMS_IN);
  RMS_IN *=100;
  AVG_IN *=100;
  *RMS_HV =(uint32_t) RMS_IN;
  *AVG_HV =(uint32_t) AVG_IN;
  
}

