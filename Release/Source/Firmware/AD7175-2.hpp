//-----------------------------------------------------------------------------
//       Novikov V.P.
//       22.04.2016
//       from Molotaliev
//-----------------------------------------------------------------------------
#ifndef __AD7175_2_H__
#define __AD7175_2_H__

#include "stm32f4xx.h"

//#pragma once
#include "AD7175-2_Registers_definitions.hpp"
#include "ByteConversion.hpp" 

#ifndef _AD7175_2
#define _AD7175_2

#ifdef __cplusplus
//#define SPI_BUS
//#define SPI_CS 
 extern "C" {
#endif  
/**
******************************************************************************
* @file    AD7175-2.h
* @author  Molotaliev A.O(molotaliev@xprom.ru)
* @version V 4.3.1
* @date    16-february-2016/17-february-2016/9-March-2016
* @brief   This file provides main definitions  function  for work
*          with Analog Device 7175-2
*/ 
   
/****************************************************************************/
void InitAD7175(void);
bool CheckConectionAD7175(void);
uint32_t ReadADCData(void);
void SelectAD7175(void);
void DeselectAD7175(void);
void Delay_us(uint16_t  us);
void ADC_SwitchToInternalOscilator(void);
/******************************************************************************/
uint8_t XOR_Checksum(uint8_t* Buff,uint8_t Count);
uint8_t CRC8_Checksum(uint8_t* Buff,uint8_t Count);
/******************************************************************************/
void ResetADC(void);
uint24_t SingleConversion(void);
uint24_t ContinConversion(void);
inline uint8_t WriteByte(uint8_t Byte);
void StandyMode(void);
void DeepSleepMode(void);
/******************************************************************************/
void ADC_Callibration(ADC_CalibrationMode CalibrationMode);
void SingleChannelSetup(ODR SampleRate);
void  Configure_ADCClock(void); 
//void SingleChannelRead(void);
/******************************************************************************/
//Continus read mode (No need send Read Data Register) without reset
void MultiChannelSetupContinusRead(void);
//void MultiChannelRead(void);
/******************************************************************************/
void SyncOn(void);//����� �������������� ������������
void SyncOff(void);
/******************************************************************************/
bool WriteADCModeRegister           (RADC_MODE_REGISTER  ADMODE);
bool WriteInterfaceModeRegister     (RINTERFACE_MODE_REGISTER   INTERFACE_MODE);
void SetGPIOsADC(void);
bool WriteGpioRegister              (RGPIO_CONFIGURATION_REGISTER  GPIO);

bool WriteChannelRegister           (RCHANNEL_REGISTER  CR,uint8_t ChannelID);
bool WriteSetupConfiguration        (RSETUP_CONFIGURATION_REGISTER SETUP,uint8_t ConfigID);
bool WriteFilterConfigurationRegister(RFILTER_CONFIGURATION_REGISTER FILTER,uint8_t FilterID);
bool WriteOffsetRegister            (uint24_t Offset,uint8_t OffsetID);
bool WriteGainRegister              (uint24_t GAIN,uint8_t GainID);
/******************************************************************************/
RSTATUS_REGISTER                 ReadStatusRegister               (bool DATA_STAT_ON = false);
RADC_MODE_REGISTER               ReadADCModeRegister              (void);
RINTERFACE_MODE_REGISTER         ReadInterfaceModeRegister        (void);
RGPIO_CONFIGURATION_REGISTER     ReadGpioRegister                 (void);
uint16_t                         ReadADC_ID                       (void);
RCHANNEL_REGISTER                ReadChannelRegister              (uint8_t ChannelID);
RSETUP_CONFIGURATION_REGISTER    ReadSetupConfiguration           (uint8_t ConfigID);
RFILTER_CONFIGURATION_REGISTER   ReadFilterConfigurationRegister  (uint8_t FilterID);
uint24_t                         ReadOffsetRegister               (uint8_t OffsetID);
uint24_t                         ReadGainRegister                 (uint8_t GainID);
/******************************************************************************/
double RMS(uint32_t* Buff,uint16_t Count, double& Averg);
bool Acquisition (uint32_t *Vadc, int N);
void EnablrExt(void);
#ifdef __cplusplus
}
#endif

#endif

#endif