#pragma once
#include "stm32f4xx.h"
#ifndef _AMP_CONTROL
#define _AMP_CONTROL
#ifdef __cplusplus
extern "C" {
#endif 
void  AmpliferControl_Init(void);  
void  AmpliferControl_SetGain1x(void);
void  AmpliferControl_SetGain10x(void);
#ifdef __cplusplus
}
#endif 
#endif  