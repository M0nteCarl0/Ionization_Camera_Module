#include "MAV_Filter.hpp"
#include "CircularBuffer.h"
#include <stdio.h>
CircularBuffer<int32_t> CBufer;
/***************************************************************************/
  MVA_Filter:: MVA_Filter()
{
      Reset();
}
/***************************************************************************/
void  MVA_Filter:: SetWindowSize(uint32_t WindowSize)
{
  
#ifdef _ARM_
  __disable_interrupt();
#endif
 _WindowSize = WindowSize;
#if 0
  _WindowsBuffer = (uintptr_t)0;
  
   
   
#endif
    
CBufer.Destroy();  
CBufer.Create(WindowSize);
#ifdef _ARM_
   __enable_interrupt();
#endif
   Reset();
}
/***************************************************************************/
void MVA_Filter:: PutSample(uint32_t Sample)
{
	if(_DinamycWindowSize)
	{

		DynamicWindowMethod(Sample);

	}
	else
	{
		ClasicalMethod(Sample);
	}
	
  _SampleIterator++;
}
/***************************************************************************/
int32_t MVA_Filter::GetResulst(void)
{
  return _Results;
}
/***************************************************************************/
bool MVA_Filter:: ResultIsReady(void)
{
  return _ResultReady;
}
/***************************************************************************/
MVA_Filter:: ~MVA_Filter()
{
  
  
  
};
/***************************************************************************/
void  MVA_Filter::Reset(void)
{
#ifdef _ARM_
   __disable_interrupt();
#endif
  _ResultReady    = false;
  _SumCurrent     = 0;
  _SumLast        = 0;
  _SampleIterator = 0;
  _CuttPeriod     = 0;
  _Results        = 0;
  _DinamycWindowSize = false;
  _FirstStatisticMeasured = false;
 
#ifdef _ARM_
  __enable_interrupt();
#endif
}
/***************************************************************************/
 void MVA_Filter:: EnableDynamicWindowSize(void)
 {
	 _DinamycWindowSize = true;
 }
/***************************************************************************/
void MVA_Filter:: DisableDynamicWindowSize(void)
{
	_DinamycWindowSize = false;
	_FirstStatisticMeasured = false;
}
/***************************************************************************/
void MVA_Filter::ClasicalMethod(uint32_t Sample)
{
	if(!_FirstStatisticMeasured)
	{
	if(_SampleIterator < _WindowSize)
  {  
     CBufer.PutData(Sample);
    _ResultReady    =  true;
	_Results = Sample;
  }
  
  if(_SampleIterator == _WindowSize)
  {
    _SumCurrent     = 0;
    for( uint32_t i = 0; i< _WindowSize;i++)
    {
      _SumCurrent+=CBufer[i];;
    }
    
   _Results =  _SumCurrent/_WindowSize;
  // CBufer[0] = _Results;
   _ResultReady    =  true;
   _SampleIterator = 0 ;
   _FirstStatisticMeasured = true;
 
  }

	}
	else
	{
     CBufer.PutData(Sample);
    _SumCurrent     = 0;;
    for(uint32_t i = 0; i< _WindowSize;i++)
    {
      _SumCurrent+=CBufer[i];
    }
   _Results =  _SumCurrent/_WindowSize;
	}

}
/***************************************************************************/
void MVA_Filter::DynamicWindowMethod(uint32_t Sample)
{

	if(_SampleIterator <= _WindowSize )
  {  
	  if(!_FirstStatisticMeasured)
	  {
		if(_SampleIterator <_WindowSize)
		{
			CurrentWindows = _SampleIterator + 1;
		}

		if(_SampleIterator == _WindowSize)
		{
			CurrentWindows = _WindowSize;
			_FirstStatisticMeasured = true;
			_SampleIterator = 0;
		}
	  }
		_WindowsBuffer[_SampleIterator]  = Sample;
      //  printf("Window Buffer Data[%i] = %i\n",_SampleIterator,Sample);
		_SumCurrent     = 0;
		
		for(uint32_t i = 0; i< CurrentWindows;i++)
		{
		  _SumCurrent+=_WindowsBuffer[i];
		}
		//_Results = _SumCurrent/CurrentWindows;
        //printf("Result SMA  = %i\n",_Results);
		_WindowsBuffer[_SampleIterator] = _Results;
		if(_SampleIterator == _WindowSize-1)
		{
			_SampleIterator = 0;
		}
	
  }
}
/***************************************************************************/

   
   

