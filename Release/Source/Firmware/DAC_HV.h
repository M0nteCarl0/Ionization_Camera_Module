#pragma once
#include "stm32f4xx.h"
#ifndef _DAC_HV
#define _DAC_HV
#ifdef __cplusplus
extern "C" {
#endif 
void DAC_HV_Init(void);
void DAC_HV_SetValue(uint16_t value);
uint16_t DAC_HV_GetValue(void);
void DAC_HV_Disable(void);
void DAC_HV_Enable(void);
#ifdef __cplusplus
}
#endif
#endif 
