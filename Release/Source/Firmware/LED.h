#pragma once
#include "stm32f4xx.h"
#ifndef _LED
#define _LED
#ifdef __cplusplus
extern "C" {
#endif  
enum LED_Definition
{
  
  LED_Definition_VD2 = GPIO_Pin_8,
  LED_Definition_VD3 = GPIO_Pin_8,
  LED_Definition_VD4 = GPIO_Pin_8,
  LED_Definition_VD5 = GPIO_Pin_7,
  LED_Definition_VD6 = GPIO_Pin_6,
};

void InitLED(void);

void GreenLedOn(void);
void GreenLedOff(void);

void RedLedOn(void);
void RedLedOff(void);
void LED_ModuleHeartBeat(void);
#ifdef __cplusplus
}
#endif  
#endif