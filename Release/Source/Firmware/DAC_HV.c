#include "DAC_HV.h"
uint16_t ScaledValueVoltage;
/*****************************************************************************/
void DAC_HV_Init(void)
{
  
  GPIO_InitTypeDef GPIO_InitStructure;
  DAC_InitTypeDef  DAC_InitStructure;
  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
  RCC_APB1PeriphClockCmd (RCC_AHB1Periph_GPIOA,ENABLE);
                              
  GPIO_InitStructure.GPIO_Pin          = GPIO_Pin_4;
  GPIO_InitStructure.GPIO_Mode         = GPIO_Mode_AN;          
  GPIO_Init (GPIOA, &GPIO_InitStructure); 
  
 
  
  DAC_StructInit(&DAC_InitStructure);  
  
  DAC_InitStructure.DAC_Trigger        =  DAC_Trigger_None;
  DAC_InitStructure.DAC_WaveGeneration =  DAC_WaveGeneration_None;
  DAC_Init(DAC_Channel_1,&DAC_InitStructure);
  
  DAC_HV_Enable();
  
  //DAC_HV_Disable();
}
/*****************************************************************************/
void DAC_HV_SetValue(uint16_t value)
{
  DAC_SetChannel1Data(DAC_Align_12b_R,value);
 
}
/*****************************************************************************/
uint16_t DAC_HV_GetValue(void)
{
  return DAC_GetDataOutputValue(DAC_Channel_1);
}
/*****************************************************************************/
void DAC_HV_Disable(void)
{
  DAC_Cmd( DAC_Channel_1,DISABLE);
}
/*****************************************************************************/
void DAC_HV_Enable(void)
{
   DAC_Cmd( DAC_Channel_1,ENABLE); 
}
/*****************************************************************************/