#include "stm32f4xx.h"
#include "AD7175-2.hpp"
#pragma once
#ifndef _IC_CORE
#define _IC_CORE

#ifdef _HSE_CLK_
#define  Polling_Timer_DIV       42-1
#define  Core_Timeout_Timer_DIV  42000-1
#define  Core_manual_Trigger_DIV 42000-1
#endif

#ifdef _HSI_CLK_168Mhz_   
#define  Polling_Timer_DIV       42-1
#define  Core_Timeout_Timer_DIV  42000-1
#define  Core_manual_Trigger_DIV 42000-1
#endif  

#ifdef _HSI_CLK_
#define  Polling_Timer_DIV       16-1
#define  Core_Timeout_Timer_DIV  16000-1
#define  Core_manual_Trigger_DIV 16000-1
#endif


 typedef struct ControlWordS
{
 __IO  uint8_t TypeTrigger:2;
 __IO  uint8_t GainFactor:1;
 __IO  uint8_t StateHV:1;
 __IO  uint8_t MovingAvergageFilter:1;
 __IO  uint8_t Reserved:3;
}ControlWordS;
  
typedef  union ControlWord
{
  ControlWordS Bits;
  uint8_t Raw;
}ControlWord;

typedef  enum   TypeTrigger
{
  TypeTrigger_InfiniteIntegration    = 0x0,
  TypeTrigger_External               = 0x1,
  TypeTrigger_Thershold              = 0x2,
}TypeTrigger; 
   
typedef enum  GainFactor  
{  
  GainFactor_10X                    = 0x0,
  GainFactor_1X                     = 0x1,
}GainFactor; 

typedef  enum  StateHV        
{        
  StateHV_Off                       = 0x0,
  StateHV_On                        = 0x1,
}StateHV;

 typedef struct StateWordS
{
 __IO uint8_t AqusitionState:1;
 __IO uint8_t SourceStart:3;
 __IO uint8_t WaitMeasure:1;
 __IO uint8_t EndMeasure:1;
 __IO uint8_t TimeoutEventIndicator:1;
}StateWordS;
  
typedef  union StateWord
{
 StateWordS Bits;
 uint8_t Raw;
}StateWord;
   

typedef  enum  TimeoutEventIndicator
{
  TimeoutEventIndicator_Measure_Normal = 0x0,
  TimeoutEventIndicator_Measure_Triggered = 0x1,
}TimeoutEventIndicator;

typedef  enum  AqusitionState
{
  AqusitionState_Complete           = 0x0,
  AqusitionState_InProgress         = 0x1,  
}AqusitionState; 

typedef  enum SourceStart
{
  SourceStart_Forced                 = 0x3,
  SourceStart_External               = 0x1,
  SourceStart_Thershold              = 0x2,
  SourceStart_InfiniteIntegration    = 0x0,
}SourceStart;
  

#ifdef __cplusplus
extern "C" {
#endif
void Core_Init(void);  
    
void  Core_manual_Trigger_Init(void);

void  Core_Init_ExpositionTimeCounter(void);
void  Core_ExpositionTimeCounterStart(void);
void  Core_ExpositionTimeCounterStop(void);
uint32_t  Core_ExpositionTimeCounterGetTime(void);

void  Core_manual_Trigger_Start(uint16_t Duration);
void  Core_manual_Trigger_Stop(void); 
void Core_External_Trigget_Init(void);



uint8_t     Core_External_Trigger_ReadInput(void);

bool  Core_External_Trigger_RissingEdgeDeteced(void);

int32_t*   Core_GetAmplitudes(void);
uint32_t    Core_GetQuanityMeasuredAmplitudes(void);

ControlWord  Core_GetControl(void);
StateWord    Core_GetState(void);
uint32_t     Core_GetThersholdTrigger(void);
int64_t     Core_GetGlobalDosa(void);
int64_t     Core_GetLocalDosa(void);
uint8_t      Core_GetCounterEventsTrigger(void);
uint32_t     Core_GetDurabilityExposition(void);
int32_t     Core_GetAvergageAmplitudeMeasure(void);
uint32_t     Core_GetDarkCurrent(void);
uint16_t     Core_GetValueHighVoltage(void);
bool         Core_CopyValuesOfArrayToBuffer(uint8_t* DestinationBuffer,uint8_t Offset );
void         Core_ComputeResult(void);
void         Core_ResetAllMeasurements(void);
void         Core_CleanAmplitudeBuffer(void);
void         Core_NormalAqusition(uint32_t CurrentSample, RSTATUS_REGISTER Status);
void         Core_SetupMVAFilter(uint64_t TimePerSample_us);
void        Core_SetControl(uint8_t Value);
void        Core_SetValueHighVoltage(uint16_t Value);
void        Core_SetThersholdTrigger(uint32_t Value);
void        Core_SetDarkCurrent(uint32_t Value);

void  TIM8_UP_TIM13_IRQHandler(void); 
void  TIM3_IRQHandler(void); 
void Core_Init_Polling_Timer(void);
void Core_Init_SetSampleRate(ODR SampleRate);
void Core_WriteSetupADC(ODR SampleRate,ORDER FilterType,uint8_t Rejection50Hertz,ENHFILT RejectionFilterRate);
void Core_ReadSetupADC(ODR& SampleRate,ORDER& FilterType,uint8_t& Rejection50Hertz,ENHFILT& RejectionFilterRate);
void Core_ExternalTriggerEvent(void);
void Core_MeasureData(int32_t CurrentSample, RSTATUS_REGISTER Status);
void Core_ComputePolling( int Frequncy);
#ifdef __cplusplus
}
#endif  
#endif