#include "LED.h"
static uint32_t DebugCounter = 0;
/*****************************************************************************/
void InitLED(void)
{
   GPIO_InitTypeDef _GPIO;
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
  _GPIO.GPIO_Mode = GPIO_Mode_OUT;
  _GPIO.GPIO_OType  = GPIO_OType_PP;
  _GPIO.GPIO_PuPd = GPIO_PuPd_UP;
  _GPIO.GPIO_Speed = GPIO_Speed_100MHz;
     
  _GPIO.GPIO_Pin =  GPIO_Pin_5| GPIO_Pin_6;
   GPIO_Init(GPIOB,&_GPIO);
   GreenLedOn();
}
/*****************************************************************************/
void GreenLedOn(void)
{
   GPIO_SetBits(GPIOB,GPIO_Pin_6);
}
/*****************************************************************************/
void GreenLedOff(void)
{
   GPIO_ResetBits(GPIOB,GPIO_Pin_6);
}
/*****************************************************************************/
void RedLedOn(void)
{
   GPIO_SetBits(GPIOB,GPIO_Pin_5);
}
/*****************************************************************************/
void RedLedOff(void)
{
   GPIO_ResetBits(GPIOB,GPIO_Pin_5);
}
/*****************************************************************************/
void LED_ModuleHeartBeat(void)
{
  
    if (((DebugCounter++)/200000) % 2)   GreenLedOn();
     else                               GreenLedOff();
             
}