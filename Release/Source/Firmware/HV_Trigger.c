#include "HV_Trigger.h"
void HV_Trigge_Init(void)
{
  GPIO_InitTypeDef _HV_Trigge;

  RCC_APB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG,ENABLE);
  EXTI_InitTypeDef _EXTIPINs;
      
  _HV_Trigge.GPIO_Mode                        = GPIO_Mode_IN;
  _HV_Trigge.GPIO_Pin                         = GPIO_Pin_6;
  _HV_Trigge.GPIO_PuPd                        = GPIO_PuPd_NOPULL;
  _HV_Trigge.GPIO_Speed                       = GPIO_Speed_50MHz;
   GPIO_Init(GPIOC,&_HV_Trigge);
   SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource6);
 
  _EXTIPINs.EXTI_Mode                         =  EXTI_Mode_Interrupt;  
  _EXTIPINs.EXTI_Line                         =  EXTI_Line6;
  _EXTIPINs.EXTI_Trigger                      =  EXTI_Trigger_Rising_Falling ;
  _EXTIPINs.EXTI_LineCmd                      =  ENABLE;
   EXTI_Init(&_EXTIPINs); 

}
/************************************************************************/
uint8_t  HV_Trigge_ReadIbput(void)
{
  return  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_8);
}
/************************************************************************/
bool  HV_Trigge_RissingEdgeDeteced(void)
{
  return HV_Trigge_ReadIbput()!=0;
}
/************************************************************************/