#include "IC_Core.hpp"
#include "ByteConversion.hpp"
#include "Commands.h"
#include "FeedBackADC.h"
#include "LED.h"
#include "Indep_WD.h"
using namespace Conversion;
const unsigned char  Name[]         = {'I','C',0,0};
const unsigned char  VersSW[2]      = {1,6};
const unsigned char  VersHW[2]      = {3,0};
static  uint8_t         Buff [NCOMD];
int N;
/*****************************************************************************/
void       Command_ProcessCommands(void)
{

    do
    {   LED_ModuleHeartBeat();
        iWD_RefreshCountet();
        int Ret = ReceiveCommand (Buff, &N);
        if(Ret)
        {
            Command_CommandParser(Buff,Buff[2],N);
        }
    }while(1);

}
/*****************************************************************************/
void       Command_CommandParser(uint8_t *Buff,uint8_t ComandID,int N)
{
    switch(ComandID)
    {

    case  CommadID_WRITE_DETECTOR_SETUP:
        {
            Command_WriteSetupDetector(Buff,N);
            break; 
        }

    case  CommadID_DETECTOR_FORCE_START:
        {
            Command_DetectorForcedStart(Buff,N);
            break;  
        }

    case CommadID_DETECTOR_RESET_ALL:
        {
            Command_DetectorResetAllMeasurements(Buff,N);
            break; 
        }

    case  CommadID_READ_HW_SW:
        {
            Command_ReadVersion(Buff,N);
            break; 
        }

    case CommadID_READ_DETECTOR_SETUP:
        {
            Command_ReadSetupDetector(Buff,N);
            break;  
        }

    case CommadID_READ_DETECTOR_AMPLITUDES:
        {
            Command_ReadDetectorAmplitudes(Buff,N);                                                  
            break; 
        }

    case CommadID_READ_ADC_SETUP:
        {

            Command_ReadADCParams(Buff,N);
            break; 
        }

    case CommadID_WRITE_ADC_SETUP:
        {

            Command_WriteADCParams(Buff,N);
            break;  
        }

    case CommadID_READ_HV_LEVEL:
        {
            Command_ReadHVLevel(Buff, N);
            break;
        }

    }

}
/*****************************************************************************/
void       Command_ReadHVLevel(uint8_t *Buff, int N)
{
    if (Buff[3] != 5)   return;
    __disable_irq();  
    uint32_t AVG = 0;
    uint32_t RMS = 0;

    FeedBackADC_GetMeasuredVoltageParams(&RMS,&AVG);
    N = Buff[3]                        = 5+6+1;
    Buff[5]                            = AVG & 0x7f;
    Buff[6]                            = (AVG  >> 7 ) & 0x7f;
    Buff[7]                            = (AVG  >> 14 ) & 0x7f;
    Buff[8]                            = RMS & 0x7f;
    Buff[9]                            = (RMS  >> 7 )  & 0x7f;
    Buff[10]                           = (RMS  >> 14 ) & 0x7f;

    __enable_irq(); 
    int Ret = SendCommand (Buff, N);
    EnableRS485();

}
/*****************************************************************************/
void       Command_ReadVersion(uint8_t *Buff, int N)
{
    if (Buff[3] != 5)   return;
    __disable_irq();
    uint8_t SB = 0x01;
    Buff[7] = Buff[8] = 0;
    Buff[5] = Name[0];
    if (Name[0] & 0x80) Buff[8] |= SB;
    SB = SB << 1;
    Buff[6] = Name[1];
    if (Name[1] & 0x80) Buff[8] |= SB;
    Buff[9]  = VersHW[0];
    Buff[10] = VersHW[1];
    Buff[11] = VersSW[0];
    Buff[12] = VersSW[1];
    N = Buff[3] = 14;
    __enable_irq(); 
    int Ret = SendCommand (Buff, N);
    EnableRS485();

}
/*****************************************************************************/
void       Command_ReadSetupDetector(uint8_t *Buff, int N)
{
    if (Buff[3] != 5)   return;
    __disable_irq();
    N = Buff[3] = 48;
    Core_ComputeResult();
    uint8_t  ControlWord               = Core_GetControl().Raw;
    uint8_t  StateWord                 = Core_GetState().Raw;
    uint32_t ThersholdTrigger          = Core_GetThersholdTrigger();
    int64_t GlobalDosa                 = Core_GetGlobalDosa();
    int64_t LocalDosa                  = Core_GetLocalDosa();
    uint8_t  CounterEventsTrigger      = Core_GetCounterEventsTrigger();
    uint32_t DurabilityExposition      = Core_GetDurabilityExposition();
    int32_t AvergageAmplitudeMeasure   = Core_GetAvergageAmplitudeMeasure();
    uint32_t QuanityMeasuredAmplitudes = Core_GetQuanityMeasuredAmplitudes();
    uint16_t ValueHighVoltage          = Core_GetValueHighVoltage();
    uint32_t  DarkCurrent              =  Core_GetDarkCurrent();

    Buff[5]                            = ControlWord;
    Buff[6]                            = StateWord;

    Buff[7]                            = ThersholdTrigger & 0x7f;
    Buff[8]                            = (ThersholdTrigger  >> 7 )  & 0x7f;
    Buff[9]                            = (ThersholdTrigger  >> 14 ) & 0x7f;

    Signed64BitToUnsigned7bitFormat_v2(GlobalDosa,
    Buff[10],
    Buff[11],
    Buff[12],
    Buff[13],
    Buff[14],
    Buff[15],
    Buff[16],
    Buff[17],
    Buff[18],
    Buff[19]);

    Signed64BitToUnsigned7bitFormat_v2(LocalDosa,
    Buff[20],
    Buff[21],
    Buff[22],
    Buff[23],
    Buff[24],
    Buff[25],
    Buff[26],
    Buff[27],
    Buff[28],
    Buff[29]);

    Buff[30]                           = CounterEventsTrigger;
    Buff[31]                           = DurabilityExposition & 0x7f;
    Buff[32]                           = (DurabilityExposition >> 7 ) & 0x7f;
    Buff[33]                           = (DurabilityExposition >> 14 ) & 0x7f;
    Buff[34]                           = (DurabilityExposition >> 21 ) & 0x7f;

    Signed32BitToUnsigned7bitFormat_v2(AvergageAmplitudeMeasure,
    Buff[35],
    Buff[36],
    Buff[37],
    Buff[38]
    );

    Buff[39]                           = QuanityMeasuredAmplitudes & 0x7f;  
    Buff[40]                           = (QuanityMeasuredAmplitudes  >> 7) & 0x7f; 
    Buff[41]                           = (QuanityMeasuredAmplitudes  >> 14) & 0x7f; 

    Buff[42]                           = ValueHighVoltage & 0x7f; 
    Buff[43]                           = (ValueHighVoltage >> 7) & 0x7f;

    Buff[44]                           = DarkCurrent & 0x7f; 
    Buff[45]                           = (DarkCurrent >> 7) & 0x7f;
    Buff[46]                           = (DarkCurrent >> 14) & 0x7f;

    __enable_irq(); 
    int Ret = SendCommand (Buff, N);
    EnableRS485();

}
/*****************************************************************************/
void       Command_ReadDetectorAmplitudes(uint8_t *Buff, int N)
{
    if (Buff[3] != 7)   return;
    __disable_irq();
    uint8_t OffsetInArray = Buff[5];
    N= Buff[3] = 126;
    int Flag  =  Core_CopyValuesOfArrayToBuffer(&Buff[5],OffsetInArray);
    __enable_irq(); 
   if(Flag)
   {
     int Ret = SendCommand (Buff, N);
     EnableRS485();
   }

}
/*****************************************************************************/
void       Command_WriteSetupDetector(uint8_t *Buff, int N)
{
    if (Buff[3] != 15)   return;
    __disable_irq();
    N = Buff[3] = 15;
    Core_SetControl(Buff[5]);
    Core_SetThersholdTrigger( Buff[6] & 0x7F |(Buff[7] << 7)|(Buff[8] << 14));
    Core_SetValueHighVoltage(Buff[9] & 0x7F |(Buff[10] << 7));
    Core_SetDarkCurrent(Buff[11] & 0x7F |(Buff[12] << 7)|(Buff[13] << 14));
    __enable_irq();
     int Ret = SendCommand (Buff, N);
     EnableRS485();
}
/*****************************************************************************/
void       Command_DetectorForcedStart(uint8_t *Buff, int N)
{
    if (Buff[3] != 8)   return;
    __disable_irq();
    N = Buff[3] = 8;
    uint16_t Duration =  Buff[5] |(Buff[6] << 7);
    __enable_irq();
    Core_manual_Trigger_Start(Duration);
    int Ret = SendCommand (Buff, N);
    EnableRS485();

}
/*****************************************************************************/
void       Command_DetectorResetAllMeasurements(uint8_t *Buff, int N)
{
    if (Buff[3] != 5)   return;
    __disable_irq();
    N = Buff[3] = 5;
    Core_ResetAllMeasurements();
    __enable_irq(); 
     int Ret = SendCommand (Buff, N);
     EnableRS485();

}
/*****************************************************************************/
void       Command_ReadADCParams(uint8_t *Buff, int N)
{
    if (Buff[3] != 5)   return;
    __disable_irq();
    N= Buff[3] = 10;
    ODR Rate;
    ORDER Order;
    ENHFILT FilterSetup;
    uint8_t FilterState;
    Core_ReadSetupADC(Rate,Order,FilterState,FilterSetup) ;
    Buff[5] = Rate;
    Buff[6] = Order;
    Buff[7] = FilterState;
    Buff[8] = FilterSetup;
    __enable_irq();
    int Ret = SendCommand (Buff, N);
    EnableRS485();

}
/*****************************************************************************/
void       Command_WriteADCParams(uint8_t *Buff, int N)
{
    if (Buff[3] != 10)   return;
    __disable_irq();
     N = Buff[3] = 10;
    Core_WriteSetupADC((ODR)Buff[5],(ORDER)Buff[6],Buff[7],(ENHFILT)Buff[8]);
    __enable_irq();
     int Ret = SendCommand (Buff, N);
     EnableRS485();
}
/*****************************************************************************/