#pragma once
#include "stm32f4xx.h"
#include <stdio.h>
#ifndef _FADCC_
#define _FADCC_
#ifdef __cplusplus
extern "C" {
#endif  
void  FeedBackADC_Init(void);
void FeedBackADC_ClockInit(void);
void FeedBackADC_GPIOInit(void);
uint16_t GetMeasuredVoltageLevel(void);
void FeedBackADC_GetMeasuredVoltageParams(uint32_t* RMS,uint32_t* AVG);
#ifdef __cplusplus
}
#endif   
#endif 