#pragma once
#include <stdint.h>
template <typename T> class CircularBuffer
{
public:
	void Create(uint32_t SizeBuffer);
	T operator[](uint32_t position);
	void PutData(T Data);
	void Destroy(void);
private:
	T* _Buffer;
	uint32_t _PositionInBuffer;
	uint32_t _SizeBuffer;
};


template <typename T>  void CircularBuffer<T>::Create(uint32_t SizeBuffer)
{
	_Buffer = new T[SizeBuffer];
	_SizeBuffer = SizeBuffer;
   
   for(int i = 0;i<SizeBuffer;i++)
   {
     _Buffer[i] = 0;
   }
   
   
}

template <typename T>	T CircularBuffer<T>::operator[](uint32_t position)
{
	T Data = 0;
	if(position <_SizeBuffer)
	{
		Data = _Buffer[position];

	}
	return Data;
}

template <typename T>	void CircularBuffer<T>::PutData(T Data)
{
	if(_PositionInBuffer == _SizeBuffer)
	{
		_PositionInBuffer = 0;
	}
	_Buffer[_PositionInBuffer] = Data;
	_PositionInBuffer++;
	
	

}

template <typename T> void CircularBuffer<T>::Destroy(void)
{
  //_//Buffer  = (void*)0;
   delete [] _Buffer;
	_PositionInBuffer = 0;
	_SizeBuffer = 0;
}