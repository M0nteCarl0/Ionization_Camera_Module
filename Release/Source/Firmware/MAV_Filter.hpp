#include <stdint.h>
#ifdef _ARM_
#include "stm32f4xx.h"
#endif
class MVA_Filter
{
public:
  MVA_Filter();
  void SetWindowSize(uint32_t WindowSize);
  void PutSample(uint32_t Sample);
  void EnableDynamicWindowSize(void);
  void DisableDynamicWindowSize(void);
  void Reset(void);
  int32_t GetResulst(void);
  bool ResultIsReady(void);
  ~MVA_Filter();
private:
  void ClasicalMethod(uint32_t Sample);
  void DynamicWindowMethod(uint32_t Sample);
  int32_t*  _WindowsBuffer;
  int64_t _SumCurrent;
  int64_t _SumLast;
  uint64_t _CuttPeriod;
  uint32_t _WindowSize;
  uint32_t CurrentWindows;
  int32_t  _Results;
  uint32_t _SampleIterator;
  
  bool _ResultReady;
  bool _DinamycWindowSize;
  bool _FirstStatisticMeasured;
};
