#include "Amplifer_control.h"
/*****************************************************************************/
void  AmpliferControl_Init(void)
{

    GPIO_InitTypeDef _GPIO;
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
    _GPIO.GPIO_Mode = GPIO_Mode_OUT;
    _GPIO.GPIO_OType  = GPIO_OType_OD;
    _GPIO.GPIO_PuPd = GPIO_PuPd_UP;
    _GPIO.GPIO_Speed = GPIO_Speed_100MHz;
    _GPIO.GPIO_Pin =  GPIO_Pin_7;
    GPIO_Init(GPIOB,&_GPIO);
    AmpliferControl_SetGain1x();
}
/*****************************************************************************/
void  AmpliferControl_SetGain1x(void)
{
    GPIO_SetBits(GPIOB,GPIO_Pin_7);  
}
/*****************************************************************************/
void  AmpliferControl_SetGain10x(void)
{
    GPIO_ResetBits(GPIOB,GPIO_Pin_7);     
}
/*****************************************************************************/