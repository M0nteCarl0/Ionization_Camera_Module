//-----------------------------------------------------------------------------
//       Novikov V.P.
//       22.04.2016
//       from Molotaliev
//-----------------------------------------------------------------------------
#ifndef __AD7175_2_REGISTERS_H__
#define __AD7175_2_REGISTERS_H__

#include "stm32f4xx.h"

#ifndef ADC7175_2_REG_DEF
#define ADC7175_2_REG_DEF
/**
  ******************************************************************************
  * @file    AD7175-2_Registers_definitions.h
  * @author  Molotaliev A.O(molotaliev@xprom.ru)
  * @version V 4.3.0
  * @date    16-february-2016/17-february-2016
  * @brief   This file provides main definitions enum  and bitfields  for work
  *          with Analog Device 7175-2
*/

/****************************************************************************
*                       COMMUNICATIONS_REGISTER SECTION
*
*
****************************************************************************/
typedef enum RA_CMD
{
  STATUS_REGISTER                                               =  0x0,
  ADC_MODE_REGISTER                                             =  0x1,
  INTERFACE_MODE_REGISTER                                       =  0x2,
  CHECKSUMM_REGISTER                                            =  0x3,
  DATA_REGISTER                                                 =  0x4,
  GPIO_CONFIGURATION_REGISTER                                   =  0x6,
  ID_REGISTER                                                   =  0x7,
  CHANNEL_0_REGISTER                                            =  0x10,
  CHANNEL_1_REGISTER                                            =  0x11,
  CHANNEL_2_REGISTER                                            =  0x12,
  CHANNEL_3_REGISTER                                            =  0x13 , 
  SETUP_CONFIGURATION_0_REGISTER                                =  0x20,
  SETUP_CONFIGURATION_1_REGISTER                                =  0x21,
  SETUP_CONFIGURATION_2_REGISTER                                =  0x22,
  SETUP_CONFIGURATION_3_REGISTER                                =  0x23,
  FILTER_CONFIGURATION_0_REGISTER                               =  0x28,
  FILTER_CONFIGURATION_1_REGISTER                               =  0x29,
  FILTER_CONFIGURATION_2_REGISTER                               =  0x2A,
  FILTER_CONFIGURATION_3_REGISTER                               =  0x2B,
  OFFSET_0_REGISTER                                             =  0x30,
  OFFSET_1_REGISTER                                             =  0x31,
  OFFSET_2_REGISTER                                             =  0x32,
  OFFSET_3_REGISTER                                             =  0x33,
  GAIN_0_REGISTER                                               =  0x38, 
  GAIN_1_REGISTER                                               =  0x39,
  GAIN_2_REGISTER                                               =  0x3A,
  GAIN_3_REGISTER                                               =  0x3B,
}RA_CMD;
/****************************************************************************/
typedef enum RW_STATE
{
  WRITE_CMD                                                    =  0x0,
  READ_CMD                                                     =  0x1,
}RW_STATE;
/****************************************************************************/
typedef enum WEN_STATE
{
  BEGIN_TRANSFER                                               = 0x0,
  BREAK_TRANSFER                                               = 0x1,
}WEN_STATE;
/****************************************************************************/
typedef  struct COMSS_REGISTER_BITS
  {
   __IO uint8_t  RA:6;
   __IO uint8_t  R_W:1;
   __IO uint8_t _WEN:1;
  }COMSS_REGISTER_BITS;
/****************************************************************************/
typedef union COMSS_REGISTER
{
COMSS_REGISTER_BITS  BITS;
   uint8_t COMSS;
}RCOMSS_REGISTER;
/****************************************************************************
*                       STATUS_REGISTER SECTION
*
*
****************************************************************************/

typedef enum CHANNELL
{
  CHANNELL_Channel_0                                               = 0x0,
  CHANNELL_Channel_1                                               = 0x1,
  CHANNELL_Channel_2                                               = 0x2,
  CHANNELL_Channel_3                                               = 0x3,
}CHANNELL;
/****************************************************************************/
typedef struct STATUS_REGISTER_BITS
  {
  __IO  uint8_t   CHANNELL:2;
  __IO  uint8_t   RESERVED :2;
  __IO  uint8_t   REG_ERROR:1;
  __IO  uint8_t   CRC_ERROR:1;
  __IO  uint8_t   ADC_ERROR:1;
  __IO  uint8_t   RDY:1;
  }STATUS_REGISTER_BITS;
/****************************************************************************/
typedef union STATUS_REGISTER
{
  STATUS_REGISTER_BITS BITS;
  uint8_t STATUS;
}RSTATUS_REGISTER;
/****************************************************************************
*                       INTERFACE_MODE_REGISTER SECTION
*
*
****************************************************************************/
typedef enum  CRC_EN
{
  CRC_EN_DISABLED                                                    = 0x0,
  CRC_EN_XOR_CHECKSUMM                                               = 0x1,
  CRC_EN_CRC_CHECKSUMM                                               = 0x2,
}CRC_EN;
/****************************************************************************/
typedef enum WL16
{
  WL16_24_Bit_data                                                   = 0x0,
  WL16_16_Bit_data                                                   = 0x1, 
}WL16;
/****************************************************************************/
typedef union INTERFACE_MODE_REGISTER
{
  struct INTERFACE_MODE_REGISTER_BITS
  {
   __IO uint16_t  WL16:1;
   __IO uint16_t  RESERVED:1;
   __IO uint16_t  CRC_EN:2;
   __IO uint16_t _RESERVED:1;
   __IO uint16_t  REG_CHECK:1;
   __IO uint16_t  DATA_STAT:1;
   __IO uint16_t  CONTREAD:1;
   __IO uint16_t  DOUT_RESET:1;
   __IO uint16_t  __RESERVED:2;
   __IO uint16_t  IOSTRENGTH:1;
   __IO uint16_t  ALT_SYNC:1;
   __IO uint16_t  ___RESERVED:3;
  }INTERFACE_MODE_REGISTER_BITS;
   uint16_t INTERFACE_MODE;
}RINTERFACE_MODE_REGISTER;
/****************************************************************************
*                       ADC_MODE_REGISTER SECTION
*
*
****************************************************************************/
typedef enum ADC_MODE
{
    ADC_MODE_Continuous_conversion_mode                           = 0x0,
    ADC_MODE_Single_conversion_mode                               = 0x1,
    ADC_MODE_Standby_mode                                         = 0x2,
    ADC_MODE_Power_down_mode                                      = 0x3,
    ADC_MODE_Internal_offset_calibration                          = 0x4,
    ADC_MODE_System_offset_calibration                            = 0x6,
    ADC_MODE_System_gain_calibration                              = 0x7,
}ADC_MODE;
/****************************************************************************/
typedef enum CLOCKSEL
{
    CLOCKSEL_Internal_oscillator                                  = 0x0,
    CLOCKSEL_Internal_oscillator_output_on_XTAL2_DIV_CLKIO_pin    = 0x1,
    CLOCKSEL_External_clock_input_on_XTAL2_DIV_CLKIO_pin          = 0x2,
    CLOCKSEL_External_crystal_on_XTAL1_and_XTAL2_DIV_CLKIO_pins   = 0x3,
}CLOCKSEL;
/****************************************************************************/
typedef enum DELLAY
{
    DELLAY_0us                                                    = 0x0,
    DELLAY_4us                                                    = 0x1,
    DELLAY_16us                                                   = 0x2,
    DELLAY_40us                                                   = 0x3,
    DELLAY_100us                                                  = 0x4,
    DELLAY_200us                                                  = 0x5,
    DELLAY_500us                                                  = 0x6,
    DELLAY_1ms                                                    = 0x7,
}DELLAY;
/****************************************************************************/
typedef union ADC_MODE_REGISTER
{
  struct ADC_MODE_REGISTER_BITS
  {
   __IO uint16_t  RESERVED:2;
   __IO uint16_t  CLOCKSEL:2;
   __IO uint16_t  MODE:3;
   __IO uint16_t  _RESERVED:1;
   __IO uint16_t  DELLAY:3;
   __IO uint16_t  __RESERVED:2;
   __IO uint16_t  SING_CYC:1;
   __IO uint16_t  HIDE_DELAY:1;
   __IO uint16_t  REF_EN:1;
  }ADC_MODE_REGISTER_BITS;
   uint16_t ADC_MODE;
}RADC_MODE_REGISTER;
/****************************************************************************
*                       FILTER_REGISTER SECTION
*
*
****************************************************************************/
typedef enum ODR{
  
    ODR_250KPS                                                    = 0x0,
    ODR_125KPS                                                    = 0x1,
    ODR_62_5KPS                                                   = 0x2,
    ODR_50KPS                                                     = 0x3,
    ODR_31_25KPS                                                  = 0x4,
    ODR_25KPS                                                     = 0x5,
    ODR_15_628KPS                                                 = 0x6,
    ODR_10KPS                                                     = 0x7,
    ODR_5KPS                                                      = 0x8,
    ODR_2_5KPS                                                    = 0x9,
    ODR_1KPS                                                      = 0xA,
    ODR_500PS                                                     = 0xB,
    ODR_397PS                                                     = 0xC,
    ODR_200PS                                                     = 0xD,
    ODR_100PS                                                     = 0xE,
    ODR_59PS                                                      = 0xF,
    ODR_49PS                                                      = 0x10,
    ODR_20PS                                                      = 0x11,
    ODR_16PS                                                      = 0x12,
    ODR_10KS                                                      = 0x13 ,
    ODR_5PS                                                       = 0x14,
}ODR;
/****************************************************************************/
typedef enum ORDER{
   ORDER_Sinc5_Sinc1                                              = 0x0,
   ORDER_Sinc3                                                    = 0x3
}ORDER;
/****************************************************************************/
typedef enum ENHFILT
{
    ENHFILT_27SPS                                                 = 0x2,
    ENHFILT_25SPS                                                 = 0x3,
    ENHFILT_20SPS                                                 = 0x5,
    ENHFILT_16SPS                                                 = 0x6,
}ENHFILT;
/****************************************************************************/
typedef union FILTER_CONFIGURATION_REGISTER
{
    struct FILTER_CONFIGURATION_REGISTER_BITS
    {
    __IO uint16_t  ODR:5;
    __IO uint16_t  ORDER:2;
    __IO uint16_t  RESERVED:1;
    __IO uint16_t  ENHFILT:3;
    __IO uint16_t  ENHFILTEN:1;
    __IO uint16_t  __RESERVED:2;
    __IO uint16_t  SINC3_MAP:1;
    }FILTER_CONFIGURATION_REGISTER_BITS;
    uint16_t FILTER_CONFIGURATION;
}RFILTER_CONFIGURATION_REGISTER;
/****************************************************************************
*                       SETUP_REGISTER SECTION
*
*
****************************************************************************/
typedef enum  REF_SEL
{
  REF_SEL_External_Reference                                   = 0x0,
  REF_SEL_Internal_2_5_V_Reference                             = 0x2,
  REF_SEL_AVDD1_MINUS_AVSS                                     = 0x3,  
}REF_SEL;
/****************************************************************************/
typedef enum BI_UNIPOLAR
{
  BI_UNIPOLAR_Unipolar_coded_output                            = 0x0,
  BI_UNIPOLAR_Bipolar_coded_output                             = 0x1,
}BI_UNIPOLAR;
/****************************************************************************/
typedef union SETUP_CONFIGURATION_REGISTER
{
    struct SETUP_CONFIGURATION_REGISTER_BITS
    {
     __IO uint16_t  RESERVED:4;
     __IO uint16_t  REF_SEL:2;
     __IO uint16_t  _RESERVED:1;
     __IO uint16_t  BURNOUT_EN:1;
     __IO uint16_t  MINUS_AINBUF:1;
     __IO uint16_t  PLUS_AINBUF:1;
     __IO uint16_t  MINUS_REFBUF:1;
     __IO uint16_t  PLUS_REFBUF:1;
     __IO uint16_t  BI_UNIPOLAR:1;
     __IO uint16_t  __RESERVED:3;
    }SETUP_CONFIGURATION_REGISTER_BITS;
    uint16_t SETUP_CONFIGURATION;
}RSETUP_CONFIGURATION_REGISTER;
/****************************************************************************
*                       GPIO_REGISTER SECTION
*
*
****************************************************************************/
typedef enum ERR_EN
{
    ERR_EN_Disabled                                               = 0x0,
    ERR_EN_SYNC_ERROR_is_an_error_input                           = 0x1,
    ERR_EN_SYNC_ERROR_is_an_open_drain_error_output               = 0x2,
    ERR_EN_SYNC_ERROR_is_a_general_purpose_output                 = 0x3,
}ERR_EN;
/****************************************************************************/
typedef union GPIO_CONFIGURATION_REGISTER
{
    struct GPIO_CONFIGURATION_REGISTER_BITS
    {
    __IO  uint16_t  GP_DATA0:1;
    __IO  uint16_t  GP_DATA1:1;
    __IO  uint16_t  OP_EN0:1;
    __IO  uint16_t  OP_EN1:1;
    __IO  uint16_t  IP_EN0:1;
    __IO  uint16_t  IP_EN1:1;
    __IO  uint16_t  __RESERVED:2;
    __IO  uint16_t  ERR_DAT:1;
    __IO  uint16_t ERR_EN:2; 
    __IO  uint16_t SYNC_EN:1; 
    __IO  uint16_t MUX_IO:1;
    }GPIO_CONFIGURATION_REGISTER_BITS;
    uint16_t GPIO_CONFIGURATION;
}RGPIO_CONFIGURATION_REGISTER;
/****************************************************************************
*                       CHANNEL_REGISTER SECTION
*
*
****************************************************************************/
  typedef enum SETUP_SEL
 {
    SETUP_SEL_Setup0                                             = 0x0,
    SETUP_SEL_Setup1                                             = 0x1,
    SETUP_SEL_Setup2                                             = 0x2,
    SETUP_SEL_Setup3                                             = 0x3,
}SETUP_SEL;
/****************************************************************************/
typedef enum AIN
{
    AIN_AIN0                                                      = 0x0,
    AIN_AIN1                                                      = 0x1,
    AIN_AIN2                                                      = 0x2,
    AIN_AIN3                                                      = 0x3,
    AIN_AIN4                                                      = 0x4,
    AIN_TEMP_PLUS                                                 = 0x11,
    AIN_TEMP_MINUS                                                = 0x12,
    AIN_AVDD1_MINSUS_AVSS_DIV_5_MINUS                             = 0x13,
    AIN_AVDD1_MINSUS_AVSS_DIV_5_PLUS                              = 0x14,
    AIN_REF_MINUS                                                 = 0x15,
    AIN_REF_PLUS                                                  = 0x16,
}AIN;
/****************************************************************************/
typedef  struct _CHANNEL_REGISTER_BITS
{
    __IO uint16_t  AINNEG:5;
    __IO uint16_t  AINPOS:5;
    __IO uint16_t  _RESERVED:2;
    __IO uint16_t  SETUP_SEL:2;
    __IO uint16_t  __RESERVED:1;
    __IO uint16_t  CH_EN:1;
}CHANNEL_REGISTER_BITS;
/****************************************************************************/
typedef union CHANNEL_REGISTER
  {
    _CHANNEL_REGISTER_BITS CHANNEL_REGISTER_BITS;
    uint16_t  _CHANNEL_REGISTER;
  }RCHANNEL_REGISTER;
/****************************************************************************
*                       INTERNAL TYPE SECTION
*
*
****************************************************************************/
typedef union uint24_t
{
  struct uint24_t_BITS
  {
  __IO uint8_t  LSB:8;
  __IO uint8_t  MMSB:8;
  __IO uint8_t  MSB:8; 
  }uint24_t_t_BITS;
   uint32_t Value;
   int  ValueSign;
}uint24_t;
#define  NOT_OPTIMIZE volatile 
/****************************************************************************/
enum ADC_CalibrationMode
{
  ADC_CalibrationMode_Internal_offset_calibration,       
  ADC_CalibrationMode_System_offset_calibration,                            
  ADC_CalibrationMode_System_gain_calibration,
};
/****************************************************************************/
#endif

#endif




