//------------------------------------------------------------------------------
//    Novikov
//    10.02.2015
//------------------------------------------------------------------------------
#include "MyUsart.h"
#include "stm32f4xx.h"
#include "Commands.h"
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
MyStack         m_StackR;
MyStack         m_StackT;
MyCommand       m_CommandR;

int m_FlgCheckCRC = 1;     // 1 - Control CRC;   0 - Dont Make and Control CRC
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void InitCommand (MyCommand *Comd)
{
  Comd->JC    = 0;
  Comd->Bytes = 0;
  Comd->Flg   = 1;
  Comd->MaxBufferSize = NCOMD;
};

int ReceiveCommandL (uint8_t *Buff, int *N, MyStack *Stack, MyCommand *Comd)
{
__disable_irq();    
  
  uint8_t Data;
  int Ret = 0;
  int i;
  
   while (GetCharStack (&Data, Stack)){

      if (Comd->JC == 0){
                                                               // ID
        if (Data == COMMAND_ID){
          Comd->Data [Comd->JC] = Data;
          Comd->JC++;
        }
        else
        {
          if(CheckByteOldProtocolSign(Data))
          {
            Comd->Data [Comd->JC] = Data;
            Comd->JC++;
            
          }
        }
        
      }
      else{
         Comd->Data [Comd->JC] = Data;
         Comd->JC++;
      }
                                                               // ADDR
      if (Comd->JC == 2){
        if(CheckByteOldProtocolSign(Comd->Data [0]))
        {
          
         
          //{
           Buff[0] = Comd->Data [0];
           Buff[1] = Comd->Data [1];
           Ret = 1;
           *N = 2;
           Comd->JC = 0;
            break;
          //}
          
        }
        else
        {
          if (Comd->Data [1] != EXPO_ADR){
              Comd->JC = 0;
              break;       
          }
        }
      }
                                                               // HEADER
      if (Comd->JC == 5){
         uint8_t CRC_Header;
         if (m_FlgCheckCRC){
            CRC_Header = MakeCRC (Comd->Data, 4);

         }
         else{
           CRC_Header = Comd->Data [Comd->JC-1];
         }
                                                               // CHECK CRC
         if (CRC_Header != Comd->Data [Comd->JC-1]){
            Comd->JC = 0;
            break;       
         }
         else{
             if (Comd->Data [3] == 5){
               for (i = 0; i < Comd->JC; i++){
                 Buff [i] = Comd->Data [i];
               }
               *N = Comd->JC;
               Comd->JC = 0;
               Ret = 1;
               break;
             }
         }
      }
                                                               // DATA
      if ((Comd->JC > 5) && (Comd->Data [3] == Comd->JC)){
                                                               // CHECK CRC
         if (0){
            Comd->JC = 0;
            break;       
         }
         else{
            for (i = 0; i < Comd->JC; i++){
              Buff [i] = Comd->Data [i];
            }
            *N = Comd->JC;
            Comd->JC = 0;
            Ret = 1;
            break;
         }
      }
                                                               // ERROR-1
      if (Comd->JC > 3){
        if (Comd->Data [3] < Comd->JC || Comd->Data [3] > Comd->MaxBufferSize){
          Comd->JC = 0;
          break;
        }
      }
                                                               // ERROR-2
      if (Comd->JC > 2){
        if (Comd->Data [Comd->JC-1] & 0x80){
          Comd->JC = 0;
          break;
        }
      }

   }
  
 __enable_irq();    
  
  return Ret;
};
//------------------------------------------------------------------------------
int SendCommandL (uint8_t *Buff, int N, MyStack *Stack)
{
  int Ret = 1;
  int i;

  for (i = 0; i < N; i++){
    Ret = PutCharStack (Buff[i], Stack);
    if (Ret == 0)
      break;
  }

  return Ret;
};
//----------------------------------------------------------------------------
uint8_t MakeCRC (uint8_t *Data, int N)
//    ���������� CRC ����� �������� 
{
   int Summ = 0;
   int i = 0;
   uint8_t mCRC = 0;
   int SummH = 0;
   for ( i = 0; i < N; i++){
      Summ += Data[i];
   }
   SummH = (Summ >> 7) & 0x7F;
   mCRC = (uint8_t) (SummH + (Summ & 0x7F));
   mCRC &= 0x7F;
   return mCRC;
};
//----------------------------------------------------------------------------
/*
uint8_t MakeCRC (uint8_t *Data, int N)
{
   uint8_t mCRC = 0xFF;
   for (int i = 0; i < N; i++){
      mCRC = mCRC ^ Data[i];
   }
   mCRC &= 0x7F;
   return mCRC;
};
*/
//----------------------------------------------------------------------------
void AddCRC (uint8_t *Data, int N)
{
   if (N < 5) return;

   Data [4] = MakeCRC (Data, 4);

   if (N > 6){
      Data [N-1] = MakeCRC (&Data[5], N-6);
   }
};
//----------------------------------------------------------------------------
int CheckCRC (uint8_t *Data, int N)
{
   uint8_t mCRC;
   if (N < 5) return 0;
  

   if (N > 6){
      mCRC = MakeCRC (&Data[5], N-6);
      if (mCRC != Data[N-1]) 
         return 0;
   }
   return 1;
};
//------------------------------------------------------------------------------
int ReceiveCommand (uint8_t *Buff, int *N)
{

   int Res = 0; 
   int Ret = ReceiveCommandL (Buff, N, &m_StackR, &m_CommandR);
   if (Ret && m_FlgCheckCRC && *N!=2){
      if (CheckCRC (Buff, *N))  
      {
        Res =  1;
      }
   }
     
    if(Ret && *N == 2)
    {
      Res = 1;
    }
      
   
   return Res;
};
//------------------------------------------------------------------------------
int SendCommand (uint8_t *Buff, int N)
{
   if (m_FlgCheckCRC)
      AddCRC (Buff, N);
   
   return SendCommandL (Buff, N, &m_StackT);
};
//------------------------------------------------------------------------------
void InitInternalReceiveStack(void)
{
	InitStack(&m_StackR);
}
/****************************************************************************/
void InitIntrenalTranceiveStack(void)
{
	InitStack(&m_StackT);
}
/****************************************************************************/
void InitIntrenalReceiveAndTranceiveStack(void)
{
	InitInternalReceiveStack();
	InitIntrenalTranceiveStack();
        SetCRCCheck(1);
}
/****************************************************************************/
 void SetCRCCheck(int Status)
{
	m_FlgCheckCRC = Status;
}
/****************************************************************************/
void BindExternalReceiveStack(MyStack ReceiveStack)
{
	m_StackR = ReceiveStack;
}
/****************************************************************************/
void BindExternalTranceiveStack(MyStack TranceiveStack)
{
	m_StackT = TranceiveStack;
}
/****************************************************************************/
MyStack GetReceiveStack(void)
{
	return m_StackR;

}
/****************************************************************************/
MyStack GetTransmitStack(void)
{

	return m_StackT;
}
/****************************************************************************/
int PutOneCharterToReceiveStack(uint8_t Charter)
{
	return PutCharStack(Charter,&m_StackR);
}
/****************************************************************************/
int GetCharterFromTranceiveStack(uint8_t* Charter)
{
	return GetCharStack(Charter,&m_StackT);
}
/****************************************************************************/
MyCommand GetComandBuffer(void)
{
	return m_CommandR;

}
/****************************************************************************/
void InitInternalCommand(void)
{
	InitCommand(&m_CommandR);
}
/****************************************************************************/
int   SendCommandLegacy(uint8_t *Buff, int N)
{
 return SendCommandL (Buff, N, &m_StackT);
}
/****************************************************************************/
int CheckByteOldProtocolSign(uint8_t Charter )
{
  int Res = 0;
  switch(Charter)
  {
  
  case CommadID_Two_Byte_READ:
  case CommadID_Two_Byte_TERMINAL:
  case CommadID_Two_Byte_Status:
  case CommadID_Two_Byte_StepsHi:
  case CommadID_Two_Byte_StepsLo:
  case CommadID_Two_Byte_VelocityHi:
  case CommadID_Two_Byte_VelocityLo:
  case CommadID_Two_Byte_RampHi:
  case CommadID_Two_Byte_RampLo:
  case CommadID_Two_Byte_CounterHi:
  case CommadID_Two_Byte_CounterLo:
  case CommadID_Two_Byte_InitialVLo:
  case CommadID_Two_Byte_TimeDelayLo:
  case CommadID_Two_Byte_TurnOffHVLo:
   {
      Res = 1;
      break;
   }
  }
    return Res;  
};
/****************************************************************************/
int ChecIs8ByteCharter(uint8_t Charter)
{
  return Charter & 0x80;
}
/****************************************************************************/
void Filter7BitTraffc(uint8_t Data)
{
  
  Data = Data &0x7f;
  
}
/****************************************************************************/
