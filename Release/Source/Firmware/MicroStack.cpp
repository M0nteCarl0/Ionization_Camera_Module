#include "MicroStack.h"
int RxCount = 0;
//------------------------------------------------------------------------------
void InitStack (MyStack *Stack)
{
  Stack->Begin  = 0;
  Stack->End    = 0;
  Stack->Flg    = 1;
 
};
//------------------------------------------------------------------------------
    int PutCharStack (uint8_t Data, MyStack *Stack)
{
  int Begin = Stack->Begin;
  
  Begin++;
  if (Begin > NST-1){
    Begin = 0;
  }
  
  if (Begin == Stack->End){
    Stack->Flg = 0;
    return Stack->Flg;
  }
  
  Stack->Begin = Begin;
  
  Stack->Data [Stack->Begin] = Data;
  RxCount++;
  Stack->Flg = 1;
  return Stack->Flg;
};
//------------------------------------------------------------------------------
   int GetCharStack (uint8_t *Data, MyStack *Stack)
{
  if (Stack->Begin == Stack->End){
    Stack->Flg = 0;
    return Stack->Flg;
  }
  
  Stack->End++;
  if (Stack->End > NST-1)
    Stack->End = 0;

  *Data = Stack->Data [Stack->End];      
      
  Stack->Flg = 1;
  return Stack->Flg;
};
/******************************************************************************/
int   GetRecivedBytesCount(void)
{
  return RxCount; 
}
/******************************************************************************/
void  ResetRecivedBytesCount(void)
{
  RxCount  = 0;
}
/******************************************************************************/
int GetPositionFirstByte(MyStack *Stack)
{
 return Stack->Begin - RxCount; 
}
/******************************************************************************/
int GetPositionOffsetFirstByte(MyStack *Stack, uint8_t Offset)
{
  return  Stack->Begin -  (RxCount -  Offset);
}
/******************************************************************************/

