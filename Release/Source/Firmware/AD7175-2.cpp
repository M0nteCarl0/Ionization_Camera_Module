//-----------------------------------------------------------------------------
//       Novikov V.P.
//       22.04.2016
//       from Molotaliev
//-----------------------------------------------------------------------------
#include "AD7175-2.hpp"

//#include "Math_AD7175_2.h"

#include <math.h>

using namespace  Conversion;
#define NUM_SAMPLES 99

//static bool RXFlag  = false;
//static uint16_t ID = 0;
//static  uint16_t DATADEBUG  = 0;
volatile  uint16_t us_Counter = 0;
uint8_t STA = 0x0;
::uint16_t RxC = 0;
uint8_t BufferRx[1024]  = {0};
uint8_t BufferTx[1024] = {0};
/*****************************************************************************/
void InitAD7175(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;;
    SPI_InitTypeDef  SPI_InitStructure;
  
    /* Enable GPIO clocks */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);
    ResetADC();
    /* SPI GPIO Configuration --------------------------------------------------*/
    /* Connect SPI pins to AF5 */  
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource12,GPIO_AF_SPI3);
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource11,GPIO_AF_SPI3);
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_SPI3);
  
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
  
    /* SPI  MOSI pin configuration */
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_11;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    //INPUT TEST
/*    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_6|GPIO_Pin_14;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
*/    
    //SYNC OUT 
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    SyncOff();     
    DeselectAD7175();
   
    SPI_I2S_DeInit(SPI3);
    SPI_StructInit(&SPI_InitStructure);
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    
    //SPI MODE 3 - CPHA -1 CPOL - 1
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPI3,&SPI_InitStructure);
    SPI_NSSInternalSoftwareConfig(SPI3,SPI_NSSInternalSoft_Set);
    SPI_Cmd(SPI3,ENABLE); 

    
    
    
}
/*****************************************************************************/
RSTATUS_REGISTER ReadStatusRegister(bool DATA_STAT_ON )
//    0x00
{
      COMSS_REGISTER _REG;
      RSTATUS_REGISTER ST;
      _REG.BITS.RA =STATUS_REGISTER;
      _REG.BITS.R_W = READ_CMD;
      _REG.BITS._WEN = 0;
      if(!DATA_STAT_ON)
      {
        SelectAD7175();
      } 
      WriteByte(_REG.COMSS);
      ST.STATUS  =  WriteByte(0x0);
      if(!DATA_STAT_ON)
      {
        DeselectAD7175();
      };
      return ST;
}
/*****************************************************************************/
RADC_MODE_REGISTER ReadADCModeRegister(void)
//    0x01
{
      COMSS_REGISTER _REG;
      RADC_MODE_REGISTER ADMR;
     _REG.BITS.RA = ADC_MODE_REGISTER;
     _REG.BITS.R_W = READ_CMD;
     _REG.BITS._WEN = 0;
      SelectAD7175();
      WriteByte(_REG.COMSS);
      uint8_t  Low    =  WriteByte(0x0);
      uint8_t  High   =  WriteByte(0x0);
      ByteToWord(ADMR.ADC_MODE,High,Low);
      DeselectAD7175();
      return  ADMR;
}
/*****************************************************************************/
RINTERFACE_MODE_REGISTER  ReadInterfaceModeRegister(void)
//    0x02
{
     COMSS_REGISTER _REG;
     RINTERFACE_MODE_REGISTER IFMODE;
     _REG.BITS.RA = INTERFACE_MODE_REGISTER;
     _REG.BITS.R_W = READ_CMD;
     _REG.BITS._WEN = 0;
      SelectAD7175();
      WriteByte(_REG.COMSS);
      uint8_t  Low    =  WriteByte(0x0);
      uint8_t  High   =  WriteByte(0x0);
      ByteToWord(IFMODE.INTERFACE_MODE,High,Low);
      DeselectAD7175();
      return  IFMODE;
}
/*****************************************************************************/
RGPIO_CONFIGURATION_REGISTER ReadGpioRegister(void)
//    0x06
{
      RGPIO_CONFIGURATION_REGISTER RGPIO;
      COMSS_REGISTER _REG;
      _REG.BITS.RA = GPIO_CONFIGURATION_REGISTER;
      _REG.BITS.R_W = READ_CMD;
      _REG.BITS._WEN = 0;
      SelectAD7175();
      WriteByte(_REG.COMSS);
      uint8_t    Low  =  WriteByte(0x0);
      uint8_t  High   =  WriteByte(0x0);
      ByteToWord(RGPIO.GPIO_CONFIGURATION,High,Low);
      DeselectAD7175();
     return RGPIO;
}
/*****************************************************************************/
uint16_t ReadADC_ID(void)
//    0x07
{
       NOT_OPTIMIZE COMSS_REGISTER _REG;
      _REG.BITS.RA = ID_REGISTER;
      _REG.BITS.R_W = READ_CMD;
      _REG.BITS._WEN = 0;
      SelectAD7175();
      uint16_t ADC_ID;
      WriteByte(_REG.COMSS);
      uint8_t Low    =  WriteByte(0x0);
      uint8_t High   =  WriteByte(0x0);
      ByteToWord(ADC_ID,High,Low);
      DeselectAD7175();
      return ADC_ID;
}
/*****************************************************************************/
RCHANNEL_REGISTER ReadChannelRegister  (uint8_t ChannelID)
//    0x10, 0x11, 0x12, 0x13 
{

    RCHANNEL_REGISTER CR ;
    CR._CHANNEL_REGISTER = 0;
    COMSS_REGISTER _REG;
    switch(ChannelID)
    {
      case 0:   _REG.BITS.RA = CHANNEL_0_REGISTER;break;
      case 1:   _REG.BITS.RA = CHANNEL_1_REGISTER;break;
      case 2:   _REG.BITS.RA = CHANNEL_2_REGISTER;break;
      case 3:   _REG.BITS.RA = CHANNEL_3_REGISTER;break;
    }
    _REG.BITS.R_W = READ_CMD;
    _REG.BITS._WEN = 0;
    SelectAD7175();
    WriteByte(_REG.COMSS);
    uint8_t  Low    =  WriteByte(0x0);
    uint8_t  High   =  WriteByte(0x0);
    ByteToWord(CR._CHANNEL_REGISTER,High,Low);
    DeselectAD7175();
  return CR;
   
}
/*****************************************************************************/
RSETUP_CONFIGURATION_REGISTER ReadSetupConfiguration(uint8_t ConfigID)
//    0x20, 0x21, 0x22, 0x23 
{
  
      COMSS_REGISTER _REG;
      RSETUP_CONFIGURATION_REGISTER SCR;
      switch(ConfigID)
      {
        case 0:   _REG.BITS.RA = SETUP_CONFIGURATION_0_REGISTER;;break;
        case 1:   _REG.BITS.RA = SETUP_CONFIGURATION_1_REGISTER;;break;
        case 2:   _REG.BITS.RA = SETUP_CONFIGURATION_2_REGISTER;;break;
        case 3:   _REG.BITS.RA = SETUP_CONFIGURATION_3_REGISTER;;break;
      }
      _REG.BITS.R_W = READ_CMD;
      _REG.BITS._WEN = 0;
      SelectAD7175();
      WriteByte(_REG.COMSS);
      uint8_t Low    =  WriteByte(0x0);
      uint8_t High   =  WriteByte(0x0);
      ByteToWord(SCR.SETUP_CONFIGURATION,High,Low);
      DeselectAD7175();
  return SCR;
}
/*****************************************************************************/
RFILTER_CONFIGURATION_REGISTER ReadFilterConfigurationRegister(uint8_t FilterID)
//    0x28, 0x29, 0x2A, 0x2B
{
    COMSS_REGISTER _REG;
    RFILTER_CONFIGURATION_REGISTER RF;
    switch(FilterID)
    {
      case 0: _REG.BITS.RA = FILTER_CONFIGURATION_0_REGISTER;break;
      case 1: _REG.BITS.RA = FILTER_CONFIGURATION_1_REGISTER;break;
      case 2: _REG.BITS.RA = FILTER_CONFIGURATION_2_REGISTER;break;
      case 3: _REG.BITS.RA = FILTER_CONFIGURATION_3_REGISTER;break;
    }
    _REG.BITS.R_W = READ_CMD;
    _REG.BITS._WEN = 0;
     SelectAD7175();
     WriteByte(_REG.COMSS);
     uint8_t  Low    =  WriteByte(0x0);
     uint8_t  High   =  WriteByte(0x0);
     ByteToWord(RF.FILTER_CONFIGURATION,High,Low);
     DeselectAD7175();
     return RF;
}
/*****************************************************************************/
uint24_t ReadOffsetRegister(uint8_t OffsetID)
//    0x30, 0x31, 0x32, 0x33
{
      COMSS_REGISTER _REG;
      uint24_t OFFSET;
      switch(OffsetID)
      {
        case 0:   _REG.BITS.RA = OFFSET_0_REGISTER;;break;
        case 1:   _REG.BITS.RA = OFFSET_1_REGISTER;;break;
        case 2:   _REG.BITS.RA = OFFSET_2_REGISTER;;break;
        case 3:   _REG.BITS.RA = OFFSET_3_REGISTER;;break;
      }
      _REG.BITS.R_W = READ_CMD;
      _REG.BITS._WEN = 0;
      SelectAD7175();
      WriteByte(_REG.COMSS);
      OFFSET.uint24_t_t_BITS.MSB   =  WriteByte(0x0);
      OFFSET.uint24_t_t_BITS.MMSB  =  WriteByte(0x0);
      OFFSET.uint24_t_t_BITS.LSB   =  WriteByte(0x0);
      DeselectAD7175();
     return  OFFSET;
}
/*****************************************************************************/
uint24_t ReadGainRegister(uint8_t GainID)
//    0x38, 0x39, 0x3A, 0x3B 
{
      COMSS_REGISTER _REG;
      uint24_t GAIN;
      switch(GainID)
      {
        case 0:   _REG.BITS.RA = GAIN_0_REGISTER;break;
        case 1:   _REG.BITS.RA = GAIN_1_REGISTER;break;
        case 2:   _REG.BITS.RA = GAIN_2_REGISTER;break;
        case 3:   _REG.BITS.RA = GAIN_3_REGISTER;break;
      }
      _REG.BITS.R_W = READ_CMD;
      _REG.BITS._WEN = 0;
      SelectAD7175();
      WriteByte(_REG.COMSS);
      GAIN.uint24_t_t_BITS.MSB    =   WriteByte(0x0);
      GAIN.uint24_t_t_BITS.MMSB   =  WriteByte(0x0);
      GAIN.uint24_t_t_BITS.LSB    =   WriteByte(0x0);
      DeselectAD7175();
      return GAIN;
}
/*****************************************************************************/
bool CheckConectionAD7175(void)
{
  static NOT_OPTIMIZE  uint16_t ID = ReadADC_ID();
  
  static NOT_OPTIMIZE  RSTATUS_REGISTER Status = ReadStatusRegister(); 
  
  static NOT_OPTIMIZE  RINTERFACE_MODE_REGISTER Interfce  =  ReadInterfaceModeRegister();
  static NOT_OPTIMIZE RGPIO_CONFIGURATION_REGISTER   GPIO =   ReadGpioRegister();
  
  static NOT_OPTIMIZE RADC_MODE_REGISTER ADC_Mode = ReadADCModeRegister();
  
  static NOT_OPTIMIZE RFILTER_CONFIGURATION_REGISTER Filter1 =   ReadFilterConfigurationRegister(0);
  static NOT_OPTIMIZE RFILTER_CONFIGURATION_REGISTER Filter2 =   ReadFilterConfigurationRegister(1);
  static NOT_OPTIMIZE RFILTER_CONFIGURATION_REGISTER Filter3 =   ReadFilterConfigurationRegister(2);
  static NOT_OPTIMIZE RFILTER_CONFIGURATION_REGISTER Filter4 =   ReadFilterConfigurationRegister(3);
  
  static NOT_OPTIMIZE RSETUP_CONFIGURATION_REGISTER Config1 =  ReadSetupConfiguration(0);
  static NOT_OPTIMIZE RSETUP_CONFIGURATION_REGISTER Config2 =  ReadSetupConfiguration(1);
  static NOT_OPTIMIZE RSETUP_CONFIGURATION_REGISTER Config3 =  ReadSetupConfiguration(2);
  static NOT_OPTIMIZE RSETUP_CONFIGURATION_REGISTER Config4 =  ReadSetupConfiguration(3);
  
  static NOT_OPTIMIZE RCHANNEL_REGISTER  Channel1 =  ReadChannelRegister(0);
  static NOT_OPTIMIZE RCHANNEL_REGISTER  Channel2 =  ReadChannelRegister(1);
  static NOT_OPTIMIZE RCHANNEL_REGISTER  Channel3 =  ReadChannelRegister(2);
  static NOT_OPTIMIZE RCHANNEL_REGISTER  Channel4 =  ReadChannelRegister(3);
  
  static NOT_OPTIMIZE uint24_t  Offset1  =  ReadOffsetRegister(0);
  static NOT_OPTIMIZE uint24_t  Offset2  =  ReadOffsetRegister(1);
  static NOT_OPTIMIZE uint24_t  Offset3  =  ReadOffsetRegister(2);
  static NOT_OPTIMIZE uint24_t  Offset4  =  ReadOffsetRegister(3);
  
  
  static NOT_OPTIMIZE uint24_t  Gain1  =  ReadGainRegister(0);
  static NOT_OPTIMIZE uint24_t  Gain2  =  ReadGainRegister(1);
  static NOT_OPTIMIZE uint24_t  Gain3  =  ReadGainRegister(2);
  static NOT_OPTIMIZE uint24_t  Gain4  =  ReadGainRegister(3);
  
  return true;
};
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool WriteChannelRegister  (RCHANNEL_REGISTER  CR,uint8_t ChannelID)
{
  COMSS_REGISTER _REG;
  uint8_t  Low  = 0; 
  uint8_t  High=  0;
  bool Result = true;
  WordToByte(CR._CHANNEL_REGISTER,High,Low); 
  switch(ChannelID)
  {
    case 0:   _REG.BITS.RA = CHANNEL_0_REGISTER;break;
    case 1:   _REG.BITS.RA = CHANNEL_1_REGISTER;break;
    case 2:   _REG.BITS.RA = CHANNEL_2_REGISTER;break;
    case 3:   _REG.BITS.RA = CHANNEL_3_REGISTER;break;
  }
  _REG.BITS.R_W = WRITE_CMD;
  _REG.BITS._WEN = 0;
   SelectAD7175();
   WriteByte(_REG.COMSS);
   WriteByte(Low);
   WriteByte(High);
   DeselectAD7175();
   Delay_us(6000);
   RCHANNEL_REGISTER  CRCC = ReadChannelRegister(ChannelID);
   if(CRCC._CHANNEL_REGISTER!= CR._CHANNEL_REGISTER)
   {
     Result = false;
   }
   
   return Result;
   
}
/*****************************************************************************/
bool WriteADCModeRegister  (RADC_MODE_REGISTER  ADMODE)
{
  
   COMSS_REGISTER _REG;
   uint8_t  Low  = 0; 
   uint8_t  High=  0;
   bool Result = true;
   WordToByte(ADMODE.ADC_MODE,High,Low);
  _REG.BITS.RA = ADC_MODE_REGISTER;
  _REG.BITS.R_W = WRITE_CMD;
  _REG.BITS._WEN = 0;
   SelectAD7175();
   WriteByte(_REG.COMSS);
   WriteByte(Low);
   WriteByte(High);
   DeselectAD7175();
  
  Delay_us(6000);
  RADC_MODE_REGISTER ADMODEC = ReadADCModeRegister();
  if(ADMODEC.ADC_MODE != ADMODE.ADC_MODE)
  {
     Result = false;
  }
  return Result;
}
/*****************************************************************************/
bool WriteInterfaceModeRegister(RINTERFACE_MODE_REGISTER   INTERFACE_MODE)
{
   COMSS_REGISTER _REG;
   uint8_t  Low  = 0; 
   uint8_t  High=  0;
   bool Result = true;
   WordToByte(INTERFACE_MODE.INTERFACE_MODE,High,Low);
  _REG.BITS.RA = INTERFACE_MODE_REGISTER;
  _REG.BITS.R_W = WRITE_CMD;
  _REG.BITS._WEN = 0;
   SelectAD7175();
   WriteByte(_REG.COMSS);
   WriteByte(Low);
   WriteByte(High);
   DeselectAD7175(); 
   
   Delay_us(6000);
   RINTERFACE_MODE_REGISTER   INTERFACE_MODEC  =  ReadInterfaceModeRegister();
   if(INTERFACE_MODEC.INTERFACE_MODE != INTERFACE_MODE.INTERFACE_MODE)
  {
     Result = false;
  }
  
     return Result;
   
}
/*****************************************************************************/
bool WriteGpioRegister  (RGPIO_CONFIGURATION_REGISTER  GPIO)
{
  
   COMSS_REGISTER _REG;
   uint8_t  Low  = 0; 
   uint8_t  High=  0;
   bool Result = true;
   WordToByte(GPIO.GPIO_CONFIGURATION,High,Low);
  _REG.BITS.RA = GPIO_CONFIGURATION_REGISTER;
  _REG.BITS.R_W = WRITE_CMD;
  _REG.BITS._WEN = 0;
   SelectAD7175();
   WriteByte(_REG.COMSS);
   WriteByte(Low);
   WriteByte(High);
   DeselectAD7175();
   
   Delay_us(6000);
   RGPIO_CONFIGURATION_REGISTER  GPIOCC = ReadGpioRegister();
      if(GPIOCC.GPIO_CONFIGURATION!= GPIO.GPIO_CONFIGURATION)
  {
     Result = false;
  }
   return Result;
}
/*****************************************************************************/
bool WriteSetupConfiguration(RSETUP_CONFIGURATION_REGISTER SETUP,uint8_t ConfigID)
{
  COMSS_REGISTER _REG;
  uint8_t  Low  = 0; 
  uint8_t  High=  0;
  bool Result = true;
  WordToByte(SETUP.SETUP_CONFIGURATION,High,Low); 
  switch(ConfigID)
  {
    case 0:   _REG.BITS.RA = SETUP_CONFIGURATION_0_REGISTER;break;
    case 1:   _REG.BITS.RA = SETUP_CONFIGURATION_1_REGISTER;break;
    case 2:   _REG.BITS.RA = SETUP_CONFIGURATION_2_REGISTER;break;
    case 3:   _REG.BITS.RA = SETUP_CONFIGURATION_3_REGISTER;break;
  }
  _REG.BITS.R_W = WRITE_CMD;
  _REG.BITS._WEN = 0;
   SelectAD7175();
   WriteByte(_REG.COMSS);
   WriteByte(Low);
   WriteByte(High);
   DeselectAD7175();
  
   Delay_us(6000);
   RSETUP_CONFIGURATION_REGISTER SETUPC = ReadSetupConfiguration(ConfigID);
  
   
     if(SETUPC.SETUP_CONFIGURATION!= SETUP.SETUP_CONFIGURATION)
  {
     Result = false;
    
  }
   return  Result;
}
/*****************************************************************************/
bool WriteFilterConfigurationRegister(RFILTER_CONFIGURATION_REGISTER FILTER,uint8_t FilterID)
{
  COMSS_REGISTER _REG;
  uint8_t  Low  = 0; 
  uint8_t  High=  0;
  bool Result = true;
  WordToByte(FILTER.FILTER_CONFIGURATION,High,Low); 
  switch(FilterID)
  {
    case 0:   _REG.BITS.RA = FILTER_CONFIGURATION_0_REGISTER;break;
    case 1:   _REG.BITS.RA = FILTER_CONFIGURATION_1_REGISTER;break;
    case 2:   _REG.BITS.RA = FILTER_CONFIGURATION_2_REGISTER;break;
    case 3:   _REG.BITS.RA = FILTER_CONFIGURATION_3_REGISTER;break;
  }
  _REG.BITS.R_W = WRITE_CMD;
  _REG.BITS._WEN = 0;
   SelectAD7175();
   WriteByte(_REG.COMSS);
   WriteByte(Low);
   WriteByte(High);
   DeselectAD7175();
   
   Delay_us(6000);
   RFILTER_CONFIGURATION_REGISTER FILTERC = ReadFilterConfigurationRegister(FilterID);
   
   if(FILTERC.FILTER_CONFIGURATION!=FILTER.FILTER_CONFIGURATION)
   {
     Result = false; 
   }
   return  Result;
}
/*****************************************************************************/
bool WriteGainRegister(uint24_t GAIN,uint8_t GainID)
{
   COMSS_REGISTER _REG;
   bool Result = true;
  switch(GainID)
  {
    case 0:   _REG.BITS.RA = GAIN_0_REGISTER;break;
    case 1:   _REG.BITS.RA = GAIN_1_REGISTER;break;
    case 2:   _REG.BITS.RA = GAIN_2_REGISTER;break;
    case 3:   _REG.BITS.RA = GAIN_3_REGISTER;break;
  }
  _REG.BITS.R_W = WRITE_CMD;
  _REG.BITS._WEN = 0;
   SelectAD7175();
   WriteByte(_REG.COMSS);
   WriteByte(GAIN.uint24_t_t_BITS.MSB);
   WriteByte(GAIN.uint24_t_t_BITS.MMSB);
   WriteByte(GAIN.uint24_t_t_BITS.LSB);
   DeselectAD7175();
   
   uint24_t GAINC =  ReadGainRegister(GainID); 
   
    if(GAINC.Value!=GAIN.Value)
   {
     Result = false; 
   }
   
   return Result;
}
/*****************************************************************************/
bool WriteOffsetRegister(uint24_t Offset,uint8_t OffsetID)
{
  COMSS_REGISTER _REG;
   bool Result = true;
  switch(OffsetID)
  {
    case 0:   _REG.BITS.RA = OFFSET_0_REGISTER;break;
    case 1:   _REG.BITS.RA = OFFSET_1_REGISTER;break;
    case 2:   _REG.BITS.RA = OFFSET_2_REGISTER;break;
    case 3:   _REG.BITS.RA = OFFSET_3_REGISTER;break;
  }
  _REG.BITS.R_W = WRITE_CMD;
  _REG.BITS._WEN = 0;
   SelectAD7175();
   WriteByte(_REG.COMSS);
   WriteByte(Offset.uint24_t_t_BITS.MSB);
   WriteByte(Offset.uint24_t_t_BITS.MMSB);
   WriteByte(Offset.uint24_t_t_BITS.LSB);
   DeselectAD7175();
   
   uint24_t OffsetC = ::ReadOffsetRegister(OffsetID);
   if(OffsetC.Value!=Offset.Value)
   {
     Result = false;
   }
   
   return Result;
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void SelectAD7175(void)
{
   GPIO_ResetBits(GPIOB,GPIO_Pin_15);
}
/*****************************************************************************/
void DeselectAD7175(void)
{
   GPIO_SetBits(GPIOB,GPIO_Pin_15);  
}
/*****************************************************************************/
void Delay_us(uint16_t us)
{
  us_Counter  = us;
  do{ 
     us_Counter --;
    __NOP();
     __NOP();
     __NOP();
  }while(us_Counter > 0);
  
}
/*****************************************************************************/
void ResetADC(void)
{
  
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_SetBits(GPIOC,GPIO_Pin_12);
    for(uint16_t i = 0;i<65535;i++){}  
    GPIO_ResetBits(GPIOC,GPIO_Pin_12);
  
}
/*****************************************************************************/
inline uint8_t WriteByte(uint8_t Byte)
{
      do{}while(SPI_I2S_GetFlagStatus(SPI3,SPI_I2S_FLAG_TXE)!=SET);
      SPI_I2S_SendData(SPI3,Byte);
      do{}while(SPI_I2S_GetFlagStatus(SPI3,SPI_I2S_FLAG_RXNE)!=SET);
      return SPI_I2S_ReceiveData(SPI3);
  
}
/*****************************************************************************/
uint32_t ReadADCData(void)
{
  
     uint32_t Result = 0;
     uint8_t Temp[4] = {0};
     COMSS_REGISTER Comunice;
     Comunice.BITS.RA =DATA_REGISTER;
     Comunice.BITS.R_W = READ_CMD;
     Comunice.BITS._WEN = 0;
     uint24_t Out;  
     Out.Value = 0;
     SelectAD7175();
     WriteByte(Comunice.COMSS);
     Temp[0] = WriteByte(0x0);//L
     Temp[1] = WriteByte(0x0);//H
     Temp[2] = WriteByte(0x0);//L
     DeselectAD7175();
     
     for(uint8_t i = 0; i < 3; i++)
    {
        Result<<= 8;
        Result += Temp[i];;
    }
                           
   return   Result;
}
/*****************************************************************************/
uint16_t ReadADCData16(void)
{
  
  
     COMSS_REGISTER Comunice;
     Comunice.BITS.RA =DATA_REGISTER;
     Comunice.BITS.R_W = READ_CMD;
     Comunice.BITS._WEN = 0;
     uint16_t ADC_DATA = 0;
     uint24_t Out;  
     SelectAD7175();
     WriteByte(Comunice.COMSS);
     uint8_t  Low  =  WriteByte(0x0);
     uint8_t  High  =  WriteByte(0x0);
     DeselectAD7175();
     ByteToWord(ADC_DATA,High,Low);                           
  
   return ADC_DATA;
}
/*****************************************************************************/
void DeepSleepMode(void)
{
  
 static RADC_MODE_REGISTER RDC;
 static RADC_MODE_REGISTER RDCCheck;
 RDC  =  ReadADCModeRegister();
 RDC.ADC_MODE_REGISTER_BITS.MODE = ADC_MODE_Power_down_mode;
 WriteADCModeRegister(RDC);
 RDCCheck = ReadADCModeRegister();
 
  
}
/*****************************************************************************/
void StandyMode(void)
{
  
 static RADC_MODE_REGISTER RDC;
 static RADC_MODE_REGISTER RDCCheck;
 RDC  =  ReadADCModeRegister();
 RDC.ADC_MODE_REGISTER_BITS.MODE = ADC_MODE_Standby_mode;
 WriteADCModeRegister(RDC);
 RDCCheck = ReadADCModeRegister();
}
/*****************************************************************************/
void SetGPIOsADC(void)
{
  
  
  RGPIO_CONFIGURATION_REGISTER REG = ReadGpioRegister();
  REG.GPIO_CONFIGURATION_REGISTER_BITS.GP_DATA0 = 0;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.GP_DATA1 = 1;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.OP_EN0 = 1;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.OP_EN1 = 1;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.ERR_EN =ERR_EN_SYNC_ERROR_is_a_general_purpose_output;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.ERR_DAT = 0;
  WriteGpioRegister(REG);
  
  
  
  REG = ReadGpioRegister();
  REG.GPIO_CONFIGURATION_REGISTER_BITS.GP_DATA0 = 1;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.GP_DATA1 = 0;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.OP_EN0 = 1;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.OP_EN1 = 1;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.ERR_DAT = 1;
  WriteGpioRegister(REG);
  
  
  REG = ReadGpioRegister();
  REG.GPIO_CONFIGURATION_REGISTER_BITS.GP_DATA0 = 0;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.GP_DATA1 = 0;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.OP_EN0 = 0;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.OP_EN1 = 0;
  REG.GPIO_CONFIGURATION_REGISTER_BITS.ERR_DAT = 0;
  WriteGpioRegister(REG);
  
  
}

/*****************************************************************************/
void MultiChannelSetupContinusRead(void)
{
  RCHANNEL_REGISTER              CH1_SETUP;
  RSETUP_CONFIGURATION_REGISTER  SETUP;
  RADC_MODE_REGISTER             ADC_SET;
  RINTERFACE_MODE_REGISTER       Interface;
  
  CH1_SETUP = ReadChannelRegister(0);
  SETUP = ReadSetupConfiguration(0);
  Interface = ReadInterfaceModeRegister();
  
  ///�������� �������� ��� OP AMP  � ���� �� �������� ������
  
  ADC_SET.ADC_MODE = 0x0;
  ADC_SET.ADC_MODE_REGISTER_BITS.DELLAY            = DELLAY_4us;  //DELLAY_4us;
  ADC_SET.ADC_MODE_REGISTER_BITS.CLOCKSEL          = CLOCKSEL_External_clock_input_on_XTAL2_DIV_CLKIO_pin;
  ADC_SET.ADC_MODE_REGISTER_BITS.REF_EN            = DISABLE; 

  WriteADCModeRegister(ADC_SET);       // !!!
  
  ///�������� �������� ��� 24b data  � ������������� ��� � AD7699
  Interface.INTERFACE_MODE = 0x0108;
  Interface.INTERFACE_MODE_REGISTER_BITS.CRC_EN    = 0x0;
  Interface.INTERFACE_MODE_REGISTER_BITS.WL16      = WL16_24_Bit_data;
  //FOR SYNC CONVECT
//  Interface.INTERFACE_MODE_REGISTER_BITS.ALT_SYNC  = ENABLE;
  
  WriteInterfaceModeRegister(Interface);     // !!!
  
  
  RGPIO_CONFIGURATION_REGISTER SGPIO;
  SGPIO.GPIO_CONFIGURATION = 0x0;
  
  WriteGpioRegister(SGPIO);     // !!!
  
  //��������� ������� ���(AIN4 - AGND)
  /*****************************************************************/
  CH1_SETUP._CHANNEL_REGISTER = 0x8004;
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINNEG              = AIN_AIN4;//AGND
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINPOS              = AIN_AIN0;//A-1
  WriteChannelRegister(CH1_SETUP,0);
  /***************************************************************/
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINNEG              = AIN_AIN4;//AGND
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINPOS              = AIN_AIN1;//B-1
  WriteChannelRegister(CH1_SETUP,1);
  /***************************************************************/
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINNEG              = AIN_AIN4;//AGND
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINPOS              = AIN_AIN2;//C-1
  WriteChannelRegister(CH1_SETUP,2);
  /***************************************************************/
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINNEG              = AIN_AIN4;//AGND
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINPOS              = AIN_AIN3;//D-1
  WriteChannelRegister(CH1_SETUP,3);
  /***************************************************************/
  SETUP.SETUP_CONFIGURATION = 0x0;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.REF_SEL       = REF_SEL_External_Reference;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.MINUS_AINBUF  = ENABLE;  //DISABLE;   //ENABLE;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.PLUS_AINBUF   = ENABLE;  //DISABLE;   //ENABLE;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.PLUS_REFBUF   = ENABLE;  //DISABLE;   //ENABLE;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.MINUS_REFBUF  = ENABLE;  //DISABLE;   //ENABLE;
  
  WriteSetupConfiguration(SETUP,0);     // !!!
  WriteSetupConfiguration(SETUP,1); 
  WriteSetupConfiguration(SETUP,2);
  WriteSetupConfiguration(SETUP,3);
  /***************************************************************/
  RFILTER_CONFIGURATION_REGISTER Filter;
  
  Filter.FILTER_CONFIGURATION = 0x00;                                            //0x020A;
  Filter.FILTER_CONFIGURATION_REGISTER_BITS.ODR          = ODR_5KPS;             //ODR_10KPS;    // was ODR_5KPS;   //ODR_2_5KPS;
  Filter.FILTER_CONFIGURATION_REGISTER_BITS.ORDER        = ORDER_Sinc5_Sinc1;    //ORDER_Sinc3;
//  Filter.FILTER_CONFIGURATION_REGISTER_BITS.ENHFILTEN    = DISABLE;
//  Filter.FILTER_CONFIGURATION_REGISTER_BITS.ENHFILT      = ENHFILT_27SPS;
  
  WriteFilterConfigurationRegister(Filter,0);      // !!!
  WriteFilterConfigurationRegister(Filter,1);
  WriteFilterConfigurationRegister(Filter,2);
  WriteFilterConfigurationRegister(Filter,3); 
}
/*****************************************************************************/
void ADC_SwitchToInternalOscilator(void)
{
  RADC_MODE_REGISTER             ADC_SET;
  ADC_SET.ADC_MODE = 0x0;
  ADC_SET.ADC_MODE_REGISTER_BITS.DELLAY            = DELLAY_4us;  //DELLAY_4us;
  ADC_SET.ADC_MODE_REGISTER_BITS.CLOCKSEL          = CLOCKSEL_Internal_oscillator;
  ADC_SET.ADC_MODE_REGISTER_BITS.REF_EN            = DISABLE; 
  WriteADCModeRegister(ADC_SET);  
}
/*****************************************************************************/
/*
void MultiChannelRead(void)
{
  static  uint32_t AIN0[NUM_SAMPLES] = {0};
  static uint32_t  AIN1[NUM_SAMPLES] = {0};
  static  uint32_t AIN2[NUM_SAMPLES] = {0};
  static uint32_t  AIN3[NUM_SAMPLES] = {0};
  
  static double MvoltsAIN0[NUM_SAMPLES] =  {0};
  static double MvoltsAIN1[NUM_SAMPLES]  =  {0};
  static double MvoltsAIN2[NUM_SAMPLES]  =  {0};
  static double MvoltsAIN3[NUM_SAMPLES]  =  {0};
 
  static RSTATUS_REGISTER RS;
  
  static int it1;
  static int it2;
  static int it3;
  static int it4;
  
  static double RMS_AIN0 = 0;
  static double RMS_AIN1 = 0;
  static double RMS_AIN2 = 0;
  static double RMS_AIN3 = 0;
 
  static double AVER_AIN0 = 0;
  static double AVER_AIN1 = 0;
  static double AVER_AIN2 = 0;
  static double AVER_AIN3 = 0;
  
  static RSTATUS_REGISTER Check;
  
  it1 = it2 = it3 = it4 = 0;
   SyncOn();
  do
  {
  
  
    do
    {
    RS =  ReadStatusRegister();
     }while(RS.BITS.RDY!=0);
      
    switch(RS.BITS.CHANNELL)
    {
      case CHANNELL_Channel_0:
      {
        AIN0[it1]  = ReadADCData();
        MvoltsAIN0[it1] =   AIN0[it1] * ( 5000/powf(2,24));
        it1++;
        break;
      }
      
      case CHANNELL_Channel_1:
       {
         
         AIN1[it2] = ReadADCData();
         MvoltsAIN1[it2] =   AIN1[it2] * ( 5000/powf(2,24));
         it2++;
         break;
       }
 
      case CHANNELL_Channel_2:
       {
         
         AIN2[it3] = ReadADCData();
         MvoltsAIN2[it3] =   AIN2[it2] * ( 5000/powf(2,24));
         it3++;
         break;
       }
      
       
        case CHANNELL_Channel_3:
       {
         AIN3[it4] = ReadADCData();
         MvoltsAIN3[it4] =   AIN3[it4] * ( 5000/powf(2,24));
         it4++;
 
         break;
       }
      
    };
   
 
  }while( it1 < NUM_SAMPLES &&  it2 < NUM_SAMPLES && it3 < NUM_SAMPLES && it4 < NUM_SAMPLES);
  SyncOff();
  
  static uint32_t MaxAI0 = AD7175_Max(AIN0,NUM_SAMPLES);
  static uint32_t MinAI0 = AD7175_Min(AIN0,NUM_SAMPLES);
    
  static uint32_t MaxAI1 = AD7175_Max(AIN1,NUM_SAMPLES);
  static uint32_t MinAI1 = AD7175_Min(AIN1,NUM_SAMPLES);
  
  static uint32_t MaxAI2 =AD7175_Max(AIN2,NUM_SAMPLES); 
  static uint32_t MinAI2 =AD7175_Min(AIN2,NUM_SAMPLES);
    
  static uint32_t MaxAI3 = AD7175_Max(AIN3,NUM_SAMPLES);
  static uint32_t MinAI3 = AD7175_Min(AIN3,NUM_SAMPLES);
    
  RMS_AIN0 = RMS(AIN0,NUM_SAMPLES-1,AVER_AIN0);
  RMS_AIN1 = RMS(AIN1,NUM_SAMPLES-1,AVER_AIN1);;
  RMS_AIN2 = RMS(AIN2,NUM_SAMPLES-1,AVER_AIN2);
  RMS_AIN3 = RMS(AIN3,NUM_SAMPLES-1,AVER_AIN3);
 
  
}
*/
/*****************************************************************************/
void SingleChannelSetup(ODR SampleRate)
{
  
 
   RCHANNEL_REGISTER CH1_SETUP;
  RSETUP_CONFIGURATION_REGISTER SETUP;
  RINTERFACE_MODE_REGISTER Interface;
  CH1_SETUP  =   ReadChannelRegister(0);
  SETUP = ReadSetupConfiguration(0);
  Interface = ReadInterfaceModeRegister();

  Interface.INTERFACE_MODE = 0x0108;
  Interface.INTERFACE_MODE_REGISTER_BITS.CRC_EN = 0x0;
  Interface.INTERFACE_MODE_REGISTER_BITS.WL16 = WL16_24_Bit_data;
  WriteInterfaceModeRegister(Interface);
  
  
  RGPIO_CONFIGURATION_REGISTER SGPIO;
  SGPIO.GPIO_CONFIGURATION = 0x0;
  WriteGpioRegister(SGPIO);
  
  
  CH1_SETUP._CHANNEL_REGISTER = 0x8004;
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINNEG =  AIN_AIN4;
  CH1_SETUP.CHANNEL_REGISTER_BITS.AINPOS =  AIN_AIN0;
  WriteChannelRegister(CH1_SETUP,0);
  
  CH1_SETUP._CHANNEL_REGISTER = 0x0000;
  
  WriteChannelRegister(CH1_SETUP,1);
  WriteChannelRegister(CH1_SETUP,2);
  WriteChannelRegister(CH1_SETUP,3);
  
  
  SETUP.SETUP_CONFIGURATION = 0x0;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.REF_SEL = REF_SEL_External_Reference;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.MINUS_AINBUF = ENABLE;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.PLUS_AINBUF =ENABLE;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.PLUS_REFBUF =ENABLE;
  SETUP.SETUP_CONFIGURATION_REGISTER_BITS.MINUS_REFBUF=ENABLE;
  WriteSetupConfiguration(SETUP,0);
  //WriteSetupConfiguration(SETUP,1); 
  //WriteSetupConfiguration(SETUP,2);
  //WriteSetupConfiguration(SETUP,3);
  
  
  RFILTER_CONFIGURATION_REGISTER Filter;
  
  Filter.FILTER_CONFIGURATION = 0x020A;
  Filter.FILTER_CONFIGURATION_REGISTER_BITS.ODR = SampleRate;
  Filter.FILTER_CONFIGURATION_REGISTER_BITS.ORDER =  ORDER_Sinc3; //ORDER_Sinc3;//ORDER_Sinc5_Sinc1;
  Filter.FILTER_CONFIGURATION_REGISTER_BITS.ENHFILTEN =0; 
  //Filter.FILTER_CONFIGURATION_REGISTER_BITS.SINC3_MAP =6;
  Filter.FILTER_CONFIGURATION_REGISTER_BITS.ENHFILT = ENHFILT_20SPS;
  WriteFilterConfigurationRegister(Filter,0);
  //  bool WriteFilterConfigurationRegister(RFILTER_CONFIGURATION_REGISTER FILTER,uint8_t FilterID);
  Filter.FILTER_CONFIGURATION =0x0200;
  WriteFilterConfigurationRegister(Filter,1);
  WriteFilterConfigurationRegister(Filter,2);
 WriteFilterConfigurationRegister(Filter,3);
    
 
}
/*****************************************************************************/
/*
void SingleChannelRead(void)
{
  //Example
 static uint32_t Buff[100];
// static uint16_t Buff16[100];
 static  double Mvolts[100];
 
 static double Avergage = 0;
  RADC_MODE_REGISTER  AD_MOD;
  static RSTATUS_REGISTER Check;
 
  
  static uint16_t i;
  for(i = 0;i<100;i++)
  {
    
    do{
    Check = ReadStatusRegister();
    }while(Check.BITS.RDY!=0);
    
 
  Buff[i] = ReadADCData();
  Mvolts[i] =   Buff[i] * ( 5000/powf(2,24));
   
  }
 static float _RMS  =  RMS(Buff,100,Avergage);
  
}
*/
/*****************************************************************************/
void ADC_Callibration(ADC_CalibrationMode CalibrationMode)
{
   RADC_MODE_REGISTER  RADC = ReadADCModeRegister();
   RSTATUS_REGISTER RS;
   switch(CalibrationMode)
   {
      case ADC_CalibrationMode_Internal_offset_calibration:RADC.ADC_MODE_REGISTER_BITS.MODE = ADC_MODE_Internal_offset_calibration; break;       
      case ADC_CalibrationMode_System_offset_calibration:RADC.ADC_MODE_REGISTER_BITS.MODE = ADC_MODE_System_offset_calibration;     break;  
      case ADC_CalibrationMode_System_gain_calibration:RADC.ADC_MODE_REGISTER_BITS.MODE = ADC_MODE_System_gain_calibration;         break;
   }
   WriteADCModeRegister(RADC);
   
   do{
      RS = ReadStatusRegister();  
  }while (RS.BITS.RDY != 0);
};
/*****************************************************************************/
uint8_t XOR_Checksum(uint8_t* Buff,uint8_t Count)
{
 uint8_t  Out =  Buff[0];
  
 for(uint8_t i = 1;i<Count;i++)
 {
   Out ^=Buff[i];  
 }
  return  Out; 
}
/*****************************************************************************/
uint8_t CRC8_Checksum(uint8_t* Buff,uint8_t Count)
{
      return 1;
}
/*****************************************************************************/
double RMS(uint32_t* Buff,uint16_t Count,double& Averg)
{
  
  double Summ = 0;
  double AVG = 0;
  
  // AVG 
  
  for(uint16_t i = 0;i<Count;i++)
 {
   AVG +=  Buff[i];  
 }
  AVG/=Count;
  Averg = AVG;
  
  
  for(uint16_t i = 0;i<Count;i++)
 {
   Summ +=  pow(Buff[i] - AVG,2);  
 }
 Summ  = sqrt(Summ/Count);
 return  Summ; 
  
}
/*****************************************************************************/
void SyncOn(void)
{
 GPIO_ResetBits(GPIOB,GPIO_Pin_14);  
}
/*****************************************************************************/
void SyncOff(void)
{
  GPIO_SetBits(GPIOB,GPIO_Pin_14);
}
//-----------------------------------------------------------------------------
/*
bool Acquisition (uint32_t *Vadc, int N)
{
  static uint32_t CounterEnters = 0;
  
    RSTATUS_REGISTER RS;//�������� ���������
    static uint32_t  it1 = 0;
    static uint32_t  it2 = 0;
    static uint32_t  it3 = 0;
    static uint32_t  it4 = 0;
    
//  SyncOn();//������ ���������
    
//      do{ RS =  ReadStatusRegister(); }while (RS.BITS.RDY != 0);
    
      RS =  ReadStatusRegister(); 
      if (RS.BITS.RDY != 0)   return false;
      
      switch(RS.BITS.CHANNELL)
      {
          case CHANNELL_Channel_0:
          {
            Vadc[0]  = ReadADCData();//A-1
            it1++;
            break;
          }
          
          case CHANNELL_Channel_1:
          {
            
            Vadc[1] = ReadADCData();//B-1
            it2++;
            break;
          }
    
          case CHANNELL_Channel_2:
          {
            Vadc[2] = ReadADCData();//C-1
            it3++;
            break;
          }
          
            case CHANNELL_Channel_3:
          {
            Vadc[3] = ReadADCData();//D-1
            it4++;
            break;
          }  
    };
  
//  SyncOff();//����� ���������
  
  if (++CounterEnters % N == 0)  return true;
  else                           return false;
};
*/
/*****************************************************************************/
bool Acquisition (uint32_t *Vadc, int N)
{
  static uint32_t CounterEnters = 0;
  
    RSTATUS_REGISTER RS;//�������� ���������
    static uint32_t  it1 = 0;
    static uint32_t  it2 = 0;
    static uint32_t  it3 = 0;
    static uint32_t  it4 = 0;

    if (CounterEnters % N == 0){
      Vadc[0] = Vadc[1] = Vadc[2] = Vadc[3] = 0;
    }
    
//  SyncOn();//������ ���������
    
//      do{ RS =  ReadStatusRegister(); }while (RS.BITS.RDY != 0);
    
      RS =  ReadStatusRegister(); 
      if (RS.BITS.RDY != 0)   return false;
      
      switch(RS.BITS.CHANNELL)
      {
          case CHANNELL_Channel_0:
          {
            Vadc[0]  = ReadADCData();
            Vadc[0] = Vadc[0] >> 3;
            it1++;
            break;
          }
          
          case CHANNELL_Channel_1:
          {
            Vadc[1] = ReadADCData();
            Vadc[1] = Vadc[1] >> 3;
            it2++;
            break;
          }
    
          case CHANNELL_Channel_2:
          {
            Vadc[2] = ReadADCData();
            Vadc[2] = Vadc[2] >> 3;
            it3++;
            break;
          }
          
          case CHANNELL_Channel_3:
          {
            Vadc[3] = ReadADCData();
            Vadc[3] = Vadc[3] >> 3;
            it4++;
            break;
          }  
    };
  
//  SyncOff();//����� ���������
  
  if (++CounterEnters % N == 0)  return true;
  else                           return false;
};
/*****************************************************************************/
void  Configure_ADCClock(void)
{

  RADC_MODE_REGISTER ADC_SET;
  RINTERFACE_MODE_REGISTER Interface;
 
  ADC_SET.ADC_MODE = 0x0;
    
  ADC_SET.ADC_MODE = 0x0;
  ADC_SET.ADC_MODE_REGISTER_BITS.DELLAY            = DELLAY_4us;  //DELLAY_4us;
#ifdef _HSE_CLK_ 
  ADC_SET.ADC_MODE_REGISTER_BITS.CLOCKSEL          = CLOCKSEL_External_clock_input_on_XTAL2_DIV_CLKIO_pin;
#endif
#ifdef _HSI_CLK_ || _HSI_CLK_168Mhz 
 ADC_SET.ADC_MODE_REGISTER_BITS.CLOCKSEL          = CLOCKSEL_Internal_oscillator;
#endif
  ADC_SET.ADC_MODE_REGISTER_BITS.REF_EN            = DISABLE; 

  WriteADCModeRegister(ADC_SET);       // !!!
  
  
}

void EnablrExt(void)
{
#if 0
      
   SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC,EXTI_PinSource11);
  _EXTIPINs.EXTI_Mode                         =  EXTI_Mode_Interrupt;  
  _EXTIPINs.EXTI_Line                         =  EXTI_Line11;
  _EXTIPINs.EXTI_Trigger                      =  EXTI_Trigger_Rising;
  _EXTIPINs.EXTI_LineCmd                      =  ENABLE;
   EXTI_Init(&_EXTIPINs);              
#endif
}
