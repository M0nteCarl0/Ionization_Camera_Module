#include "KYInput.h"
#include "LED.h"
#include "WindowsFilter.h"
#include <stdio.h>
KY_Assigment BG_In; //Response from KY
KY_Assigment BG_Out; //Request to KY
KY_BackyState StateKySignals;
bool KyTrig;         // Ky Synchro Detected
bool HVEmited;       // X-ray is Actived;
uint8_t KyRiseEdgeCounter = 0;
uint32_t CurrentStateKy;
uint32_t LastStateKy;
uint32_t PositiveFront = 0;
uint32_t NegativeFront = 0;
uint32_t StableStateCounterKy = 0;
uint32_t NoiseToleranceKy = 16;//1.6 ms

MyMemmo WindowFilterKy;

/*****************************************************************************/
void InitKYOut(KY_Assigment KYInput)
{
   RCC_AHB1PeriphClockCmd(KYInput.RCC_Sensor,ENABLE);  
  GPIO_InitTypeDef _GPIO;
  GPIO_StructInit(&_GPIO);
 _GPIO.GPIO_Mode   = GPIO_Mode_OUT;
 _GPIO.GPIO_Pin    = KYInput.InputPin;
 _GPIO.GPIO_PuPd   = GPIO_PuPd_DOWN;
 _GPIO.GPIO_Speed  = GPIO_Speed_2MHz;
  GPIO_Init(KYInput.GPIO_Port,&_GPIO);
  
};

/*****************************************************************************/
void InitKYInput(KY_Assigment KYInput)
{
   RCC_AHB1PeriphClockCmd(KYInput.RCC_Sensor,ENABLE);
  
  WindowFilterKy.Create(16);
  GPIO_InitTypeDef _GPIO;
  GPIO_StructInit(&_GPIO);
 _GPIO.GPIO_Mode   = GPIO_Mode_IN;
 _GPIO.GPIO_Pin    = KYInput.InputPin;
 _GPIO.GPIO_PuPd   = GPIO_PuPd_UP;
 _GPIO.GPIO_Speed  = GPIO_Speed_2MHz;
  GPIO_Init(KYInput.GPIO_Port,&_GPIO);
  
  
}
/*****************************************************************************/
uint32_t ReadKYInput(KY_Assigment KYInput)
{
  return GPIO_ReadInputDataBit(KYInput.GPIO_Port,KYInput.InputPin);
}

/*****************************************************************************/
void WriteKYOut(KY_Assigment KYOut,uint8_t Val)
{
  if(Val)
  {
    GPIO_WriteBit(KYOut.GPIO_Port,KYOut.InputPin,Bit_SET);
  }
  else
  {
    GPIO_WriteBit(KYOut.GPIO_Port,KYOut.InputPin,Bit_RESET);
  }
}
/*****************************************************************************/
void InitKYIO(void)
{
  BG_In.GPIO_Port  = GPIOA;
  BG_In.InputPin = GPIO_Pin_8;
  BG_In.RCC_Sensor =  RCC_AHB1Periph_GPIOA;
 
  BG_Out.GPIO_Port = GPIOA;
  BG_Out.InputPin = GPIO_Pin_9;
  BG_Out.RCC_Sensor = RCC_AHB1Periph_GPIOA;
  ResetKYBackyStateKySynchroDetected();
  ResetKYBackyStateHvEmited();
  InitKYOut(BG_Out);
  InitKYInput(BG_In);
  
}
/*****************************************************************************/
 inline bool KyTrigerd(void)
{
    return IsKYBackyStateKySynchroDetected();
};
/*****************************************************************************/
 inline void TurnOnHV(void)
{
#if 0
  if(!IsKYBackyStateHvEmited())
  { 
    SetAlgoHVEmited();
    SetKYBackyStateHvEmited();
    WriteKYOut(BG_Out,1);
    FeelTimeResponseHV();
  
  }}
/*****************************************************************************/
inline void TurnOffHV(void)
{
  if(IsKYBackyStateHvEmited())
  {  
    ResetKYBackyStateHvEmited();
    WriteKYOut(BG_Out,0);
    FeelTimeSummary();
 
  }
}
/*****************************************************************************/
bool HVisOn(void)
{ 
   return IsKYBackyStateHvEmited();
}
/*****************************************************************************/
uint32_t GetKYOutputState(void)
{
  return  ReadKYInput(BG_In);
}
/*****************************************************************************/
inline  void KyPollingEvent(void)
{
  SaveCurrentKyPinState();
  KyEdgeActionProvider();
}
/*****************************************************************************/
inline void  SetKYBackyStateHvEmited(void)
{
   StateKySignals.HVEmited = 1; 
}
/*****************************************************************************/
inline void  ResetKYBackyStateHvEmited(void)
{
   StateKySignals.HVEmited = 0;
}
/*****************************************************************************/
inline void  SetKYBackyStateKySynchroDetected(void)
{
    StateKySignals.KySynchroDetected = 1; 
}
/*****************************************************************************/
inline void  ResetKYBackyStateKySynchroDetected(void)
{
    StateKySignals.KySynchroDetected = 0; 
}
/*****************************************************************************/
inline bool IsKYBackyStateHvEmited(void)
{
  return  StateKySignals.HVEmited;
}
/*****************************************************************************/
inline bool IsKYBackyStateKySynchroDetected(void)
{
  return StateKySignals.KySynchroDetected;
}
/*****************************************************************************/
inline void KyRaiseEdgeSynchroEvent(void)
{
  
  if(!IsKYBackyStateKySynchroDetected())
          {
              ResetDetectorSignal();
              StopTotalTimeCycle();
              ResetSignalFromKy();
              ResetAlgoHVEmted();
              OnKYEvent();
              ResetTimeStructure();  
              FeelImpulseHV();
              SetKYBackyStateKySynchroDetected();
              SetSignalFromKy();
              SetBFOut(); //Ready in Rising 
          }
  
}
/*****************************************************************************/
inline void   KYFailingEdgeSynchroEvent(void)
{
  
   if(IsKYBackyStateKySynchroDetected())
          {
            FeelTimeKyFailingFront();
            OffKYEvent();
            TurnOffHV();
            SetControlKy();
            ResetBFOut(); // Ready in Faling
            ResetKYBackyStateKySynchroDetected();
          }
}
/*****************************************************************************/
inline void  KyEdgeActionProvider(void)
{
  int  SumS0 = WindowFilterKy.GetAdd(0,3);
  int  SumS1 = WindowFilterKy.GetAdd(4,7);
  if (SumS0 >= 3 && SumS1 <= 1){
        KyRaiseEdgeSynchroEvent();
   }
                                          // Back side of Puls
   if (SumS0 <= 1 && SumS1 >= 3){
          KYFailingEdgeSynchroEvent();
   }
  
}
/*****************************************************************************/
 inline void  SaveCurrentKyPinState(void)
{
  WindowFilterKy.Put(GetKYOutputState());
}
/*****************************************************************************/
 inline void StorePreviousState(void)
{
   LastStateKy =  CurrentStateKy;
}
/*****************************************************************************/