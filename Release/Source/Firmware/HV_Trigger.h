#include "stm32f4xx.h"
#ifndef _HV_TRG
#define _HV_TRG
#pragma once
#ifdef __cplusplus
extern "C" {
#endif   
void HV_Trigge_Init(void);
uint8_t  HV_Trigge_ReadIbput(void);
bool  HV_Trigge_RissingEdgeDeteced(void);
#ifdef __cplusplus
}
#endif   
#endif