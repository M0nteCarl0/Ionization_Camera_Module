#pragma once
#include "stm32f4xx.h"
#ifndef _TIM_TRG
#define _TIM_TRG
#ifdef __cplusplus
extern "C" {
#endif 
void  Timer_manual_Trigger_Init(void);
void  Timer_manual_Trigger_Start(uint16_t Duration);
void  Timer_manual_Trigger_Stop(void); 
void  TIM8_UP_TIM13_IRQHandler(void);
#ifdef __cplusplus
}
#endif 
#endif
  
  
