#include "AD7175-2.hpp"
#include "Clock.h"
#include "MyUsartHard.h"
#include "DAC_HV.h"
#include "LED.h"

#include "Amplifer_control.h"
#include "MyUsart.h"
#include "Commands.h"
#include "IC_Core.hpp"
#include "FeedBackADC.h"
#include "Indep_WD.h"
#include "SafetySystem.h"
int main()
{
   __disable_interrupt();
   InitLED();
   HSI_CLK();
   InitAD7175();
   Configure_ADCClock();
#ifdef _HSE_CLK_
   HSE_BYPASS();
#endif 
#ifdef _HSI_CLK_168Mhz_   
   HSI_168Mhz();
#endif
    SafetySystem_PVD_Init();
    AmpliferControl_Init();
    DAC_HV_Init();
    FeedBackADC_Init();
    Core_Init();   
   InitIntrenalReceiveAndTranceiveStack();
   Init_UsartHard();
   iWD_Init();
   __enable_interrupt();
   Command_ProcessCommands();
}

