#include "WindowsFilter.h"
void MyMemmo::Create (int N)
{
  if (m_Addr == 0){
      m_N = N;
      m_Addr = new uint8_t [m_N];
      m_Begin = 0;
  }
};
//------------------------------------------------------------------------------
void MyMemmo::Put (uint8_t Data)
{
   m_Begin++;
   if (m_Begin >= m_N)        m_Begin = 0;
   
   m_Addr [m_Begin] = Data;
};
//------------------------------------------------------------------------------
uint8_t MyMemmo::Get (int I)
{
   if (I >= m_N)      return 0;
   
   int J = m_Begin + I + 1;

   if (J >= m_N)      J = J - m_N;
   
   return m_Addr [J];
};
//------------------------------------------------------------------------------
int MyMemmo::GetAdd (int I0, int I1)
{
   if (I0 >= m_N)      return 0;
   if (I1 >= m_N)      return 0;
   
   int J0 = m_Begin + I0 + 1;
   if (J0 >= m_N)      J0 -= m_N;
   
   int J1 = m_Begin + I1 + 1;
   if (J1 >= m_N)      J1 -= m_N;
   
   int Summ = 0;
   if (J1 >= J0)
      for (int i = J0; i <= J1; i++)   Summ += m_Addr [i];
   else{
      for (int i = J0; i < m_N; i++)   Summ += m_Addr [i];
      for (int i = 0;  i <= J1; i++)   Summ += m_Addr [i];
   }
   
   return Summ;
};
//------------------------------------------------------------------------------
void MyMemmo::Copy (uint8_t *Buff)
{
   for (int i = 0; i < m_N; i++){
      Buff [i] = m_Addr[i];
   }
};
//------------------------------------------------------------------------------
void MyMemmo::Shift (int Shift)
{
   m_Begin += Shift;
   if (m_Begin >= m_N)        m_Begin -= m_N;
};
//------------------------------------------------------------------------------
void MyMemmo::Set (uint8_t Data)
{
   m_Begin = 0;
   for (int i = 0; i < m_N; i++)
     m_Addr [i] = Data;
};