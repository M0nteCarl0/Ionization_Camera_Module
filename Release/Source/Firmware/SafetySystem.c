#include "SafetySystem.h"
void  SafetySystem_PVD_Init(void)
{
  
  NVIC_InitTypeDef NVIC_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;

  /* Enable PWR clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

  /* Configure one bit for preemption priority */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
  
  /* Enable the PVD Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = PVD_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* Configure the PVD Level to 3 (PVD detection level set to 2.5V, refer to the
      electrical characteristics of you device datasheet for more details) */
  PWR_PVDLevelConfig(PWR_PVDLevel_3);

  /* Enable the PVD Output */
  PWR_PVDCmd(ENABLE);
 
}

void PVD_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line16) != RESET)
  {
    /* Toggle LED1 */
   
  }
}