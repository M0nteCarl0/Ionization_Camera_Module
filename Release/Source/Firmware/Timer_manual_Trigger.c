#include "Timer_manual_Trigger.h"
/*****************************************************************************/
void  Timer_manual_Trigger_Init(void)
{
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM13,ENABLE);
   TIM_TimeBaseInitTypeDef _TimerSetup;
  _TimerSetup.TIM_ClockDivision     = TIM_CKD_DIV1;
  _TimerSetup.TIM_CounterMode       = TIM_CounterMode_Up;
  _TimerSetup.TIM_Period            = 0;
  _TimerSetup.TIM_Prescaler         = 42000;
  _TimerSetup.TIM_RepetitionCounter = 0;
   TIM_TimeBaseInit(TIM13,&_TimerSetup);
  
   TIM_ClearITPendingBit(TIM13,TIM_IT_Update);
   TIM_Cmd(TIM13,DISABLE);
   
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel                    = TIM8_UP_TIM13_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0;   //1;
  NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
  NVIC_Init (&NVIC_InitStructure);
  
}
/*****************************************************************************/
void  Timer_manual_Trigger_Start(uint16_t Duration)
{
 
   TIM_ClearITPendingBit(TIM13,TIM_IT_Update);
   TIM_SetAutoreload(TIM13,Duration);
   TIM_Cmd(TIM13,ENABLE);
   NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);
  
}
/*****************************************************************************/
void  Timer_manual_Trigger_Stop(void)
{
  
   TIM_Cmd(TIM13,DISABLE);
  
}
/*****************************************************************************/
void  TIM8_UP_TIM13_IRQHandler(void)
{
  
   if(TIM_GetITStatus(TIM13,TIM_IT_Update)!=RESET)
  {  
     TIM_ClearITPendingBit(TIM13,TIM_IT_Update);
     Timer_manual_Trigger_Stop();
    
  }
}