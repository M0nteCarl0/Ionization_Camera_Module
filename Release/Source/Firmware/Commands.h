#include "MyUsart.h"
#include "MyUsartHard.h"

#pragma once
#ifndef _COMANDS
#define _COMANDS
enum CommadID
{
   CommadID_WRITE_DETECTOR_SETUP	    = 0x1,
   CommadID_DETECTOR_FORCE_START     = 0x2,
   CommadID_DETECTOR_RESET_ALL       = 0x3,
   CommadID_WRITE_ADC_SETUP          = 0x4,
   CommadID_READ_HW_SW				    = 0x40,
   CommadID_READ_DETECTOR_SETUP	    = 0x41,
   CommadID_READ_DETECTOR_AMPLITUDES = 0x42,
   CommadID_READ_ADC_SETUP           = 0x44,
   CommadID_READ_HV_LEVEL			    = 0x45,
   
};

enum CommadID_Two_Byte
{
    CommadID_Two_Byte_READ         = 0x81,
    CommadID_Two_Byte_TERMINAL     = 0xd7,   
    CommadID_Two_Byte_Status       = 0xd6,    
    CommadID_Two_Byte_StepsHi      = 0xd8,
    CommadID_Two_Byte_StepsLo      = 0xd9,
    CommadID_Two_Byte_VelocityHi   = 0xda,
    CommadID_Two_Byte_VelocityLo   = 0xdb,
    CommadID_Two_Byte_RampHi       = 0xdc,
    CommadID_Two_Byte_RampLo       = 0xdd, 
    CommadID_Two_Byte_CounterHi    = 0xde,
    CommadID_Two_Byte_CounterLo    = 0xdf,
    CommadID_Two_Byte_InitialVLo   = 0xbd,
    CommadID_Two_Byte_TimeDelayLo  = 0xbe,
    CommadID_Two_Byte_TurnOffHVLo  = 0xb8,
};
 #ifdef __cplusplus
 extern "C" {
 #endif   
 void       Command_ProcessCommands(void); 
 void       Command_CommandParser(uint8_t *Buff,uint8_t ComandID,int N);
 
 void       Command_ReadVersion(uint8_t *Buff, int N);
 void       Command_ReadHVLevel(uint8_t *Buff, int N);
 void       Command_ReadSetupDetector(uint8_t *Buff, int N);
 void       Command_ReadADCParams(uint8_t *Buff, int N);
 void       Command_ReadDetectorAmplitudes(uint8_t *Buff, int N);
 void       Command_WriteADCParams(uint8_t *Buff, int N);
 void       Command_WriteSetupDetector(uint8_t *Buff, int N);
 void       Command_DetectorForcedStart(uint8_t *Buff, int N);
 void       Command_DetectorResetAllMeasurements(uint8_t *Buff, int N);
 
 #ifdef  __cplusplus  
 }
 #endif
 #endif
 

